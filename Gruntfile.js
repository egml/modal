// const sass = require('node-sass');
// const sass = require('sass'); // Выдает больше информации при ошибке, но работает в 4 и более раз медленнее
// const sassImporter = require('node-sass-import');

var Name = require('@egml/utils/node/Name').default;
var pkg = require('./package.json');
var name = new Name(pkg[pkg.name].name, pkg[pkg.name].namespace);

module.exports = grunt => {
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		
		pkg: pkg,
		name: name,
		
		//	███████  █████  ███████ ███████
		//	██      ██   ██ ██      ██
		//	███████ ███████ ███████ ███████
		//	     ██ ██   ██      ██      ██
		//	███████ ██   ██ ███████ ███████
		// 
		//	#sass
		
		sass: {
			options: {
				// implementation: sass,
				sourceMap: false,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				// importer: sassImporter,
			},
			test: {
				files: [
					{
						cwd: 'src',
						src: [ '**/*.scss' ],
						dest: 'test',
						ext: '.css',
						expand: true,
					}
				],
			},
			dist: {
				options: {
					outputStyle: 'compressed',
				},
				files: [
					{
						cwd: 'src',
						src: [ '**/*.scss' ],
						dest: 'dist',
						rename: function(dest, src) {
							return dest + '/' + name.kebab(src);
						},
						ext: '.css',
						expand: true,
					}
				],
			},
		},

		//	██ ███    ███  █████   ██████  ███████ ███    ███ ██ ███    ██
		//	██ ████  ████ ██   ██ ██       ██      ████  ████ ██ ████   ██
		//	██ ██ ████ ██ ███████ ██   ███ █████   ██ ████ ██ ██ ██ ██  ██
		//	██ ██  ██  ██ ██   ██ ██    ██ ██      ██  ██  ██ ██ ██  ██ ██
		//	██ ██      ██ ██   ██  ██████  ███████ ██      ██ ██ ██   ████
		//
		//	* https://www.npmjs.com/package/grunt-contrib-imagemin
		//	* SVGO:
		//	  https://github.com/sindresorhus/grunt-svgmin#available-optionsplugins
		//	  https://github.com/svg/svgo/tree/master/plugins
		//
		//	#imagemin #img
		
		imagemin: {
			svg: {
				options: {
					svgoPlugins: [
						// Информация по плагинам здесь: http://bit.ly/2UXDwRf
						{ removeViewBox: false },
						{ removeDimensions: true },
						{ removeUselessStrokeAndFill: false },
						{ cleanupNumericValues: false },
						{ convertPathData: false },
						{ mergePaths: false },
					]
				},
				files: [{
					expand: true,
					cwd: 'src',
					src: ['*.svg'],
					dest: 'dist',
					ext: '.svg',
				}],
			},
			build: {
				files: [{
					expand: true,
					cwd: 'src/img',
					src: ['**/*.{svg,png,jpg,gif}', '!svg-sprite/*'],
					dest: 'test/img'
				}],
			},
		},

		//	███████ ██    ██  ██████  ███████ ████████  ██████  ██████  ███████
		//	██      ██    ██ ██       ██         ██    ██    ██ ██   ██ ██
		//	███████ ██    ██ ██   ███ ███████    ██    ██    ██ ██████  █████
		//	     ██  ██  ██  ██    ██      ██    ██    ██    ██ ██   ██ ██
		//	███████   ████    ██████  ███████    ██     ██████  ██   ██ ███████
		//
		//	https://www.npmjs.com/package/grunt-svgstore
		//	#svgstore
		
		svgstore: {
			options: {
				prefix: name.css(['svg', 'sprite']) + '-',
				svg: {
					id: name.css(['svg', 'sprite']),
					xmlns: 'http://www.w3.org/2000/svg',
					style: 'display:none',
				},
				cleanup: false,
				includeTitleElement: false,
			},
			build: {
				files: {
					'test/svg-sprite.svg': ['runtime/svg-sprite/*'],
				},
			},
		},

		//	 ██████ ██      ███████  █████  ███    ██
		//	██      ██      ██      ██   ██ ████   ██
		//	██      ██      █████   ███████ ██ ██  ██
		//	██      ██      ██      ██   ██ ██  ██ ██
		//	 ██████ ███████ ███████ ██   ██ ██   ████
		//
		//	#clean
		
		clean: {
			test: 'test/*',
			js: 'test/*.js',
			css: 'test/*.css',
			svg: 'test/svg-sprite.svg',
			dist: 'dist/*',
			svg_runtime: 'runtime/svg-sprite/*',
		},

		//	 ██████  ██████  ██████  ██    ██
		//	██      ██    ██ ██   ██  ██  ██
		//	██      ██    ██ ██████    ████
		//	██      ██    ██ ██         ██
		//	 ██████  ██████  ██         ██
		//
		//	#copy
		
		copy: {
			dist: {
				files: [
					{ expand: true, cwd: 'src/copy', src: ['**'], dest: 'dist' },
				]
			},
		},

		//	 ██████  ██████  ███    ██  ██████ ██    ██ ██████  ██████  ███████ ███    ██ ████████
		//	██      ██    ██ ████   ██ ██      ██    ██ ██   ██ ██   ██ ██      ████   ██    ██
		//	██      ██    ██ ██ ██  ██ ██      ██    ██ ██████  ██████  █████   ██ ██  ██    ██
		//	██      ██    ██ ██  ██ ██ ██      ██    ██ ██   ██ ██   ██ ██      ██  ██ ██    ██
		//	 ██████  ██████  ██   ████  ██████  ██████  ██   ██ ██   ██ ███████ ██   ████    ██
		//
		//	#concurrent

		concurrent: {
			options: {
				logConcurrentOutput: true,
			},
			default: [
				'watch:css',
				'watch:img',
				'watch:svg',
			],
		},

		//	██     ██  █████  ████████  ██████ ██   ██
		//	██     ██ ██   ██    ██    ██      ██   ██
		//	██  █  ██ ███████    ██    ██      ███████
		//	██ ███ ██ ██   ██    ██    ██      ██   ██
		//	 ███ ███  ██   ██    ██     ██████ ██   ██
		//
		//	#watch
		
		watch: {
			options: {
				spawn: false,
			},
			css: {
				files: [
					'src/**/*.scss',
				],
				tasks: [
					'sass:test',
				],
			},
			svg: {
				files: ['src/img/svg-sprite/*.svg'],
				tasks: [
					'svg',
				],
			},
			img: {
				files: ['src/img/**/*.{svg,png,jpg,gif}', '!src/img/svg-sprite/*'],
				tasks: ['newer:imagemin:test'],
			},
			copy: {
				files: ['src/copy/**/*'],
				tasks: [
					'newer:copy',
				],
			},
		},
		
	});
	
	grunt.registerTask('default', 'concurrent');
	grunt.registerTask('build', 'build:dist');
	grunt.registerTask('build:dist', [
		'sass:dist',
	]);
	grunt.registerTask('build:test', [
		'sass:test',
		'svg',
	]);
	grunt.registerTask('svg', [
		'clean:svg_runtime',
		'imagemin:svg',
		'svgstore',
	]);
};