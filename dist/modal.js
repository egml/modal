import Widget from '@egml/utils/Widget.js';
import Name from '@egml/utils/Name.js';
import { extend, rem, closestParentByClass, marginBoxHeight } from '@egml/utils';
import Spinner$1 from 'spin.js';

function Modal() {
	Widget.call(this, ...arguments);
}
Object.defineProperty(Modal, 'name', {
	value: new Name('Modal', false),
});
 
Modal.prototype = Object.create(Widget.prototype);
Object.defineProperty(Modal.prototype, 'constructor', {
	value: Modal,
	enumerable: false,
	writable: true,
});

var name = new Name('modal');
  
Modal.prototype.name = name;
Modal.prototype.shown = false;
Modal.prototype.loaded = false;
Modal.prototype.animate = true;
Modal.prototype.url = null;
Modal.prototype.spinner = null;
Modal.prototype.finishShowEvent = null;
Modal.prototype.finishShowEventName = name.event(['finish', 'show']);
Modal.prototype.finishShowEventDetail = null;
Modal.prototype.finishHideEvent = null;
Modal.prototype.finishHideEventName = name.event(['finish', 'hide']);
Modal.prototype.finishHideEventDetail = null;
Modal.prototype.defaultWidth = 600;
 
Modal.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	this.saveInstanceOf(Modal);
 
	if (this.hasOwnProperty('finishShowEvent') == false) {
		this.finishShowEvent = new CustomEvent(this.finishShowEventName, {
			detail: extend({ widget: this }, this.finishShowEventDetail),
		});
	}
	if (this.hasOwnProperty('finishHideEvent') == false) {
		this.finishHideEvent = new CustomEvent(this.finishHideEventName, {
			detail: extend({ widget: this }, this.finishHideEventDetail),
		});
	}
};

//	███████ ███████ ████████
//	██      ██         ██
//	███████ █████      ██
//	     ██ ██         ██
//	███████ ███████    ██
//
//	██     ██ ██ ██████  ████████ ██   ██
//	██     ██ ██ ██   ██    ██    ██   ██
//	██  █  ██ ██ ██   ██    ██    ███████
//	██ ███ ██ ██ ██   ██    ██    ██   ██
//	 ███ ███  ██ ██████     ██    ██   ██
//
//	#set #width
Modal.prototype.setWidth = function(value)
{
	value = value || this.defaultWidth;
	if (typeof value == 'string' || value instanceof String) {
		switch (value) {
			case 'tight':
				value = 350;
				break;
			default:
				value = this.defaultWidth;
				break;
		}
	}
	var remValue = rem(value, true);
	this.dom.box.style.width = remValue;
	this.dom.iframe.style.width = remValue;
};

//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████

Modal.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);
	this.setDatasetIdOf(Modal);

	// DOM-элементы
	if (!this.dom.self) {
		throw new Error('Для модального окна не найдены элементы в DOM');
	}
	this.dom.clip = this.dom.self.querySelector(this.name.selector('clip'));
	this.dom.box = this.dom.self.querySelector(this.name.selector('box'));
	this.dom.content = this.dom.self.querySelector(this.name.selector('content'));
	this.dom.close = this.dom.self.querySelector(this.name.selector('close'));
	this.dom.iframe = this.dom.content.querySelector('iframe');
	 
	// Привязка событий
	this.dom.self.addEventListener('click', function(event) {
		var modal = Modal.prototype.instanceFromDataset(this, Modal);
		if (event.target.closest(modal.name.selector('box')) == null) {
			modal.hide();
		}
	}, false);
	this.dom.close.addEventListener('click', function(event) {
		var modal = Modal.prototype.instanceFromDataset(
			closestParentByClass(event.target, Modal.prototype.name.css()),
			Modal
		);
		event.preventDefault();
		modal.hide();
	}, false);
	 
	this.initialized = true;
};

//	██    ██ ████████ ██ ██      ███████
//	██    ██    ██    ██ ██      ██
//	██    ██    ██    ██ ██      ███████
//	██    ██    ██    ██ ██           ██
//	 ██████     ██    ██ ███████ ███████
//
//	#utils

//	██    ██ ██ ███████ ██     ██ ██████   ██████  ██████  ████████
//	██    ██ ██ ██      ██     ██ ██   ██ ██    ██ ██   ██    ██
//	██    ██ ██ █████   ██  █  ██ ██████  ██    ██ ██████     ██
//	 ██  ██  ██ ██      ██ ███ ██ ██      ██    ██ ██   ██    ██
//	  ████   ██ ███████  ███ ███  ██       ██████  ██   ██    ██
// 
//	 ██████ ██████   ██████  ██████
//	██      ██   ██ ██    ██ ██   ██
//	██      ██████  ██    ██ ██████
//	██      ██   ██ ██    ██ ██
//	 ██████ ██   ██  ██████  ██
// 
//	#viewport #crop
function viewportCrop() {
	document.documentElement.style.overflow = 'hidden';
}
//	██    ██ ██ ███████ ██     ██ ██████   ██████  ██████  ████████
//	██    ██ ██ ██      ██     ██ ██   ██ ██    ██ ██   ██    ██
//	██    ██ ██ █████   ██  █  ██ ██████  ██    ██ ██████     ██
//	 ██  ██  ██ ██      ██ ███ ██ ██      ██    ██ ██   ██    ██
//	  ████   ██ ███████  ███ ███  ██       ██████  ██   ██    ██
// 
//	██████  ███████ ███████ ████████  ██████  ██████  ███████
//	██   ██ ██      ██         ██    ██    ██ ██   ██ ██
//	██████  █████   ███████    ██    ██    ██ ██████  █████
//	██   ██ ██           ██    ██    ██    ██ ██   ██ ██
//	██   ██ ███████ ███████    ██     ██████  ██   ██ ███████
// 
//	#viewport #restore
function viewportRestore() {
	document.documentElement.style.overflow = null;
}

var opts = {
  lines: 13 // The number of lines to draw
, length: 6 // The length of each line
, width: 2 // The line thickness
, radius: 11 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 0 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
};

var Spinner = new Spinner$1(opts);

//	███████ ██   ██  ██████  ██     ██

Modal.prototype.show = function(newOptions)
{
	if (!this.initialized || this.busy || this.shown) return;
	this.busy = true;
	this.setOptions(newOptions);
	 
	// Скрыть прокрутку страницы
	viewportCrop();
	
	// Показ диалогового окна
	this.dom.self.style.display = null;
 
	if (this.animate == false) {
		finish.call(this);
	} else {
		this.dom.clip.style.backgroundColor = 'transparent';
		this.dom.box.style.opacity = 0;
		this.dom.box.style.transform = 'translateY(3rem)';
		// this.dom.box.style.transform = 'translateY(3rem) scale(.8)';
		// this.dom.box.style.transform = 'scale(.5)';
		this.dom.iframe.style.opacity = 0;
		document.body.getBoundingClientRect(); // #reflow
		this.dom.clip.style.backgroundColor = null;
		window.setTimeout((function() {
			this.dom.box.style.opacity = null;
			this.dom.box.style.transform = null;
			window.setTimeout(finish.bind(this), 400);
		}).bind(this), 300);
	}
};
 
function finish() {
	this.busy = false;
	this.shown = true;

	this.escapeKeyHitHandler = (function(event) {
		if ((event.key ? event.key == 'Escape' : event.keyCode == 27) && this.shown) {
			this.hide();
		}
	}).bind(this);

	document.addEventListener('keydown', this.escapeKeyHitHandler, false);

	// #NOTE: Перенесено сюда из show потому что если делать там, то при нажатии Esc до установки обработчика выше, но при начавшейся загрузке iframe-а, загрузка iframe-а прекращалась и при этом модал оставался открытым. Плюс так еще можно выиграть в гладкости эффектов.
	// Загрузка начального URL
	this.dom.iframe.src = this.url;
	
	// Только при первой загрузке
	this.dom.iframe.addEventListener('load', this.iframeOnFirstLoad, false);
	
	// При каждой загрузке подгонять размер и включать/выключать отслеживание изменения размера
	this.dom.iframe.addEventListener('load', this.iframeOnEachLoad, false);
		  
	// Показ индикатора загрузки, если запрошенный URL загружается слишком долго
	// (дольше iframeUnnotifiedLoadingTimeout)
	this.iframeUnnotifiedLoadingTimeoutId = window.setTimeout((function() {
		this.iframeLoadingTooLong = true;
		this.spinner = Spinner.spin(this.dom.box);
	}).bind(this), this.iframeUnnotifiedLoadingTimeout);

	this.dom.self.dispatchEvent(this.finishShowEvent);
}

//	██   ██ ██ ██████  ███████

Modal.prototype.hide = function()
{
	if (this.initialized && !this.busy && this.shown) {
		this.busy = true;
		if (this.iframeLoaded) {
			this.iframeLoaded = false;
			// Убрать интервал вычисления высоты iframe
			window.clearInterval(this.iframeResizeIntervalId);
		} else {
			if (this.iframeLoadingTooLong) {
				this.iframeLoadingTooLong = false;
				this.spinner.stop();
			} else {
				window.clearTimeout(this.iframeUnnotifiedLoadingTimeoutId);
			}
		}
		  
		// Скрытие диалогового окна (анимация и прочее)
		if (!this.animate) {
			finish$1.call(this);
		} else {
			// this.dom.box.style.transform = 'translateY(3rem)';
			// this.dom.box.style.transform = 'translateY(3rem) scale(.8)';
			this.dom.box.style.transform = 'scale(.8)';
			this.dom.box.style.opacity = 0;
			// window.setTimeout(function() {
			// }, 50);
			window.setTimeout((function() {
				this.dom.clip.style.backgroundColor = 'transparent';
			}).bind(this), 100);
			window.setTimeout(finish$1.bind(this), 500);
 
			// this.dom.box.transition({
			// 	transform: 'scale(.5)',
			// 	duration: transitionDuration,
			// 	easing: 'cubic-bezier(0.62,-0.5,1,0.33)',
			// 	queue: false,
			// });
			// window.setTimeout(function() {
			// 	this.dom.box.animate({ opacity: 0 }, boxFadeDuration);
			// }, boxFadeDelay);
			// this.dom.clip.transition({
			// 	opacity: 0,
			// 	delay: clipHideDelay,
			// 	duration: transitionDuration,
			// 	complete: finishHide,
			// });
		}
	}
};

function finish$1() {
	this.dom.self.style.display = 'none';
	this.busy = false;
	this.shown = false;
	this.dom.iframe.removeEventListener('load', this.iframeOnFirstLoad, false);
	this.dom.iframe.removeEventListener('load', this.iframeOnEachLoad, false);
	this.dom.iframe.src = 'about:blank';
	this.dom.iframe.style = null;
	this.dom.box.style.width = null;
	this.dom.box.style.height = null;
	document.removeEventListener('keydown', this.escapeKeyHitHandler, false);
	viewportRestore();
	this.iframePreviousHeight = 0;
	  
	// Перезагрузка основного окна
	// Использование forcedReload позволяет предотвратить повторную
	// загрузку iframe по последнему URL при перезагрузке страницы
	// window.location.reload(true);
	
	this.dom.self.dispatchEvent(this.finishHideEvent);
}

//	██ ███████ ██████   █████  ███    ███ ███████

Modal.prototype.iframeLoaded = false;
Modal.prototype.iframeResizeIntervalId = null;
Modal.prototype.iframeUnnotifiedLoadingTimeout = 500;
Modal.prototype.iframeUnnotifiedLoadingTimeoutId = null;
Modal.prototype.iframeLoadingTooLong = false;
Modal.prototype.iframePreviousHeight = null;
Modal.prototype.iframeResizeInterval = 1000;

//	 ██████  ███    ██     ███████ ██ ██████  ███████ ████████
//	██    ██ ████   ██     ██      ██ ██   ██ ██         ██
//	██    ██ ██ ██  ██     █████   ██ ██████  ███████    ██
//	██    ██ ██  ██ ██     ██      ██ ██   ██      ██    ██
//	 ██████  ██   ████     ██      ██ ██   ██ ███████    ██
// 
//	██       ██████   █████  ██████
//	██      ██    ██ ██   ██ ██   ██
//	██      ██    ██ ███████ ██   ██
//	██      ██    ██ ██   ██ ██   ██
//	███████  ██████  ██   ██ ██████
// 
//	#on #first #load
Modal.prototype.iframeOnFirstLoad = function(event)
{
	// console.log('iframeOnFirstLoad()');
	var modal = Modal.prototype.instanceFromDataset(
		closestParentByClass(event.target, Modal.prototype.name.css()),
		Modal
	);
	modal.iframeLoaded = true;
	if (modal.iframeLoadingTooLong) {
		modal.iframeLoadingTooLong = false;
		modal.spinner.stop();
	} else {
		window.clearTimeout(modal.iframeUnnotifiedLoadingTimeoutId);
	}
	// Показ iframe
	modal.dom.iframe.style.opacity = null;
	// modal.dom.iframe.css('opacity', 0).transition({opacity: 1}, 500, 'linear');
	// Отключение этого обработчика
	modal.dom.iframe.removeEventListener('load', modal.iframeOnFirstLoad, false);
};

//	 ██████  ███    ██     ███████  █████   ██████ ██   ██
//	██    ██ ████   ██     ██      ██   ██ ██      ██   ██
//	██    ██ ██ ██  ██     █████   ███████ ██      ███████
//	██    ██ ██  ██ ██     ██      ██   ██ ██      ██   ██
//	 ██████  ██   ████     ███████ ██   ██  ██████ ██   ██
// 
//	██       ██████   █████  ██████
//	██      ██    ██ ██   ██ ██   ██
//	██      ██    ██ ███████ ██   ██
//	██      ██    ██ ██   ██ ██   ██
//	███████  ██████  ██   ██ ██████
// 
//	#on #each #load
Modal.prototype.iframeOnEachLoad = function()
{
	// console.log('iframeOnEachLoad()');
	var modal = Modal.prototype.instanceFromDataset(
		closestParentByClass(event.target, Modal.prototype.name.css()),
		Modal
	);
	modal.iframeResize();
	modal.iframeResizeIntervalId = window.setInterval(modal.iframeResize.bind(modal), modal.iframeResizeInterval);
	// При выгрузке iframe отключать отслеживание
	modal.dom.iframe.contentWindow.addEventListener('unload', function() {
		// console.log('unload');
		window.clearInterval(modal.iframeResizeIntervalId);
	}, false);
};

//	██████  ███████ ███████ ██ ███████ ███████
//	██   ██ ██      ██      ██    ███  ██
//	██████  █████   ███████ ██   ███   █████
//	██   ██ ██           ██ ██  ███    ██
//	██   ██ ███████ ███████ ██ ███████ ███████
// 
//	#resize
Modal.prototype.iframeResize = function()
{
	// console.log('iframeResize()');
	var h1 = this.dom.iframe.contentWindow.document.documentElement.getBoundingClientRect().height;
	// var h1 = $(this.dom.iframe.get(0).contentWindow.document.documentElement).height();
	var h2 = marginBoxHeight(this.dom.iframe.contentWindow.document.body, this.dom.iframe.contentWindow);
	var height = Math.max(h1, h2);
	// console.log('iframe check height');
	// console.log('height = ' + height + ', previousHeight = ' + iframePreviousHeight);
	// console.log('h1 = ' + h1 + ', h2 = ' + h2);
	if (height != this.iframePreviousHeight) {
		var heightUnits = rem(height, true);
		this.iframePreviousHeight = height;
		this.dom.iframe.style.height = heightUnits;
		this.dom.box.style.height = heightUnits;
		// this.dom.iframe.transition({ height: height + 'px' }, 500, 'ease');
		// console.log($().jquery);
		// console.log($.fn.transition);
	}
};

//	███████ ███████ ████████ ██    ██ ██████
//	██      ██         ██    ██    ██ ██   ██
//	███████ █████      ██    ██    ██ ██████
//	     ██ ██         ██    ██    ██ ██
//	███████ ███████    ██     ██████  ██
// 
//	 ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████
//	██      ██    ██ ████   ██    ██    ██      ████   ██    ██
//	██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██
//	██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██
//	 ██████  ██████  ██   ████    ██    ███████ ██   ████    ██
// 
//	██████   ██████   ██████ ██    ██ ███    ███ ███████ ███    ██ ████████
//	██   ██ ██    ██ ██      ██    ██ ████  ████ ██      ████   ██    ██
//	██   ██ ██    ██ ██      ██    ██ ██ ████ ██ █████   ██ ██  ██    ██
//	██   ██ ██    ██ ██      ██    ██ ██  ██  ██ ██      ██  ██ ██    ██
//	██████   ██████   ██████  ██████  ██      ██ ███████ ██   ████    ██
// 
//	#setup #content #document
Modal.prototype.iframeSetupContentDocument = function(contentDocument, modalInstance, width)
{
	contentDocument.addEventListener('keydown', function(event) {
		if (event.key ? event.key == 'Escape' : event.keyCode == 27) {
			modalInstance.hide();
		}
	});
	width = width || 'default';
	modalInstance.setWidth(width);
};

export default Modal;
