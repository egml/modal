import Widget from '@egml/utils/Widget.js';
import Name from '@egml/utils/Name.js';
import { extend, rem } from '@egml/utils';

export default function Modal() {
	Widget.call(this, ...arguments);
}
Object.defineProperty(Modal, 'name', {
	value: new Name('Modal', false),
});
 
Modal.prototype = Object.create(Widget.prototype);
Object.defineProperty(Modal.prototype, 'constructor', {
	value: Modal,
	enumerable: false,
	writable: true,
});

var name = new Name('modal');
  
Modal.prototype.name = name;
Modal.prototype.shown = false;
Modal.prototype.loaded = false;
Modal.prototype.animate = true;
Modal.prototype.url = null;
Modal.prototype.spinner = null;
Modal.prototype.finishShowEvent = null;
Modal.prototype.finishShowEventName = name.event(['finish', 'show']);
Modal.prototype.finishShowEventDetail = null;
Modal.prototype.finishHideEvent = null;
Modal.prototype.finishHideEventName = name.event(['finish', 'hide']);
Modal.prototype.finishHideEventDetail = null;
Modal.prototype.defaultWidth = 600;
 
Modal.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	this.saveInstanceOf(Modal);
 
	if (this.hasOwnProperty('finishShowEvent') == false) {
		this.finishShowEvent = new CustomEvent(this.finishShowEventName, {
			detail: extend({ widget: this }, this.finishShowEventDetail),
		});
	}
	if (this.hasOwnProperty('finishHideEvent') == false) {
		this.finishHideEvent = new CustomEvent(this.finishHideEventName, {
			detail: extend({ widget: this }, this.finishHideEventDetail),
		});
	}
};

//	███████ ███████ ████████
//	██      ██         ██
//	███████ █████      ██
//	     ██ ██         ██
//	███████ ███████    ██
//
//	██     ██ ██ ██████  ████████ ██   ██
//	██     ██ ██ ██   ██    ██    ██   ██
//	██  █  ██ ██ ██   ██    ██    ███████
//	██ ███ ██ ██ ██   ██    ██    ██   ██
//	 ███ ███  ██ ██████     ██    ██   ██
//
//	#set #width
Modal.prototype.setWidth = function(value)
{
	value = value || this.defaultWidth;
	if (typeof value == 'string' || value instanceof String) {
		switch (value) {
			case 'tight':
				value = 350;
				break;
			default:
				value = this.defaultWidth;
				break;
		}
	}
	var remValue = rem(value, true);
	this.dom.box.style.width = remValue;
	this.dom.iframe.style.width = remValue;
}