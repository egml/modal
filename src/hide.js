//	██   ██ ██ ██████  ███████
//	██   ██ ██ ██   ██ ██
//	███████ ██ ██   ██ █████
//	██   ██ ██ ██   ██ ██
//	██   ██ ██ ██████  ███████
//
//  #hide
import Modal from './Modal.js';
import { viewportRestore } from './utils.js';

Modal.prototype.hide = function()
{
	if (this.initialized && !this.busy && this.shown) {
		this.busy = true;
		if (this.iframeLoaded) {
			this.iframeLoaded = false;
			// Убрать интервал вычисления высоты iframe
			window.clearInterval(this.iframeResizeIntervalId);
		} else {
			if (this.iframeLoadingTooLong) {
				this.iframeLoadingTooLong = false;
				this.spinner.stop();
			} else {
				window.clearTimeout(this.iframeUnnotifiedLoadingTimeoutId);
			}
		}
		  
		// Скрытие диалогового окна (анимация и прочее)
		if (!this.animate) {
			finish.call(this);
		} else {
			// this.dom.box.style.transform = 'translateY(3rem)';
			// this.dom.box.style.transform = 'translateY(3rem) scale(.8)';
			this.dom.box.style.transform = 'scale(.8)';
			this.dom.box.style.opacity = 0;
			// window.setTimeout(function() {
			// }, 50);
			window.setTimeout((function() {
				this.dom.clip.style.backgroundColor = 'transparent';
			}).bind(this), 100);
			window.setTimeout(finish.bind(this), 500);
 
			// this.dom.box.transition({
			// 	transform: 'scale(.5)',
			// 	duration: transitionDuration,
			// 	easing: 'cubic-bezier(0.62,-0.5,1,0.33)',
			// 	queue: false,
			// });
			// window.setTimeout(function() {
			// 	this.dom.box.animate({ opacity: 0 }, boxFadeDuration);
			// }, boxFadeDelay);
			// this.dom.clip.transition({
			// 	opacity: 0,
			// 	delay: clipHideDelay,
			// 	duration: transitionDuration,
			// 	complete: finishHide,
			// });
		}
	}
};

function finish() {
	this.dom.self.style.display = 'none';
	this.busy = false;
	this.shown = false;
	this.dom.iframe.removeEventListener('load', this.iframeOnFirstLoad, false);
	this.dom.iframe.removeEventListener('load', this.iframeOnEachLoad, false);
	this.dom.iframe.src = 'about:blank';
	this.dom.iframe.style = null;
	this.dom.box.style.width = null;
	this.dom.box.style.height = null;
	document.removeEventListener('keydown', this.escapeKeyHitHandler, false);
	viewportRestore();
	this.iframePreviousHeight = 0;
	  
	// Перезагрузка основного окна
	// Использование forcedReload позволяет предотвратить повторную
	// загрузку iframe по последнему URL при перезагрузке страницы
	// window.location.reload(true);
	
	this.dom.self.dispatchEvent(this.finishHideEvent);
};