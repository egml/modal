//	██ ███████ ██████   █████  ███    ███ ███████
//	██ ██      ██   ██ ██   ██ ████  ████ ██
//	██ █████   ██████  ███████ ██ ████ ██ █████
//	██ ██      ██   ██ ██   ██ ██  ██  ██ ██
//	██ ██      ██   ██ ██   ██ ██      ██ ███████
// 
//	#iframe
import { rem, marginBoxHeight, closestParentByClass } from '@egml/utils';
import Modal from './Modal.js';

Modal.prototype.iframeLoaded = false;
Modal.prototype.iframeResizeIntervalId = null;
Modal.prototype.iframeUnnotifiedLoadingTimeout = 500;
Modal.prototype.iframeUnnotifiedLoadingTimeoutId = null;
Modal.prototype.iframeLoadingTooLong = false;
Modal.prototype.iframePreviousHeight = null;
Modal.prototype.iframeResizeInterval = 1000;

//	 ██████  ███    ██     ███████ ██ ██████  ███████ ████████
//	██    ██ ████   ██     ██      ██ ██   ██ ██         ██
//	██    ██ ██ ██  ██     █████   ██ ██████  ███████    ██
//	██    ██ ██  ██ ██     ██      ██ ██   ██      ██    ██
//	 ██████  ██   ████     ██      ██ ██   ██ ███████    ██
// 
//	██       ██████   █████  ██████
//	██      ██    ██ ██   ██ ██   ██
//	██      ██    ██ ███████ ██   ██
//	██      ██    ██ ██   ██ ██   ██
//	███████  ██████  ██   ██ ██████
// 
//	#on #first #load
Modal.prototype.iframeOnFirstLoad = function(event)
{
	// console.log('iframeOnFirstLoad()');
	var modal = Modal.prototype.instanceFromDataset(
		closestParentByClass(event.target, Modal.prototype.name.css()),
		Modal
	);
	modal.iframeLoaded = true;
	if (modal.iframeLoadingTooLong) {
		modal.iframeLoadingTooLong = false;
		modal.spinner.stop();
	} else {
		window.clearTimeout(modal.iframeUnnotifiedLoadingTimeoutId);
	}
	// Показ iframe
	modal.dom.iframe.style.opacity = null;
	// modal.dom.iframe.css('opacity', 0).transition({opacity: 1}, 500, 'linear');
	// Отключение этого обработчика
	modal.dom.iframe.removeEventListener('load', modal.iframeOnFirstLoad, false);
};

//	 ██████  ███    ██     ███████  █████   ██████ ██   ██
//	██    ██ ████   ██     ██      ██   ██ ██      ██   ██
//	██    ██ ██ ██  ██     █████   ███████ ██      ███████
//	██    ██ ██  ██ ██     ██      ██   ██ ██      ██   ██
//	 ██████  ██   ████     ███████ ██   ██  ██████ ██   ██
// 
//	██       ██████   █████  ██████
//	██      ██    ██ ██   ██ ██   ██
//	██      ██    ██ ███████ ██   ██
//	██      ██    ██ ██   ██ ██   ██
//	███████  ██████  ██   ██ ██████
// 
//	#on #each #load
Modal.prototype.iframeOnEachLoad = function()
{
	// console.log('iframeOnEachLoad()');
	var modal = Modal.prototype.instanceFromDataset(
		closestParentByClass(event.target, Modal.prototype.name.css()),
		Modal
	);
	modal.iframeResize();
	modal.iframeResizeIntervalId = window.setInterval(modal.iframeResize.bind(modal), modal.iframeResizeInterval);
	// При выгрузке iframe отключать отслеживание
	modal.dom.iframe.contentWindow.addEventListener('unload', function() {
		// console.log('unload');
		window.clearInterval(modal.iframeResizeIntervalId);
	}, false);
};

//	██████  ███████ ███████ ██ ███████ ███████
//	██   ██ ██      ██      ██    ███  ██
//	██████  █████   ███████ ██   ███   █████
//	██   ██ ██           ██ ██  ███    ██
//	██   ██ ███████ ███████ ██ ███████ ███████
// 
//	#resize
Modal.prototype.iframeResize = function()
{
	// console.log('iframeResize()');
	var h1 = this.dom.iframe.contentWindow.document.documentElement.getBoundingClientRect().height;
	// var h1 = $(this.dom.iframe.get(0).contentWindow.document.documentElement).height();
	var h2 = marginBoxHeight(this.dom.iframe.contentWindow.document.body, this.dom.iframe.contentWindow);
	var height = Math.max(h1, h2);
	// console.log('iframe check height');
	// console.log('height = ' + height + ', previousHeight = ' + iframePreviousHeight);
	// console.log('h1 = ' + h1 + ', h2 = ' + h2);
	if (height != this.iframePreviousHeight) {
		var heightUnits = rem(height, true);
		this.iframePreviousHeight = height;
		this.dom.iframe.style.height = heightUnits;
		this.dom.box.style.height = heightUnits;
		// this.dom.iframe.transition({ height: height + 'px' }, 500, 'ease');
		// console.log($().jquery);
		// console.log($.fn.transition);
	}
};

//	███████ ███████ ████████ ██    ██ ██████
//	██      ██         ██    ██    ██ ██   ██
//	███████ █████      ██    ██    ██ ██████
//	     ██ ██         ██    ██    ██ ██
//	███████ ███████    ██     ██████  ██
// 
//	 ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████
//	██      ██    ██ ████   ██    ██    ██      ████   ██    ██
//	██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██
//	██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██
//	 ██████  ██████  ██   ████    ██    ███████ ██   ████    ██
// 
//	██████   ██████   ██████ ██    ██ ███    ███ ███████ ███    ██ ████████
//	██   ██ ██    ██ ██      ██    ██ ████  ████ ██      ████   ██    ██
//	██   ██ ██    ██ ██      ██    ██ ██ ████ ██ █████   ██ ██  ██    ██
//	██   ██ ██    ██ ██      ██    ██ ██  ██  ██ ██      ██  ██ ██    ██
//	██████   ██████   ██████  ██████  ██      ██ ███████ ██   ████    ██
// 
//	#setup #content #document
Modal.prototype.iframeSetupContentDocument = function(contentDocument, modalInstance, width)
{
	contentDocument.addEventListener('keydown', function(event) {
		if (event.key ? event.key == 'Escape' : event.keyCode == 27) {
			modalInstance.hide();
		}
	});
	width = width || 'default';
	modalInstance.setWidth(width);
};