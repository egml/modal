//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████
//	██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██
//	██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   █████
//	██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██
//	██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ███████
//
//	#initialize
import Modal from './Modal.js';
import Widget from '@egml/utils/Widget.js';
import { closestParentByClass } from '@egml/utils';

Modal.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);
	this.setDatasetIdOf(Modal);

	// DOM-элементы
	if (!this.dom.self) {
		throw new Error('Для модального окна не найдены элементы в DOM');
	}
	this.dom.clip = this.dom.self.querySelector(this.name.selector('clip'));
	this.dom.box = this.dom.self.querySelector(this.name.selector('box'));
	this.dom.content = this.dom.self.querySelector(this.name.selector('content'));
	this.dom.close = this.dom.self.querySelector(this.name.selector('close'));
	this.dom.iframe = this.dom.content.querySelector('iframe');
	 
	// Привязка событий
	this.dom.self.addEventListener('click', function(event) {
		var modal = Modal.prototype.instanceFromDataset(this, Modal);
		if (event.target.closest(modal.name.selector('box')) == null) {
			modal.hide();
		}
	}, false);
	this.dom.close.addEventListener('click', function(event) {
		var modal = Modal.prototype.instanceFromDataset(
			closestParentByClass(event.target, Modal.prototype.name.css()),
			Modal
		);
		event.preventDefault();
		modal.hide();
	}, false);
	 
	this.initialized = true;
};