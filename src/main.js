import Modal from './Modal.js';
import './initialize.js';
import './show.js';
import './hide.js';
import './iframe.js';

export default Modal;