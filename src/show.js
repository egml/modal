//	███████ ██   ██  ██████  ██     ██
//	██      ██   ██ ██    ██ ██     ██
//	███████ ███████ ██    ██ ██  █  ██
//	     ██ ██   ██ ██    ██ ██ ███ ██
//	███████ ██   ██  ██████   ███ ███
//
// 	#show
import Modal from './Modal.js';
import { viewportCrop } from './utils.js';
import Spinner from './spinner.js';

Modal.prototype.show = function(newOptions)
{
	if (!this.initialized || this.busy || this.shown) return;
	this.busy = true;
	this.setOptions(newOptions);
	 
	// Скрыть прокрутку страницы
	viewportCrop();
	
	// Показ диалогового окна
	this.dom.self.style.display = null;
 
	if (this.animate == false) {
		finish.call(this);
	} else {
		this.dom.clip.style.backgroundColor = 'transparent';
		this.dom.box.style.opacity = 0;
		this.dom.box.style.transform = 'translateY(3rem)';
		// this.dom.box.style.transform = 'translateY(3rem) scale(.8)';
		// this.dom.box.style.transform = 'scale(.5)';
		this.dom.iframe.style.opacity = 0;
		document.body.getBoundingClientRect(); // #reflow
		this.dom.clip.style.backgroundColor = null;
		window.setTimeout((function() {
			this.dom.box.style.opacity = null;
			this.dom.box.style.transform = null;
			window.setTimeout(finish.bind(this), 400);
		}).bind(this), 300);
	}
};
 
function finish() {
	this.busy = false;
	this.shown = true;

	this.escapeKeyHitHandler = (function(event) {
		if ((event.key ? event.key == 'Escape' : event.keyCode == 27) && this.shown) {
			this.hide();
		}
	}).bind(this);

	document.addEventListener('keydown', this.escapeKeyHitHandler, false);

	// #NOTE: Перенесено сюда из show потому что если делать там, то при нажатии Esc до установки обработчика выше, но при начавшейся загрузке iframe-а, загрузка iframe-а прекращалась и при этом модал оставался открытым. Плюс так еще можно выиграть в гладкости эффектов.
	// Загрузка начального URL
	this.dom.iframe.src = this.url;
	
	// Только при первой загрузке
	this.dom.iframe.addEventListener('load', this.iframeOnFirstLoad, false);
	
	// При каждой загрузке подгонять размер и включать/выключать отслеживание изменения размера
	this.dom.iframe.addEventListener('load', this.iframeOnEachLoad, false);
		  
	// Показ индикатора загрузки, если запрошенный URL загружается слишком долго
	// (дольше iframeUnnotifiedLoadingTimeout)
	this.iframeUnnotifiedLoadingTimeoutId = window.setTimeout((function() {
		this.iframeLoadingTooLong = true;
		this.spinner = Spinner.spin(this.dom.box);
	}).bind(this), this.iframeUnnotifiedLoadingTimeout);

	this.dom.self.dispatchEvent(this.finishShowEvent);
};