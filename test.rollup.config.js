// import path from 'path';
import nodeResolvePlugin from 'rollup-plugin-node-resolve';
import commonjsPlugin from 'rollup-plugin-commonjs';
// import jsonPlugin from 'rollup-plugin-json';
import svgToSymbolPlugin from 'rollup-plugin-svg-to-symbol';
// import generateHtmlTemplatePlugin from 'rollup-plugin-generate-html-template';
import sassPlugin from 'rollup-plugin-sass';
// import nodeSass from 'node-sass';
// import progressPlugin from 'rollup-plugin-progress';
// import { string as stringPlugin } from 'rollup-plugin-string';
// import svgoPlugin from 'rollup-plugin-svgo';
// import generateHtmlTemplatePlugin from 'rollup-plugin-generate-html-template'; // Это плагин возможно надо откатить до версии 1.1.0, если ругается `Cannot find module ...`
import mustachePlugin from 'rollup-plugin-mustache';
import notifyPlugin from 'rollup-plugin-notify';
import Name from '@egml/utils/node/Name';

var name = new Name('modal');

var svgToSymbolPluginInstance = svgToSymbolPlugin({
	extractId: function(file) {
		return name.css('svg_sprite-' + file.name);
	},
	svgo: {
		removeViewBox: false,
		removeDimensions: true,
		removeUselessStrokeAndFill: false,
		cleanupNumericValues: false,
		convertPathData: false,
		mergePaths: false,
	},
});

export default {
	input: 'test/main.js',
	output: {
		file: 'test/build.js',
		format: 'iife',
		name: name.dot(),
		sourcemap: 'inline',
	},
	plugins: [
		nodeResolvePlugin(),
		commonjsPlugin(),
		// jsonPluginInstance,
		svgToSymbolPluginInstance,
		// generateHtmlTemplatePlugin({
		// 	template: 'src/test.html',
		// 	target: 'index.html',
		// }),
		sassPlugin({
			insert: true,
		}),
		mustachePlugin({
			include: '**/*.mustache',
		}),
		notifyPlugin(),
	],
	watch: {
		clearScreen: false,
	},
};