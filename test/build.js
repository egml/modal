var modal = (function () {
	'use strict';

	

	function ___$insertStyle(css) {
	  if (!css) {
	    return;
	  }
	  if (typeof window === 'undefined') {
	    return;
	  }

	  var style = document.createElement('style');

	  style.setAttribute('type', 'text/css');
	  style.innerHTML = css;
	  document.head.appendChild(style);
	  return css;
	}

	//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████

	//  ███████ ██   ██ ████████ ███████ ███    ██ ██████
	//  ██       ██ ██     ██    ██      ████   ██ ██   ██
	//  █████     ███      ██    █████   ██ ██  ██ ██   ██
	//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
	//  ███████ ██   ██    ██    ███████ ██   ████ ██████
	//
	// #extend
	function extend(dest, source) {
		for (var prop in source) {
			dest[prop] = source[prop];
		}
		return dest;
	}

	//   ██████ ██       ██████  ███████ ███████ ███████ ████████
	//  ██      ██      ██    ██ ██      ██      ██         ██
	//  ██      ██      ██    ██ ███████ █████   ███████    ██
	//  ██      ██      ██    ██      ██ ██           ██    ██
	//   ██████ ███████  ██████  ███████ ███████ ███████    ██
	// 
	//  ██████   █████  ██████  ███████ ███    ██ ████████
	//  ██   ██ ██   ██ ██   ██ ██      ████   ██    ██
	//  ██████  ███████ ██████  █████   ██ ██  ██    ██
	//  ██      ██   ██ ██   ██ ██      ██  ██ ██    ██
	//  ██      ██   ██ ██   ██ ███████ ██   ████    ██
	// 
	//  ██████  ██    ██
	//  ██   ██  ██  ██
	//  ██████    ████
	//  ██   ██    ██
	//  ██████     ██
	// 
	//   ██████ ██       █████  ███████ ███████
	//  ██      ██      ██   ██ ██      ██
	//  ██      ██      ███████ ███████ ███████
	//  ██      ██      ██   ██      ██      ██
	//   ██████ ███████ ██   ██ ███████ ███████
	//
	// #closest #parent #class
	function closestParentByClass(element, className) {
		var parent = element.parentNode;
		while (!parent.classList.contains(className)) {
			parent = parent.parentNode;
		}
		return parent;
	}

	//  ██ ███████
	//  ██ ██
	//  ██ ███████
	//  ██      ██
	//  ██ ███████
	// 
	//   ██████  ██████       ██ ███████  ██████ ████████
	//  ██    ██ ██   ██      ██ ██      ██         ██
	//  ██    ██ ██████       ██ █████   ██         ██
	//  ██    ██ ██   ██ ██   ██ ██      ██         ██
	//   ██████  ██████   █████  ███████  ██████    ██
	//
	// #is #object
	// Сперто из interact.js (сперто немного, но все равно: http://interactjs.io/)
	function isObject(thing) {
		return !!thing && (typeof thing === 'object');
	}

	//  ██████  ███████ ███    ███
	//  ██   ██ ██      ████  ████
	//  ██████  █████   ██ ████ ██
	//  ██   ██ ██      ██  ██  ██
	//  ██   ██ ███████ ██      ██
	//
	// #rem
	// Подсчёт величины в rem. На входе пиксели без `px`, на выходе величина в rem с `rem`
	var rootFontSize;
	function rem(pxNoUnits, recalc) {
		if (typeof rootFontSize != 'number' || recalc) {
			// #CBFIX: Edge (42.17134.1.0, EdgeHTML 17.17134) и IE определяют значение как, например, 9.93 в результате >> операция дает 9, поэтому тут дополнительно надо округлять. UPD: Зачем переводить в integer с помощью >>, если мы уже применям к строке Math.round, тем самым она автоматически кастуется в integer.
			// rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2)) >> 0;
			rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2));
		}
		return pxNoUnits/rootFontSize + 'rem';
	}

	//  ███    ███  █████  ██████   ██████  ██ ███    ██
	//  ████  ████ ██   ██ ██   ██ ██       ██ ████   ██
	//  ██ ████ ██ ███████ ██████  ██   ███ ██ ██ ██  ██
	//  ██  ██  ██ ██   ██ ██   ██ ██    ██ ██ ██  ██ ██
	//  ██      ██ ██   ██ ██   ██  ██████  ██ ██   ████
	// 
	//  ██████   ██████  ██   ██
	//  ██   ██ ██    ██  ██ ██
	//  ██████  ██    ██   ███
	//  ██   ██ ██    ██  ██ ██
	//  ██████   ██████  ██   ██
	// 
	//  ██   ██ ███████ ██  ██████  ██   ██ ████████
	//  ██   ██ ██      ██ ██       ██   ██    ██
	//  ███████ █████   ██ ██   ███ ███████    ██
	//  ██   ██ ██      ██ ██    ██ ██   ██    ██
	//  ██   ██ ███████ ██  ██████  ██   ██    ██
	//
	// #margin #box #height
	function marginBoxHeight(element, sourceWindow) {
		sourceWindow = sourceWindow || window;
		var style;
		if (element && (style = sourceWindow.getComputedStyle(element))) {
			return element.getBoundingClientRect().height
					+ (style.getPropertyValue('margin-top').slice(0,-2) >> 0)
					+ (style.getPropertyValue('margin-bottom').slice(0,-2) >> 0);
		} else {
			return 0;
		}
	}

	//  ███████ ████████ ██████  ██ ███    ██  ██████
	//  ██         ██    ██   ██ ██ ████   ██ ██
	//  ███████    ██    ██████  ██ ██ ██  ██ ██   ███
	//       ██    ██    ██   ██ ██ ██  ██ ██ ██    ██
	//  ███████    ██    ██   ██ ██ ██   ████  ██████
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #string #case
	function stringCase(bits, type) {
		if (!bits || !bits.length) {
			return '';
		}
		if (typeof bits == 'string') {
			bits = [ bits ];
		}
		var string =  '';
		switch (type) {
			case 'camel':
				bits.forEach(function(bit, i) {
					if (i == 0) {
						string += firstLowerCase(bit);
					} else {
						string += firstUpperCase(bit);
					}
				});
				break;

			case 'capitalCamel':
				bits.forEach(function(bit) {
					string += firstUpperCase(bit);
				});
				break;

			case 'flat':
				string = bits.join('');
				break;

			case 'snake':
				string = bits.join('_');
				break;

			case 'kebab':
				string = bits.join('-');
				break;

			case 'dot':
				string = bits.join('.');
				break;
		
			case 'asIs':
			default:
				string = bits.join('');
				break;
		}
		return string;
	}

	//  ███████ ██ ██████  ███████ ████████
	//  ██      ██ ██   ██ ██         ██
	//  █████   ██ ██████  ███████    ██
	//  ██      ██ ██   ██      ██    ██
	//  ██      ██ ██   ██ ███████    ██
	// 
	//  ██    ██ ██████  ██████  ███████ ██████      ██
	//  ██    ██ ██   ██ ██   ██ ██      ██   ██    ██
	//  ██    ██ ██████  ██████  █████   ██████    ██
	//  ██    ██ ██      ██      ██      ██   ██  ██
	//   ██████  ██      ██      ███████ ██   ██ ██
	// 
	//  ██       ██████  ██     ██ ███████ ██████
	//  ██      ██    ██ ██     ██ ██      ██   ██
	//  ██      ██    ██ ██  █  ██ █████   ██████
	//  ██      ██    ██ ██ ███ ██ ██      ██   ██
	//  ███████  ██████   ███ ███  ███████ ██   ██
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #first #upper #lower #case
	function firstUpperCase(string) {
		return string[0].toUpperCase() + string.slice(1);
	}function firstLowerCase(string) {
		return string[0].toLowerCase() + string.slice(1);
	}
	//	██ ███    ██ ███████ ███████ ██████  ████████
	//	██ ████   ██ ██      ██      ██   ██    ██
	//	██ ██ ██  ██ ███████ █████   ██████     ██
	//	██ ██  ██ ██      ██ ██      ██   ██    ██
	//	██ ██   ████ ███████ ███████ ██   ██    ██
	// 
	//	██   ██ ████████ ███    ███ ██
	//	██   ██    ██    ████  ████ ██
	//	███████    ██    ██ ████ ██ ██
	//	██   ██    ██    ██  ██  ██ ██
	//	██   ██    ██    ██      ██ ███████
	// 
	//	#insert
	function insertHtml() {
		var options = {
			html: null,
			legacy: false,
			url: null,
			targetDocument: document,
			onLoadCallback: null,
		};
		
		var mountElement = null;
		
		Object.defineProperty(options, 'mountElement', {
			get: function() {
				if (mountElement == null) {
					return options.targetDocument.body;
				} else {
					return mountElement;
				}
			},
			set: function(value) {
				mountElement = value;
			},
			configurable: true,
			enumerable: true,
		});
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.html = arguments[0];
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}

		if (options.url) {
			var xhr = new XMLHttpRequest();
			xhr.open('get', options.url);
			xhr.onload = function() {
				if (options.legacy) {
					// #CBNOTE: Старые браузеры, такие как WebView 33, не поддерживают createContextualFragment (или что-то в этом духе), в общем, если ругаются, то вот так можно обойти:
					var parser = new DOMParser();
					var externalDom = parser.parseFromString('<div id="__imported_html__">' + this.responseText + '</div>', 'text/html');
					// #TODO: В данном случае вставится <div id="__imported_html__">...полученный HTML...</div>, это надо исправить на некую возможно последовательную вставку всех дочерних нодов этого div'a
					var domFragment = options.targetDocument.importNode(externalDom.getElementById('__imported_html__'), true);
				} else {
					var domFragment = options.targetDocument.createRange().createContextualFragment(this.responseText);
				}
				options.mountElement.appendChild(domFragment);
				if (typeof options.onLoadCallback == 'function') {
					options.onLoadCallback();
				}
			};
			xhr.send();
		} else if (options.html) {
			if (options.legacy) {
				// #CBNOTE: Старые браузеры, такие как WebView 33, не поддерживают createContextualFragment (или что-то в этом духе), в общем, если ругаются, то вот так можно обойти:
				var parser = new DOMParser();
				var externalDom = parser.parseFromString('<div id="__imported_html__">' + options.html + '</div>', 'text/html');
				var domFragment = options.targetDocument.importNode(externalDom.getElementById('__imported_html__'), true);
			} else {
				var domFragment = options.targetDocument.createRange().createContextualFragment(options.html);
			}
			options.mountElement.appendChild(domFragment);
			if (typeof options.onLoadCallback == 'function') {
				options.onLoadCallback();
			}
		} else {
			return;
		}
	}

	//	██ ███    ██ ███████ ███████ ██████  ████████
	//	██ ████   ██ ██      ██      ██   ██    ██
	//	██ ██ ██  ██ ███████ █████   ██████     ██
	//	██ ██  ██ ██      ██ ██      ██   ██    ██
	//	██ ██   ████ ███████ ███████ ██   ██    ██
	// 
	//	███████ ██    ██  ██████
	//	██      ██    ██ ██
	//	███████ ██    ██ ██   ███
	//	     ██  ██  ██  ██    ██
	//	███████   ████    ██████
	// 
	//	███████ ██    ██ ███    ███ ██████   ██████  ██
	//	██       ██  ██  ████  ████ ██   ██ ██    ██ ██
	//	███████   ████   ██ ████ ██ ██████  ██    ██ ██
	//	     ██    ██    ██  ██  ██ ██   ██ ██    ██ ██
	//	███████    ██    ██      ██ ██████   ██████  ███████
	// 
	//	#insert #svg #symbol
	function insertSvgSymbol() {
		// -------------------------------------------
		//  Вариант с отдельным родительским
		// 	SVG-элементом для каждого символа:
		// -------------------------------------------
		var options = {
			symbol: null,
			mountElement: document.body,
			containerClass: null,
		};
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.symbol = arguments[0];
			options.mountElement = arguments[1] || options.mountElement;
			options.containerClass = arguments[2] || options.containerClass;
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}

		var parser = new DOMParser();
		var symbolDocument = parser.parseFromString(
			`<svg xmlns="http://www.w3.org/2000/svg" 
			${ options.containerClass ? `class="${options.containerClass}"` : '' } 
			style="display: none;">
			${options.symbol}
		</svg>`,
			'image/svg+xml'
		);
		var symbolNode = document.importNode(symbolDocument.documentElement, true);
		options.mountElement.appendChild(symbolNode);

		// -------------------------------------------
		//  Вариант со вставкой в один родительский 
		// 	SVG-элемент:
		// -------------------------------------------
		// var options = {
		// 	symbol: null,
		// 	containerId: 'svg_sprite',
		// 	mountElement: document.body,
		// };
		 
		// if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
		// 	options.symbol = arguments[0];
		// 	options.containerId = arguments[1] || options.containerId;
		// 	options.mountElement = arguments[2] || options.mountElement;
		// } else if (!!arguments[0] && typeof arguments[0] == 'object') {
		// 	extend(options, arguments[0]);
		// } else {
		// 	return;
		// }

		// var container = document.getElementById(options.containerId);
		
		// if (container == null) {
		// 	container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		// 	container.setAttribute('id', options.containerId);
		// 	container.style.cssText = 'display: none;';
		// 	options.mountElement.appendChild(container);
		// }

		// var parser = new DOMParser();
		// var symbolDocument = parser.parseFromString(
		// 	`<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
		// 		${options.symbol}
		// 	</svg>`,
		// 	'image/svg+xml'
		// );
		// var symbolNode = document.importNode(symbolDocument.querySelector('symbol'), true);
		// container.appendChild(symbolNode);
	}

	function StringCase(bits) {
		this.bits = bits;

		if (Array.isArray(bits)) {
			this.asIs = this.bits.join('');
			bits.forEach(function(bit, i) {
				bits[i] = bit.toLowerCase();
			});
		} else {
			this.asIs = this.bits;
			bits = bits.toLowerCase();
		}

		this.camel = stringCase(bits, 'camel');
		this.capitalCamel = stringCase(bits, 'capitalCamel');
		this.kebab = stringCase(bits, 'kebab');
		this.snake = stringCase(bits, 'snake');
		this.flat = stringCase(bits, 'flat');
		this.dot = stringCase(bits, 'dot');
	}
	StringCase.prototype.toString = function() {
		return this.asIs;
	};

	function Name(nameBits, nsBits) {
		var bits;

		// Далее идет обработка параметра nsBits:
		// - если он не задан, пропустить все и взять ns из прототипа
		// - если он boolean и true, то также пропустить и использовать ns 
		//   из прототипа
		// - если он boolean и false, то nsBits - пустая строка
		// - и если в результате nsBits строка или массив, то создать новое 
		//   свойство ns поверх геттера прототипа со значением nsBits
		if (nsBits != null) {
			if (typeof nsBits == 'boolean' && nsBits == false) {
				nsBits = '';
			}
			if (typeof nsBits == 'string' || Array.isArray(nsBits)) {
				Object.defineProperty(this, 'ns', {
					value: new StringCase(nsBits),
					writable: true,
					configurable: true,
					enumerable: true,
				});
			}
		}

		var hasNs = this.ns && this.ns.toString() != '';

		this.base = new StringCase(nameBits);

		// #asIs #flat #camel #capital #kebab
		var types = ['asIs', 'flat', 'camel', 'capitalCamel', 'kebab'];
		for (var i = 0; i < types.length; i++) {
			if (hasNs) {
				this['_' + types[i]] = stringCase([this.ns[types[i]], this.base[types[i]]], types[i]);
			} else {
				this['_' + types[i]] = this.base[types[i]];
			}
			this[types[i]] = (function(type, nameObj) {
				return function(suffix) {
					if (suffix) {
						if (type != 'asIs') {
							if (Array.isArray(suffix)) {
								suffix.forEach(function(bit, i) {
									suffix[i] = bit.toLowerCase();
								});
							}
						}
						return stringCase([nameObj['_' + type], stringCase(suffix, type)], type);
					} else {
						return nameObj['_' + type];
					}
				};
			})(types[i], this);
		}

		//  ██████   ██████  ████████
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██████   ██████     ██
		//
		// #dot
		var bits = [ this.base.camel ];
		if (hasNs) {
			bits.unshift(this.ns.camel);
		}
		this._dot = stringCase(bits, 'dot');
		this.dot = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._dot, stringCase(suffix, 'camel')], 'dot');
			} else {
				return this._dot;
			}
		};

		//  ███████ ██    ██ ███████ ███    ██ ████████
		//  ██      ██    ██ ██      ████   ██    ██
		//  █████   ██    ██ █████   ██ ██  ██    ██
		//  ██       ██  ██  ██      ██  ██ ██    ██
		//  ███████   ████   ███████ ██   ████    ██
		//
		// #event
		this.event = this.dot;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		//
		// #css
		bits = [ this.base.snake ];
		if (hasNs) {
			bits.unshift(this.ns.snake);
		}
		this._css = stringCase(bits, 'kebab');
		this.css = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._css, stringCase(suffix, 'snake')], 'kebab');
			} else {
				return this._css;
			}
		};

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//  ███    ███  ██████  ██████
		//  ████  ████ ██    ██ ██   ██
		//  ██ ████ ██ ██    ██ ██   ██
		//  ██  ██  ██ ██    ██ ██   ██
		//  ██      ██  ██████  ██████
		//
		// #css #modificator
		this.cssModificator = function(modificatorName) {
			if (modificatorName != null) {
				if (Array.isArray(modificatorName)) {
					modificatorName.forEach(function(bit, i) {
						modificatorName[i] = bit.toLowerCase();
					});
				}
				return this.css() + '--' + stringCase(modificatorName, 'snake');
			} else {
				return this.css();
			}
		};
		this.cssMod = this.cssModificator;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//   ██████ ██       █████  ███████ ███████
		//  ██      ██      ██   ██ ██      ██
		//  ██      ██      ███████ ███████ ███████
		//  ██      ██      ██   ██      ██      ██
		//   ██████ ███████ ██   ██ ███████ ███████
		// 
		//  ███████ ███████ ██      ███████  ██████ ████████  ██████  ██████
		//  ██      ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ █████   ██      █████   ██         ██    ██    ██ ██████
		//       ██ ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██
		//
		// #css #class #selector
		this.cssClassSelector = function(appendage) {
			return '.' + this.css(appendage);
		};
		this.selector = function(appendage) {
			return this.cssClassSelector(appendage);
		};
	}
	var ns;
	Object.defineProperty(Name.prototype, 'ns', {
		set: function(value) {
			ns = new StringCase(value);
		},
		get: function() {
			return ns;
		},
	});
	Name.prototype.toString = function() {
		return this._asIs;
	};

	function Id() {
	}
	Id.prototype.toString = function() {
		return this.current;
	};

	/**
	 * Базовый класс для виджетов
	 * @param {Array-like} arguments ...
	 */
	function Widget() {
		this.construct(...arguments);
	}
	/**
	 * Переопределение свойства `name` конструктора нужно при сохранении экзепляра класса в прототипе
	 * (см. метод `saveInstanceOf`)
	 */
	Object.defineProperty(Widget, 'name', {
		value: new Name('Widget', false),
	});

	Widget.prototype.name = new Name('widget');
	Widget.prototype.debug = false;
	Widget.prototype.busy = false;
	Widget.prototype.initialized = false;
	Widget.prototype.autoInitialize = true;

	/**
	 * Конструирование объекта.
	 * Этот метод не задуман для переопределения или расширения (для этого см. `constructionChain`).
	 * @param {Array-like} arguments ...
	 */
	Widget.prototype.construct = function()
	{
		var target, options;
		if (arguments[0] instanceof Element) {
			target = arguments[0];
			if (!!arguments[1] && typeof arguments[1] == 'object') {
				options = arguments[1];
				if (typeof arguments[2] == 'boolean') {
					options.autoInitialize = arguments[2];
				}
			}
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			options = arguments[0];
			if (typeof arguments[1] == 'boolean') {
				options.autoInitialize = arguments[1];
			}
		}
		this.constructionChain(target, options);
		if (this.autoInitialize) {
			this.initialize(this.dom.self);
		}
	};

	/**
	 * Конструирование объекта (только относящееся к этом объекту + вызов цепочки). Тут только тот код, 
	 * который нельзя назвать инициализацией (который не должен исполняться повторно). Этот метод задуман 
	 * для расширения или переопределения дочерними классами.
	 * @param {HTMLElement} target Элемент DOM, на котором создается виджет (иногда он нужен в конструкторе)
	 * @param {Object} options Параметры для экземпляра класса (перезаписывает значения по умолчанию)
	 */
	Widget.prototype.constructionChain = function(target, options)
	{
		this.dom = {
			self: target,
		};
		this.id = new Id;
		this.setOptions(options);
		this.saveInstanceOf(Widget);
	};

	/**
	 * Инициализация экзепляра класса: привязка к DOM, назначение прослушки событий и т.п.
	 * Этот метод не задуман для переопределения или расширения (для этого см. `initializationChain`)
	 * @param {HTMLElement|null} target Элемент DOM, на котором создается виджет
	 */
	Widget.prototype.initialize = function(target)
	{
		if (typeof target == 'undefined') {
			target = this.dom.self; // При отложенной инициализации
		}
		this.initializationChain(target);
		this.initialized = true;
	};

	/**
	 * Инициализация экзепляра класса (только относящаяся к этому объекту + вызов цепочки): 
	 * привязка к DOM, назначение прослушки событий и т.п. Этот метод задуман для расширения 
	 * или переопределения дочерними классами.
	 * @param {HTMLElement} target Элемент DOM, на котором создается виджет
	 */
	Widget.prototype.initializationChain = function(target)
	{
		this.dom.self = target;
	};

	Widget.prototype.setOptions = function(options)
	{
		if (isObject(options)) {
			extend(this, options);
		}
	};

	// /*
	//  ███████  █████  ██    ██ ███████
	//  ██      ██   ██ ██    ██ ██
	//  ███████ ███████ ██    ██ █████
	//       ██ ██   ██  ██  ██  ██
	//  ███████ ██   ██   ████   ███████
	// 
	//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
	//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
	//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
	//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
	//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
	// 
	//   ██████  ███████
	//  ██    ██ ██
	//  ██    ██ █████
	//  ██    ██ ██
	//   ██████  ██
	// */
	// #save #instance #of
	/**
	 * Сохранение экземпляра класса в регистре экземпляров класса этого типа
	 * @param {Function} constructor Конструктор класса, в который происходит сохранение экземпляра
	 */
	Widget.prototype.saveInstanceOf = function(constructor)
	{
		constructor = constructor || this.constructor;
		if (!constructor.prototype.hasOwnProperty('instances')) {
			(constructor.prototype.instances = []).length++; // потому что мы хотим считать id с 1, а не с 0
			// #TODO: Переделать на что-то более корректное, ведь при таком подходе с массивом его length будет выдавать результат не единицу больший, чем в действительности есть сохраненных экзепляров. Возможно лучше использовать Set или Map.
		}
		this.id.current = constructor.prototype.instances.length;
		this.id[constructor.name._camel] = this.id >> 0; // скастовать в строку, а затем в целое число
		constructor.prototype.instances[this.id] = this;
	};

	// /*
	//  ███████ ███████ ████████
	//  ██      ██         ██
	//  ███████ █████      ██
	//       ██ ██         ██
	//  ███████ ███████    ██
	// 
	//  ██████   █████  ████████  █████  ███████ ███████ ████████
	//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
	//  ██   ██ ███████    ██    ███████ ███████ █████      ██
	//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
	//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
	// 
	//  ██ ██████   ██████  ███████
	//  ██ ██   ██ ██    ██ ██
	//  ██ ██   ██ ██    ██ █████
	//  ██ ██   ██ ██    ██ ██
	//  ██ ██████   ██████  ██
	// */
	// #set #dataset #id #of
	Widget.prototype.setDatasetIdOf = function(constructor)
	{
		constructor = constructor || this.constructor;
		var id = this.id[constructor.name._camel];
		var name = constructor.prototype.name.camel('id');
		this.dom.self.dataset[name] = id;
	};

	// /*
	//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
	//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
	//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
	//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
	//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
	// 
	//  ███████ ██████   ██████  ███    ███
	//  ██      ██   ██ ██    ██ ████  ████
	//  █████   ██████  ██    ██ ██ ████ ██
	//  ██      ██   ██ ██    ██ ██  ██  ██
	//  ██      ██   ██  ██████  ██      ██
	// 
	//  ██████   █████  ████████  █████  ███████ ███████ ████████
	//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
	//  ██   ██ ███████    ██    ███████ ███████ █████      ██
	//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
	//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
	// */
	// #instance #from #dataset
	Widget.prototype.instanceFromDataset = function(element, constructor)
	{
		return constructor.prototype.instances[element.dataset[constructor.prototype.name.camel('id')]];
	};

	Widget.prototype.setState = function(state)
	{
		var methodName = 'setState' + firstUpperCase(state);
		if (typeof this[methodName] == 'function') {
			this[methodName]();
		}
	};

	Widget.prototype.clearState = function(state)
	{
		if (state == 'all') {
			// обход всех
			for (var property in this) {
				if (property.match(/^clearState(?!All).+/)) {
					this[property]();
				}
			}
		} else {
			if (!state) {
				if (!this.state) return;
				state = this.state;
			}
			var methodName = 'clearState' + firstUpperCase(state);
			if (typeof this[methodName] == 'function') {
				this[methodName]();
			}
		}
	};

	var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var spin = createCommonjsModule(function (module) {
	(function (root, factory) {

	  /* CommonJS */
	  if (module.exports) module.exports = factory();

	  /* AMD module */
	  else root.Spinner = factory();
	}(commonjsGlobal, function () {

	  var prefixes = ['webkit', 'Moz', 'ms', 'O'] /* Vendor prefixes */
	    , animations = {} /* Animation rules keyed by their name */
	    , useCssAnimations /* Whether to use CSS animations or setTimeout */
	    , sheet; /* A stylesheet to hold the @keyframe or VML rules. */

	  /**
	   * Utility function to create elements. If no tag name is given,
	   * a DIV is created. Optionally properties can be passed.
	   */
	  function createEl (tag, prop) {
	    var el = document.createElement(tag || 'div')
	      , n;

	    for (n in prop) el[n] = prop[n];
	    return el
	  }

	  /**
	   * Appends children and returns the parent.
	   */
	  function ins (parent /* child1, child2, ...*/) {
	    for (var i = 1, n = arguments.length; i < n; i++) {
	      parent.appendChild(arguments[i]);
	    }

	    return parent
	  }

	  /**
	   * Creates an opacity keyframe animation rule and returns its name.
	   * Since most mobile Webkits have timing issues with animation-delay,
	   * we create separate rules for each line/segment.
	   */
	  function addAnimation (alpha, trail, i, lines) {
	    var name = ['opacity', trail, ~~(alpha * 100), i, lines].join('-')
	      , start = 0.01 + i/lines * 100
	      , z = Math.max(1 - (1-alpha) / trail * (100-start), alpha)
	      , prefix = useCssAnimations.substring(0, useCssAnimations.indexOf('Animation')).toLowerCase()
	      , pre = prefix && '-' + prefix + '-' || '';

	    if (!animations[name]) {
	      sheet.insertRule(
	        '@' + pre + 'keyframes ' + name + '{' +
	        '0%{opacity:' + z + '}' +
	        start + '%{opacity:' + alpha + '}' +
	        (start+0.01) + '%{opacity:1}' +
	        (start+trail) % 100 + '%{opacity:' + alpha + '}' +
	        '100%{opacity:' + z + '}' +
	        '}', sheet.cssRules.length);

	      animations[name] = 1;
	    }

	    return name
	  }

	  /**
	   * Tries various vendor prefixes and returns the first supported property.
	   */
	  function vendor (el, prop) {
	    var s = el.style
	      , pp
	      , i;

	    prop = prop.charAt(0).toUpperCase() + prop.slice(1);
	    if (s[prop] !== undefined) return prop
	    for (i = 0; i < prefixes.length; i++) {
	      pp = prefixes[i]+prop;
	      if (s[pp] !== undefined) return pp
	    }
	  }

	  /**
	   * Sets multiple style properties at once.
	   */
	  function css (el, prop) {
	    for (var n in prop) {
	      el.style[vendor(el, n) || n] = prop[n];
	    }

	    return el
	  }

	  /**
	   * Fills in default values.
	   */
	  function merge (obj) {
	    for (var i = 1; i < arguments.length; i++) {
	      var def = arguments[i];
	      for (var n in def) {
	        if (obj[n] === undefined) obj[n] = def[n];
	      }
	    }
	    return obj
	  }

	  /**
	   * Returns the line color from the given string or array.
	   */
	  function getColor (color, idx) {
	    return typeof color == 'string' ? color : color[idx % color.length]
	  }

	  // Built-in defaults

	  var defaults = {
	    lines: 12             // The number of lines to draw
	  , length: 7             // The length of each line
	  , width: 5              // The line thickness
	  , radius: 10            // The radius of the inner circle
	  , scale: 1.0            // Scales overall size of the spinner
	  , corners: 1            // Roundness (0..1)
	  , color: '#000'         // #rgb or #rrggbb
	  , opacity: 1/4          // Opacity of the lines
	  , rotate: 0             // Rotation offset
	  , direction: 1          // 1: clockwise, -1: counterclockwise
	  , speed: 1              // Rounds per second
	  , trail: 100            // Afterglow percentage
	  , fps: 20               // Frames per second when using setTimeout()
	  , zIndex: 2e9           // Use a high z-index by default
	  , className: 'spinner'  // CSS class to assign to the element
	  , top: '50%'            // center vertically
	  , left: '50%'           // center horizontally
	  , shadow: false         // Whether to render a shadow
	  , hwaccel: false        // Whether to use hardware acceleration (might be buggy)
	  , position: 'absolute'  // Element positioning
	  };

	  /** The constructor */
	  function Spinner (o) {
	    this.opts = merge(o || {}, Spinner.defaults, defaults);
	  }

	  // Global defaults that override the built-ins:
	  Spinner.defaults = {};

	  merge(Spinner.prototype, {
	    /**
	     * Adds the spinner to the given target element. If this instance is already
	     * spinning, it is automatically removed from its previous target b calling
	     * stop() internally.
	     */
	    spin: function (target) {
	      this.stop();

	      var self = this
	        , o = self.opts
	        , el = self.el = createEl(null, {className: o.className});

	      css(el, {
	        position: o.position
	      , width: 0
	      , zIndex: o.zIndex
	      , left: o.left
	      , top: o.top
	      });

	      if (target) {
	        target.insertBefore(el, target.firstChild || null);
	      }

	      el.setAttribute('role', 'progressbar');
	      self.lines(el, self.opts);

	      if (!useCssAnimations) {
	        // No CSS animation support, use setTimeout() instead
	        var i = 0
	          , start = (o.lines - 1) * (1 - o.direction) / 2
	          , alpha
	          , fps = o.fps
	          , f = fps / o.speed
	          , ostep = (1 - o.opacity) / (f * o.trail / 100)
	          , astep = f / o.lines

	        ;(function anim () {
	          i++;
	          for (var j = 0; j < o.lines; j++) {
	            alpha = Math.max(1 - (i + (o.lines - j) * astep) % f * ostep, o.opacity);

	            self.opacity(el, j * o.direction + start, alpha, o);
	          }
	          self.timeout = self.el && setTimeout(anim, ~~(1000 / fps));
	        })();
	      }
	      return self
	    }

	    /**
	     * Stops and removes the Spinner.
	     */
	  , stop: function () {
	      var el = this.el;
	      if (el) {
	        clearTimeout(this.timeout);
	        if (el.parentNode) el.parentNode.removeChild(el);
	        this.el = undefined;
	      }
	      return this
	    }

	    /**
	     * Internal method that draws the individual lines. Will be overwritten
	     * in VML fallback mode below.
	     */
	  , lines: function (el, o) {
	      var i = 0
	        , start = (o.lines - 1) * (1 - o.direction) / 2
	        , seg;

	      function fill (color, shadow) {
	        return css(createEl(), {
	          position: 'absolute'
	        , width: o.scale * (o.length + o.width) + 'px'
	        , height: o.scale * o.width + 'px'
	        , background: color
	        , boxShadow: shadow
	        , transformOrigin: 'left'
	        , transform: 'rotate(' + ~~(360/o.lines*i + o.rotate) + 'deg) translate(' + o.scale*o.radius + 'px' + ',0)'
	        , borderRadius: (o.corners * o.scale * o.width >> 1) + 'px'
	        })
	      }

	      for (; i < o.lines; i++) {
	        seg = css(createEl(), {
	          position: 'absolute'
	        , top: 1 + ~(o.scale * o.width / 2) + 'px'
	        , transform: o.hwaccel ? 'translate3d(0,0,0)' : ''
	        , opacity: o.opacity
	        , animation: useCssAnimations && addAnimation(o.opacity, o.trail, start + i * o.direction, o.lines) + ' ' + 1 / o.speed + 's linear infinite'
	        });

	        if (o.shadow) ins(seg, css(fill('#000', '0 0 4px #000'), {top: '2px'}));
	        ins(el, ins(seg, fill(getColor(o.color, i), '0 0 1px rgba(0,0,0,.1)')));
	      }
	      return el
	    }

	    /**
	     * Internal method that adjusts the opacity of a single line.
	     * Will be overwritten in VML fallback mode below.
	     */
	  , opacity: function (el, i, val) {
	      if (i < el.childNodes.length) el.childNodes[i].style.opacity = val;
	    }

	  });


	  function initVML () {

	    /* Utility function to create a VML tag */
	    function vml (tag, attr) {
	      return createEl('<' + tag + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', attr)
	    }

	    // No CSS transforms but VML support, add a CSS rule for VML elements:
	    sheet.addRule('.spin-vml', 'behavior:url(#default#VML)');

	    Spinner.prototype.lines = function (el, o) {
	      var r = o.scale * (o.length + o.width)
	        , s = o.scale * 2 * r;

	      function grp () {
	        return css(
	          vml('group', {
	            coordsize: s + ' ' + s
	          , coordorigin: -r + ' ' + -r
	          })
	        , { width: s, height: s }
	        )
	      }

	      var margin = -(o.width + o.length) * o.scale * 2 + 'px'
	        , g = css(grp(), {position: 'absolute', top: margin, left: margin})
	        , i;

	      function seg (i, dx, filter) {
	        ins(
	          g
	        , ins(
	            css(grp(), {rotation: 360 / o.lines * i + 'deg', left: ~~dx})
	          , ins(
	              css(
	                vml('roundrect', {arcsize: o.corners})
	              , { width: r
	                , height: o.scale * o.width
	                , left: o.scale * o.radius
	                , top: -o.scale * o.width >> 1
	                , filter: filter
	                }
	              )
	            , vml('fill', {color: getColor(o.color, i), opacity: o.opacity})
	            , vml('stroke', {opacity: 0}) // transparent stroke to fix color bleeding upon opacity change
	            )
	          )
	        );
	      }

	      if (o.shadow)
	        for (i = 1; i <= o.lines; i++) {
	          seg(i, -2, 'progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)');
	        }

	      for (i = 1; i <= o.lines; i++) seg(i);
	      return ins(el, g)
	    };

	    Spinner.prototype.opacity = function (el, i, val, o) {
	      var c = el.firstChild;
	      o = o.shadow && o.lines || 0;
	      if (c && i + o < c.childNodes.length) {
	        c = c.childNodes[i + o]; c = c && c.firstChild; c = c && c.firstChild;
	        if (c) c.opacity = val;
	      }
	    };
	  }

	  if (typeof document !== 'undefined') {
	    sheet = (function () {
	      var el = createEl('style', {type : 'text/css'});
	      ins(document.getElementsByTagName('head')[0], el);
	      return el.sheet || el.styleSheet
	    }());

	    var probe = css(createEl('group'), {behavior: 'url(#default#VML)'});

	    if (!vendor(probe, 'transform') && probe.adj) initVML();
	    else useCssAnimations = vendor(probe, 'animation');
	  }

	  return Spinner

	}));
	});

	function Modal() {
		Widget.call(this, ...arguments);
	}
	Object.defineProperty(Modal, 'name', {
		value: new Name('Modal', false),
	});
	 
	Modal.prototype = Object.create(Widget.prototype);
	Object.defineProperty(Modal.prototype, 'constructor', {
		value: Modal,
		enumerable: false,
		writable: true,
	});

	var name = new Name('modal');
	  
	Modal.prototype.name = name;
	Modal.prototype.shown = false;
	Modal.prototype.loaded = false;
	Modal.prototype.animate = true;
	Modal.prototype.url = null;
	Modal.prototype.spinner = null;
	Modal.prototype.finishShowEvent = null;
	Modal.prototype.finishShowEventName = name.event(['finish', 'show']);
	Modal.prototype.finishShowEventDetail = null;
	Modal.prototype.finishHideEvent = null;
	Modal.prototype.finishHideEventName = name.event(['finish', 'hide']);
	Modal.prototype.finishHideEventDetail = null;
	Modal.prototype.defaultWidth = 600;
	 
	Modal.prototype.constructionChain = function(target, options)
	{
		Widget.prototype.constructionChain.call(this, target, options);
		this.saveInstanceOf(Modal);
	 
		if (this.hasOwnProperty('finishShowEvent') == false) {
			this.finishShowEvent = new CustomEvent(this.finishShowEventName, {
				detail: extend({ widget: this }, this.finishShowEventDetail),
			});
		}
		if (this.hasOwnProperty('finishHideEvent') == false) {
			this.finishHideEvent = new CustomEvent(this.finishHideEventName, {
				detail: extend({ widget: this }, this.finishHideEventDetail),
			});
		}
	};

	//	███████ ███████ ████████
	//	██      ██         ██
	//	███████ █████      ██
	//	     ██ ██         ██
	//	███████ ███████    ██
	//
	//	██     ██ ██ ██████  ████████ ██   ██
	//	██     ██ ██ ██   ██    ██    ██   ██
	//	██  █  ██ ██ ██   ██    ██    ███████
	//	██ ███ ██ ██ ██   ██    ██    ██   ██
	//	 ███ ███  ██ ██████     ██    ██   ██
	//
	//	#set #width
	Modal.prototype.setWidth = function(value)
	{
		value = value || this.defaultWidth;
		if (typeof value == 'string' || value instanceof String) {
			switch (value) {
				case 'tight':
					value = 350;
					break;
				default:
					value = this.defaultWidth;
					break;
			}
		}
		var remValue = rem(value, true);
		this.dom.box.style.width = remValue;
		this.dom.iframe.style.width = remValue;
	};

	//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████

	Modal.prototype.initializationChain = function(target)
	{
		Widget.prototype.initializationChain.call(this, target);
		this.setDatasetIdOf(Modal);

		// DOM-элементы
		if (!this.dom.self) {
			throw new Error('Для модального окна не найдены элементы в DOM');
		}
		this.dom.clip = this.dom.self.querySelector(this.name.selector('clip'));
		this.dom.box = this.dom.self.querySelector(this.name.selector('box'));
		this.dom.content = this.dom.self.querySelector(this.name.selector('content'));
		this.dom.close = this.dom.self.querySelector(this.name.selector('close'));
		this.dom.iframe = this.dom.content.querySelector('iframe');
		 
		// Привязка событий
		this.dom.self.addEventListener('click', function(event) {
			var modal = Modal.prototype.instanceFromDataset(this, Modal);
			if (event.target.closest(modal.name.selector('box')) == null) {
				modal.hide();
			}
		}, false);
		this.dom.close.addEventListener('click', function(event) {
			var modal = Modal.prototype.instanceFromDataset(
				closestParentByClass(event.target, Modal.prototype.name.css()),
				Modal
			);
			event.preventDefault();
			modal.hide();
		}, false);
		 
		this.initialized = true;
	};

	//	██    ██ ████████ ██ ██      ███████
	//	██    ██    ██    ██ ██      ██
	//	██    ██    ██    ██ ██      ███████
	//	██    ██    ██    ██ ██           ██
	//	 ██████     ██    ██ ███████ ███████
	//
	//	#utils

	//	██    ██ ██ ███████ ██     ██ ██████   ██████  ██████  ████████
	//	██    ██ ██ ██      ██     ██ ██   ██ ██    ██ ██   ██    ██
	//	██    ██ ██ █████   ██  █  ██ ██████  ██    ██ ██████     ██
	//	 ██  ██  ██ ██      ██ ███ ██ ██      ██    ██ ██   ██    ██
	//	  ████   ██ ███████  ███ ███  ██       ██████  ██   ██    ██
	// 
	//	 ██████ ██████   ██████  ██████
	//	██      ██   ██ ██    ██ ██   ██
	//	██      ██████  ██    ██ ██████
	//	██      ██   ██ ██    ██ ██
	//	 ██████ ██   ██  ██████  ██
	// 
	//	#viewport #crop
	function viewportCrop() {
		document.documentElement.style.overflow = 'hidden';
	}
	//	██    ██ ██ ███████ ██     ██ ██████   ██████  ██████  ████████
	//	██    ██ ██ ██      ██     ██ ██   ██ ██    ██ ██   ██    ██
	//	██    ██ ██ █████   ██  █  ██ ██████  ██    ██ ██████     ██
	//	 ██  ██  ██ ██      ██ ███ ██ ██      ██    ██ ██   ██    ██
	//	  ████   ██ ███████  ███ ███  ██       ██████  ██   ██    ██
	// 
	//	██████  ███████ ███████ ████████  ██████  ██████  ███████
	//	██   ██ ██      ██         ██    ██    ██ ██   ██ ██
	//	██████  █████   ███████    ██    ██    ██ ██████  █████
	//	██   ██ ██           ██    ██    ██    ██ ██   ██ ██
	//	██   ██ ███████ ███████    ██     ██████  ██   ██ ███████
	// 
	//	#viewport #restore
	function viewportRestore() {
		document.documentElement.style.overflow = null;
	}

	var opts = {
	  lines: 13 // The number of lines to draw
	, length: 6 // The length of each line
	, width: 2 // The line thickness
	, radius: 11 // The radius of the inner circle
	, scale: 1 // Scales overall size of the spinner
	, corners: 0 // Corner roundness (0..1)
	, color: '#000' // #rgb or #rrggbb or array of colors
	, opacity: 0.25 // Opacity of the lines
	, rotate: 0 // The rotation offset
	, direction: 1 // 1: clockwise, -1: counterclockwise
	, speed: 1 // Rounds per second
	, trail: 60 // Afterglow percentage
	, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
	, zIndex: 2e9 // The z-index (defaults to 2000000000)
	, className: 'spinner' // The CSS class to assign to the spinner
	, top: '50%' // Top position relative to parent
	, left: '50%' // Left position relative to parent
	, shadow: false // Whether to render a shadow
	, hwaccel: false // Whether to use hardware acceleration
	, position: 'absolute' // Element positioning
	};

	var Spinner = new spin(opts);

	//	███████ ██   ██  ██████  ██     ██

	Modal.prototype.show = function(newOptions)
	{
		if (!this.initialized || this.busy || this.shown) return;
		this.busy = true;
		this.setOptions(newOptions);
		 
		// Скрыть прокрутку страницы
		viewportCrop();
		
		// Показ диалогового окна
		this.dom.self.style.display = null;
	 
		if (this.animate == false) {
			finish.call(this);
		} else {
			this.dom.clip.style.backgroundColor = 'transparent';
			this.dom.box.style.opacity = 0;
			this.dom.box.style.transform = 'translateY(3rem)';
			// this.dom.box.style.transform = 'translateY(3rem) scale(.8)';
			// this.dom.box.style.transform = 'scale(.5)';
			this.dom.iframe.style.opacity = 0;
			document.body.getBoundingClientRect(); // #reflow
			this.dom.clip.style.backgroundColor = null;
			window.setTimeout((function() {
				this.dom.box.style.opacity = null;
				this.dom.box.style.transform = null;
				window.setTimeout(finish.bind(this), 400);
			}).bind(this), 300);
		}
	};
	 
	function finish() {
		this.busy = false;
		this.shown = true;

		this.escapeKeyHitHandler = (function(event) {
			if ((event.key ? event.key == 'Escape' : event.keyCode == 27) && this.shown) {
				this.hide();
			}
		}).bind(this);

		document.addEventListener('keydown', this.escapeKeyHitHandler, false);

		// #NOTE: Перенесено сюда из show потому что если делать там, то при нажатии Esc до установки обработчика выше, но при начавшейся загрузке iframe-а, загрузка iframe-а прекращалась и при этом модал оставался открытым. Плюс так еще можно выиграть в гладкости эффектов.
		// Загрузка начального URL
		this.dom.iframe.src = this.url;
		
		// Только при первой загрузке
		this.dom.iframe.addEventListener('load', this.iframeOnFirstLoad, false);
		
		// При каждой загрузке подгонять размер и включать/выключать отслеживание изменения размера
		this.dom.iframe.addEventListener('load', this.iframeOnEachLoad, false);
			  
		// Показ индикатора загрузки, если запрошенный URL загружается слишком долго
		// (дольше iframeUnnotifiedLoadingTimeout)
		this.iframeUnnotifiedLoadingTimeoutId = window.setTimeout((function() {
			this.iframeLoadingTooLong = true;
			this.spinner = Spinner.spin(this.dom.box);
		}).bind(this), this.iframeUnnotifiedLoadingTimeout);

		this.dom.self.dispatchEvent(this.finishShowEvent);
	}

	//	██   ██ ██ ██████  ███████

	Modal.prototype.hide = function()
	{
		if (this.initialized && !this.busy && this.shown) {
			this.busy = true;
			if (this.iframeLoaded) {
				this.iframeLoaded = false;
				// Убрать интервал вычисления высоты iframe
				window.clearInterval(this.iframeResizeIntervalId);
			} else {
				if (this.iframeLoadingTooLong) {
					this.iframeLoadingTooLong = false;
					this.spinner.stop();
				} else {
					window.clearTimeout(this.iframeUnnotifiedLoadingTimeoutId);
				}
			}
			  
			// Скрытие диалогового окна (анимация и прочее)
			if (!this.animate) {
				finish$1.call(this);
			} else {
				// this.dom.box.style.transform = 'translateY(3rem)';
				// this.dom.box.style.transform = 'translateY(3rem) scale(.8)';
				this.dom.box.style.transform = 'scale(.8)';
				this.dom.box.style.opacity = 0;
				// window.setTimeout(function() {
				// }, 50);
				window.setTimeout((function() {
					this.dom.clip.style.backgroundColor = 'transparent';
				}).bind(this), 100);
				window.setTimeout(finish$1.bind(this), 500);
	 
				// this.dom.box.transition({
				// 	transform: 'scale(.5)',
				// 	duration: transitionDuration,
				// 	easing: 'cubic-bezier(0.62,-0.5,1,0.33)',
				// 	queue: false,
				// });
				// window.setTimeout(function() {
				// 	this.dom.box.animate({ opacity: 0 }, boxFadeDuration);
				// }, boxFadeDelay);
				// this.dom.clip.transition({
				// 	opacity: 0,
				// 	delay: clipHideDelay,
				// 	duration: transitionDuration,
				// 	complete: finishHide,
				// });
			}
		}
	};

	function finish$1() {
		this.dom.self.style.display = 'none';
		this.busy = false;
		this.shown = false;
		this.dom.iframe.removeEventListener('load', this.iframeOnFirstLoad, false);
		this.dom.iframe.removeEventListener('load', this.iframeOnEachLoad, false);
		this.dom.iframe.src = 'about:blank';
		this.dom.iframe.style = null;
		this.dom.box.style.width = null;
		this.dom.box.style.height = null;
		document.removeEventListener('keydown', this.escapeKeyHitHandler, false);
		viewportRestore();
		this.iframePreviousHeight = 0;
		  
		// Перезагрузка основного окна
		// Использование forcedReload позволяет предотвратить повторную
		// загрузку iframe по последнему URL при перезагрузке страницы
		// window.location.reload(true);
		
		this.dom.self.dispatchEvent(this.finishHideEvent);
	}

	//	██ ███████ ██████   █████  ███    ███ ███████

	Modal.prototype.iframeLoaded = false;
	Modal.prototype.iframeResizeIntervalId = null;
	Modal.prototype.iframeUnnotifiedLoadingTimeout = 500;
	Modal.prototype.iframeUnnotifiedLoadingTimeoutId = null;
	Modal.prototype.iframeLoadingTooLong = false;
	Modal.prototype.iframePreviousHeight = null;
	Modal.prototype.iframeResizeInterval = 1000;

	//	 ██████  ███    ██     ███████ ██ ██████  ███████ ████████
	//	██    ██ ████   ██     ██      ██ ██   ██ ██         ██
	//	██    ██ ██ ██  ██     █████   ██ ██████  ███████    ██
	//	██    ██ ██  ██ ██     ██      ██ ██   ██      ██    ██
	//	 ██████  ██   ████     ██      ██ ██   ██ ███████    ██
	// 
	//	██       ██████   █████  ██████
	//	██      ██    ██ ██   ██ ██   ██
	//	██      ██    ██ ███████ ██   ██
	//	██      ██    ██ ██   ██ ██   ██
	//	███████  ██████  ██   ██ ██████
	// 
	//	#on #first #load
	Modal.prototype.iframeOnFirstLoad = function(event)
	{
		// console.log('iframeOnFirstLoad()');
		var modal = Modal.prototype.instanceFromDataset(
			closestParentByClass(event.target, Modal.prototype.name.css()),
			Modal
		);
		modal.iframeLoaded = true;
		if (modal.iframeLoadingTooLong) {
			modal.iframeLoadingTooLong = false;
			modal.spinner.stop();
		} else {
			window.clearTimeout(modal.iframeUnnotifiedLoadingTimeoutId);
		}
		// Показ iframe
		modal.dom.iframe.style.opacity = null;
		// modal.dom.iframe.css('opacity', 0).transition({opacity: 1}, 500, 'linear');
		// Отключение этого обработчика
		modal.dom.iframe.removeEventListener('load', modal.iframeOnFirstLoad, false);
	};

	//	 ██████  ███    ██     ███████  █████   ██████ ██   ██
	//	██    ██ ████   ██     ██      ██   ██ ██      ██   ██
	//	██    ██ ██ ██  ██     █████   ███████ ██      ███████
	//	██    ██ ██  ██ ██     ██      ██   ██ ██      ██   ██
	//	 ██████  ██   ████     ███████ ██   ██  ██████ ██   ██
	// 
	//	██       ██████   █████  ██████
	//	██      ██    ██ ██   ██ ██   ██
	//	██      ██    ██ ███████ ██   ██
	//	██      ██    ██ ██   ██ ██   ██
	//	███████  ██████  ██   ██ ██████
	// 
	//	#on #each #load
	Modal.prototype.iframeOnEachLoad = function()
	{
		// console.log('iframeOnEachLoad()');
		var modal = Modal.prototype.instanceFromDataset(
			closestParentByClass(event.target, Modal.prototype.name.css()),
			Modal
		);
		modal.iframeResize();
		modal.iframeResizeIntervalId = window.setInterval(modal.iframeResize.bind(modal), modal.iframeResizeInterval);
		// При выгрузке iframe отключать отслеживание
		modal.dom.iframe.contentWindow.addEventListener('unload', function() {
			// console.log('unload');
			window.clearInterval(modal.iframeResizeIntervalId);
		}, false);
	};

	//	██████  ███████ ███████ ██ ███████ ███████
	//	██   ██ ██      ██      ██    ███  ██
	//	██████  █████   ███████ ██   ███   █████
	//	██   ██ ██           ██ ██  ███    ██
	//	██   ██ ███████ ███████ ██ ███████ ███████
	// 
	//	#resize
	Modal.prototype.iframeResize = function()
	{
		// console.log('iframeResize()');
		var h1 = this.dom.iframe.contentWindow.document.documentElement.getBoundingClientRect().height;
		// var h1 = $(this.dom.iframe.get(0).contentWindow.document.documentElement).height();
		var h2 = marginBoxHeight(this.dom.iframe.contentWindow.document.body, this.dom.iframe.contentWindow);
		var height = Math.max(h1, h2);
		// console.log('iframe check height');
		// console.log('height = ' + height + ', previousHeight = ' + iframePreviousHeight);
		// console.log('h1 = ' + h1 + ', h2 = ' + h2);
		if (height != this.iframePreviousHeight) {
			var heightUnits = rem(height, true);
			this.iframePreviousHeight = height;
			this.dom.iframe.style.height = heightUnits;
			this.dom.box.style.height = heightUnits;
			// this.dom.iframe.transition({ height: height + 'px' }, 500, 'ease');
			// console.log($().jquery);
			// console.log($.fn.transition);
		}
	};

	//	███████ ███████ ████████ ██    ██ ██████
	//	██      ██         ██    ██    ██ ██   ██
	//	███████ █████      ██    ██    ██ ██████
	//	     ██ ██         ██    ██    ██ ██
	//	███████ ███████    ██     ██████  ██
	// 
	//	 ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████
	//	██      ██    ██ ████   ██    ██    ██      ████   ██    ██
	//	██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██
	//	██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██
	//	 ██████  ██████  ██   ████    ██    ███████ ██   ████    ██
	// 
	//	██████   ██████   ██████ ██    ██ ███    ███ ███████ ███    ██ ████████
	//	██   ██ ██    ██ ██      ██    ██ ████  ████ ██      ████   ██    ██
	//	██   ██ ██    ██ ██      ██    ██ ██ ████ ██ █████   ██ ██  ██    ██
	//	██   ██ ██    ██ ██      ██    ██ ██  ██  ██ ██      ██  ██ ██    ██
	//	██████   ██████   ██████  ██████  ██      ██ ███████ ██   ████    ██
	// 
	//	#setup #content #document
	Modal.prototype.iframeSetupContentDocument = function(contentDocument, modalInstance, width)
	{
		contentDocument.addEventListener('keydown', function(event) {
			if (event.key ? event.key == 'Escape' : event.keyCode == 27) {
				modalInstance.hide();
			}
		});
		width = width || 'default';
		modalInstance.setWidth(width);
	};

	var compiler = createCommonjsModule(function (module, exports) {
	/*
	 *  Copyright 2011 Twitter, Inc.
	 *  Licensed under the Apache License, Version 2.0 (the "License");
	 *  you may not use this file except in compliance with the License.
	 *  You may obtain a copy of the License at
	 *
	 *  http://www.apache.org/licenses/LICENSE-2.0
	 *
	 *  Unless required by applicable law or agreed to in writing, software
	 *  distributed under the License is distributed on an "AS IS" BASIS,
	 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 *  See the License for the specific language governing permissions and
	 *  limitations under the License.
	 */

	(function (Hogan) {
	  // Setup regex  assignments
	  // remove whitespace according to Mustache spec
	  var rIsWhitespace = /\S/,
	      rQuot = /\"/g,
	      rNewline =  /\n/g,
	      rCr = /\r/g,
	      rSlash = /\\/g,
	      rLineSep = /\u2028/,
	      rParagraphSep = /\u2029/;

	  Hogan.tags = {
	    '#': 1, '^': 2, '<': 3, '$': 4,
	    '/': 5, '!': 6, '>': 7, '=': 8, '_v': 9,
	    '{': 10, '&': 11, '_t': 12
	  };

	  Hogan.scan = function scan(text, delimiters) {
	    var len = text.length,
	        IN_TEXT = 0,
	        IN_TAG_TYPE = 1,
	        IN_TAG = 2,
	        state = IN_TEXT,
	        tagType = null,
	        tag = null,
	        buf = '',
	        tokens = [],
	        seenTag = false,
	        i = 0,
	        lineStart = 0,
	        otag = '{{',
	        ctag = '}}';

	    function addBuf() {
	      if (buf.length > 0) {
	        tokens.push({tag: '_t', text: new String(buf)});
	        buf = '';
	      }
	    }

	    function lineIsWhitespace() {
	      var isAllWhitespace = true;
	      for (var j = lineStart; j < tokens.length; j++) {
	        isAllWhitespace =
	          (Hogan.tags[tokens[j].tag] < Hogan.tags['_v']) ||
	          (tokens[j].tag == '_t' && tokens[j].text.match(rIsWhitespace) === null);
	        if (!isAllWhitespace) {
	          return false;
	        }
	      }

	      return isAllWhitespace;
	    }

	    function filterLine(haveSeenTag, noNewLine) {
	      addBuf();

	      if (haveSeenTag && lineIsWhitespace()) {
	        for (var j = lineStart, next; j < tokens.length; j++) {
	          if (tokens[j].text) {
	            if ((next = tokens[j+1]) && next.tag == '>') {
	              // set indent to token value
	              next.indent = tokens[j].text.toString();
	            }
	            tokens.splice(j, 1);
	          }
	        }
	      } else if (!noNewLine) {
	        tokens.push({tag:'\n'});
	      }

	      seenTag = false;
	      lineStart = tokens.length;
	    }

	    function changeDelimiters(text, index) {
	      var close = '=' + ctag,
	          closeIndex = text.indexOf(close, index),
	          delimiters = trim(
	            text.substring(text.indexOf('=', index) + 1, closeIndex)
	          ).split(' ');

	      otag = delimiters[0];
	      ctag = delimiters[delimiters.length - 1];

	      return closeIndex + close.length - 1;
	    }

	    if (delimiters) {
	      delimiters = delimiters.split(' ');
	      otag = delimiters[0];
	      ctag = delimiters[1];
	    }

	    for (i = 0; i < len; i++) {
	      if (state == IN_TEXT) {
	        if (tagChange(otag, text, i)) {
	          --i;
	          addBuf();
	          state = IN_TAG_TYPE;
	        } else {
	          if (text.charAt(i) == '\n') {
	            filterLine(seenTag);
	          } else {
	            buf += text.charAt(i);
	          }
	        }
	      } else if (state == IN_TAG_TYPE) {
	        i += otag.length - 1;
	        tag = Hogan.tags[text.charAt(i + 1)];
	        tagType = tag ? text.charAt(i + 1) : '_v';
	        if (tagType == '=') {
	          i = changeDelimiters(text, i);
	          state = IN_TEXT;
	        } else {
	          if (tag) {
	            i++;
	          }
	          state = IN_TAG;
	        }
	        seenTag = i;
	      } else {
	        if (tagChange(ctag, text, i)) {
	          tokens.push({tag: tagType, n: trim(buf), otag: otag, ctag: ctag,
	                       i: (tagType == '/') ? seenTag - otag.length : i + ctag.length});
	          buf = '';
	          i += ctag.length - 1;
	          state = IN_TEXT;
	          if (tagType == '{') {
	            if (ctag == '}}') {
	              i++;
	            } else {
	              cleanTripleStache(tokens[tokens.length - 1]);
	            }
	          }
	        } else {
	          buf += text.charAt(i);
	        }
	      }
	    }

	    filterLine(seenTag, true);

	    return tokens;
	  };

	  function cleanTripleStache(token) {
	    if (token.n.substr(token.n.length - 1) === '}') {
	      token.n = token.n.substring(0, token.n.length - 1);
	    }
	  }

	  function trim(s) {
	    if (s.trim) {
	      return s.trim();
	    }

	    return s.replace(/^\s*|\s*$/g, '');
	  }

	  function tagChange(tag, text, index) {
	    if (text.charAt(index) != tag.charAt(0)) {
	      return false;
	    }

	    for (var i = 1, l = tag.length; i < l; i++) {
	      if (text.charAt(index + i) != tag.charAt(i)) {
	        return false;
	      }
	    }

	    return true;
	  }

	  // the tags allowed inside super templates
	  var allowedInSuper = {'_t': true, '\n': true, '$': true, '/': true};

	  function buildTree(tokens, kind, stack, customTags) {
	    var instructions = [],
	        opener = null,
	        tail = null,
	        token = null;

	    tail = stack[stack.length - 1];

	    while (tokens.length > 0) {
	      token = tokens.shift();

	      if (tail && tail.tag == '<' && !(token.tag in allowedInSuper)) {
	        throw new Error('Illegal content in < super tag.');
	      }

	      if (Hogan.tags[token.tag] <= Hogan.tags['$'] || isOpener(token, customTags)) {
	        stack.push(token);
	        token.nodes = buildTree(tokens, token.tag, stack, customTags);
	      } else if (token.tag == '/') {
	        if (stack.length === 0) {
	          throw new Error('Closing tag without opener: /' + token.n);
	        }
	        opener = stack.pop();
	        if (token.n != opener.n && !isCloser(token.n, opener.n, customTags)) {
	          throw new Error('Nesting error: ' + opener.n + ' vs. ' + token.n);
	        }
	        opener.end = token.i;
	        return instructions;
	      } else if (token.tag == '\n') {
	        token.last = (tokens.length == 0) || (tokens[0].tag == '\n');
	      }

	      instructions.push(token);
	    }

	    if (stack.length > 0) {
	      throw new Error('missing closing tag: ' + stack.pop().n);
	    }

	    return instructions;
	  }

	  function isOpener(token, tags) {
	    for (var i = 0, l = tags.length; i < l; i++) {
	      if (tags[i].o == token.n) {
	        token.tag = '#';
	        return true;
	      }
	    }
	  }

	  function isCloser(close, open, tags) {
	    for (var i = 0, l = tags.length; i < l; i++) {
	      if (tags[i].c == close && tags[i].o == open) {
	        return true;
	      }
	    }
	  }

	  function stringifySubstitutions(obj) {
	    var items = [];
	    for (var key in obj) {
	      items.push('"' + esc(key) + '": function(c,p,t,i) {' + obj[key] + '}');
	    }
	    return "{ " + items.join(",") + " }";
	  }

	  function stringifyPartials(codeObj) {
	    var partials = [];
	    for (var key in codeObj.partials) {
	      partials.push('"' + esc(key) + '":{name:"' + esc(codeObj.partials[key].name) + '", ' + stringifyPartials(codeObj.partials[key]) + "}");
	    }
	    return "partials: {" + partials.join(",") + "}, subs: " + stringifySubstitutions(codeObj.subs);
	  }

	  Hogan.stringify = function(codeObj, text, options) {
	    return "{code: function (c,p,i) { " + Hogan.wrapMain(codeObj.code) + " }," + stringifyPartials(codeObj) +  "}";
	  };

	  var serialNo = 0;
	  Hogan.generate = function(tree, text, options) {
	    serialNo = 0;
	    var context = { code: '', subs: {}, partials: {} };
	    Hogan.walk(tree, context);

	    if (options.asString) {
	      return this.stringify(context, text, options);
	    }

	    return this.makeTemplate(context, text, options);
	  };

	  Hogan.wrapMain = function(code) {
	    return 'var t=this;t.b(i=i||"");' + code + 'return t.fl();';
	  };

	  Hogan.template = Hogan.Template;

	  Hogan.makeTemplate = function(codeObj, text, options) {
	    var template = this.makePartials(codeObj);
	    template.code = new Function('c', 'p', 'i', this.wrapMain(codeObj.code));
	    return new this.template(template, text, this, options);
	  };

	  Hogan.makePartials = function(codeObj) {
	    var key, template = {subs: {}, partials: codeObj.partials, name: codeObj.name};
	    for (key in template.partials) {
	      template.partials[key] = this.makePartials(template.partials[key]);
	    }
	    for (key in codeObj.subs) {
	      template.subs[key] = new Function('c', 'p', 't', 'i', codeObj.subs[key]);
	    }
	    return template;
	  };

	  function esc(s) {
	    return s.replace(rSlash, '\\\\')
	            .replace(rQuot, '\\\"')
	            .replace(rNewline, '\\n')
	            .replace(rCr, '\\r')
	            .replace(rLineSep, '\\u2028')
	            .replace(rParagraphSep, '\\u2029');
	  }

	  function chooseMethod(s) {
	    return (~s.indexOf('.')) ? 'd' : 'f';
	  }

	  function createPartial(node, context) {
	    var prefix = "<" + (context.prefix || "");
	    var sym = prefix + node.n + serialNo++;
	    context.partials[sym] = {name: node.n, partials: {}};
	    context.code += 't.b(t.rp("' +  esc(sym) + '",c,p,"' + (node.indent || '') + '"));';
	    return sym;
	  }

	  Hogan.codegen = {
	    '#': function(node, context) {
	      context.code += 'if(t.s(t.' + chooseMethod(node.n) + '("' + esc(node.n) + '",c,p,1),' +
	                      'c,p,0,' + node.i + ',' + node.end + ',"' + node.otag + " " + node.ctag + '")){' +
	                      't.rs(c,p,' + 'function(c,p,t){';
	      Hogan.walk(node.nodes, context);
	      context.code += '});c.pop();}';
	    },

	    '^': function(node, context) {
	      context.code += 'if(!t.s(t.' + chooseMethod(node.n) + '("' + esc(node.n) + '",c,p,1),c,p,1,0,0,"")){';
	      Hogan.walk(node.nodes, context);
	      context.code += '};';
	    },

	    '>': createPartial,
	    '<': function(node, context) {
	      var ctx = {partials: {}, code: '', subs: {}, inPartial: true};
	      Hogan.walk(node.nodes, ctx);
	      var template = context.partials[createPartial(node, context)];
	      template.subs = ctx.subs;
	      template.partials = ctx.partials;
	    },

	    '$': function(node, context) {
	      var ctx = {subs: {}, code: '', partials: context.partials, prefix: node.n};
	      Hogan.walk(node.nodes, ctx);
	      context.subs[node.n] = ctx.code;
	      if (!context.inPartial) {
	        context.code += 't.sub("' + esc(node.n) + '",c,p,i);';
	      }
	    },

	    '\n': function(node, context) {
	      context.code += write('"\\n"' + (node.last ? '' : ' + i'));
	    },

	    '_v': function(node, context) {
	      context.code += 't.b(t.v(t.' + chooseMethod(node.n) + '("' + esc(node.n) + '",c,p,0)));';
	    },

	    '_t': function(node, context) {
	      context.code += write('"' + esc(node.text) + '"');
	    },

	    '{': tripleStache,

	    '&': tripleStache
	  };

	  function tripleStache(node, context) {
	    context.code += 't.b(t.t(t.' + chooseMethod(node.n) + '("' + esc(node.n) + '",c,p,0)));';
	  }

	  function write(s) {
	    return 't.b(' + s + ');';
	  }

	  Hogan.walk = function(nodelist, context) {
	    var func;
	    for (var i = 0, l = nodelist.length; i < l; i++) {
	      func = Hogan.codegen[nodelist[i].tag];
	      func && func(nodelist[i], context);
	    }
	    return context;
	  };

	  Hogan.parse = function(tokens, text, options) {
	    options = options || {};
	    return buildTree(tokens, '', [], options.sectionTags || []);
	  };

	  Hogan.cache = {};

	  Hogan.cacheKey = function(text, options) {
	    return [text, !!options.asString, !!options.disableLambda, options.delimiters, !!options.modelGet].join('||');
	  };

	  Hogan.compile = function(text, options) {
	    options = options || {};
	    var key = Hogan.cacheKey(text, options);
	    var template = this.cache[key];

	    if (template) {
	      var partials = template.partials;
	      for (var name in partials) {
	        delete partials[name].instance;
	      }
	      return template;
	    }

	    template = this.generate(this.parse(this.scan(text, options.delimiters), text, options), text, options);
	    return this.cache[key] = template;
	  };
	})(exports);
	});

	var template = createCommonjsModule(function (module, exports) {

	(function (Hogan) {
	  Hogan.Template = function (codeObj, text, compiler, options) {
	    codeObj = codeObj || {};
	    this.r = codeObj.code || this.r;
	    this.c = compiler;
	    this.options = options || {};
	    this.text = text || '';
	    this.partials = codeObj.partials || {};
	    this.subs = codeObj.subs || {};
	    this.buf = '';
	  };

	  Hogan.Template.prototype = {
	    // render: replaced by generated code.
	    r: function (context, partials, indent) { return ''; },

	    // variable escaping
	    v: hoganEscape,

	    // triple stache
	    t: coerceToString,

	    render: function render(context, partials, indent) {
	      return this.ri([context], partials || {}, indent);
	    },

	    // render internal -- a hook for overrides that catches partials too
	    ri: function (context, partials, indent) {
	      return this.r(context, partials, indent);
	    },

	    // ensurePartial
	    ep: function(symbol, partials) {
	      var partial = this.partials[symbol];

	      // check to see that if we've instantiated this partial before
	      var template = partials[partial.name];
	      if (partial.instance && partial.base == template) {
	        return partial.instance;
	      }

	      if (typeof template == 'string') {
	        if (!this.c) {
	          throw new Error("No compiler available.");
	        }
	        template = this.c.compile(template, this.options);
	      }

	      if (!template) {
	        return null;
	      }

	      // We use this to check whether the partials dictionary has changed
	      this.partials[symbol].base = template;

	      if (partial.subs) {
	        // Make sure we consider parent template now
	        if (!partials.stackText) partials.stackText = {};
	        for (key in partial.subs) {
	          if (!partials.stackText[key]) {
	            partials.stackText[key] = (this.activeSub !== undefined && partials.stackText[this.activeSub]) ? partials.stackText[this.activeSub] : this.text;
	          }
	        }
	        template = createSpecializedPartial(template, partial.subs, partial.partials,
	          this.stackSubs, this.stackPartials, partials.stackText);
	      }
	      this.partials[symbol].instance = template;

	      return template;
	    },

	    // tries to find a partial in the current scope and render it
	    rp: function(symbol, context, partials, indent) {
	      var partial = this.ep(symbol, partials);
	      if (!partial) {
	        return '';
	      }

	      return partial.ri(context, partials, indent);
	    },

	    // render a section
	    rs: function(context, partials, section) {
	      var tail = context[context.length - 1];

	      if (!isArray(tail)) {
	        section(context, partials, this);
	        return;
	      }

	      for (var i = 0; i < tail.length; i++) {
	        context.push(tail[i]);
	        section(context, partials, this);
	        context.pop();
	      }
	    },

	    // maybe start a section
	    s: function(val, ctx, partials, inverted, start, end, tags) {
	      var pass;

	      if (isArray(val) && val.length === 0) {
	        return false;
	      }

	      if (typeof val == 'function') {
	        val = this.ms(val, ctx, partials, inverted, start, end, tags);
	      }

	      pass = !!val;

	      if (!inverted && pass && ctx) {
	        ctx.push((typeof val == 'object') ? val : ctx[ctx.length - 1]);
	      }

	      return pass;
	    },

	    // find values with dotted names
	    d: function(key, ctx, partials, returnFound) {
	      var found,
	          names = key.split('.'),
	          val = this.f(names[0], ctx, partials, returnFound),
	          doModelGet = this.options.modelGet,
	          cx = null;

	      if (key === '.' && isArray(ctx[ctx.length - 2])) {
	        val = ctx[ctx.length - 1];
	      } else {
	        for (var i = 1; i < names.length; i++) {
	          found = findInScope(names[i], val, doModelGet);
	          if (found !== undefined) {
	            cx = val;
	            val = found;
	          } else {
	            val = '';
	          }
	        }
	      }

	      if (returnFound && !val) {
	        return false;
	      }

	      if (!returnFound && typeof val == 'function') {
	        ctx.push(cx);
	        val = this.mv(val, ctx, partials);
	        ctx.pop();
	      }

	      return val;
	    },

	    // find values with normal names
	    f: function(key, ctx, partials, returnFound) {
	      var val = false,
	          v = null,
	          found = false,
	          doModelGet = this.options.modelGet;

	      for (var i = ctx.length - 1; i >= 0; i--) {
	        v = ctx[i];
	        val = findInScope(key, v, doModelGet);
	        if (val !== undefined) {
	          found = true;
	          break;
	        }
	      }

	      if (!found) {
	        return (returnFound) ? false : "";
	      }

	      if (!returnFound && typeof val == 'function') {
	        val = this.mv(val, ctx, partials);
	      }

	      return val;
	    },

	    // higher order templates
	    ls: function(func, cx, partials, text, tags) {
	      var oldTags = this.options.delimiters;

	      this.options.delimiters = tags;
	      this.b(this.ct(coerceToString(func.call(cx, text)), cx, partials));
	      this.options.delimiters = oldTags;

	      return false;
	    },

	    // compile text
	    ct: function(text, cx, partials) {
	      if (this.options.disableLambda) {
	        throw new Error('Lambda features disabled.');
	      }
	      return this.c.compile(text, this.options).render(cx, partials);
	    },

	    // template result buffering
	    b: function(s) { this.buf += s; },

	    fl: function() { var r = this.buf; this.buf = ''; return r; },

	    // method replace section
	    ms: function(func, ctx, partials, inverted, start, end, tags) {
	      var textSource,
	          cx = ctx[ctx.length - 1],
	          result = func.call(cx);

	      if (typeof result == 'function') {
	        if (inverted) {
	          return true;
	        } else {
	          textSource = (this.activeSub && this.subsText && this.subsText[this.activeSub]) ? this.subsText[this.activeSub] : this.text;
	          return this.ls(result, cx, partials, textSource.substring(start, end), tags);
	        }
	      }

	      return result;
	    },

	    // method replace variable
	    mv: function(func, ctx, partials) {
	      var cx = ctx[ctx.length - 1];
	      var result = func.call(cx);

	      if (typeof result == 'function') {
	        return this.ct(coerceToString(result.call(cx)), cx, partials);
	      }

	      return result;
	    },

	    sub: function(name, context, partials, indent) {
	      var f = this.subs[name];
	      if (f) {
	        this.activeSub = name;
	        f(context, partials, this, indent);
	        this.activeSub = false;
	      }
	    }

	  };

	  //Find a key in an object
	  function findInScope(key, scope, doModelGet) {
	    var val;

	    if (scope && typeof scope == 'object') {

	      if (scope[key] !== undefined) {
	        val = scope[key];

	      // try lookup with get for backbone or similar model data
	      } else if (doModelGet && scope.get && typeof scope.get == 'function') {
	        val = scope.get(key);
	      }
	    }

	    return val;
	  }

	  function createSpecializedPartial(instance, subs, partials, stackSubs, stackPartials, stackText) {
	    function PartialTemplate() {}    PartialTemplate.prototype = instance;
	    function Substitutions() {}    Substitutions.prototype = instance.subs;
	    var key;
	    var partial = new PartialTemplate();
	    partial.subs = new Substitutions();
	    partial.subsText = {};  //hehe. substext.
	    partial.buf = '';

	    stackSubs = stackSubs || {};
	    partial.stackSubs = stackSubs;
	    partial.subsText = stackText;
	    for (key in subs) {
	      if (!stackSubs[key]) stackSubs[key] = subs[key];
	    }
	    for (key in stackSubs) {
	      partial.subs[key] = stackSubs[key];
	    }

	    stackPartials = stackPartials || {};
	    partial.stackPartials = stackPartials;
	    for (key in partials) {
	      if (!stackPartials[key]) stackPartials[key] = partials[key];
	    }
	    for (key in stackPartials) {
	      partial.partials[key] = stackPartials[key];
	    }

	    return partial;
	  }

	  var rAmp = /&/g,
	      rLt = /</g,
	      rGt = />/g,
	      rApos = /\'/g,
	      rQuot = /\"/g,
	      hChars = /[&<>\"\']/;

	  function coerceToString(val) {
	    return String((val === null || val === undefined) ? '' : val);
	  }

	  function hoganEscape(str) {
	    str = coerceToString(str);
	    return hChars.test(str) ?
	      str
	        .replace(rAmp, '&amp;')
	        .replace(rLt, '&lt;')
	        .replace(rGt, '&gt;')
	        .replace(rApos, '&#39;')
	        .replace(rQuot, '&quot;') :
	      str;
	  }

	  var isArray = Array.isArray || function(a) {
	    return Object.prototype.toString.call(a) === '[object Array]';
	  };

	})(exports);
	});

	/*
	 *  Copyright 2011 Twitter, Inc.
	 *  Licensed under the Apache License, Version 2.0 (the "License");
	 *  you may not use this file except in compliance with the License.
	 *  You may obtain a copy of the License at
	 *
	 *  http://www.apache.org/licenses/LICENSE-2.0
	 *
	 *  Unless required by applicable law or agreed to in writing, software
	 *  distributed under the License is distributed on an "AS IS" BASIS,
	 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 *  See the License for the specific language governing permissions and
	 *  limitations under the License.
	 */

	// This file is for use with Node.js. See dist/ for browser files.


	compiler.Template = template.Template;
	compiler.template = compiler.Template;
	var hogan = compiler;

	var template$1 = new hogan.Template({code: function (c,p,i) { var t=this;t.b(i=i||"");t.b("<div id=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-");t.b(t.v(t.f("id",c,p,0)));t.b("\" class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("\" style=\"display:none; font-size:");t.b(t.v(t.f("scale",c,p,0)));t.b("em\">");t.b("\n" + i);t.b("	<div class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-clip\">");t.b("\n" + i);t.b("		<div class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-aligner\">");t.b("\n" + i);t.b("			<div class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-aligner-helper\">");t.b("\n" + i);t.b("				<div class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-box\">");t.b("\n" + i);t.b("					<div class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-content\">");t.b("\n" + i);t.b("						<iframe class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-content-iframe\" src=\"\" scrolling=\"no\" aria-hidden=\"true\"></iframe>");t.b("\n" + i);t.b("					</div>");t.b("\n" + i);t.b("					<button class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-close\" title=\"Закрыть\">");t.b("\n" + i);t.b("						<svg class=\"");t.b(t.v(t.f("name",c,p,0)));t.b("-close-icon\" role=\"presentation\" aria-hidden=\"true\">");t.b("\n" + i);t.b("							<title>Закрыть диалог</title>");t.b("\n" + i);t.b("							<use xlink:href=\"#egml-icons-cross\"></use>");t.b("\n" + i);t.b("						</svg>");t.b("\n" + i);t.b("					</button>");t.b("\n" + i);t.b("				</div>");t.b("\n" + i);t.b("			</div>");t.b("\n" + i);t.b("		</div>");t.b("\n" + i);t.b("	</div>");t.b("\n" + i);t.b("</div>");return t.fl(); },partials: {}, subs: {  }});

	___$insertStyle(".modal-close {\n  padding: 0;\n  margin: 0;\n  height: auto;\n  width: auto;\n  background: transparent;\n  border: 0;\n  border-radius: 0;\n}\n\n.modal-close {\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  appearance: none;\n}\n\n.modal {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  z-index: 32767;\n}\n\n.modal-clip {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  overflow: auto;\n  background-color: rgba(0, 0, 0, 0.4);\n  transition: background-color 0.4s linear;\n}\n\n.modal-aligner {\n  display: table;\n  width: 100%;\n  height: 100%;\n}\n\n.modal-aligner-helper {\n  display: table-cell;\n  padding: 3.125em 1.875em;\n  vertical-align: middle;\n}\n\n.modal-box {\n  margin: auto;\n  position: relative;\n  width: 21.875em;\n  height: 12.5em;\n  background: #fff;\n  border-radius: 0.25em;\n  box-shadow: 0 0.125em 1.25em 0 rgba(0, 0, 0, 0.25);\n  transition: width 0.5s ease, height 0.5s ease, opacity 0.2s linear, transform 0.4s ease;\n  overflow: hidden;\n}\n.modal-box.modal-box--default {\n  width: 37.5em;\n}\n.modal-box.modal-box--tight, .modal-box.modal-box--loading {\n  width: 21.875em;\n}\n\n.modal-content-iframe {\n  width: 100%;\n  display: block;\n  overflow: hidden;\n  border: 0;\n  transition: opacity 0.5s linear;\n}\n\n.modal-close {\n  font-size: 1em;\n  position: absolute;\n  top: 0;\n  right: 0;\n  padding: 0.8125em;\n  border: 0;\n  background: transparent;\n  cursor: pointer;\n}\n.modal-close:focus {\n  outline: 0;\n}\n.modal-close-icon {\n  display: block;\n  width: 1.0625em;\n  height: 1.0625em;\n  opacity: 0.2;\n  transition: opacity 0.3s linear;\n  fill: none;\n  stroke: #000;\n  stroke-width: 15.8823529412;\n  stroke-linecap: round;\n}\n\n.modal-close:hover .modal-close-icon, .modal-close:active .modal-close-icon, .modal-close:focus .modal-close-icon {\n  opacity: 0.8;\n}");

	var cross = "<symbol id=\"egml-icons-cross\" viewBox=\"0 0 100 100\"><path d=\"M 20,80 80,20\"/><path d=\"M 20,20 80,80\"/></symbol>";

	var modal = new Modal({
		autoInitialize: false,
	});

	document.addEventListener('DOMContentLoaded', function() {
		insertHtml(
			template$1.render({
				id: modal.id,
				name: modal.name.css(),
				scale: 1,
			})
		);

		insertSvgSymbol(cross);

		var modalElement = document.getElementById(modal.name.css(modal.id.current.toString()));
		modal.initialize(modalElement);
		modal.dom.self.addEventListener(modal.finishShowEventName, function() {
			var log = document.getElementById('log');
			log.textContent += '== открыто ==\n';
		}, false);
		modal.dom.self.addEventListener(modal.finishHideEventName, function() {
			// window.location.reload(true);
			var log = document.getElementById('log');
			log.textContent += '== закрыто ==\n';
		}, false);
	});

	return modal;

}());
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVpbGQuanMiLCJzb3VyY2VzIjpbIi4uL25vZGVfbW9kdWxlcy9AZWdtbC91dGlscy91dGlscy5qcyIsIi4uL25vZGVfbW9kdWxlcy9AZWdtbC91dGlscy9TdHJpbmdDYXNlLmpzIiwiLi4vbm9kZV9tb2R1bGVzL0BlZ21sL3V0aWxzL05hbWUuanMiLCIuLi9ub2RlX21vZHVsZXMvQGVnbWwvdXRpbHMvSWQuanMiLCIuLi9ub2RlX21vZHVsZXMvQGVnbWwvdXRpbHMvV2lkZ2V0LmpzIiwiLi4vbm9kZV9tb2R1bGVzL3NwaW4uanMvc3Bpbi5qcyIsIi4uL2Rpc3QvbW9kYWwuanMiLCIuLi9ub2RlX21vZHVsZXMvaG9nYW4uanMvbGliL2NvbXBpbGVyLmpzIiwiLi4vbm9kZV9tb2R1bGVzL2hvZ2FuLmpzL2xpYi90ZW1wbGF0ZS5qcyIsIi4uL25vZGVfbW9kdWxlcy9ob2dhbi5qcy9saWIvaG9nYW4uanMiLCIuLi9ub2RlX21vZHVsZXMvQGVnbWwvaWNvbnMvZGlzdC9lc20vY3Jvc3MuanMiLCJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojiloggICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCDiloTiloQg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAgICAgICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICAgICAgICAgICAgICAgICAgICAgIOKWgOKWgFxyXG4vLyAjcmVxdWVzdFxyXG5leHBvcnQgZnVuY3Rpb24gcmVxdWVzdChvcHRpb25zKSB7XHJcblx0dmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cdG9wdGlvbnMudHlwZSA9IG9wdGlvbnMudHlwZSB8fCAnZ2V0JztcclxuXHR4aHIub3BlbihvcHRpb25zLnR5cGUsIG9wdGlvbnMudXJsKTtcclxuXHQvLyBZaWkg0L/RgNC+0LLQtdGA0Y/QtdGCINC90LDQu9C40YfQuNC1INGF0LXQtNC10YDQsCBYLVJlcXVlc3RlZC1XaXRoINCyINGE0LjQu9GM0YLRgNC1IGFqYXhPbmx5XHJcblx0Ly8gaHR0cDovL3d3dy55aWlmcmFtZXdvcmsuY29tL2RvYy9hcGkvMS4xL0NIdHRwUmVxdWVzdCNnZXRJc0FqYXhSZXF1ZXN0LWRldGFpbFxyXG5cdHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdYLVJlcXVlc3RlZC1XaXRoJywnWE1MSHR0cFJlcXVlc3QnKTtcclxuXHR4aHIuc2V0UmVxdWVzdEhlYWRlcignQ29udGVudC1UeXBlJywnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkOyBjaGFyc2V0PVVURi04Jyk7XHJcblx0eGhyLm9ubG9hZCA9IG9wdGlvbnMuc3VjY2VzcztcclxuXHR4aHIuc2VuZChvcHRpb25zLnF1ZXJ5KTtcclxufTtcclxuXHJcbi8vICDilojilogg4paI4paI4paIICAgIOKWiOKWiOKWiCDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paIICDilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCDilojilojilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAg4paI4paIICDilojilogg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paI4paIXHJcbi8vICAgICAgIOKWiOKWiCAg4paI4paIICDilojiloggIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAgICDilojilojilojilojilojilohcclxuLy9cclxuLy8gI2ltcG9ydCAjc3ZnXHJcbmV4cG9ydCBmdW5jdGlvbiBpbXBvcnRTdmcodXJsLCBsb2FkRXZlbnROYW1lLCB0YXJnZXQsIGlkQXR0cmlidXRlKSB7XHJcblx0aW1wb3J0U1ZHKHVybCwgbG9hZEV2ZW50TmFtZSwgdGFyZ2V0LCBpZEF0dHJpYnV0ZSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBpbXBvcnRTVkcodXJsLCBsb2FkRXZlbnROYW1lLCB0YXJnZXQsIGlkQXR0cmlidXRlKSB7XHJcblx0bG9hZEV2ZW50TmFtZSA9IGxvYWRFdmVudE5hbWUgfHwgJ3N2Z2xvYWQnO1xyXG5cdHRhcmdldCA9IHRhcmdldCB8fCBkb2N1bWVudC5ib2R5O1xyXG5cdGlmICh0eXBlb2YgRE9NUGFyc2VyICE9PSAndW5kZWZpbmVkJykgLy8gSUU2LVxyXG5cdHJlcXVlc3Qoe1xyXG5cdFx0dXJsOiB1cmwsXHJcblx0XHRzdWNjZXNzOiBmdW5jdGlvbigpIHtcclxuXHRcdFx0Ly8g0JTQu9GPIElFNy05LCDRgi7Qui4g0L7QvdC4INC90LUg0L/QvtC00LTQtdGA0LbQuNCy0LDRjtGCINCy0YHRgtGA0L7QtdC90L3Ri9C5INC/0LDRgNGB0LjQvdCzINGB0LLQvtC50YHRgtCy0LAgcmVzcG9uc2VYTUxcclxuXHRcdFx0Ly8gaHR0cDovL21zZG4ubWljcm9zb2Z0LmNvbS9lbi11cy9saWJyYXJ5L2llL21zNTM1ODc0JTI4dj12cy44NSUyOS5hc3B4XHJcblx0XHRcdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0XHRcdHZhciB4bWwgPSBwYXJzZXIucGFyc2VGcm9tU3RyaW5nKHRoaXMucmVzcG9uc2VUZXh0LCAndGV4dC94bWwnKTtcclxuXHRcdFx0Ly8gdmFyIHhtbCA9IHRoaXMucmVzcG9uc2VYTUw7XHJcblx0XHRcdHZhciBzdmcgPSBkb2N1bWVudC5pbXBvcnROb2RlKHhtbC5kb2N1bWVudEVsZW1lbnQsIHRydWUpO1xyXG5cdFx0XHRpZiAoaWRBdHRyaWJ1dGUpIHtcclxuXHRcdFx0XHRzdmcuc2V0QXR0cmlidXRlKCdpZCcsIGlkQXR0cmlidXRlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0YXJnZXQuYXBwZW5kQ2hpbGQoc3ZnKTtcclxuXHRcdFx0dGFyZ2V0LmRpc3BhdGNoRXZlbnQobmV3IEN1c3RvbUV2ZW50KGxvYWRFdmVudE5hbWUsIHsgYnViYmxlczogdHJ1ZSB9KSk7XHJcblx0XHR9LFxyXG5cdH0pO1xyXG59O1xyXG5cclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAgIOKWiOKWiCDilojiloggICAgIOKWiOKWiCAgICDilojiloggICAgICDilojilojilojiloggICDilojilogg4paI4paIICAg4paI4paIXHJcbi8vICDilojilojilojilojiloggICAgIOKWiOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilojilojilojiloggICDilojilogg4paI4paIICDilojilogg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICAgICAg4paI4paIIOKWiOKWiCAgICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojilojilogg4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNleHRlbmRcclxuZXhwb3J0IGZ1bmN0aW9uIGV4dGVuZChkZXN0LCBzb3VyY2UpIHtcclxuXHRmb3IgKHZhciBwcm9wIGluIHNvdXJjZSkge1xyXG5cdFx0ZGVzdFtwcm9wXSA9IHNvdXJjZVtwcm9wXTtcclxuXHR9XHJcblx0cmV0dXJuIGRlc3Q7XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgICDilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAgICDilojiloggICAgICDilojilohcclxuLy8gICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNjaGlsZCAjY2xhc3NcclxuZXhwb3J0IGZ1bmN0aW9uIGNoaWxkQnlDbGFzcyhwYXJlbnQsIGNsYXNzTmFtZSkge1xyXG5cdHZhciBjaGlsZHJlbiA9IHBhcmVudC5jaGlsZHJlbjtcclxuXHRmb3IgKHZhciBpID0gMDsgaSA8IGNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRpZiAoY2hpbGRyZW5baV0uY2xhc3NMaXN0LmNvbnRhaW5zKGNsYXNzTmFtZSkpIHtcclxuXHRcdFx0cmV0dXJuIGNoaWxkcmVuW2ldO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNwYWdlICNzY3JvbGxcclxuLy8g0JrRgNC+0YHRgS3QsdGA0LDRg9C30LXRgNC90L7QtSDQvtC/0YDQtdC00LXQu9C10L3QuNC1INCy0LXQu9C40YfQuNC90Ysg0YHQutGA0L7Qu9C70LBcclxuZXhwb3J0IGZ1bmN0aW9uIHBhZ2VTY3JvbGwoKSB7XHJcblx0aWYgKHdpbmRvdy5wYWdlWE9mZnNldCAhPSB1bmRlZmluZWQpIHtcclxuXHRcdHJldHVybiB7XHJcblx0XHRcdGxlZnQ6IHBhZ2VYT2Zmc2V0LFxyXG5cdFx0XHR0b3A6IHBhZ2VZT2Zmc2V0XHJcblx0XHR9O1xyXG5cdH0gZWxzZSB7XHJcblx0XHR2YXIgaHRtbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuXHRcdHZhciBib2R5ID0gZG9jdW1lbnQuYm9keTtcclxuXHJcblx0XHR2YXIgdG9wID0gaHRtbC5zY3JvbGxUb3AgfHwgYm9keSAmJiBib2R5LnNjcm9sbFRvcCB8fCAwO1xyXG5cdFx0dG9wIC09IGh0bWwuY2xpZW50VG9wO1xyXG5cclxuXHRcdHZhciBsZWZ0ID0gaHRtbC5zY3JvbGxMZWZ0IHx8IGJvZHkgJiYgYm9keS5zY3JvbGxMZWZ0IHx8IDA7XHJcblx0XHRsZWZ0IC09IGh0bWwuY2xpZW50TGVmdDtcclxuXHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRsZWZ0OiBsZWZ0LFxyXG5cdFx0XHR0b3A6IHRvcFxyXG5cdFx0fTtcclxuXHR9XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojiloggICAgICDilojilogg4paI4paIICAgICAgICAgICDilojiloggICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojiloggICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilojilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICDilojilogg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAgICDilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAgICDilojiloggICAgICDilojilohcclxuLy8gICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNjbG9zZXN0ICNwYXJlbnQgI2NsYXNzXHJcbmV4cG9ydCBmdW5jdGlvbiBjbG9zZXN0UGFyZW50QnlDbGFzcyhlbGVtZW50LCBjbGFzc05hbWUpIHtcclxuXHR2YXIgcGFyZW50ID0gZWxlbWVudC5wYXJlbnROb2RlO1xyXG5cdHdoaWxlICghcGFyZW50LmNsYXNzTGlzdC5jb250YWlucyhjbGFzc05hbWUpKSB7XHJcblx0XHRwYXJlbnQgPSBwYXJlbnQucGFyZW50Tm9kZTtcclxuXHR9XHJcblx0cmV0dXJuIHBhcmVudDtcclxufVxyXG5cclxuLy8gIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCDilojilohcclxuLy8gIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paIICAgICAgIOKWiOKWiCDilojilojilojilojiloggICDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojilohcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vXHJcbi8vICNpcyAjb2JqZWN0XHJcbi8vINCh0L/QtdGA0YLQviDQuNC3IGludGVyYWN0LmpzICjRgdC/0LXRgNGC0L4g0L3QtdC80L3QvtCz0L4sINC90L4g0LLRgdC1INGA0LDQstC90L46IGh0dHA6Ly9pbnRlcmFjdGpzLmlvLylcclxuZXhwb3J0IGZ1bmN0aW9uIGlzT2JqZWN0KHRoaW5nKSB7XHJcblx0cmV0dXJuICEhdGhpbmcgJiYgKHR5cGVvZiB0aGluZyA9PT0gJ29iamVjdCcpO1xyXG59XHJcblxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paIICAgIOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojilojilojilogg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy9cclxuLy8gI3JlbVxyXG4vLyDQn9C+0LTRgdGH0ZHRgiDQstC10LvQuNGH0LjQvdGLINCyIHJlbS4g0J3QsCDQstGF0L7QtNC1INC/0LjQutGB0LXQu9C4INCx0LXQtyBgcHhgLCDQvdCwINCy0YvRhdC+0LTQtSDQstC10LvQuNGH0LjQvdCwINCyIHJlbSDRgSBgcmVtYFxyXG52YXIgcm9vdEZvbnRTaXplO1xyXG5leHBvcnQgZnVuY3Rpb24gcmVtKHB4Tm9Vbml0cywgcmVjYWxjKSB7XHJcblx0aWYgKHR5cGVvZiByb290Rm9udFNpemUgIT0gJ251bWJlcicgfHwgcmVjYWxjKSB7XHJcblx0XHQvLyAjQ0JGSVg6IEVkZ2UgKDQyLjE3MTM0LjEuMCwgRWRnZUhUTUwgMTcuMTcxMzQpINC4IElFINC+0L/RgNC10LTQtdC70Y/RjtGCINC30L3QsNGH0LXQvdC40LUg0LrQsNC6LCDQvdCw0L/RgNC40LzQtdGALCA5LjkzINCyINGA0LXQt9GD0LvRjNGC0LDRgtC1ID4+INC+0L/QtdGA0LDRhtC40Y8g0LTQsNC10YIgOSwg0L/QvtGN0YLQvtC80YMg0YLRg9GCINC00L7Qv9C+0LvQvdC40YLQtdC70YzQvdC+INC90LDQtNC+INC+0LrRgNGD0LPQu9GP0YLRjC4gVVBEOiDQl9Cw0YfQtdC8INC/0LXRgNC10LLQvtC00LjRgtGMINCyIGludGVnZXIg0YEg0L/QvtC80L7RidGM0Y4gPj4sINC10YHQu9C4INC80Ysg0YPQttC1INC/0YDQuNC80LXQvdGP0Lwg0Log0YHRgtGA0L7QutC1IE1hdGgucm91bmQsINGC0LXQvCDRgdCw0LzRi9C8INC+0L3QsCDQsNCy0YLQvtC80LDRgtC40YfQtdGB0LrQuCDQutCw0YHRgtGD0LXRgtGB0Y8g0LIgaW50ZWdlci5cclxuXHRcdC8vIHJvb3RGb250U2l6ZSA9IE1hdGgucm91bmQod2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KS5nZXRQcm9wZXJ0eVZhbHVlKCdmb250LXNpemUnKS5zbGljZSgwLC0yKSkgPj4gMDtcclxuXHRcdHJvb3RGb250U2l6ZSA9IE1hdGgucm91bmQod2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KS5nZXRQcm9wZXJ0eVZhbHVlKCdmb250LXNpemUnKS5zbGljZSgwLC0yKSk7XHJcblx0fVxyXG5cdHJldHVybiBweE5vVW5pdHMvcm9vdEZvbnRTaXplICsgJ3JlbSc7XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilojilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojiloggICDilojilogg4paI4paIICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAgICAg4paI4paIICDilojilogg4paI4paIICAgIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIICDilojilogg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojiloggICDilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIICDilojilogg4paI4paIXHJcbi8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilogg4paI4paIICAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojilogg4paI4paIICAg4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHJcbi8vICNjb250ZW50ICNib3ggI2hlaWdodFxyXG4vLyDQn9C+0LTRgdGH0ZHRgiDQstGL0YHQvtGC0YsgY29udGVudC1ib3gg0YEg0LTRgNC+0LHRjNGOICjQsiDQvtGC0LvQuNGH0LjQtSDQvtGCICQoLi4uKS5oZWlnaHQoKSlcclxuZXhwb3J0IGZ1bmN0aW9uIGNvbnRlbnRCb3hIZWlnaHQoZWxlbWVudCkge1xyXG5cdHZhciBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpO1xyXG5cdHJldHVybiBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodFxyXG5cdFx0XHQtIHN0eWxlLmdldFByb3BlcnR5VmFsdWUoJ3BhZGRpbmctdG9wJykuc2xpY2UoMCwtMilcclxuXHRcdFx0LSBzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdwYWRkaW5nLWJvdHRvbScpLnNsaWNlKDAsLTIpXHJcblx0XHRcdC0gc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnYm9yZGVyLXRvcC13aWR0aCcpLnNsaWNlKDAsLTIpXHJcblx0XHRcdC0gc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnYm9yZGVyLWJvdHRvbS13aWR0aCcpLnNsaWNlKDAsLTIpO1xyXG59XHJcblxyXG4vLyAg4paI4paI4paIICAgIOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiCDilojilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojiloggIOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICDilojilogg4paI4paI4paI4paIICAg4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilojilogg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiFxyXG4vLyAg4paI4paIICDilojiloggIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIICAg4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuLy8gXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vL1xyXG4vLyAjbWFyZ2luICNib3ggI2hlaWdodFxyXG5leHBvcnQgZnVuY3Rpb24gbWFyZ2luQm94SGVpZ2h0KGVsZW1lbnQsIHNvdXJjZVdpbmRvdykge1xyXG5cdHNvdXJjZVdpbmRvdyA9IHNvdXJjZVdpbmRvdyB8fCB3aW5kb3c7XHJcblx0dmFyIHN0eWxlO1xyXG5cdGlmIChlbGVtZW50ICYmIChzdHlsZSA9IHNvdXJjZVdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpKSkge1xyXG5cdFx0cmV0dXJuIGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0XHJcblx0XHRcdFx0KyAoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLXRvcCcpLnNsaWNlKDAsLTIpID4+IDApXHJcblx0XHRcdFx0KyAoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLWJvdHRvbScpLnNsaWNlKDAsLTIpID4+IDApO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm4gMDtcclxuXHR9XHJcbn1cclxuXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggICDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICDilojiloggICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICDiloggIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiCDilojilojilojilojiloggICAgICDilojiloggICAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paI4paIIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICAg4paI4paI4paIIOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilohcclxuLy9cclxuLy8gI2NyZWF0ZSAjd2lkZ2V0c1xyXG5cclxuLyoqXHJcbiAqINCh0L7Qt9C00LDQtdGCINC+0LHRitC10LrRgtGLINCy0LjQtNC20LXRgtCwINCyINC+0L/RgNC10LTQtdC70LXQvdC90L7QvCDQutC+0L3RgtC10LrRgdGC0LUuXHJcbiAqIFxyXG4gKiBAcGFyYW0ge2Z1bmN0aW9ufGNsYXNzfSBjb25zdHJ1Y3RvciDQmtC+0L3RgdGC0YDRg9C60YLQvtGAINCy0LjQtNC20LXRgtCwXHJcbiAqIEBwYXJhbSB7RE9NRWxlbWVudH0gY29udGV4dCDQkiDQutC+0L3RgtC10LrRgdGC0LUgKNCy0L3Rg9GC0YDQuCkg0LrQsNC60L7Qs9C+INGN0LvQtdC80LXQvdGC0LAg0L/RgNC+0LjQt9Cy0L7QtNC40YLRjCDQv9C+0LjRgdC6IFxyXG4gKiDRjdC70LXQvNC10L3RgtC+0LIg0LTQu9GPINC40L3QuNGG0LjQsNC70LjQt9Cw0YbQuNC4INCy0LjQtNC20LXRgtCwLiDQldGB0LvQuCDQvdC1INGD0LrQsNC30LDQvSwg0LjRgdC/0L7Qu9GM0LfRg9C10YLRgdGPIGRvY3VtZW50LlxyXG4gKiBAcGFyYW0ge29iamVjdH0gb3B0aW9ucyDQmtC+0L3RhNC40LPRg9GA0LDRhtC40L7QvdC90YvQuSDQvtCx0YrQtdC60YIsINC/0LXRgNC10LTQsNCy0LDQtdC80YvQuSDRgdC+0LfQtNCw0LLQsNC10LzQvtC80YMg0LLQuNC00LbQtdGC0YNcclxuICogQHBhcmFtIHtzdHJpbmd9IHNlbGVjdG9yINCa0LDQutC+0Lkg0YHQtdC70LXQutGC0L7RgCDQuNGB0L/QvtC70YzQt9C+0LLQsNGC0Ywg0LTQu9GPINC/0L7QuNGB0LrQsCDRjdC70LXQvNC10L3RgtC+0LIuIFxyXG4gKiDQldGB0LvQuCDQvdC1INGD0LrQsNC30LDRgtGMLCDQuNGB0L/QvtC70YzQt9GD0LXRgtGB0Y8gbmFtZS5jc3Mg0LjQtyDQv9GA0L7RgtC+0YLQuNC/0LAg0LLQuNC00LbQtdGC0LAuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlV2lkZ2V0cyhjb25zdHJ1Y3RvciwgY29udGV4dCwgb3B0aW9ucywgc2VsZWN0b3IpXHJcbntcclxuXHRpZiAodHlwZW9mIGNvbnN0cnVjdG9yID09ICd1bmRlZmluZWQnKSB7XHJcblx0XHR0aHJvdyBuZXcgRXJyb3IoJ9Cd0YPQttC90L4g0L7QsdGP0LfQsNGC0LXQu9GM0L3QviDRg9C60LDQt9Cw0YLRjCDQutC70LDRgdGBINCy0LjQtNC20LXRgtCwJyk7XHJcblx0fVxyXG5cdGNvbnRleHQgPSBjb250ZXh0IHx8IGRvY3VtZW50O1xyXG5cdHNlbGVjdG9yID0gc2VsZWN0b3IgfHwgJy4nICsgY29uc3RydWN0b3IucHJvdG90eXBlLm5hbWUuY3NzO1xyXG5cclxuXHR2YXIgZWxlbWVudHMgPSBjb250ZXh0LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpO1xyXG5cdGlmIChlbGVtZW50cy5sZW5ndGgpIHtcclxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0Ly8g0J/QvtGH0LXQvNGDLdGC0L4gdHJ5Li4uY2F0Y2gg0LLRi9C00LDQtdGCINC60YPQtNCwINC80LXQvdGM0YjQtSDQv9C+0LvQtdC30L3QvtC5INC40L3RhNC+0YDQvNCw0YbQuNC4LCDRh9C10Lwg0L/RgNC+0YHRgtCw0Y8g0L7QsdGA0LDQsdC+0YLQutCwINC+0YjQuNCx0L7QuiDQsiDQutC+0L3RgdC+0LvQuCDQsdGA0LDRg9C30LXRgNCwXHJcblx0XHRcdC8vIHRyeSB7XHJcblx0XHRcdFx0bmV3IGNvbnN0cnVjdG9yKGVsZW1lbnRzW2ldLCBvcHRpb25zKTtcclxuXHRcdFx0Ly8gfSBjYXRjaChlcnJvcikge1xyXG5cdFx0XHQvLyBcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xyXG5cdFx0XHQvLyB9XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdHRocm93IG5ldyBFcnJvcign0K3Qu9C10LzQtdC90YLRiyDQsiBET00g0LIg0LLRi9Cx0YDQsNC90L3QvtC8INC60L7QvdGC0LXQutGB0YLQtSDQv9C+INGC0LDQutC+0LzRgyDRgdC10LvQtdC60YLQvtGA0YMg0L3QtSDQvdCw0LnQtNC10L3RiycpO1xyXG5cdH1cclxufVxyXG5cclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojilogg4paI4paI4paIICAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIXHJcbi8vICDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICDilojilojilohcclxuLy8gICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCDilojiloggICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAg4paI4paI4paI4paIICDilojilojilojilojilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICDilojiloggICAgICDilojilogg4paI4paIXHJcbi8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHJcbi8vICNzdHJpbmcgI2Nhc2VcclxuZXhwb3J0IGZ1bmN0aW9uIHN0cmluZ0Nhc2UoYml0cywgdHlwZSkge1xyXG5cdGlmICghYml0cyB8fCAhYml0cy5sZW5ndGgpIHtcclxuXHRcdHJldHVybiAnJztcclxuXHR9XHJcblx0aWYgKHR5cGVvZiBiaXRzID09ICdzdHJpbmcnKSB7XHJcblx0XHRiaXRzID0gWyBiaXRzIF07XHJcblx0fVxyXG5cdHZhciBzdHJpbmcgPSAgJyc7XHJcblx0c3dpdGNoICh0eXBlKSB7XHJcblx0XHRjYXNlICdjYW1lbCc6XHJcblx0XHRcdGJpdHMuZm9yRWFjaChmdW5jdGlvbihiaXQsIGkpIHtcclxuXHRcdFx0XHRpZiAoaSA9PSAwKSB7XHJcblx0XHRcdFx0XHRzdHJpbmcgKz0gZmlyc3RMb3dlckNhc2UoYml0KTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0c3RyaW5nICs9IGZpcnN0VXBwZXJDYXNlKGJpdCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0Y2FzZSAnY2FwaXRhbENhbWVsJzpcclxuXHRcdFx0Yml0cy5mb3JFYWNoKGZ1bmN0aW9uKGJpdCkge1xyXG5cdFx0XHRcdHN0cmluZyArPSBmaXJzdFVwcGVyQ2FzZShiaXQpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0YnJlYWs7XHJcblxyXG5cdFx0Y2FzZSAnZmxhdCc6XHJcblx0XHRcdHN0cmluZyA9IGJpdHMuam9pbignJyk7XHJcblx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdGNhc2UgJ3NuYWtlJzpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCdfJyk7XHJcblx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdGNhc2UgJ2tlYmFiJzpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCctJyk7XHJcblx0XHRcdGJyZWFrO1xyXG5cclxuXHRcdGNhc2UgJ2RvdCc6XHJcblx0XHRcdHN0cmluZyA9IGJpdHMuam9pbignLicpO1xyXG5cdFx0XHRicmVhaztcclxuXHRcclxuXHRcdGNhc2UgJ2FzSXMnOlxyXG5cdFx0ZGVmYXVsdDpcclxuXHRcdFx0c3RyaW5nID0gYml0cy5qb2luKCcnKTtcclxuXHRcdFx0YnJlYWs7XHJcblx0fVxyXG5cdHJldHVybiBzdHJpbmc7XHJcbn1cclxuXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICDilojiloggICAgICDilojiloggICAg4paI4paIXHJcbi8vICDilojiloggICAgICDilojilogg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggIOKWiOKWiFxyXG4vLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilohcclxuLy8gXHJcbi8vICDilojiloggICAgICAg4paI4paI4paI4paI4paI4paIICDilojiloggICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggIOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojilogg4paI4paI4paIIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paIIOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vLyBcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojilohcclxuLy8gICDilojilojilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy9cclxuLy8gI2ZpcnN0ICN1cHBlciAjbG93ZXIgI2Nhc2VcclxuZXhwb3J0IGZ1bmN0aW9uIGZpcnN0VXBwZXJDYXNlKHN0cmluZykge1xyXG5cdHJldHVybiBzdHJpbmdbMF0udG9VcHBlckNhc2UoKSArIHN0cmluZy5zbGljZSgxKTtcclxufTtcclxuZXhwb3J0IGZ1bmN0aW9uIGZpcnN0TG93ZXJDYXNlKHN0cmluZykge1xyXG5cdHJldHVybiBzdHJpbmdbMF0udG9Mb3dlckNhc2UoKSArIHN0cmluZy5zbGljZSgxKTtcclxufTtcclxuXHJcbi8vICNUT0RPOiDQndCw0L/QuNGB0LDRgtGMINGE0YPQvdC60YbQuNC+0L3QsNC7INC70L7Qs9C40L3Qs9CwINC/0LXRgNC10LzQtdC90L3QvtC5INC40LvQuCDQvtCx0YrQtdC60YLQsCDQsiBIVE1MXHJcbi8vIC8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojilojilohcclxuLy8gLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIXHJcbi8vIC8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paIIOKWiOKWiFxyXG4vLyAvLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICDilojiloggIOKWiOKWiFxyXG4vLyAvLyAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojiloggICAgICDilojilohcclxuLy8gLy8gXHJcbi8vIC8vICDilojiloggICAgICAg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIXHJcbi8vIC8vICDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAvLyAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilojilohcclxuLy8gLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAgIOKWiOKWiFxyXG4vLyAvLyAg4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojiloggICDilojilojilojilojilojilohcclxuLy8gLy9cclxuLy8gLy8gI2RvbSAjbG9nXHJcbi8vIGV4cG9ydCBmdW5jdGlvbiBkb21Mb2cob2JqZWN0LCB0YXJnZXQpIHtcclxuLy8gXHRpZiAodHlwZW9mIHRhcmdldCA9PSAndW5kZWZpbmVkJykge1xyXG4vLyBcdFx0dGFyZ2V0ID0gZG9jdW1lbnQ7XHJcbi8vIFx0fVxyXG4vLyBcdHZhciBvdXRwdXQgPSAnJztcclxuLy8gXHRpZiAodHlwZW9mIG9iamVjdCA9PSAnb2JqZWN0Jykge1xyXG4vLyBcdFx0Zm9yICh2YXIga2V5IGluIG9iamVjdCkge1xyXG4vLyBcdFx0XHRpZiAob2JqZWN0Lmhhc093blByb3BlcnR5KGtleSkpIHtcclxuLy8gXHRcdFx0XHR2YXIgZWxlbWVudCA9IG9iamVjdFtrZXldO1xyXG4vLyBcdFx0XHRcdG91dHB1dCArPSAnJztcclxuLy8gXHRcdFx0fVxyXG4vLyBcdFx0fVxyXG4vLyBcdH1cclxuLy8gXHQnPGRpdiBzdHlsZT1cImZvbnQtZmFtaWx5Om1vbm9zcGFjZTsgZm9udC1zaXplOjFyZW07IHdoaXRlLXNwYWNlOm5vd3JhcDtcIj4nICsgXHJcbi8vIFx0J3dpZHRoICZuYnNwOz0gJyArIHJlY3Qud2lkdGggKyAnPGJyPicgK1xyXG4vLyBcdCdoZWlnaHQgPSAnICsgcmVjdC5oZWlnaHQgKyAnPGJyPicgK1xyXG4vLyBcdCd4ICZuYnNwOyZuYnNwOyZuYnNwOyZuYnNwOyZuYnNwOz0gJyArIHJlY3QueCArICc8YnI+JyArXHJcbi8vIFx0J3kgJm5ic3A7Jm5ic3A7Jm5ic3A7Jm5ic3A7Jm5ic3A7PSAnICsgcmVjdC55ICsgJzxicj4nICtcclxuLy8gXHQnbGVmdCAmbmJzcDsmbmJzcDs9ICcgKyByZWN0LmxlZnQgKyAnPGJyPicgK1xyXG4vLyBcdCd0b3AgJm5ic3A7Jm5ic3A7Jm5ic3A7PSAnICsgcmVjdC50b3AgKyAnPGJyPicgK1xyXG4vLyBcdCdyaWdodCAmbmJzcDs9ICcgKyByZWN0LnJpZ2h0ICsgJzxicj4nICtcclxuLy8gXHQnYm90dG9tID0gJyArIHJlY3QuYm90dG9tICsgJzxicj4nICtcclxuLy8gXHQnPC9kaXY+JztcclxuLy8gfVxyXG5cclxuLy9cdOKWiOKWiCDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICAgICDilojilohcclxuLy9cdOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vXHTilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICAgICDilojiloggICAgIOKWiOKWiCAg4paI4paIICDilojiloggICAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAgICDilojilojilojiloggICDilojiloggICAgICDilojilojilojilojilohcclxuLy9cdCAgICAg4paI4paIICAgIOKWiOKWiCAgICAgICDilojiloggICAg4paI4paIICAgICAg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojiloggICAg4paI4paIICAgICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vLyAjaW5zZXJ0ICNzdHlsZVxyXG5leHBvcnQgZnVuY3Rpb24gaW5zZXJ0U3R5bGUoKSB7XHJcblx0dmFyIG9wdGlvbnMgPSB7XHJcblx0XHR1cmw6IG51bGwsXHJcblx0XHRjb250ZW50OiBudWxsLFxyXG5cdFx0aW5saW5lOiBudWxsLFxyXG5cdFx0dGFyZ2V0RG9jdW1lbnQ6IG51bGwsXHJcblx0XHRvbkxvYWRDYWxsYmFjazogbnVsbCxcclxuXHR9O1xyXG5cdFxyXG5cdHZhciBtb3VudEVsZW1lbnQgPSBudWxsO1xyXG5cdFxyXG5cdG9wdGlvbnMuaW5saW5lID0gZmFsc2U7XHJcblx0b3B0aW9ucy50YXJnZXREb2N1bWVudCA9IGRvY3VtZW50O1xyXG5cdFxyXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvcHRpb25zLCAnbW91bnRFbGVtZW50Jywge1xyXG5cdFx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG1vdW50RWxlbWVudCA9PSBudWxsKSB7XHJcblx0XHRcdFx0cmV0dXJuIG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuaGVhZDtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gbW91bnRFbGVtZW50O1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFx0c2V0OiBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRtb3VudEVsZW1lbnQgPSB2YWx1ZTtcclxuXHRcdH0sXHJcblx0XHRjb25maWd1cmFibGU6IHRydWUsXHJcblx0XHRlbnVtZXJhYmxlOiB0cnVlLFxyXG5cdH0pO1xyXG5cdFxyXG5cdGlmICh0eXBlb2YgYXJndW1lbnRzWzBdID09ICdzdHJpbmcnIHx8IGFyZ3VtZW50c1swXSBpbnN0YW5jZW9mIFN0cmluZykge1xyXG5cdFx0b3B0aW9ucy51cmwgPSBhcmd1bWVudHNbMF07XHJcblx0XHRvcHRpb25zLm9uTG9hZENhbGxiYWNrID0gYXJndW1lbnRzWzFdO1xyXG5cdH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblx0XHJcblx0aWYgKG9wdGlvbnMudXJsKSB7XHJcblx0XHRpZiAob3B0aW9ucy5pbmxpbmUgPT0gZmFsc2UpIHtcclxuXHRcdFx0dmFyIHRhZyA9IG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGluaycpO1xyXG5cdFx0XHR0YWcucmVsID0gJ3N0eWxlc2hlZXQnO1xyXG5cdFx0XHRpZiAodHlwZW9mIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2sgPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHRcdHRhZy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgb3B0aW9ucy5vbkxvYWRDYWxsYmFjayk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGFnLmhyZWYgPSBvcHRpb25zLnVybDtcclxuXHRcdFx0b3B0aW9ucy50YXJnZXREb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcblx0XHRcdHhoci5vcGVuKCdnZXQnLCBvcHRpb25zLnVybCk7XHJcblx0XHRcdHhoci5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHR2YXIgdGFnID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpO1xyXG5cdFx0XHRcdHRhZy50ZXh0Q29udGVudCA9IHRoaXMucmVzcG9uc2VUZXh0O1xyXG5cdFx0XHRcdG9wdGlvbnMubW91bnRFbGVtZW50LmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHRcdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0XHRcdG9wdGlvbnMub25Mb2FkQ2FsbGJhY2soKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblx0XHRcdHhoci5zZW5kKCk7XHJcblx0XHR9XHJcblx0fSBlbHNlIGlmIChvcHRpb25zLmNvbnRlbnQpIHtcclxuXHRcdHZhciB0YWcgPSBvcHRpb25zLnRhcmdldERvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJyk7XHJcblx0XHR0YWcudGV4dENvbnRlbnQgPSBvcHRpb25zLmNvbnRlbnQ7XHJcblx0XHRvcHRpb25zLm1vdW50RWxlbWVudC5hcHBlbmRDaGlsZCh0YWcpO1xyXG5cdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0b3B0aW9ucy5vbkxvYWRDYWxsYmFjaygpO1xyXG5cdFx0fVxyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG59XHJcblxyXG4vL1x04paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICAg4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgICDilojilojilojilojilojiloggIOKWiOKWiCDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vL1x0ICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilogg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vIFxyXG4vL1x0I2luc2VydCAjc2NyaXB0XHJcbmV4cG9ydCBmdW5jdGlvbiBpbnNlcnRTY3JpcHQoKSB7XHJcblx0dmFyIG9wdGlvbnMgPSB7XHJcblx0XHR1cmw6IG51bGwsXHJcblx0XHRjb250ZW50OiBudWxsLFxyXG5cdFx0aW5saW5lOiBudWxsLFxyXG5cdFx0dGFyZ2V0RG9jdW1lbnQ6IG51bGwsXHJcblx0XHRvbkxvYWRDYWxsYmFjazogbnVsbCxcclxuXHR9O1xyXG5cclxuXHR2YXIgbW91bnRFbGVtZW50ID0gbnVsbDtcclxuXHJcblx0b3B0aW9ucy5pbmxpbmUgPSBmYWxzZTtcclxuXHRvcHRpb25zLnRhcmdldERvY3VtZW50ID0gZG9jdW1lbnQ7XHJcblxyXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvcHRpb25zLCAnbW91bnRFbGVtZW50Jywge1xyXG5cdFx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG1vdW50RWxlbWVudCA9PSBudWxsKSB7XHJcblx0XHRcdFx0cmV0dXJuIG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuaGVhZDtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gbW91bnRFbGVtZW50O1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFx0c2V0OiBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRtb3VudEVsZW1lbnQgPSB2YWx1ZTtcclxuXHRcdH0sXHJcblx0XHRjb25maWd1cmFibGU6IHRydWUsXHJcblx0XHRlbnVtZXJhYmxlOiB0cnVlLFxyXG5cdH0pO1xyXG5cclxuXHRpZiAodHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnc3RyaW5nJyB8fCBhcmd1bWVudHNbMF0gaW5zdGFuY2VvZiBTdHJpbmcpIHtcclxuXHRcdG9wdGlvbnMudXJsID0gYXJndW1lbnRzWzBdO1xyXG5cdFx0b3B0aW9ucy5vbkxvYWRDYWxsYmFjayA9IGFyZ3VtZW50c1sxXTtcclxuXHR9IGVsc2UgaWYgKCEhYXJndW1lbnRzWzBdICYmIHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ29iamVjdCcpIHtcclxuXHRcdGV4dGVuZChvcHRpb25zLCBhcmd1bWVudHNbMF0pO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG5cclxuXHR2YXIgdGFnID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuXHJcblx0aWYgKG9wdGlvbnMudXJsKSB7XHJcblx0XHRpZiAob3B0aW9ucy5pbmxpbmUgPT0gZmFsc2UpIHtcclxuXHRcdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0XHR0YWcuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2spO1xyXG5cdFx0XHR9XHJcblx0XHRcdHRhZy5zcmMgPSBvcHRpb25zLnVybDtcclxuXHRcdFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQodGFnKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHRcdFx0eGhyLm9wZW4oJ2dldCcsIG9wdGlvbnMudXJsKTtcclxuXHRcdFx0eGhyLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHRhZy50ZXh0Q29udGVudCA9IHRoaXMucmVzcG9uc2VUZXh0O1xyXG5cdFx0XHRcdG9wdGlvbnMubW91bnRFbGVtZW50LmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHRcdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0XHRcdG9wdGlvbnMub25Mb2FkQ2FsbGJhY2soKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblx0XHRcdHhoci5zZW5kKCk7XHJcblx0XHR9XHJcblx0fSBlbHNlIGlmIChvcHRpb25zLmNvbnRlbnQpIHtcclxuXHRcdHRhZy50ZXh0Q29udGVudCA9IG9wdGlvbnMuY29udGVudDtcclxuXHRcdG9wdGlvbnMubW91bnRFbGVtZW50LmFwcGVuZENoaWxkKHRhZyk7XHJcblx0XHRpZiAodHlwZW9mIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2sgPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHRvcHRpb25zLm9uTG9hZENhbGxiYWNrKCk7XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcbn1cclxuXHJcbi8vXHTilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojilogg4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCDilojilogg4paI4paIICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCDilojiloggICDilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vL1x04paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojiloggICAg4paI4paI4paIIOKWiOKWiFxyXG4vL1x04paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojilojilojiloggIOKWiOKWiOKWiOKWiCDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiCDilojilogg4paI4paIXHJcbi8vXHTilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAg4paI4paIICDilojilogg4paI4paIXHJcbi8vXHTilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vXHQjaW5zZXJ0XHJcbmV4cG9ydCBmdW5jdGlvbiBpbnNlcnRIdG1sKCkge1xyXG5cdHZhciBvcHRpb25zID0ge1xyXG5cdFx0aHRtbDogbnVsbCxcclxuXHRcdGxlZ2FjeTogZmFsc2UsXHJcblx0XHR1cmw6IG51bGwsXHJcblx0XHR0YXJnZXREb2N1bWVudDogZG9jdW1lbnQsXHJcblx0XHRvbkxvYWRDYWxsYmFjazogbnVsbCxcclxuXHR9O1xyXG5cdFxyXG5cdHZhciBtb3VudEVsZW1lbnQgPSBudWxsO1xyXG5cdFxyXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvcHRpb25zLCAnbW91bnRFbGVtZW50Jywge1xyXG5cdFx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG1vdW50RWxlbWVudCA9PSBudWxsKSB7XHJcblx0XHRcdFx0cmV0dXJuIG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuYm9keTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gbW91bnRFbGVtZW50O1xyXG5cdFx0XHR9XHJcblx0XHR9LFxyXG5cdFx0c2V0OiBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRtb3VudEVsZW1lbnQgPSB2YWx1ZTtcclxuXHRcdH0sXHJcblx0XHRjb25maWd1cmFibGU6IHRydWUsXHJcblx0XHRlbnVtZXJhYmxlOiB0cnVlLFxyXG5cdH0pO1xyXG5cdFxyXG5cdGlmICh0eXBlb2YgYXJndW1lbnRzWzBdID09ICdzdHJpbmcnIHx8IGFyZ3VtZW50c1swXSBpbnN0YW5jZW9mIFN0cmluZykge1xyXG5cdFx0b3B0aW9ucy5odG1sID0gYXJndW1lbnRzWzBdO1xyXG5cdH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblxyXG5cdGlmIChvcHRpb25zLnVybCkge1xyXG5cdFx0dmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cdFx0eGhyLm9wZW4oJ2dldCcsIG9wdGlvbnMudXJsKTtcclxuXHRcdHhoci5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKG9wdGlvbnMubGVnYWN5KSB7XHJcblx0XHRcdFx0Ly8gI0NCTk9URTog0KHRgtCw0YDRi9C1INCx0YDQsNGD0LfQtdGA0YssINGC0LDQutC40LUg0LrQsNC6IFdlYlZpZXcgMzMsINC90LUg0L/QvtC00LTQtdGA0LbQuNCy0LDRjtGCIGNyZWF0ZUNvbnRleHR1YWxGcmFnbWVudCAo0LjQu9C4INGH0YLQvi3RgtC+INCyINGN0YLQvtC8INC00YPRhdC1KSwg0LIg0L7QsdGJ0LXQvCwg0LXRgdC70Lgg0YDRg9Cz0LDRjtGC0YHRjywg0YLQviDQstC+0YIg0YLQsNC6INC80L7QttC90L4g0L7QsdC+0LnRgtC4OlxyXG5cdFx0XHRcdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0XHRcdFx0dmFyIGV4dGVybmFsRG9tID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZygnPGRpdiBpZD1cIl9faW1wb3J0ZWRfaHRtbF9fXCI+JyArIHRoaXMucmVzcG9uc2VUZXh0ICsgJzwvZGl2PicsICd0ZXh0L2h0bWwnKTtcclxuXHRcdFx0XHQvLyAjVE9ETzog0JIg0LTQsNC90L3QvtC8INGB0LvRg9GH0LDQtSDQstGB0YLQsNCy0LjRgtGB0Y8gPGRpdiBpZD1cIl9faW1wb3J0ZWRfaHRtbF9fXCI+Li4u0L/QvtC70YPRh9C10L3QvdGL0LkgSFRNTC4uLjwvZGl2Piwg0Y3RgtC+INC90LDQtNC+INC40YHQv9GA0LDQstC40YLRjCDQvdCwINC90LXQutGD0Y4g0LLQvtC30LzQvtC20L3QviDQv9C+0YHQu9C10LTQvtCy0LDRgtC10LvRjNC90YPRjiDQstGB0YLQsNCy0LrRgyDQstGB0LXRhSDQtNC+0YfQtdGA0L3QuNGFINC90L7QtNC+0LIg0Y3RgtC+0LPQviBkaXYnYVxyXG5cdFx0XHRcdHZhciBkb21GcmFnbWVudCA9IG9wdGlvbnMudGFyZ2V0RG9jdW1lbnQuaW1wb3J0Tm9kZShleHRlcm5hbERvbS5nZXRFbGVtZW50QnlJZCgnX19pbXBvcnRlZF9odG1sX18nKSwgdHJ1ZSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dmFyIGRvbUZyYWdtZW50ID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVSYW5nZSgpLmNyZWF0ZUNvbnRleHR1YWxGcmFnbWVudCh0aGlzLnJlc3BvbnNlVGV4dCk7XHJcblx0XHRcdH1cclxuXHRcdFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQoZG9tRnJhZ21lbnQpO1xyXG5cdFx0XHRpZiAodHlwZW9mIG9wdGlvbnMub25Mb2FkQ2FsbGJhY2sgPT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHRcdG9wdGlvbnMub25Mb2FkQ2FsbGJhY2soKTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHRcdHhoci5zZW5kKCk7XHJcblx0fSBlbHNlIGlmIChvcHRpb25zLmh0bWwpIHtcclxuXHRcdGlmIChvcHRpb25zLmxlZ2FjeSkge1xyXG5cdFx0XHQvLyAjQ0JOT1RFOiDQodGC0LDRgNGL0LUg0LHRgNCw0YPQt9C10YDRiywg0YLQsNC60LjQtSDQutCw0LogV2ViVmlldyAzMywg0L3QtSDQv9C+0LTQtNC10YDQttC40LLQsNGO0YIgY3JlYXRlQ29udGV4dHVhbEZyYWdtZW50ICjQuNC70Lgg0YfRgtC+LdGC0L4g0LIg0Y3RgtC+0Lwg0LTRg9GF0LUpLCDQsiDQvtCx0YnQtdC8LCDQtdGB0LvQuCDRgNGD0LPQsNGO0YLRgdGPLCDRgtC+INCy0L7RgiDRgtCw0Log0LzQvtC20L3QviDQvtCx0L7QudGC0Lg6XHJcblx0XHRcdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0XHRcdHZhciBleHRlcm5hbERvbSA9IHBhcnNlci5wYXJzZUZyb21TdHJpbmcoJzxkaXYgaWQ9XCJfX2ltcG9ydGVkX2h0bWxfX1wiPicgKyBvcHRpb25zLmh0bWwgKyAnPC9kaXY+JywgJ3RleHQvaHRtbCcpO1xyXG5cdFx0XHR2YXIgZG9tRnJhZ21lbnQgPSBvcHRpb25zLnRhcmdldERvY3VtZW50LmltcG9ydE5vZGUoZXh0ZXJuYWxEb20uZ2V0RWxlbWVudEJ5SWQoJ19faW1wb3J0ZWRfaHRtbF9fJyksIHRydWUpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0dmFyIGRvbUZyYWdtZW50ID0gb3B0aW9ucy50YXJnZXREb2N1bWVudC5jcmVhdGVSYW5nZSgpLmNyZWF0ZUNvbnRleHR1YWxGcmFnbWVudChvcHRpb25zLmh0bWwpO1xyXG5cdFx0fVxyXG5cdFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQoZG9tRnJhZ21lbnQpO1xyXG5cdFx0aWYgKHR5cGVvZiBvcHRpb25zLm9uTG9hZENhbGxiYWNrID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0b3B0aW9ucy5vbkxvYWRDYWxsYmFjaygpO1xyXG5cdFx0fVxyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG59XHJcblxyXG4vL1x04paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojilogg4paI4paIICAg4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAg4paI4paIICDilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilojilohcclxuLy9cdCAgICAg4paI4paIICDilojiloggIOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiCAgICDilojilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIICDilojilohcclxuLy9cdOKWiOKWiCAgICAgICDilojiloggIOKWiOKWiCAg4paI4paI4paI4paIICDilojilojilojilogg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojiloggICDilojilojilojiloggICDilojilogg4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vXHQgICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICDilojiloggIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vL1x0I2luc2VydCAjc3ZnICNzeW1ib2xcclxuZXhwb3J0IGZ1bmN0aW9uIGluc2VydFN2Z1N5bWJvbCgpIHtcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblx0Ly8gINCS0LDRgNC40LDQvdGCINGBINC+0YLQtNC10LvRjNC90YvQvCDRgNC+0LTQuNGC0LXQu9GM0YHQutC40LxcclxuXHQvLyBcdFNWRy3RjdC70LXQvNC10L3RgtC+0Lwg0LTQu9GPINC60LDQttC00L7Qs9C+INGB0LjQvNCy0L7Qu9CwOlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHR2YXIgb3B0aW9ucyA9IHtcclxuXHRcdHN5bWJvbDogbnVsbCxcclxuXHRcdG1vdW50RWxlbWVudDogZG9jdW1lbnQuYm9keSxcclxuXHRcdGNvbnRhaW5lckNsYXNzOiBudWxsLFxyXG5cdH07XHJcblx0XHJcblx0aWYgKHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ3N0cmluZycgfHwgYXJndW1lbnRzWzBdIGluc3RhbmNlb2YgU3RyaW5nKSB7XHJcblx0XHRvcHRpb25zLnN5bWJvbCA9IGFyZ3VtZW50c1swXTtcclxuXHRcdG9wdGlvbnMubW91bnRFbGVtZW50ID0gYXJndW1lbnRzWzFdIHx8IG9wdGlvbnMubW91bnRFbGVtZW50O1xyXG5cdFx0b3B0aW9ucy5jb250YWluZXJDbGFzcyA9IGFyZ3VtZW50c1syXSB8fCBvcHRpb25zLmNvbnRhaW5lckNsYXNzO1xyXG5cdH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblxyXG5cdHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0dmFyIHN5bWJvbERvY3VtZW50ID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZyhcclxuXHRcdGA8c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiBcclxuXHRcdFx0JHsgb3B0aW9ucy5jb250YWluZXJDbGFzcyA/IGBjbGFzcz1cIiR7b3B0aW9ucy5jb250YWluZXJDbGFzc31cImAgOiAnJyB9IFxyXG5cdFx0XHRzdHlsZT1cImRpc3BsYXk6IG5vbmU7XCI+XHJcblx0XHRcdCR7b3B0aW9ucy5zeW1ib2x9XHJcblx0XHQ8L3N2Zz5gLFxyXG5cdFx0J2ltYWdlL3N2Zyt4bWwnXHJcblx0KTtcclxuXHR2YXIgc3ltYm9sTm9kZSA9IGRvY3VtZW50LmltcG9ydE5vZGUoc3ltYm9sRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB0cnVlKTtcclxuXHRvcHRpb25zLm1vdW50RWxlbWVudC5hcHBlbmRDaGlsZChzeW1ib2xOb2RlKTtcclxuXHJcblx0Ly8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdC8vICDQktCw0YDQuNCw0L3RgiDRgdC+INCy0YHRgtCw0LLQutC+0Lkg0LIg0L7QtNC40L0g0YDQvtC00LjRgtC10LvRjNGB0LrQuNC5IFxyXG5cdC8vIFx0U1ZHLdGN0LvQtdC80LXQvdGCOlxyXG5cdC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHQvLyB2YXIgb3B0aW9ucyA9IHtcclxuXHQvLyBcdHN5bWJvbDogbnVsbCxcclxuXHQvLyBcdGNvbnRhaW5lcklkOiAnc3ZnX3Nwcml0ZScsXHJcblx0Ly8gXHRtb3VudEVsZW1lbnQ6IGRvY3VtZW50LmJvZHksXHJcblx0Ly8gfTtcclxuXHQgXHJcblx0Ly8gaWYgKHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ3N0cmluZycgfHwgYXJndW1lbnRzWzBdIGluc3RhbmNlb2YgU3RyaW5nKSB7XHJcblx0Ly8gXHRvcHRpb25zLnN5bWJvbCA9IGFyZ3VtZW50c1swXTtcclxuXHQvLyBcdG9wdGlvbnMuY29udGFpbmVySWQgPSBhcmd1bWVudHNbMV0gfHwgb3B0aW9ucy5jb250YWluZXJJZDtcclxuXHQvLyBcdG9wdGlvbnMubW91bnRFbGVtZW50ID0gYXJndW1lbnRzWzJdIHx8IG9wdGlvbnMubW91bnRFbGVtZW50O1xyXG5cdC8vIH0gZWxzZSBpZiAoISFhcmd1bWVudHNbMF0gJiYgdHlwZW9mIGFyZ3VtZW50c1swXSA9PSAnb2JqZWN0Jykge1xyXG5cdC8vIFx0ZXh0ZW5kKG9wdGlvbnMsIGFyZ3VtZW50c1swXSk7XHJcblx0Ly8gfSBlbHNlIHtcclxuXHQvLyBcdHJldHVybjtcclxuXHQvLyB9XHJcblxyXG5cdC8vIHZhciBjb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChvcHRpb25zLmNvbnRhaW5lcklkKTtcclxuXHRcclxuXHQvLyBpZiAoY29udGFpbmVyID09IG51bGwpIHtcclxuXHQvLyBcdGNvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUygnaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnLCAnc3ZnJyk7XHJcblx0Ly8gXHRjb250YWluZXIuc2V0QXR0cmlidXRlKCdpZCcsIG9wdGlvbnMuY29udGFpbmVySWQpO1xyXG5cdC8vIFx0Y29udGFpbmVyLnN0eWxlLmNzc1RleHQgPSAnZGlzcGxheTogbm9uZTsnO1xyXG5cdC8vIFx0b3B0aW9ucy5tb3VudEVsZW1lbnQuYXBwZW5kQ2hpbGQoY29udGFpbmVyKTtcclxuXHQvLyB9XHJcblxyXG5cdC8vIHZhciBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcblx0Ly8gdmFyIHN5bWJvbERvY3VtZW50ID0gcGFyc2VyLnBhcnNlRnJvbVN0cmluZyhcclxuXHQvLyBcdGA8c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiBzdHlsZT1cImRpc3BsYXk6IG5vbmU7XCI+XHJcblx0Ly8gXHRcdCR7b3B0aW9ucy5zeW1ib2x9XHJcblx0Ly8gXHQ8L3N2Zz5gLFxyXG5cdC8vIFx0J2ltYWdlL3N2Zyt4bWwnXHJcblx0Ly8gKTtcclxuXHQvLyB2YXIgc3ltYm9sTm9kZSA9IGRvY3VtZW50LmltcG9ydE5vZGUoc3ltYm9sRG9jdW1lbnQucXVlcnlTZWxlY3Rvcignc3ltYm9sJyksIHRydWUpO1xyXG5cdC8vIGNvbnRhaW5lci5hcHBlbmRDaGlsZChzeW1ib2xOb2RlKTtcclxufSIsImltcG9ydCAqIGFzIHV0aWxzIGZyb20gJy4vdXRpbHMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gU3RyaW5nQ2FzZShiaXRzKSB7XHJcblx0dGhpcy5iaXRzID0gYml0cztcclxuXHJcblx0aWYgKEFycmF5LmlzQXJyYXkoYml0cykpIHtcclxuXHRcdHRoaXMuYXNJcyA9IHRoaXMuYml0cy5qb2luKCcnKTtcclxuXHRcdGJpdHMuZm9yRWFjaChmdW5jdGlvbihiaXQsIGkpIHtcclxuXHRcdFx0Yml0c1tpXSA9IGJpdC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0fSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHRoaXMuYXNJcyA9IHRoaXMuYml0cztcclxuXHRcdGJpdHMgPSBiaXRzLnRvTG93ZXJDYXNlKCk7XHJcblx0fVxyXG5cclxuXHR0aGlzLmNhbWVsID0gdXRpbHMuc3RyaW5nQ2FzZShiaXRzLCAnY2FtZWwnKTtcclxuXHR0aGlzLmNhcGl0YWxDYW1lbCA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2NhcGl0YWxDYW1lbCcpO1xyXG5cdHRoaXMua2ViYWIgPSB1dGlscy5zdHJpbmdDYXNlKGJpdHMsICdrZWJhYicpO1xyXG5cdHRoaXMuc25ha2UgPSB1dGlscy5zdHJpbmdDYXNlKGJpdHMsICdzbmFrZScpO1xyXG5cdHRoaXMuZmxhdCA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2ZsYXQnKTtcclxuXHR0aGlzLmRvdCA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2RvdCcpO1xyXG59XHJcblN0cmluZ0Nhc2UucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XHJcblx0cmV0dXJuIHRoaXMuYXNJcztcclxufTsiLCJpbXBvcnQgKiBhcyB1dGlscyBmcm9tICcuL3V0aWxzJztcclxuaW1wb3J0IFN0cmluZ0Nhc2UgZnJvbSAnLi9TdHJpbmdDYXNlJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIE5hbWUobmFtZUJpdHMsIG5zQml0cykge1xyXG5cdHZhciBiaXRzO1xyXG5cclxuXHQvLyDQlNCw0LvQtdC1INC40LTQtdGCINC+0LHRgNCw0LHQvtGC0LrQsCDQv9Cw0YDQsNC80LXRgtGA0LAgbnNCaXRzOlxyXG5cdC8vIC0g0LXRgdC70Lgg0L7QvSDQvdC1INC30LDQtNCw0L0sINC/0YDQvtC/0YPRgdGC0LjRgtGMINCy0YHQtSDQuCDQstC30Y/RgtGMIG5zINC40Lcg0L/RgNC+0YLQvtGC0LjQv9CwXHJcblx0Ly8gLSDQtdGB0LvQuCDQvtC9IGJvb2xlYW4g0LggdHJ1ZSwg0YLQviDRgtCw0LrQttC1INC/0YDQvtC/0YPRgdGC0LjRgtGMINC4INC40YHQv9C+0LvRjNC30L7QstCw0YLRjCBucyBcclxuXHQvLyAgINC40Lcg0L/RgNC+0YLQvtGC0LjQv9CwXHJcblx0Ly8gLSDQtdGB0LvQuCDQvtC9IGJvb2xlYW4g0LggZmFsc2UsINGC0L4gbnNCaXRzIC0g0L/Rg9GB0YLQsNGPINGB0YLRgNC+0LrQsFxyXG5cdC8vIC0g0Lgg0LXRgdC70Lgg0LIg0YDQtdC30YPQu9GM0YLQsNGC0LUgbnNCaXRzINGB0YLRgNC+0LrQsCDQuNC70Lgg0LzQsNGB0YHQuNCyLCDRgtC+INGB0L7Qt9C00LDRgtGMINC90L7QstC+0LUgXHJcblx0Ly8gICDRgdCy0L7QudGB0YLQstC+IG5zINC/0L7QstC10YDRhSDQs9C10YLRgtC10YDQsCDQv9GA0L7RgtC+0YLQuNC/0LAg0YHQviDQt9C90LDRh9C10L3QuNC10LwgbnNCaXRzXHJcblx0aWYgKG5zQml0cyAhPSBudWxsKSB7XHJcblx0XHRpZiAodHlwZW9mIG5zQml0cyA9PSAnYm9vbGVhbicgJiYgbnNCaXRzID09IGZhbHNlKSB7XHJcblx0XHRcdG5zQml0cyA9ICcnO1xyXG5cdFx0fVxyXG5cdFx0aWYgKHR5cGVvZiBuc0JpdHMgPT0gJ3N0cmluZycgfHwgQXJyYXkuaXNBcnJheShuc0JpdHMpKSB7XHJcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCAnbnMnLCB7XHJcblx0XHRcdFx0dmFsdWU6IG5ldyBTdHJpbmdDYXNlKG5zQml0cyksXHJcblx0XHRcdFx0d3JpdGFibGU6IHRydWUsXHJcblx0XHRcdFx0Y29uZmlndXJhYmxlOiB0cnVlLFxyXG5cdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0dmFyIGhhc05zID0gdGhpcy5ucyAmJiB0aGlzLm5zLnRvU3RyaW5nKCkgIT0gJyc7XHJcblxyXG5cdHRoaXMuYmFzZSA9IG5ldyBTdHJpbmdDYXNlKG5hbWVCaXRzKTtcclxuXHJcblx0Ly8gI2FzSXMgI2ZsYXQgI2NhbWVsICNjYXBpdGFsICNrZWJhYlxyXG5cdHZhciB0eXBlcyA9IFsnYXNJcycsICdmbGF0JywgJ2NhbWVsJywgJ2NhcGl0YWxDYW1lbCcsICdrZWJhYiddO1xyXG5cdGZvciAodmFyIGkgPSAwOyBpIDwgdHlwZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdGlmIChoYXNOcykge1xyXG5cdFx0XHR0aGlzWydfJyArIHR5cGVzW2ldXSA9IHV0aWxzLnN0cmluZ0Nhc2UoW3RoaXMubnNbdHlwZXNbaV1dLCB0aGlzLmJhc2VbdHlwZXNbaV1dXSwgdHlwZXNbaV0pO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0dGhpc1snXycgKyB0eXBlc1tpXV0gPSB0aGlzLmJhc2VbdHlwZXNbaV1dO1xyXG5cdFx0fVxyXG5cdFx0dGhpc1t0eXBlc1tpXV0gPSAoZnVuY3Rpb24odHlwZSwgbmFtZU9iaikge1xyXG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oc3VmZml4KSB7XHJcblx0XHRcdFx0aWYgKHN1ZmZpeCkge1xyXG5cdFx0XHRcdFx0aWYgKHR5cGUgIT0gJ2FzSXMnKSB7XHJcblx0XHRcdFx0XHRcdGlmIChBcnJheS5pc0FycmF5KHN1ZmZpeCkpIHtcclxuXHRcdFx0XHRcdFx0XHRzdWZmaXguZm9yRWFjaChmdW5jdGlvbihiaXQsIGkpIHtcclxuXHRcdFx0XHRcdFx0XHRcdHN1ZmZpeFtpXSA9IGJpdC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXR1cm4gdXRpbHMuc3RyaW5nQ2FzZShbbmFtZU9ialsnXycgKyB0eXBlXSwgdXRpbHMuc3RyaW5nQ2FzZShzdWZmaXgsIHR5cGUpXSwgdHlwZSk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHJldHVybiBuYW1lT2JqWydfJyArIHR5cGVdO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcdH0pKHR5cGVzW2ldLCB0aGlzKTtcclxuXHR9XHJcblxyXG5cdC8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCAgICDilojilohcclxuXHQvLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojiloggICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIICAgIOKWiOKWiFxyXG5cdC8vICDilojilojilojilojilojiloggICDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG5cdC8vXHJcblx0Ly8gI2RvdFxyXG5cdHZhciBiaXRzID0gWyB0aGlzLmJhc2UuY2FtZWwgXTtcclxuXHRpZiAoaGFzTnMpIHtcclxuXHRcdGJpdHMudW5zaGlmdCh0aGlzLm5zLmNhbWVsKTtcclxuXHR9XHJcblx0dGhpcy5fZG90ID0gdXRpbHMuc3RyaW5nQ2FzZShiaXRzLCAnZG90Jyk7XHJcblx0dGhpcy5kb3QgPSBmdW5jdGlvbihzdWZmaXgpIHtcclxuXHRcdGlmIChzdWZmaXgpIHtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkoc3VmZml4KSkge1xyXG5cdFx0XHRcdHN1ZmZpeC5mb3JFYWNoKGZ1bmN0aW9uKGJpdCwgaSkge1xyXG5cdFx0XHRcdFx0c3VmZml4W2ldID0gYml0LnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHV0aWxzLnN0cmluZ0Nhc2UoW3RoaXMuX2RvdCwgdXRpbHMuc3RyaW5nQ2FzZShzdWZmaXgsICdjYW1lbCcpXSwgJ2RvdCcpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX2RvdDtcclxuXHRcdH1cclxuXHR9O1xyXG5cclxuXHQvLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paI4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG5cdC8vICDilojilojilojilojiloggICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojiloggIOKWiOKWiCAgICDilojilohcclxuXHQvLyAg4paI4paIICAgICAgIOKWiOKWiCAg4paI4paIICDilojiloggICAgICDilojiloggIOKWiOKWiCDilojiloggICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojilojiloggICAg4paI4paIXHJcblx0Ly9cclxuXHQvLyAjZXZlbnRcclxuXHR0aGlzLmV2ZW50ID0gdGhpcy5kb3Q7XHJcblxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAgICAgICDilojiloggICAgICDilojilohcclxuXHQvLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly9cclxuXHQvLyAjY3NzXHJcblx0Yml0cyA9IFsgdGhpcy5iYXNlLnNuYWtlIF07XHJcblx0aWYgKGhhc05zKSB7XHJcblx0XHRiaXRzLnVuc2hpZnQodGhpcy5ucy5zbmFrZSk7XHJcblx0fVxyXG5cdHRoaXMuX2NzcyA9IHV0aWxzLnN0cmluZ0Nhc2UoYml0cywgJ2tlYmFiJyk7XHJcblx0dGhpcy5jc3MgPSBmdW5jdGlvbihzdWZmaXgpIHtcclxuXHRcdGlmIChzdWZmaXgpIHtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkoc3VmZml4KSkge1xyXG5cdFx0XHRcdHN1ZmZpeC5mb3JFYWNoKGZ1bmN0aW9uKGJpdCwgaSkge1xyXG5cdFx0XHRcdFx0c3VmZml4W2ldID0gYml0LnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHV0aWxzLnN0cmluZ0Nhc2UoW3RoaXMuX2NzcywgdXRpbHMuc3RyaW5nQ2FzZShzdWZmaXgsICdzbmFrZScpXSwgJ2tlYmFiJyk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5fY3NzO1xyXG5cdFx0fVxyXG5cdH07XHJcblxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAgICAgICDilojiloggICAgICDilojilohcclxuXHQvLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gXHJcblx0Ly8gIOKWiOKWiOKWiCAgICDilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCDilojilojilojilogg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAg4paI4paIICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vXHJcblx0Ly8gI2NzcyAjbW9kaWZpY2F0b3JcclxuXHR0aGlzLmNzc01vZGlmaWNhdG9yID0gZnVuY3Rpb24obW9kaWZpY2F0b3JOYW1lKSB7XHJcblx0XHRpZiAobW9kaWZpY2F0b3JOYW1lICE9IG51bGwpIHtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkobW9kaWZpY2F0b3JOYW1lKSkge1xyXG5cdFx0XHRcdG1vZGlmaWNhdG9yTmFtZS5mb3JFYWNoKGZ1bmN0aW9uKGJpdCwgaSkge1xyXG5cdFx0XHRcdFx0bW9kaWZpY2F0b3JOYW1lW2ldID0gYml0LnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHRoaXMuY3NzKCkgKyAnLS0nICsgdXRpbHMuc3RyaW5nQ2FzZShtb2RpZmljYXRvck5hbWUsICdzbmFrZScpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuY3NzKCk7XHJcblx0XHR9XHJcblx0fTtcclxuXHR0aGlzLmNzc01vZCA9IHRoaXMuY3NzTW9kaWZpY2F0b3I7XHJcblxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAgICAgICDilojiloggICAgICDilojilohcclxuXHQvLyAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gXHJcblx0Ly8gICDilojilojilojilojilojilogg4paI4paIICAgICAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vICDilojiloggICAgICDilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcblx0Ly8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgIOKWiOKWiFxyXG5cdC8vICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cdC8vIFxyXG5cdC8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilohcclxuXHQvLyAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICDilojiloggICAgICDilojilojilojilojiloggICDilojiloggICAgICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilohcclxuXHQvLyAgICAgICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojilogg4paI4paIICAg4paI4paIXHJcblx0Ly8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilohcclxuXHQvL1xyXG5cdC8vICNjc3MgI2NsYXNzICNzZWxlY3RvclxyXG5cdHRoaXMuY3NzQ2xhc3NTZWxlY3RvciA9IGZ1bmN0aW9uKGFwcGVuZGFnZSkge1xyXG5cdFx0cmV0dXJuICcuJyArIHRoaXMuY3NzKGFwcGVuZGFnZSk7XHJcblx0fTtcclxuXHR0aGlzLnNlbGVjdG9yID0gZnVuY3Rpb24oYXBwZW5kYWdlKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5jc3NDbGFzc1NlbGVjdG9yKGFwcGVuZGFnZSk7XHJcblx0fTtcclxufVxyXG52YXIgbnM7XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShOYW1lLnByb3RvdHlwZSwgJ25zJywge1xyXG5cdHNldDogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdG5zID0gbmV3IFN0cmluZ0Nhc2UodmFsdWUpO1xyXG5cdH0sXHJcblx0Z2V0OiBmdW5jdGlvbigpIHtcclxuXHRcdHJldHVybiBucztcclxuXHR9LFxyXG59KTtcclxuTmFtZS5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcy5fYXNJcztcclxufTsiLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBJZCgpIHtcclxufVxyXG5JZC5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcy5jdXJyZW50O1xyXG59OyIsImltcG9ydCB7IGlzT2JqZWN0LCBleHRlbmQsIGZpcnN0VXBwZXJDYXNlIH0gZnJvbSAnLi91dGlscyc7XHJcbmltcG9ydCBOYW1lIGZyb20gJy4vTmFtZSc7XHJcbmltcG9ydCBJZCBmcm9tICcuL0lkJztcclxuXHJcbi8qKlxyXG4gKiDQkdCw0LfQvtCy0YvQuSDQutC70LDRgdGBINC00LvRjyDQstC40LTQttC10YLQvtCyXHJcbiAqIEBwYXJhbSB7QXJyYXktbGlrZX0gYXJndW1lbnRzIC4uLlxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gV2lkZ2V0KCkge1xyXG5cdHRoaXMuY29uc3RydWN0KC4uLmFyZ3VtZW50cyk7XHJcbn07XHJcblxyXG4vKipcclxuICog0J/QtdGA0LXQvtC/0YDQtdC00LXQu9C10L3QuNC1INGB0LLQvtC50YHRgtCy0LAgYG5hbWVgINC60L7QvdGB0YLRgNGD0LrRgtC+0YDQsCDQvdGD0LbQvdC+INC/0YDQuCDRgdC+0YXRgNCw0L3QtdC90LjQuCDRjdC60LfQtdC/0LvRj9GA0LAg0LrQu9Cw0YHRgdCwINCyINC/0YDQvtGC0L7RgtC40L/QtVxyXG4gKiAo0YHQvC4g0LzQtdGC0L7QtCBgc2F2ZUluc3RhbmNlT2ZgKVxyXG4gKi9cclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KFdpZGdldCwgJ25hbWUnLCB7XHJcblx0dmFsdWU6IG5ldyBOYW1lKCdXaWRnZXQnLCBmYWxzZSksXHJcbn0pO1xyXG5cclxuV2lkZ2V0LnByb3RvdHlwZS5uYW1lID0gbmV3IE5hbWUoJ3dpZGdldCcpO1xyXG5XaWRnZXQucHJvdG90eXBlLmRlYnVnID0gZmFsc2U7XHJcbldpZGdldC5wcm90b3R5cGUuYnVzeSA9IGZhbHNlO1xyXG5XaWRnZXQucHJvdG90eXBlLmluaXRpYWxpemVkID0gZmFsc2U7XHJcbldpZGdldC5wcm90b3R5cGUuYXV0b0luaXRpYWxpemUgPSB0cnVlO1xyXG5cclxuLyoqXHJcbiAqINCa0L7QvdGB0YLRgNGD0LjRgNC+0LLQsNC90LjQtSDQvtCx0YrQtdC60YLQsC5cclxuICog0K3RgtC+0YIg0LzQtdGC0L7QtCDQvdC1INC30LDQtNGD0LzQsNC9INC00LvRjyDQv9C10YDQtdC+0L/RgNC10LTQtdC70LXQvdC40Y8g0LjQu9C4INGA0LDRgdGI0LjRgNC10L3QuNGPICjQtNC70Y8g0Y3RgtC+0LPQviDRgdC8LiBgY29uc3RydWN0aW9uQ2hhaW5gKS5cclxuICogQHBhcmFtIHtBcnJheS1saWtlfSBhcmd1bWVudHMgLi4uXHJcbiAqL1xyXG5XaWRnZXQucHJvdG90eXBlLmNvbnN0cnVjdCA9IGZ1bmN0aW9uKClcclxue1xyXG5cdHZhciB0YXJnZXQsIG9wdGlvbnM7XHJcblx0aWYgKGFyZ3VtZW50c1swXSBpbnN0YW5jZW9mIEVsZW1lbnQpIHtcclxuXHRcdHRhcmdldCA9IGFyZ3VtZW50c1swXTtcclxuXHRcdGlmICghIWFyZ3VtZW50c1sxXSAmJiB0eXBlb2YgYXJndW1lbnRzWzFdID09ICdvYmplY3QnKSB7XHJcblx0XHRcdG9wdGlvbnMgPSBhcmd1bWVudHNbMV07XHJcblx0XHRcdGlmICh0eXBlb2YgYXJndW1lbnRzWzJdID09ICdib29sZWFuJykge1xyXG5cdFx0XHRcdG9wdGlvbnMuYXV0b0luaXRpYWxpemUgPSBhcmd1bWVudHNbMl07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9IGVsc2UgaWYgKCEhYXJndW1lbnRzWzBdICYmIHR5cGVvZiBhcmd1bWVudHNbMF0gPT0gJ29iamVjdCcpIHtcclxuXHRcdG9wdGlvbnMgPSBhcmd1bWVudHNbMF07XHJcblx0XHRpZiAodHlwZW9mIGFyZ3VtZW50c1sxXSA9PSAnYm9vbGVhbicpIHtcclxuXHRcdFx0b3B0aW9ucy5hdXRvSW5pdGlhbGl6ZSA9IGFyZ3VtZW50c1sxXTtcclxuXHRcdH1cclxuXHR9XHJcblx0dGhpcy5jb25zdHJ1Y3Rpb25DaGFpbih0YXJnZXQsIG9wdGlvbnMpO1xyXG5cdGlmICh0aGlzLmF1dG9Jbml0aWFsaXplKSB7XHJcblx0XHR0aGlzLmluaXRpYWxpemUodGhpcy5kb20uc2VsZik7XHJcblx0fVxyXG59O1xyXG5cclxuLyoqXHJcbiAqINCa0L7QvdGB0YLRgNGD0LjRgNC+0LLQsNC90LjQtSDQvtCx0YrQtdC60YLQsCAo0YLQvtC70YzQutC+INC+0YLQvdC+0YHRj9GJ0LXQtdGB0Y8g0Log0Y3RgtC+0Lwg0L7QsdGK0LXQutGC0YMgKyDQstGL0LfQvtCyINGG0LXQv9C+0YfQutC4KS4g0KLRg9GCINGC0L7Qu9GM0LrQviDRgtC+0YIg0LrQvtC0LCBcclxuICog0LrQvtGC0L7RgNGL0Lkg0L3QtdC70YzQt9GPINC90LDQt9Cy0LDRgtGMINC40L3QuNGG0LjQsNC70LjQt9Cw0YbQuNC10LkgKNC60L7RgtC+0YDRi9C5INC90LUg0LTQvtC70LbQtdC9INC40YHQv9C+0LvQvdGP0YLRjNGB0Y8g0L/QvtCy0YLQvtGA0L3QvikuINCt0YLQvtGCINC80LXRgtC+0LQg0LfQsNC00YPQvNCw0L0gXHJcbiAqINC00LvRjyDRgNCw0YHRiNC40YDQtdC90LjRjyDQuNC70Lgg0L/QtdGA0LXQvtC/0YDQtdC00LXQu9C10L3QuNGPINC00L7Rh9C10YDQvdC40LzQuCDQutC70LDRgdGB0LDQvNC4LlxyXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSB0YXJnZXQg0K3Qu9C10LzQtdC90YIgRE9NLCDQvdCwINC60L7RgtC+0YDQvtC8INGB0L7Qt9C00LDQtdGC0YHRjyDQstC40LTQttC10YIgKNC40L3QvtCz0LTQsCDQvtC9INC90YPQttC10L0g0LIg0LrQvtC90YHRgtGA0YPQutGC0L7RgNC1KVxyXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyDQn9Cw0YDQsNC80LXRgtGA0Ysg0LTQu9GPINGN0LrQt9C10LzQv9C70Y/RgNCwINC60LvQsNGB0YHQsCAo0L/QtdGA0LXQt9Cw0L/QuNGB0YvQstCw0LXRgiDQt9C90LDRh9C10L3QuNGPINC/0L4g0YPQvNC+0LvRh9Cw0L3QuNGOKVxyXG4gKi9cclxuV2lkZ2V0LnByb3RvdHlwZS5jb25zdHJ1Y3Rpb25DaGFpbiA9IGZ1bmN0aW9uKHRhcmdldCwgb3B0aW9ucylcclxue1xyXG5cdHRoaXMuZG9tID0ge1xyXG5cdFx0c2VsZjogdGFyZ2V0LFxyXG5cdH07XHJcblx0dGhpcy5pZCA9IG5ldyBJZDtcclxuXHR0aGlzLnNldE9wdGlvbnMob3B0aW9ucyk7XHJcblx0dGhpcy5zYXZlSW5zdGFuY2VPZihXaWRnZXQpO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqINCY0L3QuNGG0LjQsNC70LjQt9Cw0YbQuNGPINGN0LrQt9C10L/Qu9GP0YDQsCDQutC70LDRgdGB0LA6INC/0YDQuNCy0Y/Qt9C60LAg0LogRE9NLCDQvdCw0LfQvdCw0YfQtdC90LjQtSDQv9GA0L7RgdC70YPRiNC60Lgg0YHQvtCx0YvRgtC40Lkg0Lgg0YIu0L8uXHJcbiAqINCt0YLQvtGCINC80LXRgtC+0LQg0L3QtSDQt9Cw0LTRg9C80LDQvSDQtNC70Y8g0L/QtdGA0LXQvtC/0YDQtdC00LXQu9C10L3QuNGPINC40LvQuCDRgNCw0YHRiNC40YDQtdC90LjRjyAo0LTQu9GPINGN0YLQvtCz0L4g0YHQvC4gYGluaXRpYWxpemF0aW9uQ2hhaW5gKVxyXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fG51bGx9IHRhcmdldCDQrdC70LXQvNC10L3RgiBET00sINC90LAg0LrQvtGC0L7RgNC+0Lwg0YHQvtC30LTQsNC10YLRgdGPINCy0LjQtNC20LXRglxyXG4gKi9cclxuV2lkZ2V0LnByb3RvdHlwZS5pbml0aWFsaXplID0gZnVuY3Rpb24odGFyZ2V0KVxyXG57XHJcblx0aWYgKHR5cGVvZiB0YXJnZXQgPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdHRhcmdldCA9IHRoaXMuZG9tLnNlbGY7IC8vINCf0YDQuCDQvtGC0LvQvtC20LXQvdC90L7QuSDQuNC90LjRhtC40LDQu9C40LfQsNGG0LjQuFxyXG5cdH1cclxuXHR0aGlzLmluaXRpYWxpemF0aW9uQ2hhaW4odGFyZ2V0KTtcclxuXHR0aGlzLmluaXRpYWxpemVkID0gdHJ1ZTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiDQmNC90LjRhtC40LDQu9C40LfQsNGG0LjRjyDRjdC60LfQtdC/0LvRj9GA0LAg0LrQu9Cw0YHRgdCwICjRgtC+0LvRjNC60L4g0L7RgtC90L7RgdGP0YnQsNGP0YHRjyDQuiDRjdGC0L7QvNGDINC+0LHRitC10LrRgtGDICsg0LLRi9C30L7QsiDRhtC10L/QvtGH0LrQuCk6IFxyXG4gKiDQv9GA0LjQstGP0LfQutCwINC6IERPTSwg0L3QsNC30L3QsNGH0LXQvdC40LUg0L/RgNC+0YHQu9GD0YjQutC4INGB0L7QsdGL0YLQuNC5INC4INGCLtC/LiDQrdGC0L7RgiDQvNC10YLQvtC0INC30LDQtNGD0LzQsNC9INC00LvRjyDRgNCw0YHRiNC40YDQtdC90LjRjyBcclxuICog0LjQu9C4INC/0LXRgNC10L7Qv9GA0LXQtNC10LvQtdC90LjRjyDQtNC+0YfQtdGA0L3QuNC80Lgg0LrQu9Cw0YHRgdCw0LzQuC5cclxuICogQHBhcmFtIHtIVE1MRWxlbWVudH0gdGFyZ2V0INCt0LvQtdC80LXQvdGCIERPTSwg0L3QsCDQutC+0YLQvtGA0L7QvCDRgdC+0LfQtNCw0LXRgtGB0Y8g0LLQuNC00LbQtdGCXHJcbiAqL1xyXG5XaWRnZXQucHJvdG90eXBlLmluaXRpYWxpemF0aW9uQ2hhaW4gPSBmdW5jdGlvbih0YXJnZXQpXHJcbntcclxuXHR0aGlzLmRvbS5zZWxmID0gdGFyZ2V0O1xyXG59O1xyXG5cclxuV2lkZ2V0LnByb3RvdHlwZS5zZXRPcHRpb25zID0gZnVuY3Rpb24ob3B0aW9ucylcclxue1xyXG5cdGlmIChpc09iamVjdChvcHRpb25zKSkge1xyXG5cdFx0ZXh0ZW5kKHRoaXMsIG9wdGlvbnMpO1xyXG5cdH1cclxufTtcclxuXHJcbi8vIC8qXHJcbi8vICDilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAgICAgICDilojilogg4paI4paIICAg4paI4paIICDilojiloggIOKWiOKWiCAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICAg4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vLyAg4paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojilojiloggICAg4paI4paIICDilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilogg4paI4paIICDilojilogg4paI4paIICAgICAg4paI4paI4paI4paI4paIXHJcbi8vICDilojilogg4paI4paIICDilojilogg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paIICDilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gXHJcbi8vICAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICDilojilogg4paI4paIXHJcbi8vICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAgIOKWiOKWiCDilojilohcclxuLy8gICDilojilojilojilojilojiloggIOKWiOKWiFxyXG4vLyAqL1xyXG4vLyAjc2F2ZSAjaW5zdGFuY2UgI29mXHJcbi8qKlxyXG4gKiDQodC+0YXRgNCw0L3QtdC90LjQtSDRjdC60LfQtdC80L/Qu9GP0YDQsCDQutC70LDRgdGB0LAg0LIg0YDQtdCz0LjRgdGC0YDQtSDRjdC60LfQtdC80L/Qu9GP0YDQvtCyINC60LvQsNGB0YHQsCDRjdGC0L7Qs9C+INGC0LjQv9CwXHJcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNvbnN0cnVjdG9yINCa0L7QvdGB0YLRgNGD0LrRgtC+0YAg0LrQu9Cw0YHRgdCwLCDQsiDQutC+0YLQvtGA0YvQuSDQv9GA0L7QuNGB0YXQvtC00LjRgiDRgdC+0YXRgNCw0L3QtdC90LjQtSDRjdC60LfQtdC80L/Qu9GP0YDQsFxyXG4gKi9cclxuV2lkZ2V0LnByb3RvdHlwZS5zYXZlSW5zdGFuY2VPZiA9IGZ1bmN0aW9uKGNvbnN0cnVjdG9yKVxyXG57XHJcblx0Y29uc3RydWN0b3IgPSBjb25zdHJ1Y3RvciB8fCB0aGlzLmNvbnN0cnVjdG9yO1xyXG5cdGlmICghY29uc3RydWN0b3IucHJvdG90eXBlLmhhc093blByb3BlcnR5KCdpbnN0YW5jZXMnKSkge1xyXG5cdFx0KGNvbnN0cnVjdG9yLnByb3RvdHlwZS5pbnN0YW5jZXMgPSBbXSkubGVuZ3RoKys7IC8vINC/0L7RgtC+0LzRgyDRh9GC0L4g0LzRiyDRhdC+0YLQuNC8INGB0YfQuNGC0LDRgtGMIGlkINGBIDEsINCwINC90LUg0YEgMFxyXG5cdFx0Ly8gI1RPRE86INCf0LXRgNC10LTQtdC70LDRgtGMINC90LAg0YfRgtC+LdGC0L4g0LHQvtC70LXQtSDQutC+0YDRgNC10LrRgtC90L7QtSwg0LLQtdC00Ywg0L/RgNC4INGC0LDQutC+0Lwg0L/QvtC00YXQvtC00LUg0YEg0LzQsNGB0YHQuNCy0L7QvCDQtdCz0L4gbGVuZ3RoINCx0YPQtNC10YIg0LLRi9C00LDQstCw0YLRjCDRgNC10LfRg9C70YzRgtCw0YIg0L3QtSDQtdC00LjQvdC40YbRgyDQsdC+0LvRjNGI0LjQuSwg0YfQtdC8INCyINC00LXQudGB0YLQstC40YLQtdC70YzQvdC+0YHRgtC4INC10YHRgtGMINGB0L7RhdGA0LDQvdC10L3QvdGL0YUg0Y3QutC30LXQv9C70Y/RgNC+0LIuINCS0L7Qt9C80L7QttC90L4g0LvRg9GH0YjQtSDQuNGB0L/QvtC70YzQt9C+0LLQsNGC0YwgU2V0INC40LvQuCBNYXAuXHJcblx0fVxyXG5cdHRoaXMuaWQuY3VycmVudCA9IGNvbnN0cnVjdG9yLnByb3RvdHlwZS5pbnN0YW5jZXMubGVuZ3RoO1xyXG5cdHRoaXMuaWRbY29uc3RydWN0b3IubmFtZS5fY2FtZWxdID0gdGhpcy5pZCA+PiAwOyAvLyDRgdC60LDRgdGC0L7QstCw0YLRjCDQsiDRgdGC0YDQvtC60YMsINCwINC30LDRgtC10Lwg0LIg0YbQtdC70L7QtSDRh9C40YHQu9C+XHJcblx0Y29uc3RydWN0b3IucHJvdG90eXBlLmluc3RhbmNlc1t0aGlzLmlkXSA9IHRoaXM7XHJcbn07XHJcblxyXG4vLyAvKlxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAgICAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiCDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vLyAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIXHJcbi8vICovXHJcbi8vICNzZXQgI2RhdGFzZXQgI2lkICNvZlxyXG5XaWRnZXQucHJvdG90eXBlLnNldERhdGFzZXRJZE9mID0gZnVuY3Rpb24oY29uc3RydWN0b3IpXHJcbntcclxuXHRjb25zdHJ1Y3RvciA9IGNvbnN0cnVjdG9yIHx8IHRoaXMuY29uc3RydWN0b3I7XHJcblx0dmFyIGlkID0gdGhpcy5pZFtjb25zdHJ1Y3Rvci5uYW1lLl9jYW1lbF07XHJcblx0dmFyIG5hbWUgPSBjb25zdHJ1Y3Rvci5wcm90b3R5cGUubmFtZS5jYW1lbCgnaWQnKTtcclxuXHR0aGlzLmRvbS5zZWxmLmRhdGFzZXRbbmFtZV0gPSBpZDtcclxufTtcclxuXHJcbi8vIC8qXHJcbi8vICDilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIICDilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy8gIOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojilojilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAgICDilojilojilojilojilohcclxuLy8gIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggIOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paIXHJcbi8vICDilojilogg4paI4paIICAg4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIXHJcbi8vICDilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiCDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAg4paI4paIICDilojilohcclxuLy8gIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICAgICDilojilohcclxuLy8gXHJcbi8vICDilojilojilojilojilojiloggICDilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vLyAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICAgICDilojilohcclxuLy8gIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy8gIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gKi9cclxuLy8gI2luc3RhbmNlICNmcm9tICNkYXRhc2V0XHJcbldpZGdldC5wcm90b3R5cGUuaW5zdGFuY2VGcm9tRGF0YXNldCA9IGZ1bmN0aW9uKGVsZW1lbnQsIGNvbnN0cnVjdG9yKVxyXG57XHJcblx0cmV0dXJuIGNvbnN0cnVjdG9yLnByb3RvdHlwZS5pbnN0YW5jZXNbZWxlbWVudC5kYXRhc2V0W2NvbnN0cnVjdG9yLnByb3RvdHlwZS5uYW1lLmNhbWVsKCdpZCcpXV07XHJcbn07XHJcblxyXG5XaWRnZXQucHJvdG90eXBlLnNldFN0YXRlID0gZnVuY3Rpb24oc3RhdGUpXHJcbntcclxuXHR2YXIgbWV0aG9kTmFtZSA9ICdzZXRTdGF0ZScgKyBmaXJzdFVwcGVyQ2FzZShzdGF0ZSk7XHJcblx0aWYgKHR5cGVvZiB0aGlzW21ldGhvZE5hbWVdID09ICdmdW5jdGlvbicpIHtcclxuXHRcdHRoaXNbbWV0aG9kTmFtZV0oKTtcclxuXHR9XHJcbn07XHJcblxyXG5XaWRnZXQucHJvdG90eXBlLmNsZWFyU3RhdGUgPSBmdW5jdGlvbihzdGF0ZSlcclxue1xyXG5cdGlmIChzdGF0ZSA9PSAnYWxsJykge1xyXG5cdFx0Ly8g0L7QsdGF0L7QtCDQstGB0LXRhVxyXG5cdFx0Zm9yICh2YXIgcHJvcGVydHkgaW4gdGhpcykge1xyXG5cdFx0XHRpZiAocHJvcGVydHkubWF0Y2goL15jbGVhclN0YXRlKD8hQWxsKS4rLykpIHtcclxuXHRcdFx0XHR0aGlzW3Byb3BlcnR5XSgpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdGlmICghc3RhdGUpIHtcclxuXHRcdFx0aWYgKCF0aGlzLnN0YXRlKSByZXR1cm47XHJcblx0XHRcdHN0YXRlID0gdGhpcy5zdGF0ZTtcclxuXHRcdH1cclxuXHRcdHZhciBtZXRob2ROYW1lID0gJ2NsZWFyU3RhdGUnICsgZmlyc3RVcHBlckNhc2Uoc3RhdGUpO1xyXG5cdFx0aWYgKHR5cGVvZiB0aGlzW21ldGhvZE5hbWVdID09ICdmdW5jdGlvbicpIHtcclxuXHRcdFx0dGhpc1ttZXRob2ROYW1lXSgpO1xyXG5cdFx0fVxyXG5cdH1cclxufTsiLCIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMS0yMDE0IEZlbGl4IEduYXNzXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9zcGluLmpzLm9yZy9cbiAqXG4gKiBFeGFtcGxlOlxuICAgIHZhciBvcHRzID0ge1xuICAgICAgbGluZXM6IDEyICAgICAgICAgICAgIC8vIFRoZSBudW1iZXIgb2YgbGluZXMgdG8gZHJhd1xuICAgICwgbGVuZ3RoOiA3ICAgICAgICAgICAgIC8vIFRoZSBsZW5ndGggb2YgZWFjaCBsaW5lXG4gICAgLCB3aWR0aDogNSAgICAgICAgICAgICAgLy8gVGhlIGxpbmUgdGhpY2tuZXNzXG4gICAgLCByYWRpdXM6IDEwICAgICAgICAgICAgLy8gVGhlIHJhZGl1cyBvZiB0aGUgaW5uZXIgY2lyY2xlXG4gICAgLCBzY2FsZTogMS4wICAgICAgICAgICAgLy8gU2NhbGVzIG92ZXJhbGwgc2l6ZSBvZiB0aGUgc3Bpbm5lclxuICAgICwgY29ybmVyczogMSAgICAgICAgICAgIC8vIFJvdW5kbmVzcyAoMC4uMSlcbiAgICAsIGNvbG9yOiAnIzAwMCcgICAgICAgICAvLyAjcmdiIG9yICNycmdnYmJcbiAgICAsIG9wYWNpdHk6IDEvNCAgICAgICAgICAvLyBPcGFjaXR5IG9mIHRoZSBsaW5lc1xuICAgICwgcm90YXRlOiAwICAgICAgICAgICAgIC8vIFJvdGF0aW9uIG9mZnNldFxuICAgICwgZGlyZWN0aW9uOiAxICAgICAgICAgIC8vIDE6IGNsb2Nrd2lzZSwgLTE6IGNvdW50ZXJjbG9ja3dpc2VcbiAgICAsIHNwZWVkOiAxICAgICAgICAgICAgICAvLyBSb3VuZHMgcGVyIHNlY29uZFxuICAgICwgdHJhaWw6IDEwMCAgICAgICAgICAgIC8vIEFmdGVyZ2xvdyBwZXJjZW50YWdlXG4gICAgLCBmcHM6IDIwICAgICAgICAgICAgICAgLy8gRnJhbWVzIHBlciBzZWNvbmQgd2hlbiB1c2luZyBzZXRUaW1lb3V0KClcbiAgICAsIHpJbmRleDogMmU5ICAgICAgICAgICAvLyBVc2UgYSBoaWdoIHotaW5kZXggYnkgZGVmYXVsdFxuICAgICwgY2xhc3NOYW1lOiAnc3Bpbm5lcicgIC8vIENTUyBjbGFzcyB0byBhc3NpZ24gdG8gdGhlIGVsZW1lbnRcbiAgICAsIHRvcDogJzUwJScgICAgICAgICAgICAvLyBjZW50ZXIgdmVydGljYWxseVxuICAgICwgbGVmdDogJzUwJScgICAgICAgICAgIC8vIGNlbnRlciBob3Jpem9udGFsbHlcbiAgICAsIHNoYWRvdzogZmFsc2UgICAgICAgICAvLyBXaGV0aGVyIHRvIHJlbmRlciBhIHNoYWRvd1xuICAgICwgaHdhY2NlbDogZmFsc2UgICAgICAgIC8vIFdoZXRoZXIgdG8gdXNlIGhhcmR3YXJlIGFjY2VsZXJhdGlvbiAobWlnaHQgYmUgYnVnZ3kpXG4gICAgLCBwb3NpdGlvbjogJ2Fic29sdXRlJyAgLy8gRWxlbWVudCBwb3NpdGlvbmluZ1xuICAgIH1cbiAgICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2ZvbycpXG4gICAgdmFyIHNwaW5uZXIgPSBuZXcgU3Bpbm5lcihvcHRzKS5zcGluKHRhcmdldClcbiAqL1xuOyhmdW5jdGlvbiAocm9vdCwgZmFjdG9yeSkge1xuXG4gIC8qIENvbW1vbkpTICovXG4gIGlmICh0eXBlb2YgbW9kdWxlID09ICdvYmplY3QnICYmIG1vZHVsZS5leHBvcnRzKSBtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKVxuXG4gIC8qIEFNRCBtb2R1bGUgKi9cbiAgZWxzZSBpZiAodHlwZW9mIGRlZmluZSA9PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIGRlZmluZShmYWN0b3J5KVxuXG4gIC8qIEJyb3dzZXIgZ2xvYmFsICovXG4gIGVsc2Ugcm9vdC5TcGlubmVyID0gZmFjdG9yeSgpXG59KHRoaXMsIGZ1bmN0aW9uICgpIHtcbiAgXCJ1c2Ugc3RyaWN0XCJcblxuICB2YXIgcHJlZml4ZXMgPSBbJ3dlYmtpdCcsICdNb3onLCAnbXMnLCAnTyddIC8qIFZlbmRvciBwcmVmaXhlcyAqL1xuICAgICwgYW5pbWF0aW9ucyA9IHt9IC8qIEFuaW1hdGlvbiBydWxlcyBrZXllZCBieSB0aGVpciBuYW1lICovXG4gICAgLCB1c2VDc3NBbmltYXRpb25zIC8qIFdoZXRoZXIgdG8gdXNlIENTUyBhbmltYXRpb25zIG9yIHNldFRpbWVvdXQgKi9cbiAgICAsIHNoZWV0IC8qIEEgc3R5bGVzaGVldCB0byBob2xkIHRoZSBAa2V5ZnJhbWUgb3IgVk1MIHJ1bGVzLiAqL1xuXG4gIC8qKlxuICAgKiBVdGlsaXR5IGZ1bmN0aW9uIHRvIGNyZWF0ZSBlbGVtZW50cy4gSWYgbm8gdGFnIG5hbWUgaXMgZ2l2ZW4sXG4gICAqIGEgRElWIGlzIGNyZWF0ZWQuIE9wdGlvbmFsbHkgcHJvcGVydGllcyBjYW4gYmUgcGFzc2VkLlxuICAgKi9cbiAgZnVuY3Rpb24gY3JlYXRlRWwgKHRhZywgcHJvcCkge1xuICAgIHZhciBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQodGFnIHx8ICdkaXYnKVxuICAgICAgLCBuXG5cbiAgICBmb3IgKG4gaW4gcHJvcCkgZWxbbl0gPSBwcm9wW25dXG4gICAgcmV0dXJuIGVsXG4gIH1cblxuICAvKipcbiAgICogQXBwZW5kcyBjaGlsZHJlbiBhbmQgcmV0dXJucyB0aGUgcGFyZW50LlxuICAgKi9cbiAgZnVuY3Rpb24gaW5zIChwYXJlbnQgLyogY2hpbGQxLCBjaGlsZDIsIC4uLiovKSB7XG4gICAgZm9yICh2YXIgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICBwYXJlbnQuYXBwZW5kQ2hpbGQoYXJndW1lbnRzW2ldKVxuICAgIH1cblxuICAgIHJldHVybiBwYXJlbnRcbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGVzIGFuIG9wYWNpdHkga2V5ZnJhbWUgYW5pbWF0aW9uIHJ1bGUgYW5kIHJldHVybnMgaXRzIG5hbWUuXG4gICAqIFNpbmNlIG1vc3QgbW9iaWxlIFdlYmtpdHMgaGF2ZSB0aW1pbmcgaXNzdWVzIHdpdGggYW5pbWF0aW9uLWRlbGF5LFxuICAgKiB3ZSBjcmVhdGUgc2VwYXJhdGUgcnVsZXMgZm9yIGVhY2ggbGluZS9zZWdtZW50LlxuICAgKi9cbiAgZnVuY3Rpb24gYWRkQW5pbWF0aW9uIChhbHBoYSwgdHJhaWwsIGksIGxpbmVzKSB7XG4gICAgdmFyIG5hbWUgPSBbJ29wYWNpdHknLCB0cmFpbCwgfn4oYWxwaGEgKiAxMDApLCBpLCBsaW5lc10uam9pbignLScpXG4gICAgICAsIHN0YXJ0ID0gMC4wMSArIGkvbGluZXMgKiAxMDBcbiAgICAgICwgeiA9IE1hdGgubWF4KDEgLSAoMS1hbHBoYSkgLyB0cmFpbCAqICgxMDAtc3RhcnQpLCBhbHBoYSlcbiAgICAgICwgcHJlZml4ID0gdXNlQ3NzQW5pbWF0aW9ucy5zdWJzdHJpbmcoMCwgdXNlQ3NzQW5pbWF0aW9ucy5pbmRleE9mKCdBbmltYXRpb24nKSkudG9Mb3dlckNhc2UoKVxuICAgICAgLCBwcmUgPSBwcmVmaXggJiYgJy0nICsgcHJlZml4ICsgJy0nIHx8ICcnXG5cbiAgICBpZiAoIWFuaW1hdGlvbnNbbmFtZV0pIHtcbiAgICAgIHNoZWV0Lmluc2VydFJ1bGUoXG4gICAgICAgICdAJyArIHByZSArICdrZXlmcmFtZXMgJyArIG5hbWUgKyAneycgK1xuICAgICAgICAnMCV7b3BhY2l0eTonICsgeiArICd9JyArXG4gICAgICAgIHN0YXJ0ICsgJyV7b3BhY2l0eTonICsgYWxwaGEgKyAnfScgK1xuICAgICAgICAoc3RhcnQrMC4wMSkgKyAnJXtvcGFjaXR5OjF9JyArXG4gICAgICAgIChzdGFydCt0cmFpbCkgJSAxMDAgKyAnJXtvcGFjaXR5OicgKyBhbHBoYSArICd9JyArXG4gICAgICAgICcxMDAle29wYWNpdHk6JyArIHogKyAnfScgK1xuICAgICAgICAnfScsIHNoZWV0LmNzc1J1bGVzLmxlbmd0aClcblxuICAgICAgYW5pbWF0aW9uc1tuYW1lXSA9IDFcbiAgICB9XG5cbiAgICByZXR1cm4gbmFtZVxuICB9XG5cbiAgLyoqXG4gICAqIFRyaWVzIHZhcmlvdXMgdmVuZG9yIHByZWZpeGVzIGFuZCByZXR1cm5zIHRoZSBmaXJzdCBzdXBwb3J0ZWQgcHJvcGVydHkuXG4gICAqL1xuICBmdW5jdGlvbiB2ZW5kb3IgKGVsLCBwcm9wKSB7XG4gICAgdmFyIHMgPSBlbC5zdHlsZVxuICAgICAgLCBwcFxuICAgICAgLCBpXG5cbiAgICBwcm9wID0gcHJvcC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHByb3Auc2xpY2UoMSlcbiAgICBpZiAoc1twcm9wXSAhPT0gdW5kZWZpbmVkKSByZXR1cm4gcHJvcFxuICAgIGZvciAoaSA9IDA7IGkgPCBwcmVmaXhlcy5sZW5ndGg7IGkrKykge1xuICAgICAgcHAgPSBwcmVmaXhlc1tpXStwcm9wXG4gICAgICBpZiAoc1twcF0gIT09IHVuZGVmaW5lZCkgcmV0dXJuIHBwXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFNldHMgbXVsdGlwbGUgc3R5bGUgcHJvcGVydGllcyBhdCBvbmNlLlxuICAgKi9cbiAgZnVuY3Rpb24gY3NzIChlbCwgcHJvcCkge1xuICAgIGZvciAodmFyIG4gaW4gcHJvcCkge1xuICAgICAgZWwuc3R5bGVbdmVuZG9yKGVsLCBuKSB8fCBuXSA9IHByb3Bbbl1cbiAgICB9XG5cbiAgICByZXR1cm4gZWxcbiAgfVxuXG4gIC8qKlxuICAgKiBGaWxscyBpbiBkZWZhdWx0IHZhbHVlcy5cbiAgICovXG4gIGZ1bmN0aW9uIG1lcmdlIChvYmopIHtcbiAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlZiA9IGFyZ3VtZW50c1tpXVxuICAgICAgZm9yICh2YXIgbiBpbiBkZWYpIHtcbiAgICAgICAgaWYgKG9ialtuXSA9PT0gdW5kZWZpbmVkKSBvYmpbbl0gPSBkZWZbbl1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG9ialxuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIGxpbmUgY29sb3IgZnJvbSB0aGUgZ2l2ZW4gc3RyaW5nIG9yIGFycmF5LlxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0Q29sb3IgKGNvbG9yLCBpZHgpIHtcbiAgICByZXR1cm4gdHlwZW9mIGNvbG9yID09ICdzdHJpbmcnID8gY29sb3IgOiBjb2xvcltpZHggJSBjb2xvci5sZW5ndGhdXG4gIH1cblxuICAvLyBCdWlsdC1pbiBkZWZhdWx0c1xuXG4gIHZhciBkZWZhdWx0cyA9IHtcbiAgICBsaW5lczogMTIgICAgICAgICAgICAgLy8gVGhlIG51bWJlciBvZiBsaW5lcyB0byBkcmF3XG4gICwgbGVuZ3RoOiA3ICAgICAgICAgICAgIC8vIFRoZSBsZW5ndGggb2YgZWFjaCBsaW5lXG4gICwgd2lkdGg6IDUgICAgICAgICAgICAgIC8vIFRoZSBsaW5lIHRoaWNrbmVzc1xuICAsIHJhZGl1czogMTAgICAgICAgICAgICAvLyBUaGUgcmFkaXVzIG9mIHRoZSBpbm5lciBjaXJjbGVcbiAgLCBzY2FsZTogMS4wICAgICAgICAgICAgLy8gU2NhbGVzIG92ZXJhbGwgc2l6ZSBvZiB0aGUgc3Bpbm5lclxuICAsIGNvcm5lcnM6IDEgICAgICAgICAgICAvLyBSb3VuZG5lc3MgKDAuLjEpXG4gICwgY29sb3I6ICcjMDAwJyAgICAgICAgIC8vICNyZ2Igb3IgI3JyZ2diYlxuICAsIG9wYWNpdHk6IDEvNCAgICAgICAgICAvLyBPcGFjaXR5IG9mIHRoZSBsaW5lc1xuICAsIHJvdGF0ZTogMCAgICAgICAgICAgICAvLyBSb3RhdGlvbiBvZmZzZXRcbiAgLCBkaXJlY3Rpb246IDEgICAgICAgICAgLy8gMTogY2xvY2t3aXNlLCAtMTogY291bnRlcmNsb2Nrd2lzZVxuICAsIHNwZWVkOiAxICAgICAgICAgICAgICAvLyBSb3VuZHMgcGVyIHNlY29uZFxuICAsIHRyYWlsOiAxMDAgICAgICAgICAgICAvLyBBZnRlcmdsb3cgcGVyY2VudGFnZVxuICAsIGZwczogMjAgICAgICAgICAgICAgICAvLyBGcmFtZXMgcGVyIHNlY29uZCB3aGVuIHVzaW5nIHNldFRpbWVvdXQoKVxuICAsIHpJbmRleDogMmU5ICAgICAgICAgICAvLyBVc2UgYSBoaWdoIHotaW5kZXggYnkgZGVmYXVsdFxuICAsIGNsYXNzTmFtZTogJ3NwaW5uZXInICAvLyBDU1MgY2xhc3MgdG8gYXNzaWduIHRvIHRoZSBlbGVtZW50XG4gICwgdG9wOiAnNTAlJyAgICAgICAgICAgIC8vIGNlbnRlciB2ZXJ0aWNhbGx5XG4gICwgbGVmdDogJzUwJScgICAgICAgICAgIC8vIGNlbnRlciBob3Jpem9udGFsbHlcbiAgLCBzaGFkb3c6IGZhbHNlICAgICAgICAgLy8gV2hldGhlciB0byByZW5kZXIgYSBzaGFkb3dcbiAgLCBod2FjY2VsOiBmYWxzZSAgICAgICAgLy8gV2hldGhlciB0byB1c2UgaGFyZHdhcmUgYWNjZWxlcmF0aW9uIChtaWdodCBiZSBidWdneSlcbiAgLCBwb3NpdGlvbjogJ2Fic29sdXRlJyAgLy8gRWxlbWVudCBwb3NpdGlvbmluZ1xuICB9XG5cbiAgLyoqIFRoZSBjb25zdHJ1Y3RvciAqL1xuICBmdW5jdGlvbiBTcGlubmVyIChvKSB7XG4gICAgdGhpcy5vcHRzID0gbWVyZ2UobyB8fCB7fSwgU3Bpbm5lci5kZWZhdWx0cywgZGVmYXVsdHMpXG4gIH1cblxuICAvLyBHbG9iYWwgZGVmYXVsdHMgdGhhdCBvdmVycmlkZSB0aGUgYnVpbHQtaW5zOlxuICBTcGlubmVyLmRlZmF1bHRzID0ge31cblxuICBtZXJnZShTcGlubmVyLnByb3RvdHlwZSwge1xuICAgIC8qKlxuICAgICAqIEFkZHMgdGhlIHNwaW5uZXIgdG8gdGhlIGdpdmVuIHRhcmdldCBlbGVtZW50LiBJZiB0aGlzIGluc3RhbmNlIGlzIGFscmVhZHlcbiAgICAgKiBzcGlubmluZywgaXQgaXMgYXV0b21hdGljYWxseSByZW1vdmVkIGZyb20gaXRzIHByZXZpb3VzIHRhcmdldCBiIGNhbGxpbmdcbiAgICAgKiBzdG9wKCkgaW50ZXJuYWxseS5cbiAgICAgKi9cbiAgICBzcGluOiBmdW5jdGlvbiAodGFyZ2V0KSB7XG4gICAgICB0aGlzLnN0b3AoKVxuXG4gICAgICB2YXIgc2VsZiA9IHRoaXNcbiAgICAgICAgLCBvID0gc2VsZi5vcHRzXG4gICAgICAgICwgZWwgPSBzZWxmLmVsID0gY3JlYXRlRWwobnVsbCwge2NsYXNzTmFtZTogby5jbGFzc05hbWV9KVxuXG4gICAgICBjc3MoZWwsIHtcbiAgICAgICAgcG9zaXRpb246IG8ucG9zaXRpb25cbiAgICAgICwgd2lkdGg6IDBcbiAgICAgICwgekluZGV4OiBvLnpJbmRleFxuICAgICAgLCBsZWZ0OiBvLmxlZnRcbiAgICAgICwgdG9wOiBvLnRvcFxuICAgICAgfSlcblxuICAgICAgaWYgKHRhcmdldCkge1xuICAgICAgICB0YXJnZXQuaW5zZXJ0QmVmb3JlKGVsLCB0YXJnZXQuZmlyc3RDaGlsZCB8fCBudWxsKVxuICAgICAgfVxuXG4gICAgICBlbC5zZXRBdHRyaWJ1dGUoJ3JvbGUnLCAncHJvZ3Jlc3NiYXInKVxuICAgICAgc2VsZi5saW5lcyhlbCwgc2VsZi5vcHRzKVxuXG4gICAgICBpZiAoIXVzZUNzc0FuaW1hdGlvbnMpIHtcbiAgICAgICAgLy8gTm8gQ1NTIGFuaW1hdGlvbiBzdXBwb3J0LCB1c2Ugc2V0VGltZW91dCgpIGluc3RlYWRcbiAgICAgICAgdmFyIGkgPSAwXG4gICAgICAgICAgLCBzdGFydCA9IChvLmxpbmVzIC0gMSkgKiAoMSAtIG8uZGlyZWN0aW9uKSAvIDJcbiAgICAgICAgICAsIGFscGhhXG4gICAgICAgICAgLCBmcHMgPSBvLmZwc1xuICAgICAgICAgICwgZiA9IGZwcyAvIG8uc3BlZWRcbiAgICAgICAgICAsIG9zdGVwID0gKDEgLSBvLm9wYWNpdHkpIC8gKGYgKiBvLnRyYWlsIC8gMTAwKVxuICAgICAgICAgICwgYXN0ZXAgPSBmIC8gby5saW5lc1xuXG4gICAgICAgIDsoZnVuY3Rpb24gYW5pbSAoKSB7XG4gICAgICAgICAgaSsrXG4gICAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBvLmxpbmVzOyBqKyspIHtcbiAgICAgICAgICAgIGFscGhhID0gTWF0aC5tYXgoMSAtIChpICsgKG8ubGluZXMgLSBqKSAqIGFzdGVwKSAlIGYgKiBvc3RlcCwgby5vcGFjaXR5KVxuXG4gICAgICAgICAgICBzZWxmLm9wYWNpdHkoZWwsIGogKiBvLmRpcmVjdGlvbiArIHN0YXJ0LCBhbHBoYSwgbylcbiAgICAgICAgICB9XG4gICAgICAgICAgc2VsZi50aW1lb3V0ID0gc2VsZi5lbCAmJiBzZXRUaW1lb3V0KGFuaW0sIH5+KDEwMDAgLyBmcHMpKVxuICAgICAgICB9KSgpXG4gICAgICB9XG4gICAgICByZXR1cm4gc2VsZlxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFN0b3BzIGFuZCByZW1vdmVzIHRoZSBTcGlubmVyLlxuICAgICAqL1xuICAsIHN0b3A6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBlbCA9IHRoaXMuZWxcbiAgICAgIGlmIChlbCkge1xuICAgICAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KVxuICAgICAgICBpZiAoZWwucGFyZW50Tm9kZSkgZWwucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChlbClcbiAgICAgICAgdGhpcy5lbCA9IHVuZGVmaW5lZFxuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXNcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJbnRlcm5hbCBtZXRob2QgdGhhdCBkcmF3cyB0aGUgaW5kaXZpZHVhbCBsaW5lcy4gV2lsbCBiZSBvdmVyd3JpdHRlblxuICAgICAqIGluIFZNTCBmYWxsYmFjayBtb2RlIGJlbG93LlxuICAgICAqL1xuICAsIGxpbmVzOiBmdW5jdGlvbiAoZWwsIG8pIHtcbiAgICAgIHZhciBpID0gMFxuICAgICAgICAsIHN0YXJ0ID0gKG8ubGluZXMgLSAxKSAqICgxIC0gby5kaXJlY3Rpb24pIC8gMlxuICAgICAgICAsIHNlZ1xuXG4gICAgICBmdW5jdGlvbiBmaWxsIChjb2xvciwgc2hhZG93KSB7XG4gICAgICAgIHJldHVybiBjc3MoY3JlYXRlRWwoKSwge1xuICAgICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnXG4gICAgICAgICwgd2lkdGg6IG8uc2NhbGUgKiAoby5sZW5ndGggKyBvLndpZHRoKSArICdweCdcbiAgICAgICAgLCBoZWlnaHQ6IG8uc2NhbGUgKiBvLndpZHRoICsgJ3B4J1xuICAgICAgICAsIGJhY2tncm91bmQ6IGNvbG9yXG4gICAgICAgICwgYm94U2hhZG93OiBzaGFkb3dcbiAgICAgICAgLCB0cmFuc2Zvcm1PcmlnaW46ICdsZWZ0J1xuICAgICAgICAsIHRyYW5zZm9ybTogJ3JvdGF0ZSgnICsgfn4oMzYwL28ubGluZXMqaSArIG8ucm90YXRlKSArICdkZWcpIHRyYW5zbGF0ZSgnICsgby5zY2FsZSpvLnJhZGl1cyArICdweCcgKyAnLDApJ1xuICAgICAgICAsIGJvcmRlclJhZGl1czogKG8uY29ybmVycyAqIG8uc2NhbGUgKiBvLndpZHRoID4+IDEpICsgJ3B4J1xuICAgICAgICB9KVxuICAgICAgfVxuXG4gICAgICBmb3IgKDsgaSA8IG8ubGluZXM7IGkrKykge1xuICAgICAgICBzZWcgPSBjc3MoY3JlYXRlRWwoKSwge1xuICAgICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnXG4gICAgICAgICwgdG9wOiAxICsgfihvLnNjYWxlICogby53aWR0aCAvIDIpICsgJ3B4J1xuICAgICAgICAsIHRyYW5zZm9ybTogby5od2FjY2VsID8gJ3RyYW5zbGF0ZTNkKDAsMCwwKScgOiAnJ1xuICAgICAgICAsIG9wYWNpdHk6IG8ub3BhY2l0eVxuICAgICAgICAsIGFuaW1hdGlvbjogdXNlQ3NzQW5pbWF0aW9ucyAmJiBhZGRBbmltYXRpb24oby5vcGFjaXR5LCBvLnRyYWlsLCBzdGFydCArIGkgKiBvLmRpcmVjdGlvbiwgby5saW5lcykgKyAnICcgKyAxIC8gby5zcGVlZCArICdzIGxpbmVhciBpbmZpbml0ZSdcbiAgICAgICAgfSlcblxuICAgICAgICBpZiAoby5zaGFkb3cpIGlucyhzZWcsIGNzcyhmaWxsKCcjMDAwJywgJzAgMCA0cHggIzAwMCcpLCB7dG9wOiAnMnB4J30pKVxuICAgICAgICBpbnMoZWwsIGlucyhzZWcsIGZpbGwoZ2V0Q29sb3Ioby5jb2xvciwgaSksICcwIDAgMXB4IHJnYmEoMCwwLDAsLjEpJykpKVxuICAgICAgfVxuICAgICAgcmV0dXJuIGVsXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW50ZXJuYWwgbWV0aG9kIHRoYXQgYWRqdXN0cyB0aGUgb3BhY2l0eSBvZiBhIHNpbmdsZSBsaW5lLlxuICAgICAqIFdpbGwgYmUgb3ZlcndyaXR0ZW4gaW4gVk1MIGZhbGxiYWNrIG1vZGUgYmVsb3cuXG4gICAgICovXG4gICwgb3BhY2l0eTogZnVuY3Rpb24gKGVsLCBpLCB2YWwpIHtcbiAgICAgIGlmIChpIDwgZWwuY2hpbGROb2Rlcy5sZW5ndGgpIGVsLmNoaWxkTm9kZXNbaV0uc3R5bGUub3BhY2l0eSA9IHZhbFxuICAgIH1cblxuICB9KVxuXG5cbiAgZnVuY3Rpb24gaW5pdFZNTCAoKSB7XG5cbiAgICAvKiBVdGlsaXR5IGZ1bmN0aW9uIHRvIGNyZWF0ZSBhIFZNTCB0YWcgKi9cbiAgICBmdW5jdGlvbiB2bWwgKHRhZywgYXR0cikge1xuICAgICAgcmV0dXJuIGNyZWF0ZUVsKCc8JyArIHRhZyArICcgeG1sbnM9XCJ1cm46c2NoZW1hcy1taWNyb3NvZnQuY29tOnZtbFwiIGNsYXNzPVwic3Bpbi12bWxcIj4nLCBhdHRyKVxuICAgIH1cblxuICAgIC8vIE5vIENTUyB0cmFuc2Zvcm1zIGJ1dCBWTUwgc3VwcG9ydCwgYWRkIGEgQ1NTIHJ1bGUgZm9yIFZNTCBlbGVtZW50czpcbiAgICBzaGVldC5hZGRSdWxlKCcuc3Bpbi12bWwnLCAnYmVoYXZpb3I6dXJsKCNkZWZhdWx0I1ZNTCknKVxuXG4gICAgU3Bpbm5lci5wcm90b3R5cGUubGluZXMgPSBmdW5jdGlvbiAoZWwsIG8pIHtcbiAgICAgIHZhciByID0gby5zY2FsZSAqIChvLmxlbmd0aCArIG8ud2lkdGgpXG4gICAgICAgICwgcyA9IG8uc2NhbGUgKiAyICogclxuXG4gICAgICBmdW5jdGlvbiBncnAgKCkge1xuICAgICAgICByZXR1cm4gY3NzKFxuICAgICAgICAgIHZtbCgnZ3JvdXAnLCB7XG4gICAgICAgICAgICBjb29yZHNpemU6IHMgKyAnICcgKyBzXG4gICAgICAgICAgLCBjb29yZG9yaWdpbjogLXIgKyAnICcgKyAtclxuICAgICAgICAgIH0pXG4gICAgICAgICwgeyB3aWR0aDogcywgaGVpZ2h0OiBzIH1cbiAgICAgICAgKVxuICAgICAgfVxuXG4gICAgICB2YXIgbWFyZ2luID0gLShvLndpZHRoICsgby5sZW5ndGgpICogby5zY2FsZSAqIDIgKyAncHgnXG4gICAgICAgICwgZyA9IGNzcyhncnAoKSwge3Bvc2l0aW9uOiAnYWJzb2x1dGUnLCB0b3A6IG1hcmdpbiwgbGVmdDogbWFyZ2lufSlcbiAgICAgICAgLCBpXG5cbiAgICAgIGZ1bmN0aW9uIHNlZyAoaSwgZHgsIGZpbHRlcikge1xuICAgICAgICBpbnMoXG4gICAgICAgICAgZ1xuICAgICAgICAsIGlucyhcbiAgICAgICAgICAgIGNzcyhncnAoKSwge3JvdGF0aW9uOiAzNjAgLyBvLmxpbmVzICogaSArICdkZWcnLCBsZWZ0OiB+fmR4fSlcbiAgICAgICAgICAsIGlucyhcbiAgICAgICAgICAgICAgY3NzKFxuICAgICAgICAgICAgICAgIHZtbCgncm91bmRyZWN0Jywge2FyY3NpemU6IG8uY29ybmVyc30pXG4gICAgICAgICAgICAgICwgeyB3aWR0aDogclxuICAgICAgICAgICAgICAgICwgaGVpZ2h0OiBvLnNjYWxlICogby53aWR0aFxuICAgICAgICAgICAgICAgICwgbGVmdDogby5zY2FsZSAqIG8ucmFkaXVzXG4gICAgICAgICAgICAgICAgLCB0b3A6IC1vLnNjYWxlICogby53aWR0aCA+PiAxXG4gICAgICAgICAgICAgICAgLCBmaWx0ZXI6IGZpbHRlclxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgLCB2bWwoJ2ZpbGwnLCB7Y29sb3I6IGdldENvbG9yKG8uY29sb3IsIGkpLCBvcGFjaXR5OiBvLm9wYWNpdHl9KVxuICAgICAgICAgICAgLCB2bWwoJ3N0cm9rZScsIHtvcGFjaXR5OiAwfSkgLy8gdHJhbnNwYXJlbnQgc3Ryb2tlIHRvIGZpeCBjb2xvciBibGVlZGluZyB1cG9uIG9wYWNpdHkgY2hhbmdlXG4gICAgICAgICAgICApXG4gICAgICAgICAgKVxuICAgICAgICApXG4gICAgICB9XG5cbiAgICAgIGlmIChvLnNoYWRvdylcbiAgICAgICAgZm9yIChpID0gMTsgaSA8PSBvLmxpbmVzOyBpKyspIHtcbiAgICAgICAgICBzZWcoaSwgLTIsICdwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuQmx1cihwaXhlbHJhZGl1cz0yLG1ha2VzaGFkb3c9MSxzaGFkb3dvcGFjaXR5PS4zKScpXG4gICAgICAgIH1cblxuICAgICAgZm9yIChpID0gMTsgaSA8PSBvLmxpbmVzOyBpKyspIHNlZyhpKVxuICAgICAgcmV0dXJuIGlucyhlbCwgZylcbiAgICB9XG5cbiAgICBTcGlubmVyLnByb3RvdHlwZS5vcGFjaXR5ID0gZnVuY3Rpb24gKGVsLCBpLCB2YWwsIG8pIHtcbiAgICAgIHZhciBjID0gZWwuZmlyc3RDaGlsZFxuICAgICAgbyA9IG8uc2hhZG93ICYmIG8ubGluZXMgfHwgMFxuICAgICAgaWYgKGMgJiYgaSArIG8gPCBjLmNoaWxkTm9kZXMubGVuZ3RoKSB7XG4gICAgICAgIGMgPSBjLmNoaWxkTm9kZXNbaSArIG9dOyBjID0gYyAmJiBjLmZpcnN0Q2hpbGQ7IGMgPSBjICYmIGMuZmlyc3RDaGlsZFxuICAgICAgICBpZiAoYykgYy5vcGFjaXR5ID0gdmFsXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKHR5cGVvZiBkb2N1bWVudCAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBzaGVldCA9IChmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgZWwgPSBjcmVhdGVFbCgnc3R5bGUnLCB7dHlwZSA6ICd0ZXh0L2Nzcyd9KVxuICAgICAgaW5zKGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdoZWFkJylbMF0sIGVsKVxuICAgICAgcmV0dXJuIGVsLnNoZWV0IHx8IGVsLnN0eWxlU2hlZXRcbiAgICB9KCkpXG5cbiAgICB2YXIgcHJvYmUgPSBjc3MoY3JlYXRlRWwoJ2dyb3VwJyksIHtiZWhhdmlvcjogJ3VybCgjZGVmYXVsdCNWTUwpJ30pXG5cbiAgICBpZiAoIXZlbmRvcihwcm9iZSwgJ3RyYW5zZm9ybScpICYmIHByb2JlLmFkaikgaW5pdFZNTCgpXG4gICAgZWxzZSB1c2VDc3NBbmltYXRpb25zID0gdmVuZG9yKHByb2JlLCAnYW5pbWF0aW9uJylcbiAgfVxuXG4gIHJldHVybiBTcGlubmVyXG5cbn0pKTtcbiIsImltcG9ydCBXaWRnZXQgZnJvbSAnQGVnbWwvdXRpbHMvV2lkZ2V0LmpzJztcbmltcG9ydCBOYW1lIGZyb20gJ0BlZ21sL3V0aWxzL05hbWUuanMnO1xuaW1wb3J0IHsgZXh0ZW5kLCByZW0sIGNsb3Nlc3RQYXJlbnRCeUNsYXNzLCBtYXJnaW5Cb3hIZWlnaHQgfSBmcm9tICdAZWdtbC91dGlscyc7XG5pbXBvcnQgU3Bpbm5lciQxIGZyb20gJ3NwaW4uanMnO1xuXG5mdW5jdGlvbiBNb2RhbCgpIHtcclxuXHRXaWRnZXQuY2FsbCh0aGlzLCAuLi5hcmd1bWVudHMpO1xyXG59XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShNb2RhbCwgJ25hbWUnLCB7XHJcblx0dmFsdWU6IG5ldyBOYW1lKCdNb2RhbCcsIGZhbHNlKSxcclxufSk7XHJcbiBcclxuTW9kYWwucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShXaWRnZXQucHJvdG90eXBlKTtcclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KE1vZGFsLnByb3RvdHlwZSwgJ2NvbnN0cnVjdG9yJywge1xyXG5cdHZhbHVlOiBNb2RhbCxcclxuXHRlbnVtZXJhYmxlOiBmYWxzZSxcclxuXHR3cml0YWJsZTogdHJ1ZSxcclxufSk7XHJcblxyXG52YXIgbmFtZSA9IG5ldyBOYW1lKCdtb2RhbCcpO1xyXG4gIFxyXG5Nb2RhbC5wcm90b3R5cGUubmFtZSA9IG5hbWU7XHJcbk1vZGFsLnByb3RvdHlwZS5zaG93biA9IGZhbHNlO1xyXG5Nb2RhbC5wcm90b3R5cGUubG9hZGVkID0gZmFsc2U7XHJcbk1vZGFsLnByb3RvdHlwZS5hbmltYXRlID0gdHJ1ZTtcclxuTW9kYWwucHJvdG90eXBlLnVybCA9IG51bGw7XHJcbk1vZGFsLnByb3RvdHlwZS5zcGlubmVyID0gbnVsbDtcclxuTW9kYWwucHJvdG90eXBlLmZpbmlzaFNob3dFdmVudCA9IG51bGw7XHJcbk1vZGFsLnByb3RvdHlwZS5maW5pc2hTaG93RXZlbnROYW1lID0gbmFtZS5ldmVudChbJ2ZpbmlzaCcsICdzaG93J10pO1xyXG5Nb2RhbC5wcm90b3R5cGUuZmluaXNoU2hvd0V2ZW50RGV0YWlsID0gbnVsbDtcclxuTW9kYWwucHJvdG90eXBlLmZpbmlzaEhpZGVFdmVudCA9IG51bGw7XHJcbk1vZGFsLnByb3RvdHlwZS5maW5pc2hIaWRlRXZlbnROYW1lID0gbmFtZS5ldmVudChbJ2ZpbmlzaCcsICdoaWRlJ10pO1xyXG5Nb2RhbC5wcm90b3R5cGUuZmluaXNoSGlkZUV2ZW50RGV0YWlsID0gbnVsbDtcclxuTW9kYWwucHJvdG90eXBlLmRlZmF1bHRXaWR0aCA9IDYwMDtcclxuIFxyXG5Nb2RhbC5wcm90b3R5cGUuY29uc3RydWN0aW9uQ2hhaW4gPSBmdW5jdGlvbih0YXJnZXQsIG9wdGlvbnMpXHJcbntcclxuXHRXaWRnZXQucHJvdG90eXBlLmNvbnN0cnVjdGlvbkNoYWluLmNhbGwodGhpcywgdGFyZ2V0LCBvcHRpb25zKTtcclxuXHR0aGlzLnNhdmVJbnN0YW5jZU9mKE1vZGFsKTtcclxuIFxyXG5cdGlmICh0aGlzLmhhc093blByb3BlcnR5KCdmaW5pc2hTaG93RXZlbnQnKSA9PSBmYWxzZSkge1xyXG5cdFx0dGhpcy5maW5pc2hTaG93RXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQodGhpcy5maW5pc2hTaG93RXZlbnROYW1lLCB7XHJcblx0XHRcdGRldGFpbDogZXh0ZW5kKHsgd2lkZ2V0OiB0aGlzIH0sIHRoaXMuZmluaXNoU2hvd0V2ZW50RGV0YWlsKSxcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRpZiAodGhpcy5oYXNPd25Qcm9wZXJ0eSgnZmluaXNoSGlkZUV2ZW50JykgPT0gZmFsc2UpIHtcclxuXHRcdHRoaXMuZmluaXNoSGlkZUV2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KHRoaXMuZmluaXNoSGlkZUV2ZW50TmFtZSwge1xyXG5cdFx0XHRkZXRhaWw6IGV4dGVuZCh7IHdpZGdldDogdGhpcyB9LCB0aGlzLmZpbmlzaEhpZGVFdmVudERldGFpbCksXHJcblx0XHR9KTtcclxuXHR9XHJcbn07XHJcblxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgICAgICAgIOKWiOKWiFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vL1x0ICAgICDilojilogg4paI4paIICAgICAgICAg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vL1xyXG4vL1x04paI4paIICAgICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vL1x04paI4paIICAgICDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIXHJcbi8vXHTilojiloggIOKWiCAg4paI4paIIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIIOKWiOKWiOKWiCDilojilogg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIICAg4paI4paIXHJcbi8vXHQg4paI4paI4paIIOKWiOKWiOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAg4paI4paIICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vL1xyXG4vL1x0I3NldCAjd2lkdGhcclxuTW9kYWwucHJvdG90eXBlLnNldFdpZHRoID0gZnVuY3Rpb24odmFsdWUpXHJcbntcclxuXHR2YWx1ZSA9IHZhbHVlIHx8IHRoaXMuZGVmYXVsdFdpZHRoO1xyXG5cdGlmICh0eXBlb2YgdmFsdWUgPT0gJ3N0cmluZycgfHwgdmFsdWUgaW5zdGFuY2VvZiBTdHJpbmcpIHtcclxuXHRcdHN3aXRjaCAodmFsdWUpIHtcclxuXHRcdFx0Y2FzZSAndGlnaHQnOlxyXG5cdFx0XHRcdHZhbHVlID0gMzUwO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdHZhbHVlID0gdGhpcy5kZWZhdWx0V2lkdGg7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHR9XHJcblx0fVxyXG5cdHZhciByZW1WYWx1ZSA9IHJlbSh2YWx1ZSwgdHJ1ZSk7XHJcblx0dGhpcy5kb20uYm94LnN0eWxlLndpZHRoID0gcmVtVmFsdWU7XHJcblx0dGhpcy5kb20uaWZyYW1lLnN0eWxlLndpZHRoID0gcmVtVmFsdWU7XHJcbn07XG5cbi8vXHTilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAg4paI4paI4paI4paI4paIICDilojiloggICAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cclxuTW9kYWwucHJvdG90eXBlLmluaXRpYWxpemF0aW9uQ2hhaW4gPSBmdW5jdGlvbih0YXJnZXQpXHJcbntcclxuXHRXaWRnZXQucHJvdG90eXBlLmluaXRpYWxpemF0aW9uQ2hhaW4uY2FsbCh0aGlzLCB0YXJnZXQpO1xyXG5cdHRoaXMuc2V0RGF0YXNldElkT2YoTW9kYWwpO1xyXG5cclxuXHQvLyBET00t0Y3Qu9C10LzQtdC90YLRi1xyXG5cdGlmICghdGhpcy5kb20uc2VsZikge1xyXG5cdFx0dGhyb3cgbmV3IEVycm9yKCfQlNC70Y8g0LzQvtC00LDQu9GM0L3QvtCz0L4g0L7QutC90LAg0L3QtSDQvdCw0LnQtNC10L3RiyDRjdC70LXQvNC10L3RgtGLINCyIERPTScpO1xyXG5cdH1cclxuXHR0aGlzLmRvbS5jbGlwID0gdGhpcy5kb20uc2VsZi5xdWVyeVNlbGVjdG9yKHRoaXMubmFtZS5zZWxlY3RvcignY2xpcCcpKTtcclxuXHR0aGlzLmRvbS5ib3ggPSB0aGlzLmRvbS5zZWxmLnF1ZXJ5U2VsZWN0b3IodGhpcy5uYW1lLnNlbGVjdG9yKCdib3gnKSk7XHJcblx0dGhpcy5kb20uY29udGVudCA9IHRoaXMuZG9tLnNlbGYucXVlcnlTZWxlY3Rvcih0aGlzLm5hbWUuc2VsZWN0b3IoJ2NvbnRlbnQnKSk7XHJcblx0dGhpcy5kb20uY2xvc2UgPSB0aGlzLmRvbS5zZWxmLnF1ZXJ5U2VsZWN0b3IodGhpcy5uYW1lLnNlbGVjdG9yKCdjbG9zZScpKTtcclxuXHR0aGlzLmRvbS5pZnJhbWUgPSB0aGlzLmRvbS5jb250ZW50LnF1ZXJ5U2VsZWN0b3IoJ2lmcmFtZScpO1xyXG5cdCBcclxuXHQvLyDQn9GA0LjQstGP0LfQutCwINGB0L7QsdGL0YLQuNC5XHJcblx0dGhpcy5kb20uc2VsZi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKGV2ZW50KSB7XHJcblx0XHR2YXIgbW9kYWwgPSBNb2RhbC5wcm90b3R5cGUuaW5zdGFuY2VGcm9tRGF0YXNldCh0aGlzLCBNb2RhbCk7XHJcblx0XHRpZiAoZXZlbnQudGFyZ2V0LmNsb3Nlc3QobW9kYWwubmFtZS5zZWxlY3RvcignYm94JykpID09IG51bGwpIHtcclxuXHRcdFx0bW9kYWwuaGlkZSgpO1xyXG5cdFx0fVxyXG5cdH0sIGZhbHNlKTtcclxuXHR0aGlzLmRvbS5jbG9zZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKGV2ZW50KSB7XHJcblx0XHR2YXIgbW9kYWwgPSBNb2RhbC5wcm90b3R5cGUuaW5zdGFuY2VGcm9tRGF0YXNldChcclxuXHRcdFx0Y2xvc2VzdFBhcmVudEJ5Q2xhc3MoZXZlbnQudGFyZ2V0LCBNb2RhbC5wcm90b3R5cGUubmFtZS5jc3MoKSksXHJcblx0XHRcdE1vZGFsXHJcblx0XHQpO1xyXG5cdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdG1vZGFsLmhpZGUoKTtcclxuXHR9LCBmYWxzZSk7XHJcblx0IFxyXG5cdHRoaXMuaW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG59O1xuXG4vL1x04paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiFxyXG4vL1x04paI4paIICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgICAgICAg4paI4paIXHJcbi8vXHQg4paI4paI4paI4paI4paI4paIICAgICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilohcclxuLy9cclxuLy9cdCN1dGlsc1xyXG5cclxuLy9cdOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAgIOKWiOKWiCDilojilojilojilojilojiloggICDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAg4paI4paIIOKWiOKWiCDilojiloggICAgICDilojiloggICAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojiloggICAg4paI4paIIOKWiOKWiCDilojilojilojilojiloggICDilojiloggIOKWiCAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojiloggICAgIOKWiOKWiFxyXG4vL1x0IOKWiOKWiCAg4paI4paIICDilojilogg4paI4paIICAgICAg4paI4paIIOKWiOKWiOKWiCDilojilogg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHQgIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojiloggIOKWiOKWiOKWiCDilojilojiloggIOKWiOKWiCAgICAgICDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vXHQg4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vL1x0IOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIXHJcbi8vIFxyXG4vL1x0I3ZpZXdwb3J0ICNjcm9wXHJcbmZ1bmN0aW9uIHZpZXdwb3J0Q3JvcCgpIHtcclxuXHRkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJztcclxufVxyXG4vL1x04paI4paIICAgIOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAg4paIICDilojilogg4paI4paI4paI4paI4paI4paIICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAgICAg4paI4paIXHJcbi8vXHQg4paI4paIICDilojiloggIOKWiOKWiCDilojiloggICAgICDilojilogg4paI4paI4paIIOKWiOKWiCDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdCAg4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paIIOKWiOKWiOKWiCAg4paI4paIICAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAgICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIXHJcbi8vXHTilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiFxyXG4vL1x04paI4paIICAg4paI4paIIOKWiOKWiCAgICAgICAgICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilohcclxuLy9cdOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIICAgIOKWiOKWiCAgICAg4paI4paI4paI4paI4paI4paIICDilojiloggICDilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vL1x0I3ZpZXdwb3J0ICNyZXN0b3JlXHJcbmZ1bmN0aW9uIHZpZXdwb3J0UmVzdG9yZSgpIHtcclxuXHRkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUub3ZlcmZsb3cgPSBudWxsO1xyXG59XG5cbnZhciBvcHRzID0ge1xuICBsaW5lczogMTMgLy8gVGhlIG51bWJlciBvZiBsaW5lcyB0byBkcmF3XG4sIGxlbmd0aDogNiAvLyBUaGUgbGVuZ3RoIG9mIGVhY2ggbGluZVxuLCB3aWR0aDogMiAvLyBUaGUgbGluZSB0aGlja25lc3NcbiwgcmFkaXVzOiAxMSAvLyBUaGUgcmFkaXVzIG9mIHRoZSBpbm5lciBjaXJjbGVcbiwgc2NhbGU6IDEgLy8gU2NhbGVzIG92ZXJhbGwgc2l6ZSBvZiB0aGUgc3Bpbm5lclxuLCBjb3JuZXJzOiAwIC8vIENvcm5lciByb3VuZG5lc3MgKDAuLjEpXG4sIGNvbG9yOiAnIzAwMCcgLy8gI3JnYiBvciAjcnJnZ2JiIG9yIGFycmF5IG9mIGNvbG9yc1xuLCBvcGFjaXR5OiAwLjI1IC8vIE9wYWNpdHkgb2YgdGhlIGxpbmVzXG4sIHJvdGF0ZTogMCAvLyBUaGUgcm90YXRpb24gb2Zmc2V0XG4sIGRpcmVjdGlvbjogMSAvLyAxOiBjbG9ja3dpc2UsIC0xOiBjb3VudGVyY2xvY2t3aXNlXG4sIHNwZWVkOiAxIC8vIFJvdW5kcyBwZXIgc2Vjb25kXG4sIHRyYWlsOiA2MCAvLyBBZnRlcmdsb3cgcGVyY2VudGFnZVxuLCBmcHM6IDIwIC8vIEZyYW1lcyBwZXIgc2Vjb25kIHdoZW4gdXNpbmcgc2V0VGltZW91dCgpIGFzIGEgZmFsbGJhY2sgZm9yIENTU1xuLCB6SW5kZXg6IDJlOSAvLyBUaGUgei1pbmRleCAoZGVmYXVsdHMgdG8gMjAwMDAwMDAwMClcbiwgY2xhc3NOYW1lOiAnc3Bpbm5lcicgLy8gVGhlIENTUyBjbGFzcyB0byBhc3NpZ24gdG8gdGhlIHNwaW5uZXJcbiwgdG9wOiAnNTAlJyAvLyBUb3AgcG9zaXRpb24gcmVsYXRpdmUgdG8gcGFyZW50XG4sIGxlZnQ6ICc1MCUnIC8vIExlZnQgcG9zaXRpb24gcmVsYXRpdmUgdG8gcGFyZW50XG4sIHNoYWRvdzogZmFsc2UgLy8gV2hldGhlciB0byByZW5kZXIgYSBzaGFkb3dcbiwgaHdhY2NlbDogZmFsc2UgLy8gV2hldGhlciB0byB1c2UgaGFyZHdhcmUgYWNjZWxlcmF0aW9uXG4sIHBvc2l0aW9uOiAnYWJzb2x1dGUnIC8vIEVsZW1lbnQgcG9zaXRpb25pbmdcbn07XG5cbnZhciBTcGlubmVyID0gbmV3IFNwaW5uZXIkMShvcHRzKTtcblxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAgICDilojilohcclxuXHJcbk1vZGFsLnByb3RvdHlwZS5zaG93ID0gZnVuY3Rpb24obmV3T3B0aW9ucylcclxue1xyXG5cdGlmICghdGhpcy5pbml0aWFsaXplZCB8fCB0aGlzLmJ1c3kgfHwgdGhpcy5zaG93bikgcmV0dXJuO1xyXG5cdHRoaXMuYnVzeSA9IHRydWU7XHJcblx0dGhpcy5zZXRPcHRpb25zKG5ld09wdGlvbnMpO1xyXG5cdCBcclxuXHQvLyDQodC60YDRi9GC0Ywg0L/RgNC+0LrRgNGD0YLQutGDINGB0YLRgNCw0L3QuNGG0YtcclxuXHR2aWV3cG9ydENyb3AoKTtcclxuXHRcclxuXHQvLyDQn9C+0LrQsNC3INC00LjQsNC70L7Qs9C+0LLQvtCz0L4g0L7QutC90LBcclxuXHR0aGlzLmRvbS5zZWxmLnN0eWxlLmRpc3BsYXkgPSBudWxsO1xyXG4gXHJcblx0aWYgKHRoaXMuYW5pbWF0ZSA9PSBmYWxzZSkge1xyXG5cdFx0ZmluaXNoLmNhbGwodGhpcyk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHRoaXMuZG9tLmNsaXAuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gJ3RyYW5zcGFyZW50JztcclxuXHRcdHRoaXMuZG9tLmJveC5zdHlsZS5vcGFjaXR5ID0gMDtcclxuXHRcdHRoaXMuZG9tLmJveC5zdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlWSgzcmVtKSc7XHJcblx0XHQvLyB0aGlzLmRvbS5ib3guc3R5bGUudHJhbnNmb3JtID0gJ3RyYW5zbGF0ZVkoM3JlbSkgc2NhbGUoLjgpJztcclxuXHRcdC8vIHRoaXMuZG9tLmJveC5zdHlsZS50cmFuc2Zvcm0gPSAnc2NhbGUoLjUpJztcclxuXHRcdHRoaXMuZG9tLmlmcmFtZS5zdHlsZS5vcGFjaXR5ID0gMDtcclxuXHRcdGRvY3VtZW50LmJvZHkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7IC8vICNyZWZsb3dcclxuXHRcdHRoaXMuZG9tLmNsaXAuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gbnVsbDtcclxuXHRcdHdpbmRvdy5zZXRUaW1lb3V0KChmdW5jdGlvbigpIHtcclxuXHRcdFx0dGhpcy5kb20uYm94LnN0eWxlLm9wYWNpdHkgPSBudWxsO1xyXG5cdFx0XHR0aGlzLmRvbS5ib3guc3R5bGUudHJhbnNmb3JtID0gbnVsbDtcclxuXHRcdFx0d2luZG93LnNldFRpbWVvdXQoZmluaXNoLmJpbmQodGhpcyksIDQwMCk7XHJcblx0XHR9KS5iaW5kKHRoaXMpLCAzMDApO1xyXG5cdH1cclxufTtcclxuIFxyXG5mdW5jdGlvbiBmaW5pc2goKSB7XHJcblx0dGhpcy5idXN5ID0gZmFsc2U7XHJcblx0dGhpcy5zaG93biA9IHRydWU7XHJcblxyXG5cdHRoaXMuZXNjYXBlS2V5SGl0SGFuZGxlciA9IChmdW5jdGlvbihldmVudCkge1xyXG5cdFx0aWYgKChldmVudC5rZXkgPyBldmVudC5rZXkgPT0gJ0VzY2FwZScgOiBldmVudC5rZXlDb2RlID09IDI3KSAmJiB0aGlzLnNob3duKSB7XHJcblx0XHRcdHRoaXMuaGlkZSgpO1xyXG5cdFx0fVxyXG5cdH0pLmJpbmQodGhpcyk7XHJcblxyXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCB0aGlzLmVzY2FwZUtleUhpdEhhbmRsZXIsIGZhbHNlKTtcclxuXHJcblx0Ly8gI05PVEU6INCf0LXRgNC10L3QtdGB0LXQvdC+INGB0Y7QtNCwINC40Lcgc2hvdyDQv9C+0YLQvtC80YMg0YfRgtC+INC10YHQu9C4INC00LXQu9Cw0YLRjCDRgtCw0LwsINGC0L4g0L/RgNC4INC90LDQttCw0YLQuNC4IEVzYyDQtNC+INGD0YHRgtCw0L3QvtCy0LrQuCDQvtCx0YDQsNCx0L7RgtGH0LjQutCwINCy0YvRiNC1LCDQvdC+INC/0YDQuCDQvdCw0YfQsNCy0YjQtdC50YHRjyDQt9Cw0LPRgNGD0LfQutC1IGlmcmFtZS3QsCwg0LfQsNCz0YDRg9C30LrQsCBpZnJhbWUt0LAg0L/RgNC10LrRgNCw0YnQsNC70LDRgdGMINC4INC/0YDQuCDRjdGC0L7QvCDQvNC+0LTQsNC7INC+0YHRgtCw0LLQsNC70YHRjyDQvtGC0LrRgNGL0YLRi9C8LiDQn9C70Y7RgSDRgtCw0Log0LXRidC1INC80L7QttC90L4g0LLRi9C40LPRgNCw0YLRjCDQsiDQs9C70LDQtNC60L7RgdGC0Lgg0Y3RhNGE0LXQutGC0L7Qsi5cclxuXHQvLyDQl9Cw0LPRgNGD0LfQutCwINC90LDRh9Cw0LvRjNC90L7Qs9C+IFVSTFxyXG5cdHRoaXMuZG9tLmlmcmFtZS5zcmMgPSB0aGlzLnVybDtcclxuXHRcclxuXHQvLyDQotC+0LvRjNC60L4g0L/RgNC4INC/0LXRgNCy0L7QuSDQt9Cw0LPRgNGD0LfQutC1XHJcblx0dGhpcy5kb20uaWZyYW1lLmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCB0aGlzLmlmcmFtZU9uRmlyc3RMb2FkLCBmYWxzZSk7XHJcblx0XHJcblx0Ly8g0J/RgNC4INC60LDQttC00L7QuSDQt9Cw0LPRgNGD0LfQutC1INC/0L7QtNCz0L7QvdGP0YLRjCDRgNCw0LfQvNC10YAg0Lgg0LLQutC70Y7Rh9Cw0YLRjC/QstGL0LrQu9GO0YfQsNGC0Ywg0L7RgtGB0LvQtdC20LjQstCw0L3QuNC1INC40LfQvNC10L3QtdC90LjRjyDRgNCw0LfQvNC10YDQsFxyXG5cdHRoaXMuZG9tLmlmcmFtZS5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgdGhpcy5pZnJhbWVPbkVhY2hMb2FkLCBmYWxzZSk7XHJcblx0XHQgIFxyXG5cdC8vINCf0L7QutCw0Lcg0LjQvdC00LjQutCw0YLQvtGA0LAg0LfQsNCz0YDRg9C30LrQuCwg0LXRgdC70Lgg0LfQsNC/0YDQvtGI0LXQvdC90YvQuSBVUkwg0LfQsNCz0YDRg9C20LDQtdGC0YHRjyDRgdC70LjRiNC60L7QvCDQtNC+0LvQs9C+XHJcblx0Ly8gKNC00L7Qu9GM0YjQtSBpZnJhbWVVbm5vdGlmaWVkTG9hZGluZ1RpbWVvdXQpXHJcblx0dGhpcy5pZnJhbWVVbm5vdGlmaWVkTG9hZGluZ1RpbWVvdXRJZCA9IHdpbmRvdy5zZXRUaW1lb3V0KChmdW5jdGlvbigpIHtcclxuXHRcdHRoaXMuaWZyYW1lTG9hZGluZ1Rvb0xvbmcgPSB0cnVlO1xyXG5cdFx0dGhpcy5zcGlubmVyID0gU3Bpbm5lci5zcGluKHRoaXMuZG9tLmJveCk7XHJcblx0fSkuYmluZCh0aGlzKSwgdGhpcy5pZnJhbWVVbm5vdGlmaWVkTG9hZGluZ1RpbWVvdXQpO1xyXG5cclxuXHR0aGlzLmRvbS5zZWxmLmRpc3BhdGNoRXZlbnQodGhpcy5maW5pc2hTaG93RXZlbnQpO1xyXG59XG5cbi8vXHTilojiloggICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paI4paIXHJcblxyXG5Nb2RhbC5wcm90b3R5cGUuaGlkZSA9IGZ1bmN0aW9uKClcclxue1xyXG5cdGlmICh0aGlzLmluaXRpYWxpemVkICYmICF0aGlzLmJ1c3kgJiYgdGhpcy5zaG93bikge1xyXG5cdFx0dGhpcy5idXN5ID0gdHJ1ZTtcclxuXHRcdGlmICh0aGlzLmlmcmFtZUxvYWRlZCkge1xyXG5cdFx0XHR0aGlzLmlmcmFtZUxvYWRlZCA9IGZhbHNlO1xyXG5cdFx0XHQvLyDQo9Cx0YDQsNGC0Ywg0LjQvdGC0LXRgNCy0LDQuyDQstGL0YfQuNGB0LvQtdC90LjRjyDQstGL0YHQvtGC0YsgaWZyYW1lXHJcblx0XHRcdHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMuaWZyYW1lUmVzaXplSW50ZXJ2YWxJZCk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRpZiAodGhpcy5pZnJhbWVMb2FkaW5nVG9vTG9uZykge1xyXG5cdFx0XHRcdHRoaXMuaWZyYW1lTG9hZGluZ1Rvb0xvbmcgPSBmYWxzZTtcclxuXHRcdFx0XHR0aGlzLnNwaW5uZXIuc3RvcCgpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHdpbmRvdy5jbGVhclRpbWVvdXQodGhpcy5pZnJhbWVVbm5vdGlmaWVkTG9hZGluZ1RpbWVvdXRJZCk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdCAgXHJcblx0XHQvLyDQodC60YDRi9GC0LjQtSDQtNC40LDQu9C+0LPQvtCy0L7Qs9C+INC+0LrQvdCwICjQsNC90LjQvNCw0YbQuNGPINC4INC/0YDQvtGH0LXQtSlcclxuXHRcdGlmICghdGhpcy5hbmltYXRlKSB7XHJcblx0XHRcdGZpbmlzaCQxLmNhbGwodGhpcyk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHQvLyB0aGlzLmRvbS5ib3guc3R5bGUudHJhbnNmb3JtID0gJ3RyYW5zbGF0ZVkoM3JlbSknO1xyXG5cdFx0XHQvLyB0aGlzLmRvbS5ib3guc3R5bGUudHJhbnNmb3JtID0gJ3RyYW5zbGF0ZVkoM3JlbSkgc2NhbGUoLjgpJztcclxuXHRcdFx0dGhpcy5kb20uYm94LnN0eWxlLnRyYW5zZm9ybSA9ICdzY2FsZSguOCknO1xyXG5cdFx0XHR0aGlzLmRvbS5ib3guc3R5bGUub3BhY2l0eSA9IDA7XHJcblx0XHRcdC8vIHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0XHQvLyB9LCA1MCk7XHJcblx0XHRcdHdpbmRvdy5zZXRUaW1lb3V0KChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHR0aGlzLmRvbS5jbGlwLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICd0cmFuc3BhcmVudCc7XHJcblx0XHRcdH0pLmJpbmQodGhpcyksIDEwMCk7XHJcblx0XHRcdHdpbmRvdy5zZXRUaW1lb3V0KGZpbmlzaCQxLmJpbmQodGhpcyksIDUwMCk7XHJcbiBcclxuXHRcdFx0Ly8gdGhpcy5kb20uYm94LnRyYW5zaXRpb24oe1xyXG5cdFx0XHQvLyBcdHRyYW5zZm9ybTogJ3NjYWxlKC41KScsXHJcblx0XHRcdC8vIFx0ZHVyYXRpb246IHRyYW5zaXRpb25EdXJhdGlvbixcclxuXHRcdFx0Ly8gXHRlYXNpbmc6ICdjdWJpYy1iZXppZXIoMC42MiwtMC41LDEsMC4zMyknLFxyXG5cdFx0XHQvLyBcdHF1ZXVlOiBmYWxzZSxcclxuXHRcdFx0Ly8gfSk7XHJcblx0XHRcdC8vIHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0XHQvLyBcdHRoaXMuZG9tLmJveC5hbmltYXRlKHsgb3BhY2l0eTogMCB9LCBib3hGYWRlRHVyYXRpb24pO1xyXG5cdFx0XHQvLyB9LCBib3hGYWRlRGVsYXkpO1xyXG5cdFx0XHQvLyB0aGlzLmRvbS5jbGlwLnRyYW5zaXRpb24oe1xyXG5cdFx0XHQvLyBcdG9wYWNpdHk6IDAsXHJcblx0XHRcdC8vIFx0ZGVsYXk6IGNsaXBIaWRlRGVsYXksXHJcblx0XHRcdC8vIFx0ZHVyYXRpb246IHRyYW5zaXRpb25EdXJhdGlvbixcclxuXHRcdFx0Ly8gXHRjb21wbGV0ZTogZmluaXNoSGlkZSxcclxuXHRcdFx0Ly8gfSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG5cclxuZnVuY3Rpb24gZmluaXNoJDEoKSB7XHJcblx0dGhpcy5kb20uc2VsZi5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG5cdHRoaXMuYnVzeSA9IGZhbHNlO1xyXG5cdHRoaXMuc2hvd24gPSBmYWxzZTtcclxuXHR0aGlzLmRvbS5pZnJhbWUucmVtb3ZlRXZlbnRMaXN0ZW5lcignbG9hZCcsIHRoaXMuaWZyYW1lT25GaXJzdExvYWQsIGZhbHNlKTtcclxuXHR0aGlzLmRvbS5pZnJhbWUucmVtb3ZlRXZlbnRMaXN0ZW5lcignbG9hZCcsIHRoaXMuaWZyYW1lT25FYWNoTG9hZCwgZmFsc2UpO1xyXG5cdHRoaXMuZG9tLmlmcmFtZS5zcmMgPSAnYWJvdXQ6YmxhbmsnO1xyXG5cdHRoaXMuZG9tLmlmcmFtZS5zdHlsZSA9IG51bGw7XHJcblx0dGhpcy5kb20uYm94LnN0eWxlLndpZHRoID0gbnVsbDtcclxuXHR0aGlzLmRvbS5ib3guc3R5bGUuaGVpZ2h0ID0gbnVsbDtcclxuXHRkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdrZXlkb3duJywgdGhpcy5lc2NhcGVLZXlIaXRIYW5kbGVyLCBmYWxzZSk7XHJcblx0dmlld3BvcnRSZXN0b3JlKCk7XHJcblx0dGhpcy5pZnJhbWVQcmV2aW91c0hlaWdodCA9IDA7XHJcblx0ICBcclxuXHQvLyDQn9C10YDQtdC30LDQs9GA0YPQt9C60LAg0L7RgdC90L7QstC90L7Qs9C+INC+0LrQvdCwXHJcblx0Ly8g0JjRgdC/0L7Qu9GM0LfQvtCy0LDQvdC40LUgZm9yY2VkUmVsb2FkINC/0L7Qt9Cy0L7Qu9GP0LXRgiDQv9GA0LXQtNC+0YLQstGA0LDRgtC40YLRjCDQv9C+0LLRgtC+0YDQvdGD0Y5cclxuXHQvLyDQt9Cw0LPRgNGD0LfQutGDIGlmcmFtZSDQv9C+INC/0L7RgdC70LXQtNC90LXQvNGDIFVSTCDQv9GA0Lgg0L/QtdGA0LXQt9Cw0LPRgNGD0LfQutC1INGB0YLRgNCw0L3QuNGG0YtcclxuXHQvLyB3aW5kb3cubG9jYXRpb24ucmVsb2FkKHRydWUpO1xyXG5cdFxyXG5cdHRoaXMuZG9tLnNlbGYuZGlzcGF0Y2hFdmVudCh0aGlzLmZpbmlzaEhpZGVFdmVudCk7XHJcbn1cblxuLy9cdOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paIICDilojilojiloggICAg4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiFxyXG5cclxuTW9kYWwucHJvdG90eXBlLmlmcmFtZUxvYWRlZCA9IGZhbHNlO1xyXG5Nb2RhbC5wcm90b3R5cGUuaWZyYW1lUmVzaXplSW50ZXJ2YWxJZCA9IG51bGw7XHJcbk1vZGFsLnByb3RvdHlwZS5pZnJhbWVVbm5vdGlmaWVkTG9hZGluZ1RpbWVvdXQgPSA1MDA7XHJcbk1vZGFsLnByb3RvdHlwZS5pZnJhbWVVbm5vdGlmaWVkTG9hZGluZ1RpbWVvdXRJZCA9IG51bGw7XHJcbk1vZGFsLnByb3RvdHlwZS5pZnJhbWVMb2FkaW5nVG9vTG9uZyA9IGZhbHNlO1xyXG5Nb2RhbC5wcm90b3R5cGUuaWZyYW1lUHJldmlvdXNIZWlnaHQgPSBudWxsO1xyXG5Nb2RhbC5wcm90b3R5cGUuaWZyYW1lUmVzaXplSW50ZXJ2YWwgPSAxMDAwO1xyXG5cclxuLy9cdCDilojilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojiloggICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAgICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAgICDilojilohcclxuLy9cdOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIICAgICDilojilojilojilojiloggICDilojilogg4paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vXHTilojiloggICAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilohcclxuLy9cdCDilojilojilojilojilojiloggIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAgICAg4paI4paIICAgICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojiloggICAg4paI4paIXHJcbi8vIFxyXG4vL1x04paI4paIICAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vL1x04paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy9cdCNvbiAjZmlyc3QgI2xvYWRcclxuTW9kYWwucHJvdG90eXBlLmlmcmFtZU9uRmlyc3RMb2FkID0gZnVuY3Rpb24oZXZlbnQpXHJcbntcclxuXHQvLyBjb25zb2xlLmxvZygnaWZyYW1lT25GaXJzdExvYWQoKScpO1xyXG5cdHZhciBtb2RhbCA9IE1vZGFsLnByb3RvdHlwZS5pbnN0YW5jZUZyb21EYXRhc2V0KFxyXG5cdFx0Y2xvc2VzdFBhcmVudEJ5Q2xhc3MoZXZlbnQudGFyZ2V0LCBNb2RhbC5wcm90b3R5cGUubmFtZS5jc3MoKSksXHJcblx0XHRNb2RhbFxyXG5cdCk7XHJcblx0bW9kYWwuaWZyYW1lTG9hZGVkID0gdHJ1ZTtcclxuXHRpZiAobW9kYWwuaWZyYW1lTG9hZGluZ1Rvb0xvbmcpIHtcclxuXHRcdG1vZGFsLmlmcmFtZUxvYWRpbmdUb29Mb25nID0gZmFsc2U7XHJcblx0XHRtb2RhbC5zcGlubmVyLnN0b3AoKTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0d2luZG93LmNsZWFyVGltZW91dChtb2RhbC5pZnJhbWVVbm5vdGlmaWVkTG9hZGluZ1RpbWVvdXRJZCk7XHJcblx0fVxyXG5cdC8vINCf0L7QutCw0LcgaWZyYW1lXHJcblx0bW9kYWwuZG9tLmlmcmFtZS5zdHlsZS5vcGFjaXR5ID0gbnVsbDtcclxuXHQvLyBtb2RhbC5kb20uaWZyYW1lLmNzcygnb3BhY2l0eScsIDApLnRyYW5zaXRpb24oe29wYWNpdHk6IDF9LCA1MDAsICdsaW5lYXInKTtcclxuXHQvLyDQntGC0LrQu9GO0YfQtdC90LjQtSDRjdGC0L7Qs9C+INC+0LHRgNCw0LHQvtGC0YfQuNC60LBcclxuXHRtb2RhbC5kb20uaWZyYW1lLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBtb2RhbC5pZnJhbWVPbkZpcnN0TG9hZCwgZmFsc2UpO1xyXG59O1xyXG5cclxuLy9cdCDilojilojilojilojilojiloggIOKWiOKWiOKWiCAgICDilojiloggICAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paIICAg4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vL1x04paI4paIICAgIOKWiOKWiCDilojilojilojiloggICDilojiloggICAgIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICDilojiloggICDilojilohcclxuLy9cdOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiCAg4paI4paIICAgICDilojilojilojilojiloggICDilojilojilojilojilojilojilogg4paI4paIICAgICAg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAg4paI4paIIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAg4paI4paIICAgICAg4paI4paIICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgIOKWiOKWiFxyXG4vL1x0IOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paI4paI4paIICAgICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paIICDilojilojilojilojilojilogg4paI4paIICAg4paI4paIXHJcbi8vIFxyXG4vL1x04paI4paIICAgICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCAgIOKWiOKWiCDilojiloggICDilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiFxyXG4vL1x04paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilogg4paI4paIICAg4paI4paIXHJcbi8vXHTilojilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiFxyXG4vLyBcclxuLy9cdCNvbiAjZWFjaCAjbG9hZFxyXG5Nb2RhbC5wcm90b3R5cGUuaWZyYW1lT25FYWNoTG9hZCA9IGZ1bmN0aW9uKClcclxue1xyXG5cdC8vIGNvbnNvbGUubG9nKCdpZnJhbWVPbkVhY2hMb2FkKCknKTtcclxuXHR2YXIgbW9kYWwgPSBNb2RhbC5wcm90b3R5cGUuaW5zdGFuY2VGcm9tRGF0YXNldChcclxuXHRcdGNsb3Nlc3RQYXJlbnRCeUNsYXNzKGV2ZW50LnRhcmdldCwgTW9kYWwucHJvdG90eXBlLm5hbWUuY3NzKCkpLFxyXG5cdFx0TW9kYWxcclxuXHQpO1xyXG5cdG1vZGFsLmlmcmFtZVJlc2l6ZSgpO1xyXG5cdG1vZGFsLmlmcmFtZVJlc2l6ZUludGVydmFsSWQgPSB3aW5kb3cuc2V0SW50ZXJ2YWwobW9kYWwuaWZyYW1lUmVzaXplLmJpbmQobW9kYWwpLCBtb2RhbC5pZnJhbWVSZXNpemVJbnRlcnZhbCk7XHJcblx0Ly8g0J/RgNC4INCy0YvQs9GA0YPQt9C60LUgaWZyYW1lINC+0YLQutC70Y7Rh9Cw0YLRjCDQvtGC0YHQu9C10LbQuNCy0LDQvdC40LVcclxuXHRtb2RhbC5kb20uaWZyYW1lLmNvbnRlbnRXaW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigndW5sb2FkJywgZnVuY3Rpb24oKSB7XHJcblx0XHQvLyBjb25zb2xlLmxvZygndW5sb2FkJyk7XHJcblx0XHR3aW5kb3cuY2xlYXJJbnRlcnZhbChtb2RhbC5pZnJhbWVSZXNpemVJbnRlcnZhbElkKTtcclxuXHR9LCBmYWxzZSk7XHJcbn07XHJcblxyXG4vL1x04paI4paI4paI4paI4paI4paIICDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICDilojilogg4paI4paIICAgICAg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiOKWiCAg4paI4paIXHJcbi8vXHTilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICDilojilojiloggICDilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgIOKWiOKWiCDilojiloggICAgICAgICAgIOKWiOKWiCDilojiloggIOKWiOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCAgIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paIXHJcbi8vIFxyXG4vL1x0I3Jlc2l6ZVxyXG5Nb2RhbC5wcm90b3R5cGUuaWZyYW1lUmVzaXplID0gZnVuY3Rpb24oKVxyXG57XHJcblx0Ly8gY29uc29sZS5sb2coJ2lmcmFtZVJlc2l6ZSgpJyk7XHJcblx0dmFyIGgxID0gdGhpcy5kb20uaWZyYW1lLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodDtcclxuXHQvLyB2YXIgaDEgPSAkKHRoaXMuZG9tLmlmcmFtZS5nZXQoMCkuY29udGVudFdpbmRvdy5kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpLmhlaWdodCgpO1xyXG5cdHZhciBoMiA9IG1hcmdpbkJveEhlaWdodCh0aGlzLmRvbS5pZnJhbWUuY29udGVudFdpbmRvdy5kb2N1bWVudC5ib2R5LCB0aGlzLmRvbS5pZnJhbWUuY29udGVudFdpbmRvdyk7XHJcblx0dmFyIGhlaWdodCA9IE1hdGgubWF4KGgxLCBoMik7XHJcblx0Ly8gY29uc29sZS5sb2coJ2lmcmFtZSBjaGVjayBoZWlnaHQnKTtcclxuXHQvLyBjb25zb2xlLmxvZygnaGVpZ2h0ID0gJyArIGhlaWdodCArICcsIHByZXZpb3VzSGVpZ2h0ID0gJyArIGlmcmFtZVByZXZpb3VzSGVpZ2h0KTtcclxuXHQvLyBjb25zb2xlLmxvZygnaDEgPSAnICsgaDEgKyAnLCBoMiA9ICcgKyBoMik7XHJcblx0aWYgKGhlaWdodCAhPSB0aGlzLmlmcmFtZVByZXZpb3VzSGVpZ2h0KSB7XHJcblx0XHR2YXIgaGVpZ2h0VW5pdHMgPSByZW0oaGVpZ2h0LCB0cnVlKTtcclxuXHRcdHRoaXMuaWZyYW1lUHJldmlvdXNIZWlnaHQgPSBoZWlnaHQ7XHJcblx0XHR0aGlzLmRvbS5pZnJhbWUuc3R5bGUuaGVpZ2h0ID0gaGVpZ2h0VW5pdHM7XHJcblx0XHR0aGlzLmRvbS5ib3guc3R5bGUuaGVpZ2h0ID0gaGVpZ2h0VW5pdHM7XHJcblx0XHQvLyB0aGlzLmRvbS5pZnJhbWUudHJhbnNpdGlvbih7IGhlaWdodDogaGVpZ2h0ICsgJ3B4JyB9LCA1MDAsICdlYXNlJyk7XHJcblx0XHQvLyBjb25zb2xlLmxvZygkKCkuanF1ZXJ5KTtcclxuXHRcdC8vIGNvbnNvbGUubG9nKCQuZm4udHJhbnNpdGlvbik7XHJcblx0fVxyXG59O1xyXG5cclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICAgICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCDilojiloggICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojiloggICAgICDilojiloggICAg4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilohcclxuLy9cdCAgICAg4paI4paIIOKWiOKWiCAgICAgICAgIOKWiOKWiCAgICDilojiloggICAg4paI4paIIOKWiOKWiFxyXG4vL1x04paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiCAgICDilojiloggICAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIXHJcbi8vIFxyXG4vL1x0IOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiOKWiOKWiOKWiOKWiCDilojilojilojilojilojilojilogg4paI4paI4paIICAgIOKWiOKWiCDilojilojilojilojilojilojilojilohcclxuLy9cdOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paI4paI4paIICAg4paI4paIICAgIOKWiOKWiCAgICDilojiloggICAgICDilojilojilojiloggICDilojiloggICAg4paI4paIXHJcbi8vXHTilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiCDilojiloggIOKWiOKWiCAgICDilojiloggICAg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggIOKWiOKWiCDilojiloggICAg4paI4paIICAgIOKWiOKWiCAgICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICDilojilohcclxuLy9cdCDilojilojilojilojilojiloggIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiCAgICDilojilojilojilojilojilojilogg4paI4paIICAg4paI4paI4paI4paIICAgIOKWiOKWiFxyXG4vLyBcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiOKWiCAgICDilojilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiOKWiCAgICDilojilogg4paI4paI4paI4paI4paI4paI4paI4paIXHJcbi8vXHTilojiloggICDilojilogg4paI4paIICAgIOKWiOKWiCDilojiloggICAgICDilojiloggICAg4paI4paIIOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiOKWiOKWiCAgIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiCAgIOKWiOKWiCDilojiloggICAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAgICDilojilogg4paI4paIIOKWiOKWiOKWiOKWiCDilojilogg4paI4paI4paI4paI4paIICAg4paI4paIIOKWiOKWiCAg4paI4paIICAgIOKWiOKWiFxyXG4vL1x04paI4paIICAg4paI4paIIOKWiOKWiCAgICDilojilogg4paI4paIICAgICAg4paI4paIICAgIOKWiOKWiCDilojiloggIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICAgIOKWiOKWiCAg4paI4paIIOKWiOKWiCAgICDilojilohcclxuLy9cdOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAgIOKWiOKWiOKWiOKWiOKWiOKWiCAg4paI4paI4paI4paI4paI4paIICDilojiloggICAgICDilojilogg4paI4paI4paI4paI4paI4paI4paIIOKWiOKWiCAgIOKWiOKWiOKWiOKWiCAgICDilojilohcclxuLy8gXHJcbi8vXHQjc2V0dXAgI2NvbnRlbnQgI2RvY3VtZW50XHJcbk1vZGFsLnByb3RvdHlwZS5pZnJhbWVTZXR1cENvbnRlbnREb2N1bWVudCA9IGZ1bmN0aW9uKGNvbnRlbnREb2N1bWVudCwgbW9kYWxJbnN0YW5jZSwgd2lkdGgpXHJcbntcclxuXHRjb250ZW50RG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIGZ1bmN0aW9uKGV2ZW50KSB7XHJcblx0XHRpZiAoZXZlbnQua2V5ID8gZXZlbnQua2V5ID09ICdFc2NhcGUnIDogZXZlbnQua2V5Q29kZSA9PSAyNykge1xyXG5cdFx0XHRtb2RhbEluc3RhbmNlLmhpZGUoKTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHR3aWR0aCA9IHdpZHRoIHx8ICdkZWZhdWx0JztcclxuXHRtb2RhbEluc3RhbmNlLnNldFdpZHRoKHdpZHRoKTtcclxufTtcblxuZXhwb3J0IGRlZmF1bHQgTW9kYWw7XG4iLCIvKlxuICogIENvcHlyaWdodCAyMDExIFR3aXR0ZXIsIEluYy5cbiAqICBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqICBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiAgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbihmdW5jdGlvbiAoSG9nYW4pIHtcbiAgLy8gU2V0dXAgcmVnZXggIGFzc2lnbm1lbnRzXG4gIC8vIHJlbW92ZSB3aGl0ZXNwYWNlIGFjY29yZGluZyB0byBNdXN0YWNoZSBzcGVjXG4gIHZhciBySXNXaGl0ZXNwYWNlID0gL1xcUy8sXG4gICAgICByUXVvdCA9IC9cXFwiL2csXG4gICAgICByTmV3bGluZSA9ICAvXFxuL2csXG4gICAgICByQ3IgPSAvXFxyL2csXG4gICAgICByU2xhc2ggPSAvXFxcXC9nLFxuICAgICAgckxpbmVTZXAgPSAvXFx1MjAyOC8sXG4gICAgICByUGFyYWdyYXBoU2VwID0gL1xcdTIwMjkvO1xuXG4gIEhvZ2FuLnRhZ3MgPSB7XG4gICAgJyMnOiAxLCAnXic6IDIsICc8JzogMywgJyQnOiA0LFxuICAgICcvJzogNSwgJyEnOiA2LCAnPic6IDcsICc9JzogOCwgJ192JzogOSxcbiAgICAneyc6IDEwLCAnJic6IDExLCAnX3QnOiAxMlxuICB9O1xuXG4gIEhvZ2FuLnNjYW4gPSBmdW5jdGlvbiBzY2FuKHRleHQsIGRlbGltaXRlcnMpIHtcbiAgICB2YXIgbGVuID0gdGV4dC5sZW5ndGgsXG4gICAgICAgIElOX1RFWFQgPSAwLFxuICAgICAgICBJTl9UQUdfVFlQRSA9IDEsXG4gICAgICAgIElOX1RBRyA9IDIsXG4gICAgICAgIHN0YXRlID0gSU5fVEVYVCxcbiAgICAgICAgdGFnVHlwZSA9IG51bGwsXG4gICAgICAgIHRhZyA9IG51bGwsXG4gICAgICAgIGJ1ZiA9ICcnLFxuICAgICAgICB0b2tlbnMgPSBbXSxcbiAgICAgICAgc2VlblRhZyA9IGZhbHNlLFxuICAgICAgICBpID0gMCxcbiAgICAgICAgbGluZVN0YXJ0ID0gMCxcbiAgICAgICAgb3RhZyA9ICd7eycsXG4gICAgICAgIGN0YWcgPSAnfX0nO1xuXG4gICAgZnVuY3Rpb24gYWRkQnVmKCkge1xuICAgICAgaWYgKGJ1Zi5sZW5ndGggPiAwKSB7XG4gICAgICAgIHRva2Vucy5wdXNoKHt0YWc6ICdfdCcsIHRleHQ6IG5ldyBTdHJpbmcoYnVmKX0pO1xuICAgICAgICBidWYgPSAnJztcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBsaW5lSXNXaGl0ZXNwYWNlKCkge1xuICAgICAgdmFyIGlzQWxsV2hpdGVzcGFjZSA9IHRydWU7XG4gICAgICBmb3IgKHZhciBqID0gbGluZVN0YXJ0OyBqIDwgdG9rZW5zLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGlzQWxsV2hpdGVzcGFjZSA9XG4gICAgICAgICAgKEhvZ2FuLnRhZ3NbdG9rZW5zW2pdLnRhZ10gPCBIb2dhbi50YWdzWydfdiddKSB8fFxuICAgICAgICAgICh0b2tlbnNbal0udGFnID09ICdfdCcgJiYgdG9rZW5zW2pdLnRleHQubWF0Y2gocklzV2hpdGVzcGFjZSkgPT09IG51bGwpO1xuICAgICAgICBpZiAoIWlzQWxsV2hpdGVzcGFjZSkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gaXNBbGxXaGl0ZXNwYWNlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGZpbHRlckxpbmUoaGF2ZVNlZW5UYWcsIG5vTmV3TGluZSkge1xuICAgICAgYWRkQnVmKCk7XG5cbiAgICAgIGlmIChoYXZlU2VlblRhZyAmJiBsaW5lSXNXaGl0ZXNwYWNlKCkpIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IGxpbmVTdGFydCwgbmV4dDsgaiA8IHRva2Vucy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGlmICh0b2tlbnNbal0udGV4dCkge1xuICAgICAgICAgICAgaWYgKChuZXh0ID0gdG9rZW5zW2orMV0pICYmIG5leHQudGFnID09ICc+Jykge1xuICAgICAgICAgICAgICAvLyBzZXQgaW5kZW50IHRvIHRva2VuIHZhbHVlXG4gICAgICAgICAgICAgIG5leHQuaW5kZW50ID0gdG9rZW5zW2pdLnRleHQudG9TdHJpbmcoKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdG9rZW5zLnNwbGljZShqLCAxKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoIW5vTmV3TGluZSkge1xuICAgICAgICB0b2tlbnMucHVzaCh7dGFnOidcXG4nfSk7XG4gICAgICB9XG5cbiAgICAgIHNlZW5UYWcgPSBmYWxzZTtcbiAgICAgIGxpbmVTdGFydCA9IHRva2Vucy5sZW5ndGg7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2hhbmdlRGVsaW1pdGVycyh0ZXh0LCBpbmRleCkge1xuICAgICAgdmFyIGNsb3NlID0gJz0nICsgY3RhZyxcbiAgICAgICAgICBjbG9zZUluZGV4ID0gdGV4dC5pbmRleE9mKGNsb3NlLCBpbmRleCksXG4gICAgICAgICAgZGVsaW1pdGVycyA9IHRyaW0oXG4gICAgICAgICAgICB0ZXh0LnN1YnN0cmluZyh0ZXh0LmluZGV4T2YoJz0nLCBpbmRleCkgKyAxLCBjbG9zZUluZGV4KVxuICAgICAgICAgICkuc3BsaXQoJyAnKTtcblxuICAgICAgb3RhZyA9IGRlbGltaXRlcnNbMF07XG4gICAgICBjdGFnID0gZGVsaW1pdGVyc1tkZWxpbWl0ZXJzLmxlbmd0aCAtIDFdO1xuXG4gICAgICByZXR1cm4gY2xvc2VJbmRleCArIGNsb3NlLmxlbmd0aCAtIDE7XG4gICAgfVxuXG4gICAgaWYgKGRlbGltaXRlcnMpIHtcbiAgICAgIGRlbGltaXRlcnMgPSBkZWxpbWl0ZXJzLnNwbGl0KCcgJyk7XG4gICAgICBvdGFnID0gZGVsaW1pdGVyc1swXTtcbiAgICAgIGN0YWcgPSBkZWxpbWl0ZXJzWzFdO1xuICAgIH1cblxuICAgIGZvciAoaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgaWYgKHN0YXRlID09IElOX1RFWFQpIHtcbiAgICAgICAgaWYgKHRhZ0NoYW5nZShvdGFnLCB0ZXh0LCBpKSkge1xuICAgICAgICAgIC0taTtcbiAgICAgICAgICBhZGRCdWYoKTtcbiAgICAgICAgICBzdGF0ZSA9IElOX1RBR19UWVBFO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmICh0ZXh0LmNoYXJBdChpKSA9PSAnXFxuJykge1xuICAgICAgICAgICAgZmlsdGVyTGluZShzZWVuVGFnKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYnVmICs9IHRleHQuY2hhckF0KGkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChzdGF0ZSA9PSBJTl9UQUdfVFlQRSkge1xuICAgICAgICBpICs9IG90YWcubGVuZ3RoIC0gMTtcbiAgICAgICAgdGFnID0gSG9nYW4udGFnc1t0ZXh0LmNoYXJBdChpICsgMSldO1xuICAgICAgICB0YWdUeXBlID0gdGFnID8gdGV4dC5jaGFyQXQoaSArIDEpIDogJ192JztcbiAgICAgICAgaWYgKHRhZ1R5cGUgPT0gJz0nKSB7XG4gICAgICAgICAgaSA9IGNoYW5nZURlbGltaXRlcnModGV4dCwgaSk7XG4gICAgICAgICAgc3RhdGUgPSBJTl9URVhUO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmICh0YWcpIHtcbiAgICAgICAgICAgIGkrKztcbiAgICAgICAgICB9XG4gICAgICAgICAgc3RhdGUgPSBJTl9UQUc7XG4gICAgICAgIH1cbiAgICAgICAgc2VlblRhZyA9IGk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAodGFnQ2hhbmdlKGN0YWcsIHRleHQsIGkpKSB7XG4gICAgICAgICAgdG9rZW5zLnB1c2goe3RhZzogdGFnVHlwZSwgbjogdHJpbShidWYpLCBvdGFnOiBvdGFnLCBjdGFnOiBjdGFnLFxuICAgICAgICAgICAgICAgICAgICAgICBpOiAodGFnVHlwZSA9PSAnLycpID8gc2VlblRhZyAtIG90YWcubGVuZ3RoIDogaSArIGN0YWcubGVuZ3RofSk7XG4gICAgICAgICAgYnVmID0gJyc7XG4gICAgICAgICAgaSArPSBjdGFnLmxlbmd0aCAtIDE7XG4gICAgICAgICAgc3RhdGUgPSBJTl9URVhUO1xuICAgICAgICAgIGlmICh0YWdUeXBlID09ICd7Jykge1xuICAgICAgICAgICAgaWYgKGN0YWcgPT0gJ319Jykge1xuICAgICAgICAgICAgICBpKys7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBjbGVhblRyaXBsZVN0YWNoZSh0b2tlbnNbdG9rZW5zLmxlbmd0aCAtIDFdKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYnVmICs9IHRleHQuY2hhckF0KGkpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZmlsdGVyTGluZShzZWVuVGFnLCB0cnVlKTtcblxuICAgIHJldHVybiB0b2tlbnM7XG4gIH1cblxuICBmdW5jdGlvbiBjbGVhblRyaXBsZVN0YWNoZSh0b2tlbikge1xuICAgIGlmICh0b2tlbi5uLnN1YnN0cih0b2tlbi5uLmxlbmd0aCAtIDEpID09PSAnfScpIHtcbiAgICAgIHRva2VuLm4gPSB0b2tlbi5uLnN1YnN0cmluZygwLCB0b2tlbi5uLmxlbmd0aCAtIDEpO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIHRyaW0ocykge1xuICAgIGlmIChzLnRyaW0pIHtcbiAgICAgIHJldHVybiBzLnRyaW0oKTtcbiAgICB9XG5cbiAgICByZXR1cm4gcy5yZXBsYWNlKC9eXFxzKnxcXHMqJC9nLCAnJyk7XG4gIH1cblxuICBmdW5jdGlvbiB0YWdDaGFuZ2UodGFnLCB0ZXh0LCBpbmRleCkge1xuICAgIGlmICh0ZXh0LmNoYXJBdChpbmRleCkgIT0gdGFnLmNoYXJBdCgwKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGZvciAodmFyIGkgPSAxLCBsID0gdGFnLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgaWYgKHRleHQuY2hhckF0KGluZGV4ICsgaSkgIT0gdGFnLmNoYXJBdChpKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICAvLyB0aGUgdGFncyBhbGxvd2VkIGluc2lkZSBzdXBlciB0ZW1wbGF0ZXNcbiAgdmFyIGFsbG93ZWRJblN1cGVyID0geydfdCc6IHRydWUsICdcXG4nOiB0cnVlLCAnJCc6IHRydWUsICcvJzogdHJ1ZX07XG5cbiAgZnVuY3Rpb24gYnVpbGRUcmVlKHRva2Vucywga2luZCwgc3RhY2ssIGN1c3RvbVRhZ3MpIHtcbiAgICB2YXIgaW5zdHJ1Y3Rpb25zID0gW10sXG4gICAgICAgIG9wZW5lciA9IG51bGwsXG4gICAgICAgIHRhaWwgPSBudWxsLFxuICAgICAgICB0b2tlbiA9IG51bGw7XG5cbiAgICB0YWlsID0gc3RhY2tbc3RhY2subGVuZ3RoIC0gMV07XG5cbiAgICB3aGlsZSAodG9rZW5zLmxlbmd0aCA+IDApIHtcbiAgICAgIHRva2VuID0gdG9rZW5zLnNoaWZ0KCk7XG5cbiAgICAgIGlmICh0YWlsICYmIHRhaWwudGFnID09ICc8JyAmJiAhKHRva2VuLnRhZyBpbiBhbGxvd2VkSW5TdXBlcikpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbGxlZ2FsIGNvbnRlbnQgaW4gPCBzdXBlciB0YWcuJyk7XG4gICAgICB9XG5cbiAgICAgIGlmIChIb2dhbi50YWdzW3Rva2VuLnRhZ10gPD0gSG9nYW4udGFnc1snJCddIHx8IGlzT3BlbmVyKHRva2VuLCBjdXN0b21UYWdzKSkge1xuICAgICAgICBzdGFjay5wdXNoKHRva2VuKTtcbiAgICAgICAgdG9rZW4ubm9kZXMgPSBidWlsZFRyZWUodG9rZW5zLCB0b2tlbi50YWcsIHN0YWNrLCBjdXN0b21UYWdzKTtcbiAgICAgIH0gZWxzZSBpZiAodG9rZW4udGFnID09ICcvJykge1xuICAgICAgICBpZiAoc3RhY2subGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdDbG9zaW5nIHRhZyB3aXRob3V0IG9wZW5lcjogLycgKyB0b2tlbi5uKTtcbiAgICAgICAgfVxuICAgICAgICBvcGVuZXIgPSBzdGFjay5wb3AoKTtcbiAgICAgICAgaWYgKHRva2VuLm4gIT0gb3BlbmVyLm4gJiYgIWlzQ2xvc2VyKHRva2VuLm4sIG9wZW5lci5uLCBjdXN0b21UYWdzKSkge1xuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTmVzdGluZyBlcnJvcjogJyArIG9wZW5lci5uICsgJyB2cy4gJyArIHRva2VuLm4pO1xuICAgICAgICB9XG4gICAgICAgIG9wZW5lci5lbmQgPSB0b2tlbi5pO1xuICAgICAgICByZXR1cm4gaW5zdHJ1Y3Rpb25zO1xuICAgICAgfSBlbHNlIGlmICh0b2tlbi50YWcgPT0gJ1xcbicpIHtcbiAgICAgICAgdG9rZW4ubGFzdCA9ICh0b2tlbnMubGVuZ3RoID09IDApIHx8ICh0b2tlbnNbMF0udGFnID09ICdcXG4nKTtcbiAgICAgIH1cblxuICAgICAgaW5zdHJ1Y3Rpb25zLnB1c2godG9rZW4pO1xuICAgIH1cblxuICAgIGlmIChzdGFjay5sZW5ndGggPiAwKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ21pc3NpbmcgY2xvc2luZyB0YWc6ICcgKyBzdGFjay5wb3AoKS5uKTtcbiAgICB9XG5cbiAgICByZXR1cm4gaW5zdHJ1Y3Rpb25zO1xuICB9XG5cbiAgZnVuY3Rpb24gaXNPcGVuZXIodG9rZW4sIHRhZ3MpIHtcbiAgICBmb3IgKHZhciBpID0gMCwgbCA9IHRhZ3MubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgICBpZiAodGFnc1tpXS5vID09IHRva2VuLm4pIHtcbiAgICAgICAgdG9rZW4udGFnID0gJyMnO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBpc0Nsb3NlcihjbG9zZSwgb3BlbiwgdGFncykge1xuICAgIGZvciAodmFyIGkgPSAwLCBsID0gdGFncy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGlmICh0YWdzW2ldLmMgPT0gY2xvc2UgJiYgdGFnc1tpXS5vID09IG9wZW4pIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gc3RyaW5naWZ5U3Vic3RpdHV0aW9ucyhvYmopIHtcbiAgICB2YXIgaXRlbXMgPSBbXTtcbiAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSB7XG4gICAgICBpdGVtcy5wdXNoKCdcIicgKyBlc2Moa2V5KSArICdcIjogZnVuY3Rpb24oYyxwLHQsaSkgeycgKyBvYmpba2V5XSArICd9Jyk7XG4gICAgfVxuICAgIHJldHVybiBcInsgXCIgKyBpdGVtcy5qb2luKFwiLFwiKSArIFwiIH1cIjtcbiAgfVxuXG4gIGZ1bmN0aW9uIHN0cmluZ2lmeVBhcnRpYWxzKGNvZGVPYmopIHtcbiAgICB2YXIgcGFydGlhbHMgPSBbXTtcbiAgICBmb3IgKHZhciBrZXkgaW4gY29kZU9iai5wYXJ0aWFscykge1xuICAgICAgcGFydGlhbHMucHVzaCgnXCInICsgZXNjKGtleSkgKyAnXCI6e25hbWU6XCInICsgZXNjKGNvZGVPYmoucGFydGlhbHNba2V5XS5uYW1lKSArICdcIiwgJyArIHN0cmluZ2lmeVBhcnRpYWxzKGNvZGVPYmoucGFydGlhbHNba2V5XSkgKyBcIn1cIik7XG4gICAgfVxuICAgIHJldHVybiBcInBhcnRpYWxzOiB7XCIgKyBwYXJ0aWFscy5qb2luKFwiLFwiKSArIFwifSwgc3ViczogXCIgKyBzdHJpbmdpZnlTdWJzdGl0dXRpb25zKGNvZGVPYmouc3Vicyk7XG4gIH1cblxuICBIb2dhbi5zdHJpbmdpZnkgPSBmdW5jdGlvbihjb2RlT2JqLCB0ZXh0LCBvcHRpb25zKSB7XG4gICAgcmV0dXJuIFwie2NvZGU6IGZ1bmN0aW9uIChjLHAsaSkgeyBcIiArIEhvZ2FuLndyYXBNYWluKGNvZGVPYmouY29kZSkgKyBcIiB9LFwiICsgc3RyaW5naWZ5UGFydGlhbHMoY29kZU9iaikgKyAgXCJ9XCI7XG4gIH1cblxuICB2YXIgc2VyaWFsTm8gPSAwO1xuICBIb2dhbi5nZW5lcmF0ZSA9IGZ1bmN0aW9uKHRyZWUsIHRleHQsIG9wdGlvbnMpIHtcbiAgICBzZXJpYWxObyA9IDA7XG4gICAgdmFyIGNvbnRleHQgPSB7IGNvZGU6ICcnLCBzdWJzOiB7fSwgcGFydGlhbHM6IHt9IH07XG4gICAgSG9nYW4ud2Fsayh0cmVlLCBjb250ZXh0KTtcblxuICAgIGlmIChvcHRpb25zLmFzU3RyaW5nKSB7XG4gICAgICByZXR1cm4gdGhpcy5zdHJpbmdpZnkoY29udGV4dCwgdGV4dCwgb3B0aW9ucyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMubWFrZVRlbXBsYXRlKGNvbnRleHQsIHRleHQsIG9wdGlvbnMpO1xuICB9XG5cbiAgSG9nYW4ud3JhcE1haW4gPSBmdW5jdGlvbihjb2RlKSB7XG4gICAgcmV0dXJuICd2YXIgdD10aGlzO3QuYihpPWl8fFwiXCIpOycgKyBjb2RlICsgJ3JldHVybiB0LmZsKCk7JztcbiAgfVxuXG4gIEhvZ2FuLnRlbXBsYXRlID0gSG9nYW4uVGVtcGxhdGU7XG5cbiAgSG9nYW4ubWFrZVRlbXBsYXRlID0gZnVuY3Rpb24oY29kZU9iaiwgdGV4dCwgb3B0aW9ucykge1xuICAgIHZhciB0ZW1wbGF0ZSA9IHRoaXMubWFrZVBhcnRpYWxzKGNvZGVPYmopO1xuICAgIHRlbXBsYXRlLmNvZGUgPSBuZXcgRnVuY3Rpb24oJ2MnLCAncCcsICdpJywgdGhpcy53cmFwTWFpbihjb2RlT2JqLmNvZGUpKTtcbiAgICByZXR1cm4gbmV3IHRoaXMudGVtcGxhdGUodGVtcGxhdGUsIHRleHQsIHRoaXMsIG9wdGlvbnMpO1xuICB9XG5cbiAgSG9nYW4ubWFrZVBhcnRpYWxzID0gZnVuY3Rpb24oY29kZU9iaikge1xuICAgIHZhciBrZXksIHRlbXBsYXRlID0ge3N1YnM6IHt9LCBwYXJ0aWFsczogY29kZU9iai5wYXJ0aWFscywgbmFtZTogY29kZU9iai5uYW1lfTtcbiAgICBmb3IgKGtleSBpbiB0ZW1wbGF0ZS5wYXJ0aWFscykge1xuICAgICAgdGVtcGxhdGUucGFydGlhbHNba2V5XSA9IHRoaXMubWFrZVBhcnRpYWxzKHRlbXBsYXRlLnBhcnRpYWxzW2tleV0pO1xuICAgIH1cbiAgICBmb3IgKGtleSBpbiBjb2RlT2JqLnN1YnMpIHtcbiAgICAgIHRlbXBsYXRlLnN1YnNba2V5XSA9IG5ldyBGdW5jdGlvbignYycsICdwJywgJ3QnLCAnaScsIGNvZGVPYmouc3Vic1trZXldKTtcbiAgICB9XG4gICAgcmV0dXJuIHRlbXBsYXRlO1xuICB9XG5cbiAgZnVuY3Rpb24gZXNjKHMpIHtcbiAgICByZXR1cm4gcy5yZXBsYWNlKHJTbGFzaCwgJ1xcXFxcXFxcJylcbiAgICAgICAgICAgIC5yZXBsYWNlKHJRdW90LCAnXFxcXFxcXCInKVxuICAgICAgICAgICAgLnJlcGxhY2Uock5ld2xpbmUsICdcXFxcbicpXG4gICAgICAgICAgICAucmVwbGFjZShyQ3IsICdcXFxccicpXG4gICAgICAgICAgICAucmVwbGFjZShyTGluZVNlcCwgJ1xcXFx1MjAyOCcpXG4gICAgICAgICAgICAucmVwbGFjZShyUGFyYWdyYXBoU2VwLCAnXFxcXHUyMDI5Jyk7XG4gIH1cblxuICBmdW5jdGlvbiBjaG9vc2VNZXRob2Qocykge1xuICAgIHJldHVybiAofnMuaW5kZXhPZignLicpKSA/ICdkJyA6ICdmJztcbiAgfVxuXG4gIGZ1bmN0aW9uIGNyZWF0ZVBhcnRpYWwobm9kZSwgY29udGV4dCkge1xuICAgIHZhciBwcmVmaXggPSBcIjxcIiArIChjb250ZXh0LnByZWZpeCB8fCBcIlwiKTtcbiAgICB2YXIgc3ltID0gcHJlZml4ICsgbm9kZS5uICsgc2VyaWFsTm8rKztcbiAgICBjb250ZXh0LnBhcnRpYWxzW3N5bV0gPSB7bmFtZTogbm9kZS5uLCBwYXJ0aWFsczoge319O1xuICAgIGNvbnRleHQuY29kZSArPSAndC5iKHQucnAoXCInICsgIGVzYyhzeW0pICsgJ1wiLGMscCxcIicgKyAobm9kZS5pbmRlbnQgfHwgJycpICsgJ1wiKSk7JztcbiAgICByZXR1cm4gc3ltO1xuICB9XG5cbiAgSG9nYW4uY29kZWdlbiA9IHtcbiAgICAnIyc6IGZ1bmN0aW9uKG5vZGUsIGNvbnRleHQpIHtcbiAgICAgIGNvbnRleHQuY29kZSArPSAnaWYodC5zKHQuJyArIGNob29zZU1ldGhvZChub2RlLm4pICsgJyhcIicgKyBlc2Mobm9kZS5uKSArICdcIixjLHAsMSksJyArXG4gICAgICAgICAgICAgICAgICAgICAgJ2MscCwwLCcgKyBub2RlLmkgKyAnLCcgKyBub2RlLmVuZCArICcsXCInICsgbm9kZS5vdGFnICsgXCIgXCIgKyBub2RlLmN0YWcgKyAnXCIpKXsnICtcbiAgICAgICAgICAgICAgICAgICAgICAndC5ycyhjLHAsJyArICdmdW5jdGlvbihjLHAsdCl7JztcbiAgICAgIEhvZ2FuLndhbGsobm9kZS5ub2RlcywgY29udGV4dCk7XG4gICAgICBjb250ZXh0LmNvZGUgKz0gJ30pO2MucG9wKCk7fSc7XG4gICAgfSxcblxuICAgICdeJzogZnVuY3Rpb24obm9kZSwgY29udGV4dCkge1xuICAgICAgY29udGV4dC5jb2RlICs9ICdpZighdC5zKHQuJyArIGNob29zZU1ldGhvZChub2RlLm4pICsgJyhcIicgKyBlc2Mobm9kZS5uKSArICdcIixjLHAsMSksYyxwLDEsMCwwLFwiXCIpKXsnO1xuICAgICAgSG9nYW4ud2Fsayhub2RlLm5vZGVzLCBjb250ZXh0KTtcbiAgICAgIGNvbnRleHQuY29kZSArPSAnfTsnO1xuICAgIH0sXG5cbiAgICAnPic6IGNyZWF0ZVBhcnRpYWwsXG4gICAgJzwnOiBmdW5jdGlvbihub2RlLCBjb250ZXh0KSB7XG4gICAgICB2YXIgY3R4ID0ge3BhcnRpYWxzOiB7fSwgY29kZTogJycsIHN1YnM6IHt9LCBpblBhcnRpYWw6IHRydWV9O1xuICAgICAgSG9nYW4ud2Fsayhub2RlLm5vZGVzLCBjdHgpO1xuICAgICAgdmFyIHRlbXBsYXRlID0gY29udGV4dC5wYXJ0aWFsc1tjcmVhdGVQYXJ0aWFsKG5vZGUsIGNvbnRleHQpXTtcbiAgICAgIHRlbXBsYXRlLnN1YnMgPSBjdHguc3VicztcbiAgICAgIHRlbXBsYXRlLnBhcnRpYWxzID0gY3R4LnBhcnRpYWxzO1xuICAgIH0sXG5cbiAgICAnJCc6IGZ1bmN0aW9uKG5vZGUsIGNvbnRleHQpIHtcbiAgICAgIHZhciBjdHggPSB7c3Viczoge30sIGNvZGU6ICcnLCBwYXJ0aWFsczogY29udGV4dC5wYXJ0aWFscywgcHJlZml4OiBub2RlLm59O1xuICAgICAgSG9nYW4ud2Fsayhub2RlLm5vZGVzLCBjdHgpO1xuICAgICAgY29udGV4dC5zdWJzW25vZGUubl0gPSBjdHguY29kZTtcbiAgICAgIGlmICghY29udGV4dC5pblBhcnRpYWwpIHtcbiAgICAgICAgY29udGV4dC5jb2RlICs9ICd0LnN1YihcIicgKyBlc2Mobm9kZS5uKSArICdcIixjLHAsaSk7JztcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgJ1xcbic6IGZ1bmN0aW9uKG5vZGUsIGNvbnRleHQpIHtcbiAgICAgIGNvbnRleHQuY29kZSArPSB3cml0ZSgnXCJcXFxcblwiJyArIChub2RlLmxhc3QgPyAnJyA6ICcgKyBpJykpO1xuICAgIH0sXG5cbiAgICAnX3YnOiBmdW5jdGlvbihub2RlLCBjb250ZXh0KSB7XG4gICAgICBjb250ZXh0LmNvZGUgKz0gJ3QuYih0LnYodC4nICsgY2hvb3NlTWV0aG9kKG5vZGUubikgKyAnKFwiJyArIGVzYyhub2RlLm4pICsgJ1wiLGMscCwwKSkpOyc7XG4gICAgfSxcblxuICAgICdfdCc6IGZ1bmN0aW9uKG5vZGUsIGNvbnRleHQpIHtcbiAgICAgIGNvbnRleHQuY29kZSArPSB3cml0ZSgnXCInICsgZXNjKG5vZGUudGV4dCkgKyAnXCInKTtcbiAgICB9LFxuXG4gICAgJ3snOiB0cmlwbGVTdGFjaGUsXG5cbiAgICAnJic6IHRyaXBsZVN0YWNoZVxuICB9XG5cbiAgZnVuY3Rpb24gdHJpcGxlU3RhY2hlKG5vZGUsIGNvbnRleHQpIHtcbiAgICBjb250ZXh0LmNvZGUgKz0gJ3QuYih0LnQodC4nICsgY2hvb3NlTWV0aG9kKG5vZGUubikgKyAnKFwiJyArIGVzYyhub2RlLm4pICsgJ1wiLGMscCwwKSkpOyc7XG4gIH1cblxuICBmdW5jdGlvbiB3cml0ZShzKSB7XG4gICAgcmV0dXJuICd0LmIoJyArIHMgKyAnKTsnO1xuICB9XG5cbiAgSG9nYW4ud2FsayA9IGZ1bmN0aW9uKG5vZGVsaXN0LCBjb250ZXh0KSB7XG4gICAgdmFyIGZ1bmM7XG4gICAgZm9yICh2YXIgaSA9IDAsIGwgPSBub2RlbGlzdC5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGZ1bmMgPSBIb2dhbi5jb2RlZ2VuW25vZGVsaXN0W2ldLnRhZ107XG4gICAgICBmdW5jICYmIGZ1bmMobm9kZWxpc3RbaV0sIGNvbnRleHQpO1xuICAgIH1cbiAgICByZXR1cm4gY29udGV4dDtcbiAgfVxuXG4gIEhvZ2FuLnBhcnNlID0gZnVuY3Rpb24odG9rZW5zLCB0ZXh0LCBvcHRpb25zKSB7XG4gICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gICAgcmV0dXJuIGJ1aWxkVHJlZSh0b2tlbnMsICcnLCBbXSwgb3B0aW9ucy5zZWN0aW9uVGFncyB8fCBbXSk7XG4gIH1cblxuICBIb2dhbi5jYWNoZSA9IHt9O1xuXG4gIEhvZ2FuLmNhY2hlS2V5ID0gZnVuY3Rpb24odGV4dCwgb3B0aW9ucykge1xuICAgIHJldHVybiBbdGV4dCwgISFvcHRpb25zLmFzU3RyaW5nLCAhIW9wdGlvbnMuZGlzYWJsZUxhbWJkYSwgb3B0aW9ucy5kZWxpbWl0ZXJzLCAhIW9wdGlvbnMubW9kZWxHZXRdLmpvaW4oJ3x8Jyk7XG4gIH1cblxuICBIb2dhbi5jb21waWxlID0gZnVuY3Rpb24odGV4dCwgb3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgIHZhciBrZXkgPSBIb2dhbi5jYWNoZUtleSh0ZXh0LCBvcHRpb25zKTtcbiAgICB2YXIgdGVtcGxhdGUgPSB0aGlzLmNhY2hlW2tleV07XG5cbiAgICBpZiAodGVtcGxhdGUpIHtcbiAgICAgIHZhciBwYXJ0aWFscyA9IHRlbXBsYXRlLnBhcnRpYWxzO1xuICAgICAgZm9yICh2YXIgbmFtZSBpbiBwYXJ0aWFscykge1xuICAgICAgICBkZWxldGUgcGFydGlhbHNbbmFtZV0uaW5zdGFuY2U7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGVtcGxhdGU7XG4gICAgfVxuXG4gICAgdGVtcGxhdGUgPSB0aGlzLmdlbmVyYXRlKHRoaXMucGFyc2UodGhpcy5zY2FuKHRleHQsIG9wdGlvbnMuZGVsaW1pdGVycyksIHRleHQsIG9wdGlvbnMpLCB0ZXh0LCBvcHRpb25zKTtcbiAgICByZXR1cm4gdGhpcy5jYWNoZVtrZXldID0gdGVtcGxhdGU7XG4gIH1cbn0pKHR5cGVvZiBleHBvcnRzICE9PSAndW5kZWZpbmVkJyA/IGV4cG9ydHMgOiBIb2dhbik7XG4iLCIvKlxuICogIENvcHlyaWdodCAyMDExIFR3aXR0ZXIsIEluYy5cbiAqICBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqICBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiAgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbnZhciBIb2dhbiA9IHt9O1xuXG4oZnVuY3Rpb24gKEhvZ2FuKSB7XG4gIEhvZ2FuLlRlbXBsYXRlID0gZnVuY3Rpb24gKGNvZGVPYmosIHRleHQsIGNvbXBpbGVyLCBvcHRpb25zKSB7XG4gICAgY29kZU9iaiA9IGNvZGVPYmogfHwge307XG4gICAgdGhpcy5yID0gY29kZU9iai5jb2RlIHx8IHRoaXMucjtcbiAgICB0aGlzLmMgPSBjb21waWxlcjtcbiAgICB0aGlzLm9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgIHRoaXMudGV4dCA9IHRleHQgfHwgJyc7XG4gICAgdGhpcy5wYXJ0aWFscyA9IGNvZGVPYmoucGFydGlhbHMgfHwge307XG4gICAgdGhpcy5zdWJzID0gY29kZU9iai5zdWJzIHx8IHt9O1xuICAgIHRoaXMuYnVmID0gJyc7XG4gIH1cblxuICBIb2dhbi5UZW1wbGF0ZS5wcm90b3R5cGUgPSB7XG4gICAgLy8gcmVuZGVyOiByZXBsYWNlZCBieSBnZW5lcmF0ZWQgY29kZS5cbiAgICByOiBmdW5jdGlvbiAoY29udGV4dCwgcGFydGlhbHMsIGluZGVudCkgeyByZXR1cm4gJyc7IH0sXG5cbiAgICAvLyB2YXJpYWJsZSBlc2NhcGluZ1xuICAgIHY6IGhvZ2FuRXNjYXBlLFxuXG4gICAgLy8gdHJpcGxlIHN0YWNoZVxuICAgIHQ6IGNvZXJjZVRvU3RyaW5nLFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbiByZW5kZXIoY29udGV4dCwgcGFydGlhbHMsIGluZGVudCkge1xuICAgICAgcmV0dXJuIHRoaXMucmkoW2NvbnRleHRdLCBwYXJ0aWFscyB8fCB7fSwgaW5kZW50KTtcbiAgICB9LFxuXG4gICAgLy8gcmVuZGVyIGludGVybmFsIC0tIGEgaG9vayBmb3Igb3ZlcnJpZGVzIHRoYXQgY2F0Y2hlcyBwYXJ0aWFscyB0b29cbiAgICByaTogZnVuY3Rpb24gKGNvbnRleHQsIHBhcnRpYWxzLCBpbmRlbnQpIHtcbiAgICAgIHJldHVybiB0aGlzLnIoY29udGV4dCwgcGFydGlhbHMsIGluZGVudCk7XG4gICAgfSxcblxuICAgIC8vIGVuc3VyZVBhcnRpYWxcbiAgICBlcDogZnVuY3Rpb24oc3ltYm9sLCBwYXJ0aWFscykge1xuICAgICAgdmFyIHBhcnRpYWwgPSB0aGlzLnBhcnRpYWxzW3N5bWJvbF07XG5cbiAgICAgIC8vIGNoZWNrIHRvIHNlZSB0aGF0IGlmIHdlJ3ZlIGluc3RhbnRpYXRlZCB0aGlzIHBhcnRpYWwgYmVmb3JlXG4gICAgICB2YXIgdGVtcGxhdGUgPSBwYXJ0aWFsc1twYXJ0aWFsLm5hbWVdO1xuICAgICAgaWYgKHBhcnRpYWwuaW5zdGFuY2UgJiYgcGFydGlhbC5iYXNlID09IHRlbXBsYXRlKSB7XG4gICAgICAgIHJldHVybiBwYXJ0aWFsLmluc3RhbmNlO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIHRlbXBsYXRlID09ICdzdHJpbmcnKSB7XG4gICAgICAgIGlmICghdGhpcy5jKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTm8gY29tcGlsZXIgYXZhaWxhYmxlLlwiKTtcbiAgICAgICAgfVxuICAgICAgICB0ZW1wbGF0ZSA9IHRoaXMuYy5jb21waWxlKHRlbXBsYXRlLCB0aGlzLm9wdGlvbnMpO1xuICAgICAgfVxuXG4gICAgICBpZiAoIXRlbXBsYXRlKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICAvLyBXZSB1c2UgdGhpcyB0byBjaGVjayB3aGV0aGVyIHRoZSBwYXJ0aWFscyBkaWN0aW9uYXJ5IGhhcyBjaGFuZ2VkXG4gICAgICB0aGlzLnBhcnRpYWxzW3N5bWJvbF0uYmFzZSA9IHRlbXBsYXRlO1xuXG4gICAgICBpZiAocGFydGlhbC5zdWJzKSB7XG4gICAgICAgIC8vIE1ha2Ugc3VyZSB3ZSBjb25zaWRlciBwYXJlbnQgdGVtcGxhdGUgbm93XG4gICAgICAgIGlmICghcGFydGlhbHMuc3RhY2tUZXh0KSBwYXJ0aWFscy5zdGFja1RleHQgPSB7fTtcbiAgICAgICAgZm9yIChrZXkgaW4gcGFydGlhbC5zdWJzKSB7XG4gICAgICAgICAgaWYgKCFwYXJ0aWFscy5zdGFja1RleHRba2V5XSkge1xuICAgICAgICAgICAgcGFydGlhbHMuc3RhY2tUZXh0W2tleV0gPSAodGhpcy5hY3RpdmVTdWIgIT09IHVuZGVmaW5lZCAmJiBwYXJ0aWFscy5zdGFja1RleHRbdGhpcy5hY3RpdmVTdWJdKSA/IHBhcnRpYWxzLnN0YWNrVGV4dFt0aGlzLmFjdGl2ZVN1Yl0gOiB0aGlzLnRleHQ7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRlbXBsYXRlID0gY3JlYXRlU3BlY2lhbGl6ZWRQYXJ0aWFsKHRlbXBsYXRlLCBwYXJ0aWFsLnN1YnMsIHBhcnRpYWwucGFydGlhbHMsXG4gICAgICAgICAgdGhpcy5zdGFja1N1YnMsIHRoaXMuc3RhY2tQYXJ0aWFscywgcGFydGlhbHMuc3RhY2tUZXh0KTtcbiAgICAgIH1cbiAgICAgIHRoaXMucGFydGlhbHNbc3ltYm9sXS5pbnN0YW5jZSA9IHRlbXBsYXRlO1xuXG4gICAgICByZXR1cm4gdGVtcGxhdGU7XG4gICAgfSxcblxuICAgIC8vIHRyaWVzIHRvIGZpbmQgYSBwYXJ0aWFsIGluIHRoZSBjdXJyZW50IHNjb3BlIGFuZCByZW5kZXIgaXRcbiAgICBycDogZnVuY3Rpb24oc3ltYm9sLCBjb250ZXh0LCBwYXJ0aWFscywgaW5kZW50KSB7XG4gICAgICB2YXIgcGFydGlhbCA9IHRoaXMuZXAoc3ltYm9sLCBwYXJ0aWFscyk7XG4gICAgICBpZiAoIXBhcnRpYWwpIHtcbiAgICAgICAgcmV0dXJuICcnO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcGFydGlhbC5yaShjb250ZXh0LCBwYXJ0aWFscywgaW5kZW50KTtcbiAgICB9LFxuXG4gICAgLy8gcmVuZGVyIGEgc2VjdGlvblxuICAgIHJzOiBmdW5jdGlvbihjb250ZXh0LCBwYXJ0aWFscywgc2VjdGlvbikge1xuICAgICAgdmFyIHRhaWwgPSBjb250ZXh0W2NvbnRleHQubGVuZ3RoIC0gMV07XG5cbiAgICAgIGlmICghaXNBcnJheSh0YWlsKSkge1xuICAgICAgICBzZWN0aW9uKGNvbnRleHQsIHBhcnRpYWxzLCB0aGlzKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRhaWwubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29udGV4dC5wdXNoKHRhaWxbaV0pO1xuICAgICAgICBzZWN0aW9uKGNvbnRleHQsIHBhcnRpYWxzLCB0aGlzKTtcbiAgICAgICAgY29udGV4dC5wb3AoKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLy8gbWF5YmUgc3RhcnQgYSBzZWN0aW9uXG4gICAgczogZnVuY3Rpb24odmFsLCBjdHgsIHBhcnRpYWxzLCBpbnZlcnRlZCwgc3RhcnQsIGVuZCwgdGFncykge1xuICAgICAgdmFyIHBhc3M7XG5cbiAgICAgIGlmIChpc0FycmF5KHZhbCkgJiYgdmFsLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGlmICh0eXBlb2YgdmFsID09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgdmFsID0gdGhpcy5tcyh2YWwsIGN0eCwgcGFydGlhbHMsIGludmVydGVkLCBzdGFydCwgZW5kLCB0YWdzKTtcbiAgICAgIH1cblxuICAgICAgcGFzcyA9ICEhdmFsO1xuXG4gICAgICBpZiAoIWludmVydGVkICYmIHBhc3MgJiYgY3R4KSB7XG4gICAgICAgIGN0eC5wdXNoKCh0eXBlb2YgdmFsID09ICdvYmplY3QnKSA/IHZhbCA6IGN0eFtjdHgubGVuZ3RoIC0gMV0pO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcGFzcztcbiAgICB9LFxuXG4gICAgLy8gZmluZCB2YWx1ZXMgd2l0aCBkb3R0ZWQgbmFtZXNcbiAgICBkOiBmdW5jdGlvbihrZXksIGN0eCwgcGFydGlhbHMsIHJldHVybkZvdW5kKSB7XG4gICAgICB2YXIgZm91bmQsXG4gICAgICAgICAgbmFtZXMgPSBrZXkuc3BsaXQoJy4nKSxcbiAgICAgICAgICB2YWwgPSB0aGlzLmYobmFtZXNbMF0sIGN0eCwgcGFydGlhbHMsIHJldHVybkZvdW5kKSxcbiAgICAgICAgICBkb01vZGVsR2V0ID0gdGhpcy5vcHRpb25zLm1vZGVsR2V0LFxuICAgICAgICAgIGN4ID0gbnVsbDtcblxuICAgICAgaWYgKGtleSA9PT0gJy4nICYmIGlzQXJyYXkoY3R4W2N0eC5sZW5ndGggLSAyXSkpIHtcbiAgICAgICAgdmFsID0gY3R4W2N0eC5sZW5ndGggLSAxXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgbmFtZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBmb3VuZCA9IGZpbmRJblNjb3BlKG5hbWVzW2ldLCB2YWwsIGRvTW9kZWxHZXQpO1xuICAgICAgICAgIGlmIChmb3VuZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBjeCA9IHZhbDtcbiAgICAgICAgICAgIHZhbCA9IGZvdW5kO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YWwgPSAnJztcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKHJldHVybkZvdW5kICYmICF2YWwpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBpZiAoIXJldHVybkZvdW5kICYmIHR5cGVvZiB2YWwgPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBjdHgucHVzaChjeCk7XG4gICAgICAgIHZhbCA9IHRoaXMubXYodmFsLCBjdHgsIHBhcnRpYWxzKTtcbiAgICAgICAgY3R4LnBvcCgpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdmFsO1xuICAgIH0sXG5cbiAgICAvLyBmaW5kIHZhbHVlcyB3aXRoIG5vcm1hbCBuYW1lc1xuICAgIGY6IGZ1bmN0aW9uKGtleSwgY3R4LCBwYXJ0aWFscywgcmV0dXJuRm91bmQpIHtcbiAgICAgIHZhciB2YWwgPSBmYWxzZSxcbiAgICAgICAgICB2ID0gbnVsbCxcbiAgICAgICAgICBmb3VuZCA9IGZhbHNlLFxuICAgICAgICAgIGRvTW9kZWxHZXQgPSB0aGlzLm9wdGlvbnMubW9kZWxHZXQ7XG5cbiAgICAgIGZvciAodmFyIGkgPSBjdHgubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgdiA9IGN0eFtpXTtcbiAgICAgICAgdmFsID0gZmluZEluU2NvcGUoa2V5LCB2LCBkb01vZGVsR2V0KTtcbiAgICAgICAgaWYgKHZhbCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgZm91bmQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmICghZm91bmQpIHtcbiAgICAgICAgcmV0dXJuIChyZXR1cm5Gb3VuZCkgPyBmYWxzZSA6IFwiXCI7XG4gICAgICB9XG5cbiAgICAgIGlmICghcmV0dXJuRm91bmQgJiYgdHlwZW9mIHZhbCA9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHZhbCA9IHRoaXMubXYodmFsLCBjdHgsIHBhcnRpYWxzKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHZhbDtcbiAgICB9LFxuXG4gICAgLy8gaGlnaGVyIG9yZGVyIHRlbXBsYXRlc1xuICAgIGxzOiBmdW5jdGlvbihmdW5jLCBjeCwgcGFydGlhbHMsIHRleHQsIHRhZ3MpIHtcbiAgICAgIHZhciBvbGRUYWdzID0gdGhpcy5vcHRpb25zLmRlbGltaXRlcnM7XG5cbiAgICAgIHRoaXMub3B0aW9ucy5kZWxpbWl0ZXJzID0gdGFncztcbiAgICAgIHRoaXMuYih0aGlzLmN0KGNvZXJjZVRvU3RyaW5nKGZ1bmMuY2FsbChjeCwgdGV4dCkpLCBjeCwgcGFydGlhbHMpKTtcbiAgICAgIHRoaXMub3B0aW9ucy5kZWxpbWl0ZXJzID0gb2xkVGFncztcblxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0sXG5cbiAgICAvLyBjb21waWxlIHRleHRcbiAgICBjdDogZnVuY3Rpb24odGV4dCwgY3gsIHBhcnRpYWxzKSB7XG4gICAgICBpZiAodGhpcy5vcHRpb25zLmRpc2FibGVMYW1iZGEpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdMYW1iZGEgZmVhdHVyZXMgZGlzYWJsZWQuJyk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5jLmNvbXBpbGUodGV4dCwgdGhpcy5vcHRpb25zKS5yZW5kZXIoY3gsIHBhcnRpYWxzKTtcbiAgICB9LFxuXG4gICAgLy8gdGVtcGxhdGUgcmVzdWx0IGJ1ZmZlcmluZ1xuICAgIGI6IGZ1bmN0aW9uKHMpIHsgdGhpcy5idWYgKz0gczsgfSxcblxuICAgIGZsOiBmdW5jdGlvbigpIHsgdmFyIHIgPSB0aGlzLmJ1ZjsgdGhpcy5idWYgPSAnJzsgcmV0dXJuIHI7IH0sXG5cbiAgICAvLyBtZXRob2QgcmVwbGFjZSBzZWN0aW9uXG4gICAgbXM6IGZ1bmN0aW9uKGZ1bmMsIGN0eCwgcGFydGlhbHMsIGludmVydGVkLCBzdGFydCwgZW5kLCB0YWdzKSB7XG4gICAgICB2YXIgdGV4dFNvdXJjZSxcbiAgICAgICAgICBjeCA9IGN0eFtjdHgubGVuZ3RoIC0gMV0sXG4gICAgICAgICAgcmVzdWx0ID0gZnVuYy5jYWxsKGN4KTtcblxuICAgICAgaWYgKHR5cGVvZiByZXN1bHQgPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBpZiAoaW52ZXJ0ZWQpIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0ZXh0U291cmNlID0gKHRoaXMuYWN0aXZlU3ViICYmIHRoaXMuc3Vic1RleHQgJiYgdGhpcy5zdWJzVGV4dFt0aGlzLmFjdGl2ZVN1Yl0pID8gdGhpcy5zdWJzVGV4dFt0aGlzLmFjdGl2ZVN1Yl0gOiB0aGlzLnRleHQ7XG4gICAgICAgICAgcmV0dXJuIHRoaXMubHMocmVzdWx0LCBjeCwgcGFydGlhbHMsIHRleHRTb3VyY2Uuc3Vic3RyaW5nKHN0YXJ0LCBlbmQpLCB0YWdzKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0sXG5cbiAgICAvLyBtZXRob2QgcmVwbGFjZSB2YXJpYWJsZVxuICAgIG12OiBmdW5jdGlvbihmdW5jLCBjdHgsIHBhcnRpYWxzKSB7XG4gICAgICB2YXIgY3ggPSBjdHhbY3R4Lmxlbmd0aCAtIDFdO1xuICAgICAgdmFyIHJlc3VsdCA9IGZ1bmMuY2FsbChjeCk7XG5cbiAgICAgIGlmICh0eXBlb2YgcmVzdWx0ID09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY3QoY29lcmNlVG9TdHJpbmcocmVzdWx0LmNhbGwoY3gpKSwgY3gsIHBhcnRpYWxzKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9LFxuXG4gICAgc3ViOiBmdW5jdGlvbihuYW1lLCBjb250ZXh0LCBwYXJ0aWFscywgaW5kZW50KSB7XG4gICAgICB2YXIgZiA9IHRoaXMuc3Vic1tuYW1lXTtcbiAgICAgIGlmIChmKSB7XG4gICAgICAgIHRoaXMuYWN0aXZlU3ViID0gbmFtZTtcbiAgICAgICAgZihjb250ZXh0LCBwYXJ0aWFscywgdGhpcywgaW5kZW50KTtcbiAgICAgICAgdGhpcy5hY3RpdmVTdWIgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgfTtcblxuICAvL0ZpbmQgYSBrZXkgaW4gYW4gb2JqZWN0XG4gIGZ1bmN0aW9uIGZpbmRJblNjb3BlKGtleSwgc2NvcGUsIGRvTW9kZWxHZXQpIHtcbiAgICB2YXIgdmFsO1xuXG4gICAgaWYgKHNjb3BlICYmIHR5cGVvZiBzY29wZSA9PSAnb2JqZWN0Jykge1xuXG4gICAgICBpZiAoc2NvcGVba2V5XSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHZhbCA9IHNjb3BlW2tleV07XG5cbiAgICAgIC8vIHRyeSBsb29rdXAgd2l0aCBnZXQgZm9yIGJhY2tib25lIG9yIHNpbWlsYXIgbW9kZWwgZGF0YVxuICAgICAgfSBlbHNlIGlmIChkb01vZGVsR2V0ICYmIHNjb3BlLmdldCAmJiB0eXBlb2Ygc2NvcGUuZ2V0ID09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgdmFsID0gc2NvcGUuZ2V0KGtleSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHZhbDtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNyZWF0ZVNwZWNpYWxpemVkUGFydGlhbChpbnN0YW5jZSwgc3VicywgcGFydGlhbHMsIHN0YWNrU3Vicywgc3RhY2tQYXJ0aWFscywgc3RhY2tUZXh0KSB7XG4gICAgZnVuY3Rpb24gUGFydGlhbFRlbXBsYXRlKCkge307XG4gICAgUGFydGlhbFRlbXBsYXRlLnByb3RvdHlwZSA9IGluc3RhbmNlO1xuICAgIGZ1bmN0aW9uIFN1YnN0aXR1dGlvbnMoKSB7fTtcbiAgICBTdWJzdGl0dXRpb25zLnByb3RvdHlwZSA9IGluc3RhbmNlLnN1YnM7XG4gICAgdmFyIGtleTtcbiAgICB2YXIgcGFydGlhbCA9IG5ldyBQYXJ0aWFsVGVtcGxhdGUoKTtcbiAgICBwYXJ0aWFsLnN1YnMgPSBuZXcgU3Vic3RpdHV0aW9ucygpO1xuICAgIHBhcnRpYWwuc3Vic1RleHQgPSB7fTsgIC8vaGVoZS4gc3Vic3RleHQuXG4gICAgcGFydGlhbC5idWYgPSAnJztcblxuICAgIHN0YWNrU3VicyA9IHN0YWNrU3VicyB8fCB7fTtcbiAgICBwYXJ0aWFsLnN0YWNrU3VicyA9IHN0YWNrU3VicztcbiAgICBwYXJ0aWFsLnN1YnNUZXh0ID0gc3RhY2tUZXh0O1xuICAgIGZvciAoa2V5IGluIHN1YnMpIHtcbiAgICAgIGlmICghc3RhY2tTdWJzW2tleV0pIHN0YWNrU3Vic1trZXldID0gc3Vic1trZXldO1xuICAgIH1cbiAgICBmb3IgKGtleSBpbiBzdGFja1N1YnMpIHtcbiAgICAgIHBhcnRpYWwuc3Vic1trZXldID0gc3RhY2tTdWJzW2tleV07XG4gICAgfVxuXG4gICAgc3RhY2tQYXJ0aWFscyA9IHN0YWNrUGFydGlhbHMgfHwge307XG4gICAgcGFydGlhbC5zdGFja1BhcnRpYWxzID0gc3RhY2tQYXJ0aWFscztcbiAgICBmb3IgKGtleSBpbiBwYXJ0aWFscykge1xuICAgICAgaWYgKCFzdGFja1BhcnRpYWxzW2tleV0pIHN0YWNrUGFydGlhbHNba2V5XSA9IHBhcnRpYWxzW2tleV07XG4gICAgfVxuICAgIGZvciAoa2V5IGluIHN0YWNrUGFydGlhbHMpIHtcbiAgICAgIHBhcnRpYWwucGFydGlhbHNba2V5XSA9IHN0YWNrUGFydGlhbHNba2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gcGFydGlhbDtcbiAgfVxuXG4gIHZhciByQW1wID0gLyYvZyxcbiAgICAgIHJMdCA9IC88L2csXG4gICAgICByR3QgPSAvPi9nLFxuICAgICAgckFwb3MgPSAvXFwnL2csXG4gICAgICByUXVvdCA9IC9cXFwiL2csXG4gICAgICBoQ2hhcnMgPSAvWyY8PlxcXCJcXCddLztcblxuICBmdW5jdGlvbiBjb2VyY2VUb1N0cmluZyh2YWwpIHtcbiAgICByZXR1cm4gU3RyaW5nKCh2YWwgPT09IG51bGwgfHwgdmFsID09PSB1bmRlZmluZWQpID8gJycgOiB2YWwpO1xuICB9XG5cbiAgZnVuY3Rpb24gaG9nYW5Fc2NhcGUoc3RyKSB7XG4gICAgc3RyID0gY29lcmNlVG9TdHJpbmcoc3RyKTtcbiAgICByZXR1cm4gaENoYXJzLnRlc3Qoc3RyKSA/XG4gICAgICBzdHJcbiAgICAgICAgLnJlcGxhY2UockFtcCwgJyZhbXA7JylcbiAgICAgICAgLnJlcGxhY2Uockx0LCAnJmx0OycpXG4gICAgICAgIC5yZXBsYWNlKHJHdCwgJyZndDsnKVxuICAgICAgICAucmVwbGFjZShyQXBvcywgJyYjMzk7JylcbiAgICAgICAgLnJlcGxhY2UoclF1b3QsICcmcXVvdDsnKSA6XG4gICAgICBzdHI7XG4gIH1cblxuICB2YXIgaXNBcnJheSA9IEFycmF5LmlzQXJyYXkgfHwgZnVuY3Rpb24oYSkge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoYSkgPT09ICdbb2JqZWN0IEFycmF5XSc7XG4gIH07XG5cbn0pKHR5cGVvZiBleHBvcnRzICE9PSAndW5kZWZpbmVkJyA/IGV4cG9ydHMgOiBIb2dhbik7XG4iLCIvKlxuICogIENvcHlyaWdodCAyMDExIFR3aXR0ZXIsIEluYy5cbiAqICBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqICBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiAgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbi8vIFRoaXMgZmlsZSBpcyBmb3IgdXNlIHdpdGggTm9kZS5qcy4gU2VlIGRpc3QvIGZvciBicm93c2VyIGZpbGVzLlxuXG52YXIgSG9nYW4gPSByZXF1aXJlKCcuL2NvbXBpbGVyJyk7XG5Ib2dhbi5UZW1wbGF0ZSA9IHJlcXVpcmUoJy4vdGVtcGxhdGUnKS5UZW1wbGF0ZTtcbkhvZ2FuLnRlbXBsYXRlID0gSG9nYW4uVGVtcGxhdGU7XG5tb2R1bGUuZXhwb3J0cyA9IEhvZ2FuO1xuIiwidmFyIGNyb3NzID0gXCI8c3ltYm9sIGlkPVxcXCJlZ21sLWljb25zLWNyb3NzXFxcIiB2aWV3Qm94PVxcXCIwIDAgMTAwIDEwMFxcXCI+PHBhdGggZD1cXFwiTSAyMCw4MCA4MCwyMFxcXCIvPjxwYXRoIGQ9XFxcIk0gMjAsMjAgODAsODBcXFwiLz48L3N5bWJvbD5cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyb3NzO1xyXG4iLCJpbXBvcnQgeyBpbnNlcnRIdG1sLCBpbnNlcnRTdmdTeW1ib2wgfSBmcm9tICdAZWdtbC91dGlscyc7XHJcbmltcG9ydCBNb2RhbCBmcm9tICcuLi9kaXN0L21vZGFsLmpzJztcclxuaW1wb3J0IHRlbXBsYXRlIGZyb20gJy4uL2Rpc3QvbW9kYWwubXVzdGFjaGUnO1xyXG5pbXBvcnQgJy4vbWFpbi5zY3NzJztcclxuaW1wb3J0IGljb25Dcm9zcyBmcm9tICdAZWdtbC9pY29ucy9kaXN0L2VzbS9jcm9zcy5qcyc7XHJcblxyXG52YXIgbW9kYWwgPSBuZXcgTW9kYWwoe1xyXG5cdGF1dG9Jbml0aWFsaXplOiBmYWxzZSxcclxufSk7XHJcblxyXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24oKSB7XHJcblx0aW5zZXJ0SHRtbChcclxuXHRcdHRlbXBsYXRlLnJlbmRlcih7XHJcblx0XHRcdGlkOiBtb2RhbC5pZCxcclxuXHRcdFx0bmFtZTogbW9kYWwubmFtZS5jc3MoKSxcclxuXHRcdFx0c2NhbGU6IDEsXHJcblx0XHR9KVxyXG5cdCk7XHJcblxyXG5cdGluc2VydFN2Z1N5bWJvbChpY29uQ3Jvc3MpO1xyXG5cclxuXHR2YXIgbW9kYWxFbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQobW9kYWwubmFtZS5jc3MobW9kYWwuaWQuY3VycmVudC50b1N0cmluZygpKSk7XHJcblx0bW9kYWwuaW5pdGlhbGl6ZShtb2RhbEVsZW1lbnQpO1xyXG5cdG1vZGFsLmRvbS5zZWxmLmFkZEV2ZW50TGlzdGVuZXIobW9kYWwuZmluaXNoU2hvd0V2ZW50TmFtZSwgZnVuY3Rpb24oKSB7XHJcblx0XHR2YXIgbG9nID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvZycpO1xyXG5cdFx0bG9nLnRleHRDb250ZW50ICs9ICc9PSDQvtGC0LrRgNGL0YLQviA9PVxcbic7XHJcblx0fSwgZmFsc2UpO1xyXG5cdG1vZGFsLmRvbS5zZWxmLmFkZEV2ZW50TGlzdGVuZXIobW9kYWwuZmluaXNoSGlkZUV2ZW50TmFtZSwgZnVuY3Rpb24oKSB7XHJcblx0XHQvLyB3aW5kb3cubG9jYXRpb24ucmVsb2FkKHRydWUpO1xyXG5cdFx0dmFyIGxvZyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2cnKTtcclxuXHRcdGxvZy50ZXh0Q29udGVudCArPSAnPT0g0LfQsNC60YDRi9GC0L4gPT1cXG4nO1xyXG5cdH0sIGZhbHNlKTtcclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBtb2RhbDsiXSwibmFtZXMiOlsidXRpbHMuc3RyaW5nQ2FzZSIsInRoaXMiLCJTcGlubmVyJDEiLCJIb2dhbiIsInJlcXVpcmUkJDAiLCJ0ZW1wbGF0ZSIsImljb25Dcm9zcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBQUE7QUFDQSxBQXdEQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0FBQ0EsQ0FBTyxTQUFTLE1BQU0sQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFO0NBQ3JDLENBQUMsS0FBSyxJQUFJLElBQUksSUFBSSxNQUFNLEVBQUU7Q0FDMUIsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0NBQzVCLEVBQUU7Q0FDRixDQUFDLE9BQU8sSUFBSSxDQUFDO0NBQ2IsQ0FBQztBQUNELEFBaUVBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7QUFDQSxDQUFPLFNBQVMsb0JBQW9CLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRTtDQUN6RCxDQUFDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7Q0FDakMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUU7Q0FDL0MsRUFBRSxNQUFNLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztDQUM3QixFQUFFO0NBQ0YsQ0FBQyxPQUFPLE1BQU0sQ0FBQztDQUNmLENBQUM7O0NBRUQ7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtBQUNBLENBQU8sU0FBUyxRQUFRLENBQUMsS0FBSyxFQUFFO0NBQ2hDLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxLQUFLLE9BQU8sS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDO0NBQy9DLENBQUM7O0NBRUQ7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLElBQUksWUFBWSxDQUFDO0FBQ2pCLENBQU8sU0FBUyxHQUFHLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRTtDQUN2QyxDQUFDLElBQUksT0FBTyxZQUFZLElBQUksUUFBUSxJQUFJLE1BQU0sRUFBRTtDQUNoRDtDQUNBO0NBQ0EsRUFBRSxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQ3pILEVBQUU7Q0FDRixDQUFDLE9BQU8sU0FBUyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7Q0FDdkMsQ0FBQztBQUNELEFBNkJBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7QUFDQSxDQUFPLFNBQVMsZUFBZSxDQUFDLE9BQU8sRUFBRSxZQUFZLEVBQUU7Q0FDdkQsQ0FBQyxZQUFZLEdBQUcsWUFBWSxJQUFJLE1BQU0sQ0FBQztDQUN2QyxDQUFDLElBQUksS0FBSyxDQUFDO0NBQ1gsQ0FBQyxJQUFJLE9BQU8sS0FBSyxLQUFLLEdBQUcsWUFBWSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUU7Q0FDbEUsRUFBRSxPQUFPLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLE1BQU07Q0FDL0MsT0FBTyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztDQUM3RCxPQUFPLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Q0FDakUsRUFBRSxNQUFNO0NBQ1IsRUFBRSxPQUFPLENBQUMsQ0FBQztDQUNYLEVBQUU7Q0FDRixDQUFDO0FBQ0QsQUErQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtBQUNBLENBQU8sU0FBUyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRTtDQUN2QyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO0NBQzVCLEVBQUUsT0FBTyxFQUFFLENBQUM7Q0FDWixFQUFFO0NBQ0YsQ0FBQyxJQUFJLE9BQU8sSUFBSSxJQUFJLFFBQVEsRUFBRTtDQUM5QixFQUFFLElBQUksR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDO0NBQ2xCLEVBQUU7Q0FDRixDQUFDLElBQUksTUFBTSxJQUFJLEVBQUUsQ0FBQztDQUNsQixDQUFDLFFBQVEsSUFBSTtDQUNiLEVBQUUsS0FBSyxPQUFPO0NBQ2QsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtDQUNqQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtDQUNoQixLQUFLLE1BQU0sSUFBSSxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7Q0FDbkMsS0FBSyxNQUFNO0NBQ1gsS0FBSyxNQUFNLElBQUksY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0NBQ25DLEtBQUs7Q0FDTCxJQUFJLENBQUMsQ0FBQztDQUNOLEdBQUcsTUFBTTs7Q0FFVCxFQUFFLEtBQUssY0FBYztDQUNyQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLEVBQUU7Q0FDOUIsSUFBSSxNQUFNLElBQUksY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0NBQ2xDLElBQUksQ0FBQyxDQUFDO0NBQ04sR0FBRyxNQUFNOztDQUVULEVBQUUsS0FBSyxNQUFNO0NBQ2IsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztDQUMxQixHQUFHLE1BQU07O0NBRVQsRUFBRSxLQUFLLE9BQU87Q0FDZCxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0NBQzNCLEdBQUcsTUFBTTs7Q0FFVCxFQUFFLEtBQUssT0FBTztDQUNkLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Q0FDM0IsR0FBRyxNQUFNOztDQUVULEVBQUUsS0FBSyxLQUFLO0NBQ1osR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztDQUMzQixHQUFHLE1BQU07Q0FDVDtDQUNBLEVBQUUsS0FBSyxNQUFNLENBQUM7Q0FDZCxFQUFFO0NBQ0YsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztDQUMxQixHQUFHLE1BQU07Q0FDVCxFQUFFO0NBQ0YsQ0FBQyxPQUFPLE1BQU0sQ0FBQztDQUNmLENBQUM7O0NBRUQ7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7QUFDQSxDQUFPLFNBQVMsY0FBYyxDQUFDLE1BQU0sRUFBRTtDQUN2QyxDQUFDLE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDbEQsQ0FBQyxBQUNNLFNBQVMsY0FBYyxDQUFDLE1BQU0sRUFBRTtDQUN2QyxDQUFDLE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDbEQsQ0FBQyxBQWdORDtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0FBQ0EsQ0FBTyxTQUFTLFVBQVUsR0FBRztDQUM3QixDQUFDLElBQUksT0FBTyxHQUFHO0NBQ2YsRUFBRSxJQUFJLEVBQUUsSUFBSTtDQUNaLEVBQUUsTUFBTSxFQUFFLEtBQUs7Q0FDZixFQUFFLEdBQUcsRUFBRSxJQUFJO0NBQ1gsRUFBRSxjQUFjLEVBQUUsUUFBUTtDQUMxQixFQUFFLGNBQWMsRUFBRSxJQUFJO0NBQ3RCLEVBQUUsQ0FBQztDQUNIO0NBQ0EsQ0FBQyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUM7Q0FDekI7Q0FDQSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLGNBQWMsRUFBRTtDQUNoRCxFQUFFLEdBQUcsRUFBRSxXQUFXO0NBQ2xCLEdBQUcsSUFBSSxZQUFZLElBQUksSUFBSSxFQUFFO0NBQzdCLElBQUksT0FBTyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztDQUN2QyxJQUFJLE1BQU07Q0FDVixJQUFJLE9BQU8sWUFBWSxDQUFDO0NBQ3hCLElBQUk7Q0FDSixHQUFHO0NBQ0gsRUFBRSxHQUFHLEVBQUUsU0FBUyxLQUFLLEVBQUU7Q0FDdkIsR0FBRyxZQUFZLEdBQUcsS0FBSyxDQUFDO0NBQ3hCLEdBQUc7Q0FDSCxFQUFFLFlBQVksRUFBRSxJQUFJO0NBQ3BCLEVBQUUsVUFBVSxFQUFFLElBQUk7Q0FDbEIsRUFBRSxDQUFDLENBQUM7Q0FDSjtDQUNBLENBQUMsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZLE1BQU0sRUFBRTtDQUN4RSxFQUFFLE9BQU8sQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQzlCLEVBQUUsTUFBTSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksUUFBUSxFQUFFO0NBQy9ELEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUNoQyxFQUFFLE1BQU07Q0FDUixFQUFFLE9BQU87Q0FDVCxFQUFFOztDQUVGLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxFQUFFO0NBQ2xCLEVBQUUsSUFBSSxHQUFHLEdBQUcsSUFBSSxjQUFjLEVBQUUsQ0FBQztDQUNqQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztDQUMvQixFQUFFLEdBQUcsQ0FBQyxNQUFNLEdBQUcsV0FBVztDQUMxQixHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sRUFBRTtDQUN2QjtDQUNBLElBQUksSUFBSSxNQUFNLEdBQUcsSUFBSSxTQUFTLEVBQUUsQ0FBQztDQUNqQyxJQUFJLElBQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsOEJBQThCLEdBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7Q0FDekg7Q0FDQSxJQUFJLElBQUksV0FBVyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztDQUMvRyxJQUFJLE1BQU07Q0FDVixJQUFJLElBQUksV0FBVyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0NBQ3ZHLElBQUk7Q0FDSixHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0NBQ2pELEdBQUcsSUFBSSxPQUFPLE9BQU8sQ0FBQyxjQUFjLElBQUksVUFBVSxFQUFFO0NBQ3BELElBQUksT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDO0NBQzdCLElBQUk7Q0FDSixHQUFHLENBQUM7Q0FDSixFQUFFLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztDQUNiLEVBQUUsTUFBTSxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQUU7Q0FDMUIsRUFBRSxJQUFJLE9BQU8sQ0FBQyxNQUFNLEVBQUU7Q0FDdEI7Q0FDQSxHQUFHLElBQUksTUFBTSxHQUFHLElBQUksU0FBUyxFQUFFLENBQUM7Q0FDaEMsR0FBRyxJQUFJLFdBQVcsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDLDhCQUE4QixHQUFHLE9BQU8sQ0FBQyxJQUFJLEdBQUcsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0NBQ25ILEdBQUcsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0NBQzlHLEdBQUcsTUFBTTtDQUNULEdBQUcsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Q0FDakcsR0FBRztDQUNILEVBQUUsT0FBTyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7Q0FDaEQsRUFBRSxJQUFJLE9BQU8sT0FBTyxDQUFDLGNBQWMsSUFBSSxVQUFVLEVBQUU7Q0FDbkQsR0FBRyxPQUFPLENBQUMsY0FBYyxFQUFFLENBQUM7Q0FDNUIsR0FBRztDQUNILEVBQUUsTUFBTTtDQUNSLEVBQUUsT0FBTztDQUNULEVBQUU7Q0FDRixDQUFDOztDQUVEO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0FBQ0EsQ0FBTyxTQUFTLGVBQWUsR0FBRztDQUNsQztDQUNBO0NBQ0E7Q0FDQTtDQUNBLENBQUMsSUFBSSxPQUFPLEdBQUc7Q0FDZixFQUFFLE1BQU0sRUFBRSxJQUFJO0NBQ2QsRUFBRSxZQUFZLEVBQUUsUUFBUSxDQUFDLElBQUk7Q0FDN0IsRUFBRSxjQUFjLEVBQUUsSUFBSTtDQUN0QixFQUFFLENBQUM7Q0FDSDtDQUNBLENBQUMsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZLE1BQU0sRUFBRTtDQUN4RSxFQUFFLE9BQU8sQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQ2hDLEVBQUUsT0FBTyxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQztDQUM5RCxFQUFFLE9BQU8sQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUM7Q0FDbEUsRUFBRSxNQUFNLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLEVBQUU7Q0FDL0QsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQ2hDLEVBQUUsTUFBTTtDQUNSLEVBQUUsT0FBTztDQUNULEVBQUU7O0NBRUYsQ0FBQyxJQUFJLE1BQU0sR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO0NBQzlCLENBQUMsSUFBSSxjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWU7Q0FDNUMsRUFBRSxDQUFDO0dBQ0EsR0FBRyxPQUFPLENBQUMsY0FBYyxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFOztHQUV0RSxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFDWixDQUFDO0NBQ1QsRUFBRSxlQUFlO0NBQ2pCLEVBQUUsQ0FBQztDQUNILENBQUMsSUFBSSxVQUFVLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO0NBQzVFLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7O0NBRTlDO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBOztDQUVBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7O0NBRUE7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7O0VBQUMsRENseEJjLFNBQVMsVUFBVSxDQUFDLElBQUksRUFBRTtDQUN6QyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDOztDQUVsQixDQUFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtDQUMxQixFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Q0FDakMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtDQUNoQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7Q0FDL0IsR0FBRyxDQUFDLENBQUM7Q0FDTCxFQUFFLE1BQU07Q0FDUixFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztDQUN4QixFQUFFLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Q0FDNUIsRUFBRTs7Q0FFRixDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUdBLFVBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQzlDLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBR0EsVUFBZ0IsQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUM7Q0FDNUQsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHQSxVQUFnQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztDQUM5QyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUdBLFVBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQzlDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBR0EsVUFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Q0FDNUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHQSxVQUFnQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztDQUMxQyxDQUFDO0NBQ0QsVUFBVSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsV0FBVztDQUMzQyxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztDQUNsQixDQUFDOztHQUFDLEZDckJhLFNBQVMsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUU7Q0FDL0MsQ0FBQyxJQUFJLElBQUksQ0FBQzs7Q0FFVjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLENBQUMsSUFBSSxNQUFNLElBQUksSUFBSSxFQUFFO0NBQ3JCLEVBQUUsSUFBSSxPQUFPLE1BQU0sSUFBSSxTQUFTLElBQUksTUFBTSxJQUFJLEtBQUssRUFBRTtDQUNyRCxHQUFHLE1BQU0sR0FBRyxFQUFFLENBQUM7Q0FDZixHQUFHO0NBQ0gsRUFBRSxJQUFJLE9BQU8sTUFBTSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO0NBQzFELEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFO0NBQ3JDLElBQUksS0FBSyxFQUFFLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQztDQUNqQyxJQUFJLFFBQVEsRUFBRSxJQUFJO0NBQ2xCLElBQUksWUFBWSxFQUFFLElBQUk7Q0FDdEIsSUFBSSxVQUFVLEVBQUUsSUFBSTtDQUNwQixJQUFJLENBQUMsQ0FBQztDQUNOLEdBQUc7Q0FDSCxFQUFFOztDQUVGLENBQUMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQzs7Q0FFakQsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztDQUV0QztDQUNBLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7Q0FDaEUsQ0FBQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtDQUN4QyxFQUFFLElBQUksS0FBSyxFQUFFO0NBQ2IsR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHQSxVQUFnQixDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDL0YsR0FBRyxNQUFNO0NBQ1QsR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDOUMsR0FBRztDQUNILEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxJQUFJLEVBQUUsT0FBTyxFQUFFO0NBQzVDLEdBQUcsT0FBTyxTQUFTLE1BQU0sRUFBRTtDQUMzQixJQUFJLElBQUksTUFBTSxFQUFFO0NBQ2hCLEtBQUssSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO0NBQ3pCLE1BQU0sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO0NBQ2pDLE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLEVBQUU7Q0FDdkMsUUFBUSxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO0NBQ3RDLFFBQVEsQ0FBQyxDQUFDO0NBQ1YsT0FBTztDQUNQLE1BQU07Q0FDTixLQUFLLE9BQU9BLFVBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFQSxVQUFnQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0NBQzFGLEtBQUssTUFBTTtDQUNYLEtBQUssT0FBTyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDO0NBQ2hDLEtBQUs7Q0FDTCxJQUFJLENBQUM7Q0FDTCxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0NBQ3JCLEVBQUU7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksSUFBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztDQUNoQyxDQUFDLElBQUksS0FBSyxFQUFFO0NBQ1osRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUM7Q0FDOUIsRUFBRTtDQUNGLENBQUMsSUFBSSxDQUFDLElBQUksR0FBR0EsVUFBZ0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7Q0FDM0MsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLFNBQVMsTUFBTSxFQUFFO0NBQzdCLEVBQUUsSUFBSSxNQUFNLEVBQUU7Q0FDZCxHQUFHLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtDQUM5QixJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxFQUFFO0NBQ3BDLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztDQUNuQyxLQUFLLENBQUMsQ0FBQztDQUNQLElBQUk7Q0FDSixHQUFHLE9BQU9BLFVBQWdCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFQSxVQUFnQixDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0NBQ2xGLEdBQUcsTUFBTTtDQUNULEdBQUcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0NBQ3BCLEdBQUc7Q0FDSCxFQUFFLENBQUM7O0NBRUg7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQzs7Q0FFdkI7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7Q0FDNUIsQ0FBQyxJQUFJLEtBQUssRUFBRTtDQUNaLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQzlCLEVBQUU7Q0FDRixDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUdBLFVBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQzdDLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxTQUFTLE1BQU0sRUFBRTtDQUM3QixFQUFFLElBQUksTUFBTSxFQUFFO0NBQ2QsR0FBRyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7Q0FDOUIsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtDQUNwQyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7Q0FDbkMsS0FBSyxDQUFDLENBQUM7Q0FDUCxJQUFJO0NBQ0osR0FBRyxPQUFPQSxVQUFnQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRUEsVUFBZ0IsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztDQUNwRixHQUFHLE1BQU07Q0FDVCxHQUFHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztDQUNwQixHQUFHO0NBQ0gsRUFBRSxDQUFDOztDQUVIO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLFNBQVMsZUFBZSxFQUFFO0NBQ2pELEVBQUUsSUFBSSxlQUFlLElBQUksSUFBSSxFQUFFO0NBQy9CLEdBQUcsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxFQUFFO0NBQ3ZDLElBQUksZUFBZSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLEVBQUU7Q0FDN0MsS0FBSyxlQUFlLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO0NBQzVDLEtBQUssQ0FBQyxDQUFDO0NBQ1AsSUFBSTtDQUNKLEdBQUcsT0FBTyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxHQUFHQSxVQUFnQixDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsQ0FBQztDQUN6RSxHQUFHLE1BQU07Q0FDVCxHQUFHLE9BQU8sSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0NBQ3JCLEdBQUc7Q0FDSCxFQUFFLENBQUM7Q0FDSCxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQzs7Q0FFbkM7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLFNBQVMsRUFBRTtDQUM3QyxFQUFFLE9BQU8sR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7Q0FDbkMsRUFBRSxDQUFDO0NBQ0gsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsU0FBUyxFQUFFO0NBQ3JDLEVBQUUsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7Q0FDMUMsRUFBRSxDQUFDO0NBQ0gsQ0FBQztDQUNELElBQUksRUFBRSxDQUFDO0NBQ1AsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRTtDQUM1QyxDQUFDLEdBQUcsRUFBRSxTQUFTLEtBQUssRUFBRTtDQUN0QixFQUFFLEVBQUUsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztDQUM3QixFQUFFO0NBQ0YsQ0FBQyxHQUFHLEVBQUUsV0FBVztDQUNqQixFQUFFLE9BQU8sRUFBRSxDQUFDO0NBQ1osRUFBRTtDQUNGLENBQUMsQ0FBQyxDQUFDO0NBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsV0FBVztDQUNyQyxDQUFDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztDQUNuQixDQUFDOztHQUFDLEZDcExhLFNBQVMsRUFBRSxHQUFHO0NBQzdCLENBQUM7Q0FDRCxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxXQUFXO0NBQ25DLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0NBQ3JCLENBQUM7O0dBQUMsRkNBRjtDQUNBO0NBQ0E7Q0FDQTtBQUNBLENBQWUsU0FBUyxNQUFNLEdBQUc7Q0FDakMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUM7Q0FDOUIsQ0FBQyxBQUNEO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUU7Q0FDdEMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQztDQUNqQyxDQUFDLENBQUMsQ0FBQzs7Q0FFSCxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztDQUMzQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Q0FDL0IsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO0NBQzlCLE1BQU0sQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztDQUNyQyxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7O0NBRXZDO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRztDQUM3QjtDQUNBLENBQUMsSUFBSSxNQUFNLEVBQUUsT0FBTyxDQUFDO0NBQ3JCLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLFlBQVksT0FBTyxFQUFFO0NBQ3RDLEVBQUUsTUFBTSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUN4QixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxRQUFRLEVBQUU7Q0FDekQsR0FBRyxPQUFPLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQzFCLEdBQUcsSUFBSSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxTQUFTLEVBQUU7Q0FDekMsSUFBSSxPQUFPLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUMxQyxJQUFJO0NBQ0osR0FBRztDQUNILEVBQUUsTUFBTSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksUUFBUSxFQUFFO0NBQy9ELEVBQUUsT0FBTyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUN6QixFQUFFLElBQUksT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksU0FBUyxFQUFFO0NBQ3hDLEdBQUcsT0FBTyxDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDekMsR0FBRztDQUNILEVBQUU7Q0FDRixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Q0FDekMsQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Q0FDMUIsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Q0FDakMsRUFBRTtDQUNGLENBQUMsQ0FBQzs7Q0FFRjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLE1BQU0sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxNQUFNLEVBQUUsT0FBTztDQUM3RDtDQUNBLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRztDQUNaLEVBQUUsSUFBSSxFQUFFLE1BQU07Q0FDZCxFQUFFLENBQUM7Q0FDSCxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxFQUFFLENBQUM7Q0FDbEIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0NBQzFCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztDQUM3QixDQUFDLENBQUM7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFNBQVMsTUFBTTtDQUM3QztDQUNBLENBQUMsSUFBSSxPQUFPLE1BQU0sSUFBSSxXQUFXLEVBQUU7Q0FDbkMsRUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7Q0FDekIsRUFBRTtDQUNGLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0NBQ2xDLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7Q0FDekIsQ0FBQyxDQUFDOztDQUVGO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLE1BQU0sQ0FBQyxTQUFTLENBQUMsbUJBQW1CLEdBQUcsU0FBUyxNQUFNO0NBQ3REO0NBQ0EsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7Q0FDeEIsQ0FBQyxDQUFDOztDQUVGLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFNBQVMsT0FBTztDQUM5QztDQUNBLENBQUMsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUU7Q0FDeEIsRUFBRSxNQUFNLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQ3hCLEVBQUU7Q0FDRixDQUFDLENBQUM7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsTUFBTSxDQUFDLFNBQVMsQ0FBQyxjQUFjLEdBQUcsU0FBUyxXQUFXO0NBQ3REO0NBQ0EsQ0FBQyxXQUFXLEdBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUM7Q0FDL0MsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUU7Q0FDekQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQztDQUNsRDtDQUNBLEVBQUU7Q0FDRixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztDQUMxRCxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztDQUNqRCxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7Q0FDakQsQ0FBQyxDQUFDOztDQUVGO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxTQUFTLFdBQVc7Q0FDdEQ7Q0FDQSxDQUFDLFdBQVcsR0FBRyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQztDQUMvQyxDQUFDLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztDQUMzQyxDQUFDLElBQUksSUFBSSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztDQUNuRCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7Q0FDbEMsQ0FBQyxDQUFDOztDQUVGO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxNQUFNLENBQUMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLFNBQVMsT0FBTyxFQUFFLFdBQVc7Q0FDcEU7Q0FDQSxDQUFDLE9BQU8sV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQ2pHLENBQUMsQ0FBQzs7Q0FFRixNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLEtBQUs7Q0FDMUM7Q0FDQSxDQUFDLElBQUksVUFBVSxHQUFHLFVBQVUsR0FBRyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7Q0FDckQsQ0FBQyxJQUFJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLFVBQVUsRUFBRTtDQUM1QyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO0NBQ3JCLEVBQUU7Q0FDRixDQUFDLENBQUM7O0NBRUYsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsU0FBUyxLQUFLO0NBQzVDO0NBQ0EsQ0FBQyxJQUFJLEtBQUssSUFBSSxLQUFLLEVBQUU7Q0FDckI7Q0FDQSxFQUFFLEtBQUssSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO0NBQzdCLEdBQUcsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLEVBQUU7Q0FDL0MsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQztDQUNyQixJQUFJO0NBQ0osR0FBRztDQUNILEVBQUUsTUFBTTtDQUNSLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRTtDQUNkLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTztDQUMzQixHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0NBQ3RCLEdBQUc7Q0FDSCxFQUFFLElBQUksVUFBVSxHQUFHLFlBQVksR0FBRyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7Q0FDeEQsRUFBRSxJQUFJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLFVBQVUsRUFBRTtDQUM3QyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO0NBQ3RCLEdBQUc7Q0FDSCxFQUFFO0NBQ0YsQ0FBQzs7Ozs7Ozs7R0FBQztBQzNORixDQStCQyxDQUFDLFVBQVUsSUFBSSxFQUFFLE9BQU8sRUFBRTs7O0dBR3pCLElBQUksQUFBNkIsTUFBTSxDQUFDLE9BQU8sRUFBRSxjQUFjLEdBQUcsT0FBTyxHQUFFOzs7UUFHdEUsQUFHQSxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sR0FBRTtFQUM5QixDQUFDQyxjQUFJLEVBQUUsWUFBWTs7R0FHbEIsSUFBSSxRQUFRLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLENBQUM7T0FDdkMsVUFBVSxHQUFHLEVBQUU7T0FDZixnQkFBZ0I7T0FDaEIsTUFBSzs7Ozs7O0dBTVQsU0FBUyxRQUFRLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRTtLQUM1QixJQUFJLEVBQUUsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxLQUFLLENBQUM7U0FDekMsRUFBQzs7S0FFTCxLQUFLLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEVBQUM7S0FDL0IsT0FBTyxFQUFFO0lBQ1Y7Ozs7O0dBS0QsU0FBUyxHQUFHLEVBQUUsTUFBTSwyQkFBMkI7S0FDN0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtPQUNoRCxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBQztNQUNqQzs7S0FFRCxPQUFPLE1BQU07SUFDZDs7Ozs7OztHQU9ELFNBQVMsWUFBWSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRTtLQUM3QyxJQUFJLElBQUksR0FBRyxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEtBQUssR0FBRyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztTQUM5RCxLQUFLLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsR0FBRztTQUM1QixDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLEtBQUssSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxDQUFDO1NBQ3hELE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRTtTQUMzRixHQUFHLEdBQUcsTUFBTSxJQUFJLEdBQUcsR0FBRyxNQUFNLEdBQUcsR0FBRyxJQUFJLEdBQUU7O0tBRTVDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7T0FDckIsS0FBSyxDQUFDLFVBQVU7U0FDZCxHQUFHLEdBQUcsR0FBRyxHQUFHLFlBQVksR0FBRyxJQUFJLEdBQUcsR0FBRztTQUNyQyxhQUFhLEdBQUcsQ0FBQyxHQUFHLEdBQUc7U0FDdkIsS0FBSyxHQUFHLFlBQVksR0FBRyxLQUFLLEdBQUcsR0FBRztVQUNqQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsY0FBYztTQUM3QixDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksR0FBRyxHQUFHLFlBQVksR0FBRyxLQUFLLEdBQUcsR0FBRztTQUNoRCxlQUFlLEdBQUcsQ0FBQyxHQUFHLEdBQUc7U0FDekIsR0FBRyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFDOztPQUU3QixVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBQztNQUNyQjs7S0FFRCxPQUFPLElBQUk7SUFDWjs7Ozs7R0FLRCxTQUFTLE1BQU0sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFO0tBQ3pCLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxLQUFLO1NBQ1osRUFBRTtTQUNGLEVBQUM7O0tBRUwsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUM7S0FDbkQsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssU0FBUyxFQUFFLE9BQU8sSUFBSTtLQUN0QyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7T0FDcEMsRUFBRSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJO09BQ3JCLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFNBQVMsRUFBRSxPQUFPLEVBQUU7TUFDbkM7SUFDRjs7Ozs7R0FLRCxTQUFTLEdBQUcsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFO0tBQ3RCLEtBQUssSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFO09BQ2xCLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxFQUFDO01BQ3ZDOztLQUVELE9BQU8sRUFBRTtJQUNWOzs7OztHQUtELFNBQVMsS0FBSyxFQUFFLEdBQUcsRUFBRTtLQUNuQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtPQUN6QyxJQUFJLEdBQUcsR0FBRyxTQUFTLENBQUMsQ0FBQyxFQUFDO09BQ3RCLEtBQUssSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFO1NBQ2pCLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsRUFBQztRQUMxQztNQUNGO0tBQ0QsT0FBTyxHQUFHO0lBQ1g7Ozs7O0dBS0QsU0FBUyxRQUFRLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRTtLQUM3QixPQUFPLE9BQU8sS0FBSyxJQUFJLFFBQVEsR0FBRyxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQ3BFOzs7O0dBSUQsSUFBSSxRQUFRLEdBQUc7S0FDYixLQUFLLEVBQUUsRUFBRTtLQUNULE1BQU0sRUFBRSxDQUFDO0tBQ1QsS0FBSyxFQUFFLENBQUM7S0FDUixNQUFNLEVBQUUsRUFBRTtLQUNWLEtBQUssRUFBRSxHQUFHO0tBQ1YsT0FBTyxFQUFFLENBQUM7S0FDVixLQUFLLEVBQUUsTUFBTTtLQUNiLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztLQUNaLE1BQU0sRUFBRSxDQUFDO0tBQ1QsU0FBUyxFQUFFLENBQUM7S0FDWixLQUFLLEVBQUUsQ0FBQztLQUNSLEtBQUssRUFBRSxHQUFHO0tBQ1YsR0FBRyxFQUFFLEVBQUU7S0FDUCxNQUFNLEVBQUUsR0FBRztLQUNYLFNBQVMsRUFBRSxTQUFTO0tBQ3BCLEdBQUcsRUFBRSxLQUFLO0tBQ1YsSUFBSSxFQUFFLEtBQUs7S0FDWCxNQUFNLEVBQUUsS0FBSztLQUNiLE9BQU8sRUFBRSxLQUFLO0tBQ2QsUUFBUSxFQUFFLFVBQVU7S0FDckI7OztHQUdELFNBQVMsT0FBTyxFQUFFLENBQUMsRUFBRTtLQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLE9BQU8sQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFDO0lBQ3ZEOzs7R0FHRCxPQUFPLENBQUMsUUFBUSxHQUFHLEdBQUU7O0dBRXJCLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFOzs7Ozs7S0FNdkIsSUFBSSxFQUFFLFVBQVUsTUFBTSxFQUFFO09BQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUU7O09BRVgsSUFBSSxJQUFJLEdBQUcsSUFBSTtXQUNYLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSTtXQUNiLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFDOztPQUUzRCxHQUFHLENBQUMsRUFBRSxFQUFFO1NBQ04sUUFBUSxFQUFFLENBQUMsQ0FBQyxRQUFRO1NBQ3BCLEtBQUssRUFBRSxDQUFDO1NBQ1IsTUFBTSxFQUFFLENBQUMsQ0FBQyxNQUFNO1NBQ2hCLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSTtTQUNaLEdBQUcsRUFBRSxDQUFDLENBQUMsR0FBRztRQUNYLEVBQUM7O09BRUYsSUFBSSxNQUFNLEVBQUU7U0FDVixNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsVUFBVSxJQUFJLElBQUksRUFBQztRQUNuRDs7T0FFRCxFQUFFLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxhQUFhLEVBQUM7T0FDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBQzs7T0FFekIsSUFBSSxDQUFDLGdCQUFnQixFQUFFOztTQUVyQixJQUFJLENBQUMsR0FBRyxDQUFDO2FBQ0wsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDO2FBQzdDLEtBQUs7YUFDTCxHQUFHLEdBQUcsQ0FBQyxDQUFDLEdBQUc7YUFDWCxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxLQUFLO2FBQ2pCLEtBQUssR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQzthQUM3QyxLQUFLLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLOztVQUV0QixDQUFDLFNBQVMsSUFBSSxJQUFJO1dBQ2pCLENBQUMsR0FBRTtXQUNILEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFO2FBQ2hDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUM7O2FBRXhFLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxHQUFHLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFDO1lBQ3BEO1dBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsRUFBRSxJQUFJLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLElBQUksR0FBRyxHQUFHLENBQUMsRUFBQztVQUMzRCxJQUFHO1FBQ0w7T0FDRCxPQUFPLElBQUk7TUFDWjs7Ozs7S0FLRCxJQUFJLEVBQUUsWUFBWTtPQUNoQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsR0FBRTtPQUNoQixJQUFJLEVBQUUsRUFBRTtTQUNOLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFDO1NBQzFCLElBQUksRUFBRSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxFQUFFLEVBQUM7U0FDaEQsSUFBSSxDQUFDLEVBQUUsR0FBRyxVQUFTO1FBQ3BCO09BQ0QsT0FBTyxJQUFJO01BQ1o7Ozs7OztLQU1ELEtBQUssRUFBRSxVQUFVLEVBQUUsRUFBRSxDQUFDLEVBQUU7T0FDdEIsSUFBSSxDQUFDLEdBQUcsQ0FBQztXQUNMLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztXQUM3QyxJQUFHOztPQUVQLFNBQVMsSUFBSSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUU7U0FDNUIsT0FBTyxHQUFHLENBQUMsUUFBUSxFQUFFLEVBQUU7V0FDckIsUUFBUSxFQUFFLFVBQVU7V0FDcEIsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSTtXQUM1QyxNQUFNLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUk7V0FDaEMsVUFBVSxFQUFFLEtBQUs7V0FDakIsU0FBUyxFQUFFLE1BQU07V0FDakIsZUFBZSxFQUFFLE1BQU07V0FDdkIsU0FBUyxFQUFFLFNBQVMsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxpQkFBaUIsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLEtBQUs7V0FDekcsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLElBQUk7VUFDMUQsQ0FBQztRQUNIOztPQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUU7U0FDdkIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxRQUFRLEVBQUUsRUFBRTtXQUNwQixRQUFRLEVBQUUsVUFBVTtXQUNwQixHQUFHLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUk7V0FDeEMsU0FBUyxFQUFFLENBQUMsQ0FBQyxPQUFPLEdBQUcsb0JBQW9CLEdBQUcsRUFBRTtXQUNoRCxPQUFPLEVBQUUsQ0FBQyxDQUFDLE9BQU87V0FDbEIsU0FBUyxFQUFFLGdCQUFnQixJQUFJLFlBQVksQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxLQUFLLEVBQUUsS0FBSyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsbUJBQW1CO1VBQzVJLEVBQUM7O1NBRUYsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsY0FBYyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUMsRUFBQztTQUN2RSxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxFQUFFLHdCQUF3QixDQUFDLENBQUMsRUFBQztRQUN4RTtPQUNELE9BQU8sRUFBRTtNQUNWOzs7Ozs7S0FNRCxPQUFPLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRTtPQUM3QixJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBRztNQUNuRTs7SUFFRixFQUFDOzs7R0FHRixTQUFTLE9BQU8sSUFBSTs7O0tBR2xCLFNBQVMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUU7T0FDdkIsT0FBTyxRQUFRLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRywwREFBMEQsRUFBRSxJQUFJLENBQUM7TUFDOUY7OztLQUdELEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLDRCQUE0QixFQUFDOztLQUV4RCxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxVQUFVLEVBQUUsRUFBRSxDQUFDLEVBQUU7T0FDekMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7V0FDbEMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLEVBQUM7O09BRXZCLFNBQVMsR0FBRyxJQUFJO1NBQ2QsT0FBTyxHQUFHO1dBQ1IsR0FBRyxDQUFDLE9BQU8sRUFBRTthQUNYLFNBQVMsRUFBRSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7YUFDdEIsV0FBVyxFQUFFLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDM0IsQ0FBQztXQUNGLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFO1VBQ3hCO1FBQ0Y7O09BRUQsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxJQUFJO1dBQ25ELENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1dBQ2pFLEVBQUM7O09BRUwsU0FBUyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUU7U0FDM0IsR0FBRztXQUNELENBQUM7V0FDRCxHQUFHO2FBQ0QsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM3RCxHQUFHO2VBQ0QsR0FBRztpQkFDRCxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDdEMsRUFBRSxLQUFLLEVBQUUsQ0FBQzttQkFDUixNQUFNLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSzttQkFDekIsSUFBSSxFQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLE1BQU07bUJBQ3hCLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDO21CQUM1QixNQUFNLEVBQUUsTUFBTTtrQkFDZjtnQkFDRjtlQUNELEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztlQUM5RCxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO2NBQzVCO1lBQ0Y7V0FDRjtRQUNGOztPQUVELElBQUksQ0FBQyxDQUFDLE1BQU07U0FDVixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUU7V0FDN0IsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxxRkFBcUYsRUFBQztVQUNsRzs7T0FFSCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBQztPQUNyQyxPQUFPLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO09BQ2xCOztLQUVELE9BQU8sQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLFVBQVUsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFO09BQ25ELElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxXQUFVO09BQ3JCLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBQztPQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFO1NBQ3BDLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLFdBQVU7U0FDckUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sR0FBRyxJQUFHO1FBQ3ZCO09BQ0Y7SUFDRjs7R0FFRCxJQUFJLE9BQU8sUUFBUSxLQUFLLFdBQVcsRUFBRTtLQUNuQyxLQUFLLElBQUksWUFBWTtPQUNuQixJQUFJLEVBQUUsR0FBRyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxFQUFDO09BQy9DLEdBQUcsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFDO09BQ2pELE9BQU8sRUFBRSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsVUFBVTtNQUNqQyxFQUFFLEVBQUM7O0tBRUosSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxFQUFDOztLQUVuRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsSUFBSSxLQUFLLENBQUMsR0FBRyxFQUFFLE9BQU8sR0FBRTtVQUNsRCxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBQztJQUNuRDs7R0FFRCxPQUFPLE9BQU87O0VBRWYsQ0FBQyxFQUFFOzs7Q0NuWEosU0FBUyxLQUFLLEdBQUc7Q0FDakIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLFNBQVMsQ0FBQyxDQUFDO0NBQ2pDLENBQUM7Q0FDRCxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUU7Q0FDckMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQztDQUNoQyxDQUFDLENBQUMsQ0FBQztDQUNIO0NBQ0EsS0FBSyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztDQUNsRCxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsYUFBYSxFQUFFO0NBQ3RELENBQUMsS0FBSyxFQUFFLEtBQUs7Q0FDYixDQUFDLFVBQVUsRUFBRSxLQUFLO0NBQ2xCLENBQUMsUUFBUSxFQUFFLElBQUk7Q0FDZixDQUFDLENBQUMsQ0FBQzs7Q0FFSCxJQUFJLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztDQUM3QjtDQUNBLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztDQUM1QixLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Q0FDOUIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0NBQy9CLEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztDQUMvQixLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7Q0FDM0IsS0FBSyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0NBQy9CLEtBQUssQ0FBQyxTQUFTLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztDQUN2QyxLQUFLLENBQUMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztDQUNyRSxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztDQUM3QyxLQUFLLENBQUMsU0FBUyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7Q0FDdkMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7Q0FDckUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7Q0FDN0MsS0FBSyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDO0NBQ25DO0NBQ0EsS0FBSyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLE1BQU0sRUFBRSxPQUFPO0NBQzVEO0NBQ0EsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0NBQ2hFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztDQUM1QjtDQUNBLENBQUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksS0FBSyxFQUFFO0NBQ3RELEVBQUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Q0FDbkUsR0FBRyxNQUFNLEVBQUUsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztDQUMvRCxHQUFHLENBQUMsQ0FBQztDQUNMLEVBQUU7Q0FDRixDQUFDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEtBQUssRUFBRTtDQUN0RCxFQUFFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO0NBQ25FLEdBQUcsTUFBTSxFQUFFLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUM7Q0FDL0QsR0FBRyxDQUFDLENBQUM7Q0FDTCxFQUFFO0NBQ0YsQ0FBQyxDQUFDOztDQUVGO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsS0FBSyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxLQUFLO0NBQ3pDO0NBQ0EsQ0FBQyxLQUFLLEdBQUcsS0FBSyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUM7Q0FDcEMsQ0FBQyxJQUFJLE9BQU8sS0FBSyxJQUFJLFFBQVEsSUFBSSxLQUFLLFlBQVksTUFBTSxFQUFFO0NBQzFELEVBQUUsUUFBUSxLQUFLO0NBQ2YsR0FBRyxLQUFLLE9BQU87Q0FDZixJQUFJLEtBQUssR0FBRyxHQUFHLENBQUM7Q0FDaEIsSUFBSSxNQUFNO0NBQ1YsR0FBRztDQUNILElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7Q0FDOUIsSUFBSSxNQUFNO0NBQ1YsR0FBRztDQUNILEVBQUU7Q0FDRixDQUFDLElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7Q0FDakMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztDQUNyQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO0NBQ3hDLENBQUMsQ0FBQzs7Q0FFRjs7Q0FFQSxLQUFLLENBQUMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLFNBQVMsTUFBTTtDQUNyRDtDQUNBLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0NBQ3pELENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Q0FFNUI7Q0FDQSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRTtDQUNyQixFQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQztDQUNuRSxFQUFFO0NBQ0YsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztDQUN6RSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0NBQ3ZFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Q0FDL0UsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztDQUMzRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztDQUM1RDtDQUNBO0NBQ0EsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsU0FBUyxLQUFLLEVBQUU7Q0FDekQsRUFBRSxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztDQUMvRCxFQUFFLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUU7Q0FDaEUsR0FBRyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7Q0FDaEIsR0FBRztDQUNILEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQztDQUNYLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFNBQVMsS0FBSyxFQUFFO0NBQzFELEVBQUUsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUI7Q0FDakQsR0FBRyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0NBQ2pFLEdBQUcsS0FBSztDQUNSLEdBQUcsQ0FBQztDQUNKLEVBQUUsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO0NBQ3pCLEVBQUUsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO0NBQ2YsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO0NBQ1g7Q0FDQSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0NBQ3pCLENBQUMsQ0FBQzs7Q0FFRjtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTs7Q0FFQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLFNBQVMsWUFBWSxHQUFHO0NBQ3hCLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztDQUNwRCxDQUFDO0NBQ0Q7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxTQUFTLGVBQWUsR0FBRztDQUMzQixDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Q0FDaEQsQ0FBQzs7Q0FFRCxJQUFJLElBQUksR0FBRztDQUNYLEVBQUUsS0FBSyxFQUFFLEVBQUU7Q0FDWCxFQUFFLE1BQU0sRUFBRSxDQUFDO0NBQ1gsRUFBRSxLQUFLLEVBQUUsQ0FBQztDQUNWLEVBQUUsTUFBTSxFQUFFLEVBQUU7Q0FDWixFQUFFLEtBQUssRUFBRSxDQUFDO0NBQ1YsRUFBRSxPQUFPLEVBQUUsQ0FBQztDQUNaLEVBQUUsS0FBSyxFQUFFLE1BQU07Q0FDZixFQUFFLE9BQU8sRUFBRSxJQUFJO0NBQ2YsRUFBRSxNQUFNLEVBQUUsQ0FBQztDQUNYLEVBQUUsU0FBUyxFQUFFLENBQUM7Q0FDZCxFQUFFLEtBQUssRUFBRSxDQUFDO0NBQ1YsRUFBRSxLQUFLLEVBQUUsRUFBRTtDQUNYLEVBQUUsR0FBRyxFQUFFLEVBQUU7Q0FDVCxFQUFFLE1BQU0sRUFBRSxHQUFHO0NBQ2IsRUFBRSxTQUFTLEVBQUUsU0FBUztDQUN0QixFQUFFLEdBQUcsRUFBRSxLQUFLO0NBQ1osRUFBRSxJQUFJLEVBQUUsS0FBSztDQUNiLEVBQUUsTUFBTSxFQUFFLEtBQUs7Q0FDZixFQUFFLE9BQU8sRUFBRSxLQUFLO0NBQ2hCLEVBQUUsUUFBUSxFQUFFLFVBQVU7Q0FDdEIsQ0FBQyxDQUFDOztDQUVGLElBQUksT0FBTyxHQUFHLElBQUlDLElBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7Q0FFbEM7O0NBRUEsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsU0FBUyxVQUFVO0NBQzFDO0NBQ0EsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTztDQUMxRCxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0NBQ2xCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztDQUM3QjtDQUNBO0NBQ0EsQ0FBQyxZQUFZLEVBQUUsQ0FBQztDQUNoQjtDQUNBO0NBQ0EsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztDQUNwQztDQUNBLENBQUMsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLEtBQUssRUFBRTtDQUM1QixFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Q0FDcEIsRUFBRSxNQUFNO0NBQ1IsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLGFBQWEsQ0FBQztDQUN0RCxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO0NBQ2pDLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQztDQUNwRDtDQUNBO0NBQ0EsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztDQUNwQyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztDQUN4QyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0NBQzdDLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFdBQVc7Q0FDaEMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztDQUNyQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0NBQ3ZDLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0NBQzdDLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7Q0FDdEIsRUFBRTtDQUNGLENBQUMsQ0FBQztDQUNGO0NBQ0EsU0FBUyxNQUFNLEdBQUc7Q0FDbEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztDQUNuQixDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDOztDQUVuQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxDQUFDLFNBQVMsS0FBSyxFQUFFO0NBQzdDLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLEdBQUcsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDLE9BQU8sSUFBSSxFQUFFLEtBQUssSUFBSSxDQUFDLEtBQUssRUFBRTtDQUMvRSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztDQUNmLEdBQUc7Q0FDSCxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOztDQUVmLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUM7O0NBRXZFO0NBQ0E7Q0FDQSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO0NBQ2hDO0NBQ0E7Q0FDQSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLENBQUM7Q0FDekU7Q0FDQTtDQUNBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQztDQUN4RTtDQUNBO0NBQ0E7Q0FDQSxDQUFDLElBQUksQ0FBQyxnQ0FBZ0MsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsV0FBVztDQUN2RSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7Q0FDbkMsRUFBRSxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztDQUM1QyxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDOztDQUVyRCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7Q0FDbkQsQ0FBQzs7Q0FFRDs7Q0FFQSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRztDQUN2QjtDQUNBLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO0NBQ25ELEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Q0FDbkIsRUFBRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Q0FDekIsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztDQUM3QjtDQUNBLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztDQUNyRCxHQUFHLE1BQU07Q0FDVCxHQUFHLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO0NBQ2xDLElBQUksSUFBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztDQUN0QyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7Q0FDeEIsSUFBSSxNQUFNO0NBQ1YsSUFBSSxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO0NBQy9ELElBQUk7Q0FDSixHQUFHO0NBQ0g7Q0FDQTtDQUNBLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7Q0FDckIsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0NBQ3ZCLEdBQUcsTUFBTTtDQUNUO0NBQ0E7Q0FDQSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO0NBQzlDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7Q0FDbEM7Q0FDQTtDQUNBLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFdBQVc7Q0FDakMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLGFBQWEsQ0FBQztDQUN4RCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0NBQ3ZCLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0NBQy9DO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsR0FBRztDQUNILEVBQUU7Q0FDRixDQUFDLENBQUM7O0NBRUYsU0FBUyxRQUFRLEdBQUc7Q0FDcEIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztDQUN0QyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO0NBQ25CLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7Q0FDcEIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxDQUFDO0NBQzVFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQztDQUMzRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUM7Q0FDckMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0NBQzlCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Q0FDakMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztDQUNsQyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxDQUFDO0NBQzFFLENBQUMsZUFBZSxFQUFFLENBQUM7Q0FDbkIsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFDO0NBQy9CO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztDQUNuRCxDQUFDOztDQUVEOztDQUVBLEtBQUssQ0FBQyxTQUFTLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztDQUNyQyxLQUFLLENBQUMsU0FBUyxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztDQUM5QyxLQUFLLENBQUMsU0FBUyxDQUFDLDhCQUE4QixHQUFHLEdBQUcsQ0FBQztDQUNyRCxLQUFLLENBQUMsU0FBUyxDQUFDLGdDQUFnQyxHQUFHLElBQUksQ0FBQztDQUN4RCxLQUFLLENBQUMsU0FBUyxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztDQUM3QyxLQUFLLENBQUMsU0FBUyxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztDQUM1QyxLQUFLLENBQUMsU0FBUyxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQzs7Q0FFNUM7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxLQUFLLENBQUMsU0FBUyxDQUFDLGlCQUFpQixHQUFHLFNBQVMsS0FBSztDQUNsRDtDQUNBO0NBQ0EsQ0FBQyxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLG1CQUFtQjtDQUNoRCxFQUFFLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7Q0FDaEUsRUFBRSxLQUFLO0NBQ1AsRUFBRSxDQUFDO0NBQ0gsQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztDQUMzQixDQUFDLElBQUksS0FBSyxDQUFDLG9CQUFvQixFQUFFO0NBQ2pDLEVBQUUsS0FBSyxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztDQUNyQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7Q0FDdkIsRUFBRSxNQUFNO0NBQ1IsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO0NBQzlELEVBQUU7Q0FDRjtDQUNBLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Q0FDdkM7Q0FDQTtDQUNBLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsQ0FBQztDQUM5RSxDQUFDLENBQUM7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxLQUFLLENBQUMsU0FBUyxDQUFDLGdCQUFnQixHQUFHO0NBQ25DO0NBQ0E7Q0FDQSxDQUFDLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsbUJBQW1CO0NBQ2hELEVBQUUsb0JBQW9CLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztDQUNoRSxFQUFFLEtBQUs7Q0FDUCxFQUFFLENBQUM7Q0FDSCxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsQ0FBQztDQUN0QixDQUFDLEtBQUssQ0FBQyxzQkFBc0IsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0NBQy9HO0NBQ0EsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFdBQVc7Q0FDdEU7Q0FDQSxFQUFFLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Q0FDckQsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO0NBQ1gsQ0FBQyxDQUFDOztDQUVGO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0EsS0FBSyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEdBQUc7Q0FDL0I7Q0FDQTtDQUNBLENBQUMsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxNQUFNLENBQUM7Q0FDaEc7Q0FDQSxDQUFDLElBQUksRUFBRSxHQUFHLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztDQUN0RyxDQUFDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0NBQy9CO0NBQ0E7Q0FDQTtDQUNBLENBQUMsSUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO0NBQzFDLEVBQUUsSUFBSSxXQUFXLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztDQUN0QyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxNQUFNLENBQUM7Q0FDckMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQztDQUM3QyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDO0NBQzFDO0NBQ0E7Q0FDQTtDQUNBLEVBQUU7Q0FDRixDQUFDLENBQUM7O0NBRUY7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQTtDQUNBO0NBQ0E7Q0FDQSxLQUFLLENBQUMsU0FBUyxDQUFDLDBCQUEwQixHQUFHLFNBQVMsZUFBZSxFQUFFLGFBQWEsRUFBRSxLQUFLO0NBQzNGO0NBQ0EsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLFNBQVMsS0FBSyxFQUFFO0NBQzdELEVBQUUsSUFBSSxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQyxPQUFPLElBQUksRUFBRSxFQUFFO0NBQy9ELEdBQUcsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO0NBQ3hCLEdBQUc7Q0FDSCxFQUFFLENBQUMsQ0FBQztDQUNKLENBQUMsS0FBSyxHQUFHLEtBQUssSUFBSSxTQUFTLENBQUM7Q0FDNUIsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQy9CLENBQUMsQ0FBQzs7O0NDdGNGOzs7Ozs7Ozs7Ozs7Ozs7Q0FlQSxDQUFDLFVBQVUsS0FBSyxFQUFFOzs7R0FHaEIsSUFBSSxhQUFhLEdBQUcsSUFBSTtPQUNwQixLQUFLLEdBQUcsS0FBSztPQUNiLFFBQVEsSUFBSSxLQUFLO09BQ2pCLEdBQUcsR0FBRyxLQUFLO09BQ1gsTUFBTSxHQUFHLEtBQUs7T0FDZCxRQUFRLEdBQUcsUUFBUTtPQUNuQixhQUFhLEdBQUcsUUFBUSxDQUFDOztHQUU3QixLQUFLLENBQUMsSUFBSSxHQUFHO0tBQ1gsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7S0FDOUIsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQztLQUN2QyxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUU7SUFDM0IsQ0FBQzs7R0FFRixLQUFLLENBQUMsSUFBSSxHQUFHLFNBQVMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUU7S0FDM0MsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU07U0FDakIsT0FBTyxHQUFHLENBQUM7U0FDWCxXQUFXLEdBQUcsQ0FBQztTQUNmLE1BQU0sR0FBRyxDQUFDO1NBQ1YsS0FBSyxHQUFHLE9BQU87U0FDZixPQUFPLEdBQUcsSUFBSTtTQUNkLEdBQUcsR0FBRyxJQUFJO1NBQ1YsR0FBRyxHQUFHLEVBQUU7U0FDUixNQUFNLEdBQUcsRUFBRTtTQUNYLE9BQU8sR0FBRyxLQUFLO1NBQ2YsQ0FBQyxHQUFHLENBQUM7U0FDTCxTQUFTLEdBQUcsQ0FBQztTQUNiLElBQUksR0FBRyxJQUFJO1NBQ1gsSUFBSSxHQUFHLElBQUksQ0FBQzs7S0FFaEIsU0FBUyxNQUFNLEdBQUc7T0FDaEIsSUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtTQUNsQixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2hELEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDVjtNQUNGOztLQUVELFNBQVMsZ0JBQWdCLEdBQUc7T0FDMUIsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDO09BQzNCLEtBQUssSUFBSSxDQUFDLEdBQUcsU0FBUyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1NBQzlDLGVBQWU7V0FDYixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzVDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksSUFBSSxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDO1NBQzFFLElBQUksQ0FBQyxlQUFlLEVBQUU7V0FDcEIsT0FBTyxLQUFLLENBQUM7VUFDZDtRQUNGOztPQUVELE9BQU8sZUFBZSxDQUFDO01BQ3hCOztLQUVELFNBQVMsVUFBVSxDQUFDLFdBQVcsRUFBRSxTQUFTLEVBQUU7T0FDMUMsTUFBTSxFQUFFLENBQUM7O09BRVQsSUFBSSxXQUFXLElBQUksZ0JBQWdCLEVBQUUsRUFBRTtTQUNyQyxLQUFLLElBQUksQ0FBQyxHQUFHLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7V0FDcEQsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFO2FBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsRUFBRTs7ZUFFM0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRTtjQUN4QzthQUNELE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3JCO1VBQ0Y7UUFDRixNQUFNLElBQUksQ0FBQyxTQUFTLEVBQUU7U0FDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3pCOztPQUVELE9BQU8sR0FBRyxLQUFLLENBQUM7T0FDaEIsU0FBUyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7TUFDM0I7O0tBRUQsU0FBUyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFO09BQ3JDLElBQUksS0FBSyxHQUFHLEdBQUcsR0FBRyxJQUFJO1dBQ2xCLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUM7V0FDdkMsVUFBVSxHQUFHLElBQUk7YUFDZixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUM7WUFDekQsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7O09BRWpCLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7T0FDckIsSUFBSSxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDOztPQUV6QyxPQUFPLFVBQVUsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztNQUN0Qzs7S0FFRCxJQUFJLFVBQVUsRUFBRTtPQUNkLFVBQVUsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO09BQ25DLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7T0FDckIsSUFBSSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztNQUN0Qjs7S0FFRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRTtPQUN4QixJQUFJLEtBQUssSUFBSSxPQUFPLEVBQUU7U0FDcEIsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsRUFBRTtXQUM1QixFQUFFLENBQUMsQ0FBQztXQUNKLE1BQU0sRUFBRSxDQUFDO1dBQ1QsS0FBSyxHQUFHLFdBQVcsQ0FBQztVQUNyQixNQUFNO1dBQ0wsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRTthQUMxQixVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDckIsTUFBTTthQUNMLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCO1VBQ0Y7UUFDRixNQUFNLElBQUksS0FBSyxJQUFJLFdBQVcsRUFBRTtTQUMvQixDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7U0FDckIsR0FBRyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNyQyxPQUFPLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQztTQUMxQyxJQUFJLE9BQU8sSUFBSSxHQUFHLEVBQUU7V0FDbEIsQ0FBQyxHQUFHLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztXQUM5QixLQUFLLEdBQUcsT0FBTyxDQUFDO1VBQ2pCLE1BQU07V0FDTCxJQUFJLEdBQUcsRUFBRTthQUNQLENBQUMsRUFBRSxDQUFDO1lBQ0w7V0FDRCxLQUFLLEdBQUcsTUFBTSxDQUFDO1VBQ2hCO1NBQ0QsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUNiLE1BQU07U0FDTCxJQUFJLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxFQUFFO1dBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSTt3QkFDbEQsQ0FBQyxFQUFFLENBQUMsT0FBTyxJQUFJLEdBQUcsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7V0FDN0UsR0FBRyxHQUFHLEVBQUUsQ0FBQztXQUNULENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztXQUNyQixLQUFLLEdBQUcsT0FBTyxDQUFDO1dBQ2hCLElBQUksT0FBTyxJQUFJLEdBQUcsRUFBRTthQUNsQixJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7ZUFDaEIsQ0FBQyxFQUFFLENBQUM7Y0FDTCxNQUFNO2VBQ0wsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztjQUM5QztZQUNGO1VBQ0YsTUFBTTtXQUNMLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1VBQ3ZCO1FBQ0Y7TUFDRjs7S0FFRCxVQUFVLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDOztLQUUxQixPQUFPLE1BQU0sQ0FBQztLQUNmOztHQUVELFNBQVMsaUJBQWlCLENBQUMsS0FBSyxFQUFFO0tBQ2hDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO09BQzlDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO01BQ3BEO0lBQ0Y7O0dBRUQsU0FBUyxJQUFJLENBQUMsQ0FBQyxFQUFFO0tBQ2YsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFO09BQ1YsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7TUFDakI7O0tBRUQsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNwQzs7R0FFRCxTQUFTLFNBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRTtLQUNuQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtPQUN2QyxPQUFPLEtBQUssQ0FBQztNQUNkOztLQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7T0FDMUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO1NBQzNDLE9BQU8sS0FBSyxDQUFDO1FBQ2Q7TUFDRjs7S0FFRCxPQUFPLElBQUksQ0FBQztJQUNiOzs7R0FHRCxJQUFJLGNBQWMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQzs7R0FFcEUsU0FBUyxTQUFTLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFO0tBQ2xELElBQUksWUFBWSxHQUFHLEVBQUU7U0FDakIsTUFBTSxHQUFHLElBQUk7U0FDYixJQUFJLEdBQUcsSUFBSTtTQUNYLEtBQUssR0FBRyxJQUFJLENBQUM7O0tBRWpCLElBQUksR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQzs7S0FFL0IsT0FBTyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtPQUN4QixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDOztPQUV2QixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsSUFBSSxFQUFFLEtBQUssQ0FBQyxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUU7U0FDN0QsTUFBTSxJQUFJLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1FBQ3BEOztPQUVELElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxRQUFRLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxFQUFFO1NBQzNFLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbEIsS0FBSyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQy9ELE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxJQUFJLEdBQUcsRUFBRTtTQUMzQixJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1dBQ3RCLE1BQU0sSUFBSSxLQUFLLENBQUMsK0JBQStCLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1VBQzVEO1NBQ0QsTUFBTSxHQUFHLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUNyQixJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLEVBQUU7V0FDbkUsTUFBTSxJQUFJLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUMsQ0FBQyxHQUFHLE9BQU8sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7VUFDbkU7U0FDRCxNQUFNLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDckIsT0FBTyxZQUFZLENBQUM7UUFDckIsTUFBTSxJQUFJLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxFQUFFO1NBQzVCLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDO1FBQzlEOztPQUVELFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7TUFDMUI7O0tBRUQsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtPQUNwQixNQUFNLElBQUksS0FBSyxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztNQUMxRDs7S0FFRCxPQUFPLFlBQVksQ0FBQztJQUNyQjs7R0FFRCxTQUFTLFFBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFO0tBQzdCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7T0FDM0MsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLEVBQUU7U0FDeEIsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7U0FDaEIsT0FBTyxJQUFJLENBQUM7UUFDYjtNQUNGO0lBQ0Y7O0dBRUQsU0FBUyxRQUFRLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUU7S0FDbkMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtPQUMzQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFO1NBQzNDLE9BQU8sSUFBSSxDQUFDO1FBQ2I7TUFDRjtJQUNGOztHQUVELFNBQVMsc0JBQXNCLENBQUMsR0FBRyxFQUFFO0tBQ25DLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztLQUNmLEtBQUssSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFO09BQ25CLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyx3QkFBd0IsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7TUFDeEU7S0FDRCxPQUFPLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztJQUN0Qzs7R0FFRCxTQUFTLGlCQUFpQixDQUFDLE9BQU8sRUFBRTtLQUNsQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7S0FDbEIsS0FBSyxJQUFJLEdBQUcsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO09BQ2hDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFXLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQztNQUN4STtLQUNELE9BQU8sYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsV0FBVyxHQUFHLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoRzs7R0FFRCxLQUFLLENBQUMsU0FBUyxHQUFHLFNBQVMsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUU7S0FDakQsT0FBTyw0QkFBNEIsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDO0tBQ2hIOztHQUVELElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQztHQUNqQixLQUFLLENBQUMsUUFBUSxHQUFHLFNBQVMsSUFBSSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUU7S0FDN0MsUUFBUSxHQUFHLENBQUMsQ0FBQztLQUNiLElBQUksT0FBTyxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQztLQUNuRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQzs7S0FFMUIsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO09BQ3BCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO01BQy9DOztLQUVELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ2xEOztHQUVELEtBQUssQ0FBQyxRQUFRLEdBQUcsU0FBUyxJQUFJLEVBQUU7S0FDOUIsT0FBTywwQkFBMEIsR0FBRyxJQUFJLEdBQUcsZ0JBQWdCLENBQUM7S0FDN0Q7O0dBRUQsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDOztHQUVoQyxLQUFLLENBQUMsWUFBWSxHQUFHLFNBQVMsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUU7S0FDcEQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUMxQyxRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksUUFBUSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDekUsT0FBTyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDekQ7O0dBRUQsS0FBSyxDQUFDLFlBQVksR0FBRyxTQUFTLE9BQU8sRUFBRTtLQUNyQyxJQUFJLEdBQUcsRUFBRSxRQUFRLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDL0UsS0FBSyxHQUFHLElBQUksUUFBUSxDQUFDLFFBQVEsRUFBRTtPQUM3QixRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO01BQ3BFO0tBQ0QsS0FBSyxHQUFHLElBQUksT0FBTyxDQUFDLElBQUksRUFBRTtPQUN4QixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksUUFBUSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7TUFDMUU7S0FDRCxPQUFPLFFBQVEsQ0FBQztLQUNqQjs7R0FFRCxTQUFTLEdBQUcsQ0FBQyxDQUFDLEVBQUU7S0FDZCxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQztjQUN2QixPQUFPLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQztjQUN0QixPQUFPLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQztjQUN4QixPQUFPLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztjQUNuQixPQUFPLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQztjQUM1QixPQUFPLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQzVDOztHQUVELFNBQVMsWUFBWSxDQUFDLENBQUMsRUFBRTtLQUN2QixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLENBQUM7SUFDdEM7O0dBRUQsU0FBUyxhQUFhLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRTtLQUNwQyxJQUFJLE1BQU0sR0FBRyxHQUFHLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUMsQ0FBQztLQUMxQyxJQUFJLEdBQUcsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxRQUFRLEVBQUUsQ0FBQztLQUN2QyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0tBQ3JELE9BQU8sQ0FBQyxJQUFJLElBQUksWUFBWSxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxTQUFTLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUM7S0FDcEYsT0FBTyxHQUFHLENBQUM7SUFDWjs7R0FFRCxLQUFLLENBQUMsT0FBTyxHQUFHO0tBQ2QsR0FBRyxFQUFFLFNBQVMsSUFBSSxFQUFFLE9BQU8sRUFBRTtPQUMzQixPQUFPLENBQUMsSUFBSSxJQUFJLFdBQVcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVc7dUJBQ3JFLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU07dUJBQ2hGLFdBQVcsR0FBRyxrQkFBa0IsQ0FBQztPQUNqRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7T0FDaEMsT0FBTyxDQUFDLElBQUksSUFBSSxjQUFjLENBQUM7TUFDaEM7O0tBRUQsR0FBRyxFQUFFLFNBQVMsSUFBSSxFQUFFLE9BQU8sRUFBRTtPQUMzQixPQUFPLENBQUMsSUFBSSxJQUFJLFlBQVksR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLDBCQUEwQixDQUFDO09BQ3RHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztPQUNoQyxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztNQUN0Qjs7S0FFRCxHQUFHLEVBQUUsYUFBYTtLQUNsQixHQUFHLEVBQUUsU0FBUyxJQUFJLEVBQUUsT0FBTyxFQUFFO09BQzNCLElBQUksR0FBRyxHQUFHLENBQUMsUUFBUSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO09BQzlELEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztPQUM1QixJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztPQUM5RCxRQUFRLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7T0FDekIsUUFBUSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO01BQ2xDOztLQUVELEdBQUcsRUFBRSxTQUFTLElBQUksRUFBRSxPQUFPLEVBQUU7T0FDM0IsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLE9BQU8sQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztPQUMzRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7T0FDNUIsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztPQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRTtTQUN0QixPQUFPLENBQUMsSUFBSSxJQUFJLFNBQVMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQztRQUN2RDtNQUNGOztLQUVELElBQUksRUFBRSxTQUFTLElBQUksRUFBRSxPQUFPLEVBQUU7T0FDNUIsT0FBTyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUM7TUFDNUQ7O0tBRUQsSUFBSSxFQUFFLFNBQVMsSUFBSSxFQUFFLE9BQU8sRUFBRTtPQUM1QixPQUFPLENBQUMsSUFBSSxJQUFJLFlBQVksR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLGFBQWEsQ0FBQztNQUMxRjs7S0FFRCxJQUFJLEVBQUUsU0FBUyxJQUFJLEVBQUUsT0FBTyxFQUFFO09BQzVCLE9BQU8sQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO01BQ25EOztLQUVELEdBQUcsRUFBRSxZQUFZOztLQUVqQixHQUFHLEVBQUUsWUFBWTtLQUNsQjs7R0FFRCxTQUFTLFlBQVksQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFO0tBQ25DLE9BQU8sQ0FBQyxJQUFJLElBQUksWUFBWSxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsYUFBYSxDQUFDO0lBQzFGOztHQUVELFNBQVMsS0FBSyxDQUFDLENBQUMsRUFBRTtLQUNoQixPQUFPLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO0lBQzFCOztHQUVELEtBQUssQ0FBQyxJQUFJLEdBQUcsU0FBUyxRQUFRLEVBQUUsT0FBTyxFQUFFO0tBQ3ZDLElBQUksSUFBSSxDQUFDO0tBQ1QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtPQUMvQyxJQUFJLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7T0FDdEMsSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7TUFDcEM7S0FDRCxPQUFPLE9BQU8sQ0FBQztLQUNoQjs7R0FFRCxLQUFLLENBQUMsS0FBSyxHQUFHLFNBQVMsTUFBTSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUU7S0FDNUMsT0FBTyxHQUFHLE9BQU8sSUFBSSxFQUFFLENBQUM7S0FDeEIsT0FBTyxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUMsQ0FBQztLQUM3RDs7R0FFRCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzs7R0FFakIsS0FBSyxDQUFDLFFBQVEsR0FBRyxTQUFTLElBQUksRUFBRSxPQUFPLEVBQUU7S0FDdkMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQy9HOztHQUVELEtBQUssQ0FBQyxPQUFPLEdBQUcsU0FBUyxJQUFJLEVBQUUsT0FBTyxFQUFFO0tBQ3RDLE9BQU8sR0FBRyxPQUFPLElBQUksRUFBRSxDQUFDO0tBQ3hCLElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3hDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7O0tBRS9CLElBQUksUUFBUSxFQUFFO09BQ1osSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztPQUNqQyxLQUFLLElBQUksSUFBSSxJQUFJLFFBQVEsRUFBRTtTQUN6QixPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUM7UUFDaEM7T0FDRCxPQUFPLFFBQVEsQ0FBQztNQUNqQjs7S0FFRCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3hHLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUM7S0FDbkM7RUFDRixFQUFFLEFBQWlDLE9BQU8sQUFBUSxDQUFDLENBQUM7Ozs7QUN0YXJEO0NBaUJBLENBQUMsVUFBVSxLQUFLLEVBQUU7R0FDaEIsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLE9BQU8sRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRTtLQUMzRCxPQUFPLEdBQUcsT0FBTyxJQUFJLEVBQUUsQ0FBQztLQUN4QixJQUFJLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztLQUNoQyxJQUFJLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQztLQUNsQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sSUFBSSxFQUFFLENBQUM7S0FDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO0tBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUM7S0FDdkMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztLQUMvQixJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztLQUNmOztHQUVELEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxHQUFHOztLQUV6QixDQUFDLEVBQUUsVUFBVSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUU7OztLQUd0RCxDQUFDLEVBQUUsV0FBVzs7O0tBR2QsQ0FBQyxFQUFFLGNBQWM7O0tBRWpCLE1BQU0sRUFBRSxTQUFTLE1BQU0sQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtPQUNqRCxPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxRQUFRLElBQUksRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO01BQ25EOzs7S0FHRCxFQUFFLEVBQUUsVUFBVSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtPQUN2QyxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztNQUMxQzs7O0tBR0QsRUFBRSxFQUFFLFNBQVMsTUFBTSxFQUFFLFFBQVEsRUFBRTtPQUM3QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7T0FHcEMsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztPQUN0QyxJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLElBQUksSUFBSSxRQUFRLEVBQUU7U0FDaEQsT0FBTyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQ3pCOztPQUVELElBQUksT0FBTyxRQUFRLElBQUksUUFBUSxFQUFFO1NBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFO1dBQ1gsTUFBTSxJQUFJLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1VBQzNDO1NBQ0QsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkQ7O09BRUQsSUFBSSxDQUFDLFFBQVEsRUFBRTtTQUNiLE9BQU8sSUFBSSxDQUFDO1FBQ2I7OztPQUdELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQzs7T0FFdEMsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFOztTQUVoQixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztTQUNqRCxLQUFLLEdBQUcsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO1dBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2FBQzVCLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLFNBQVMsSUFBSSxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2pKO1VBQ0Y7U0FDRCxRQUFRLEdBQUcsd0JBQXdCLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLFFBQVE7V0FDMUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzRDtPQUNELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQzs7T0FFMUMsT0FBTyxRQUFRLENBQUM7TUFDakI7OztLQUdELEVBQUUsRUFBRSxTQUFTLE1BQU0sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtPQUM5QyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztPQUN4QyxJQUFJLENBQUMsT0FBTyxFQUFFO1NBQ1osT0FBTyxFQUFFLENBQUM7UUFDWDs7T0FFRCxPQUFPLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztNQUM5Qzs7O0tBR0QsRUFBRSxFQUFFLFNBQVMsT0FBTyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUU7T0FDdkMsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7O09BRXZDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7U0FDbEIsT0FBTyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakMsT0FBTztRQUNSOztPQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1NBQ3BDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEIsT0FBTyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ2Y7TUFDRjs7O0tBR0QsQ0FBQyxFQUFFLFNBQVMsR0FBRyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFO09BQzFELElBQUksSUFBSSxDQUFDOztPQUVULElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1NBQ3BDLE9BQU8sS0FBSyxDQUFDO1FBQ2Q7O09BRUQsSUFBSSxPQUFPLEdBQUcsSUFBSSxVQUFVLEVBQUU7U0FDNUIsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0Q7O09BRUQsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUM7O09BRWIsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLElBQUksR0FBRyxFQUFFO1NBQzVCLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxRQUFRLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEU7O09BRUQsT0FBTyxJQUFJLENBQUM7TUFDYjs7O0tBR0QsQ0FBQyxFQUFFLFNBQVMsR0FBRyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFO09BQzNDLElBQUksS0FBSztXQUNMLEtBQUssR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztXQUN0QixHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxXQUFXLENBQUM7V0FDbEQsVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUTtXQUNsQyxFQUFFLEdBQUcsSUFBSSxDQUFDOztPQUVkLElBQUksR0FBRyxLQUFLLEdBQUcsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRTtTQUMvQyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDM0IsTUFBTTtTQUNMLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1dBQ3JDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQztXQUMvQyxJQUFJLEtBQUssS0FBSyxTQUFTLEVBQUU7YUFDdkIsRUFBRSxHQUFHLEdBQUcsQ0FBQzthQUNULEdBQUcsR0FBRyxLQUFLLENBQUM7WUFDYixNQUFNO2FBQ0wsR0FBRyxHQUFHLEVBQUUsQ0FBQztZQUNWO1VBQ0Y7UUFDRjs7T0FFRCxJQUFJLFdBQVcsSUFBSSxDQUFDLEdBQUcsRUFBRTtTQUN2QixPQUFPLEtBQUssQ0FBQztRQUNkOztPQUVELElBQUksQ0FBQyxXQUFXLElBQUksT0FBTyxHQUFHLElBQUksVUFBVSxFQUFFO1NBQzVDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDYixHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2xDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNYOztPQUVELE9BQU8sR0FBRyxDQUFDO01BQ1o7OztLQUdELENBQUMsRUFBRSxTQUFTLEdBQUcsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRTtPQUMzQyxJQUFJLEdBQUcsR0FBRyxLQUFLO1dBQ1gsQ0FBQyxHQUFHLElBQUk7V0FDUixLQUFLLEdBQUcsS0FBSztXQUNiLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQzs7T0FFdkMsS0FBSyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1NBQ3hDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDWCxHQUFHLEdBQUcsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDdEMsSUFBSSxHQUFHLEtBQUssU0FBUyxFQUFFO1dBQ3JCLEtBQUssR0FBRyxJQUFJLENBQUM7V0FDYixNQUFNO1VBQ1A7UUFDRjs7T0FFRCxJQUFJLENBQUMsS0FBSyxFQUFFO1NBQ1YsT0FBTyxDQUFDLFdBQVcsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ25DOztPQUVELElBQUksQ0FBQyxXQUFXLElBQUksT0FBTyxHQUFHLElBQUksVUFBVSxFQUFFO1NBQzVDLEdBQUcsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDbkM7O09BRUQsT0FBTyxHQUFHLENBQUM7TUFDWjs7O0tBR0QsRUFBRSxFQUFFLFNBQVMsSUFBSSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRTtPQUMzQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQzs7T0FFdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO09BQy9CLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztPQUNuRSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7O09BRWxDLE9BQU8sS0FBSyxDQUFDO01BQ2Q7OztLQUdELEVBQUUsRUFBRSxTQUFTLElBQUksRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFO09BQy9CLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUU7U0FDOUIsTUFBTSxJQUFJLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQzlDO09BQ0QsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUM7TUFDaEU7OztLQUdELENBQUMsRUFBRSxTQUFTLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEVBQUU7O0tBRWpDLEVBQUUsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRTs7O0tBRzdELEVBQUUsRUFBRSxTQUFTLElBQUksRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRTtPQUM1RCxJQUFJLFVBQVU7V0FDVixFQUFFLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1dBQ3hCLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDOztPQUUzQixJQUFJLE9BQU8sTUFBTSxJQUFJLFVBQVUsRUFBRTtTQUMvQixJQUFJLFFBQVEsRUFBRTtXQUNaLE9BQU8sSUFBSSxDQUFDO1VBQ2IsTUFBTTtXQUNMLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1dBQzVILE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztVQUM5RTtRQUNGOztPQUVELE9BQU8sTUFBTSxDQUFDO01BQ2Y7OztLQUdELEVBQUUsRUFBRSxTQUFTLElBQUksRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFO09BQ2hDLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO09BQzdCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7O09BRTNCLElBQUksT0FBTyxNQUFNLElBQUksVUFBVSxFQUFFO1NBQy9CLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMvRDs7T0FFRCxPQUFPLE1BQU0sQ0FBQztNQUNmOztLQUVELEdBQUcsRUFBRSxTQUFTLElBQUksRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtPQUM3QyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO09BQ3hCLElBQUksQ0FBQyxFQUFFO1NBQ0wsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDdEIsQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ25DLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3hCO01BQ0Y7O0lBRUYsQ0FBQzs7O0dBR0YsU0FBUyxXQUFXLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUU7S0FDM0MsSUFBSSxHQUFHLENBQUM7O0tBRVIsSUFBSSxLQUFLLElBQUksT0FBTyxLQUFLLElBQUksUUFBUSxFQUFFOztPQUVyQyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxTQUFTLEVBQUU7U0FDNUIsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzs7O1FBR2xCLE1BQU0sSUFBSSxVQUFVLElBQUksS0FBSyxDQUFDLEdBQUcsSUFBSSxPQUFPLEtBQUssQ0FBQyxHQUFHLElBQUksVUFBVSxFQUFFO1NBQ3BFLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCO01BQ0Y7O0tBRUQsT0FBTyxHQUFHLENBQUM7SUFDWjs7R0FFRCxTQUFTLHdCQUF3QixDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFO0tBQy9GLFNBQVMsZUFBZSxHQUFHLEVBQUUsSUFDN0IsZUFBZSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7S0FDckMsU0FBUyxhQUFhLEdBQUcsRUFBRSxJQUMzQixhQUFhLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7S0FDeEMsSUFBSSxHQUFHLENBQUM7S0FDUixJQUFJLE9BQU8sR0FBRyxJQUFJLGVBQWUsRUFBRSxDQUFDO0tBQ3BDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxhQUFhLEVBQUUsQ0FBQztLQUNuQyxPQUFPLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztLQUN0QixPQUFPLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQzs7S0FFakIsU0FBUyxHQUFHLFNBQVMsSUFBSSxFQUFFLENBQUM7S0FDNUIsT0FBTyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7S0FDOUIsT0FBTyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7S0FDN0IsS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFO09BQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztNQUNqRDtLQUNELEtBQUssR0FBRyxJQUFJLFNBQVMsRUFBRTtPQUNyQixPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztNQUNwQzs7S0FFRCxhQUFhLEdBQUcsYUFBYSxJQUFJLEVBQUUsQ0FBQztLQUNwQyxPQUFPLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztLQUN0QyxLQUFLLEdBQUcsSUFBSSxRQUFRLEVBQUU7T0FDcEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLENBQUMsR0FBRyxDQUFDLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO01BQzdEO0tBQ0QsS0FBSyxHQUFHLElBQUksYUFBYSxFQUFFO09BQ3pCLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO01BQzVDOztLQUVELE9BQU8sT0FBTyxDQUFDO0lBQ2hCOztHQUVELElBQUksSUFBSSxHQUFHLElBQUk7T0FDWCxHQUFHLEdBQUcsSUFBSTtPQUNWLEdBQUcsR0FBRyxJQUFJO09BQ1YsS0FBSyxHQUFHLEtBQUs7T0FDYixLQUFLLEdBQUcsS0FBSztPQUNiLE1BQU0sR0FBRyxXQUFXLENBQUM7O0dBRXpCLFNBQVMsY0FBYyxDQUFDLEdBQUcsRUFBRTtLQUMzQixPQUFPLE1BQU0sQ0FBQyxDQUFDLEdBQUcsS0FBSyxJQUFJLElBQUksR0FBRyxLQUFLLFNBQVMsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUM7SUFDL0Q7O0dBRUQsU0FBUyxXQUFXLENBQUMsR0FBRyxFQUFFO0tBQ3hCLEdBQUcsR0FBRyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7S0FDMUIsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztPQUNyQixHQUFHO1VBQ0EsT0FBTyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUM7VUFDdEIsT0FBTyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUM7VUFDcEIsT0FBTyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUM7VUFDcEIsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUM7VUFDdkIsT0FBTyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUM7T0FDM0IsR0FBRyxDQUFDO0lBQ1A7O0dBRUQsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sSUFBSSxTQUFTLENBQUMsRUFBRTtLQUN6QyxPQUFPLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxnQkFBZ0IsQ0FBQztJQUMvRCxDQUFDOztFQUVILEVBQUUsQUFBaUMsT0FBTyxBQUFRLENBQUMsQ0FBQzs7O0NDcFZyRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBQyxTQUFLLENBQUMsUUFBUSxHQUFHQyxRQUFxQixDQUFDLFFBQVEsQ0FBQztBQUNoREQsU0FBSyxDQUFDLFFBQVEsR0FBR0EsUUFBSyxDQUFDLFFBQVEsQ0FBQztDQUNoQyxTQUFjLEdBQUdBLFFBQUssQ0FBQzs7Ozs7O0NDcEJ2QixJQUFJLEtBQUssR0FBRyx5SEFBeUgsQ0FBQzs7Q0NNdEksSUFBSSxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUM7Q0FDdEIsQ0FBQyxjQUFjLEVBQUUsS0FBSztDQUN0QixDQUFDLENBQUMsQ0FBQzs7Q0FFSCxRQUFRLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsV0FBVztDQUN6RCxDQUFDLFVBQVU7Q0FDWCxFQUFFRSxVQUFRLENBQUMsTUFBTSxDQUFDO0NBQ2xCLEdBQUcsRUFBRSxFQUFFLEtBQUssQ0FBQyxFQUFFO0NBQ2YsR0FBRyxJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7Q0FDekIsR0FBRyxLQUFLLEVBQUUsQ0FBQztDQUNYLEdBQUcsQ0FBQztDQUNKLEVBQUUsQ0FBQzs7Q0FFSCxDQUFDLGVBQWUsQ0FBQ0MsS0FBUyxDQUFDLENBQUM7O0NBRTVCLENBQUMsSUFBSSxZQUFZLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7Q0FDekYsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO0NBQ2hDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLG1CQUFtQixFQUFFLFdBQVc7Q0FDdkUsRUFBRSxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQzNDLEVBQUUsR0FBRyxDQUFDLFdBQVcsSUFBSSxpQkFBaUIsQ0FBQztDQUN2QyxFQUFFLEVBQUUsS0FBSyxDQUFDLENBQUM7Q0FDWCxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxtQkFBbUIsRUFBRSxXQUFXO0NBQ3ZFO0NBQ0EsRUFBRSxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQzNDLEVBQUUsR0FBRyxDQUFDLFdBQVcsSUFBSSxpQkFBaUIsQ0FBQztDQUN2QyxFQUFFLEVBQUUsS0FBSyxDQUFDLENBQUM7Q0FDWCxDQUFDLENBQUMsQ0FBQzs7Ozs7Ozs7In0=
