import { insertHtml, insertSvgSymbol } from '@egml/utils';
import Modal from '../dist/modal.js';
import template from '../dist/modal.mustache';
import './main.scss';
import iconCross from '@egml/icons/dist/esm/cross.js';

var modal = new Modal({
	autoInitialize: false,
});

document.addEventListener('DOMContentLoaded', function() {
	insertHtml(
		template.render({
			id: modal.id,
			name: modal.name.css(),
			scale: 1,
		})
	);

	insertSvgSymbol(iconCross);

	var modalElement = document.getElementById(modal.name.css(modal.id.current.toString()));
	modal.initialize(modalElement);
	modal.dom.self.addEventListener(modal.finishShowEventName, function() {
		var log = document.getElementById('log');
		log.textContent += '== открыто ==\n';
	}, false);
	modal.dom.self.addEventListener(modal.finishHideEventName, function() {
		// window.location.reload(true);
		var log = document.getElementById('log');
		log.textContent += '== закрыто ==\n';
	}, false);
});

export default modal;